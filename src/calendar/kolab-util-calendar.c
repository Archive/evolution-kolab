/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-calendar.h
 *
 *  2011
 *  Copyright  2011  Silvan Marco Fin
 *  <silvan@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <libekolabutil/camel-system-headers.h>
#include <libekolab/kolab-settings-handler.h>
#include <libekolab/kolab-mail-access.h>

#include "kolab-util-calendar.h"

/*----------------------------------------------------------------------------*/
/* internal statics */

static ESource*
util_calendar_ref_top_source (ESourceRegistry *registry,
                              ESource *source)
{
	ESource *top_source = NULL;
	gchar *top_uid = NULL;

	g_return_val_if_fail (E_IS_SOURCE_REGISTRY (registry), NULL);
	g_return_val_if_fail (E_IS_SOURCE (source), NULL);

	top_source = g_object_ref (source);
	while (TRUE) {
		top_uid = e_source_dup_parent (top_source);
		if (top_uid == NULL)
			break;
		g_object_unref (top_source);
		top_source = e_source_registry_ref_source (registry,
		                                           top_uid);
		g_free (top_uid);
	}

	return top_source;
}

static KolabUtilHttpJob*
util_calendar_create_http_request (KolabSettingsHandler *ksettings,
                                   const gchar *path,
                                   GError **error)
{
	CamelURL *url = NULL;
	CamelKolabIMAPXSettings *settings;
	const gchar *passwd = NULL;
	KolabUtilHttpJob *job = NULL;
	GError *tmp_err = NULL;

	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_assert (path != NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	settings = kolab_settings_handler_get_camel_settings (ksettings);
	url = camel_kolab_imapx_settings_build_url (settings);
	camel_url_set_path (url, path);

	/* from the Camel settings, we will get set the configured IMAP
	 * port, which we need to unset here for HTTP(S). The protocol
	 * type (SSL/non-SSL) will be reflected in the URL, just we're
	 * not yet able to set a custom port for HTTP(S). Instead, the
	 * default ports for this protocol type will be used
	 */
	camel_url_set_port (url, 0);

	passwd = kolab_settings_handler_get_char_field (ksettings,
	                                                KOLAB_SETTINGS_HANDLER_CHAR_FIELD_KOLAB_USER_PASSWORD,
	                                                &tmp_err);
	if (passwd == NULL) {
		g_warning ("%s()[%u]: no password supplied!", __func__, __LINE__);
		if (tmp_err != NULL) {
			g_warning ("%s()[%u]: %s", __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
		}
	}

	job = kolab_util_http_job_new ();
	job->url = url;
	job->passwd = g_strdup (passwd); /* no longer carried by CamelURL */

	return job;
} /* util_calendar_create_http_request () */

static KolabUtilHttpJob*
util_calendar_create_pfb_trigger (KolabSettingsHandler *ksettings,
                                  const gchar *sourcename,
                                  GError **error)
{
	CamelKolabIMAPXSettings *camel_settings;
	CamelNetworkSettings *network_settings;
	KolabUtilHttpJob *job = NULL;
	gchar *path = NULL;
	const gchar *username = NULL;
	gint source_offset = 0;
	GError *tmp_err = NULL;

	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_assert (sourcename != NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	camel_settings = kolab_settings_handler_get_camel_settings (ksettings);

	network_settings = CAMEL_NETWORK_SETTINGS (camel_settings);
	username = camel_network_settings_get_user (network_settings);

	if (strncmp (sourcename, "INBOX/", 6) == 0)
		source_offset = 6;
	path = g_strdup_printf ("/freebusy/trigger/%s/%s.pfb",
	                        username, sourcename+source_offset);

	job = util_calendar_create_http_request (ksettings,
	                                         path,
	                                         &tmp_err);
	g_free (path);
	if (tmp_err != NULL) {
		g_propagate_error (error, tmp_err);
		return NULL;
	}
	return job;
} /* util_calendar_create_pfb_trigger () */

static KolabUtilHttpJob*
util_calendar_create_xfb_request (KolabSettingsHandler *ksettings,
                                  gchar *query,
                                  gboolean extended,
                                  GError **error)
{
	KolabUtilHttpJob *job = NULL;
	gchar *path = NULL;
	GError *tmp_err = NULL;

	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_assert (query != NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	if (extended)
		path = g_strdup_printf ("/freebusy/%s.xfb", query);
	else
		path = g_strdup_printf ("/freebusy/%s.ifb", query);

	job = util_calendar_create_http_request (ksettings,
	                                         path,
	                                         &tmp_err);
	g_free (path);

	if (tmp_err != NULL) {
		g_propagate_error (error, tmp_err);
		return NULL;
	}

	return job;
} /* util_calendar_create_xfb_request () */

static KolabUtilHttpJob*
util_calendar_retrieve_xfb (KolabSettingsHandler *ksettings,
                            gchar *query,
                            gboolean extended,
                            GError **error)
{
	KolabUtilHttpJob *job = NULL;
	GError *tmp_err = NULL;

	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_assert (query != NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	job = util_calendar_create_xfb_request (ksettings,
	                                        query,
	                                        extended,
	                                        &tmp_err);
	if (job == NULL)
		goto exit;

	job->buffer = g_byte_array_new ();
	(void) kolab_util_http_get (job, &tmp_err);

 exit:

	if (tmp_err != NULL) {
		g_propagate_error (error, tmp_err);
		if (job != NULL) {
			kolab_util_http_job_free (job);
			job = NULL;
		}
	}

	return job;
} /* util_calendar_retrieve_xfb */

/*----------------------------------------------------------------------------*/
/* public API */

/**
 * kolab_util_calendar_dup_email_address:
 * @backend: the cal backend via which we get to the source registry
 *
 * Retrieves the email address associated with the @backend
 * from the source registry. The caller needs to free the
 * result, if non-NULL.
 *
 * Returns: An email address or NULL if none found.
 * Since: 3.6
 */
gchar*
kolab_util_calendar_dup_email_address (ECalBackend *backend)
{
	ESourceRegistry *registry = NULL;
	ESourceMailIdentity *extension = NULL;
	ESource *esource = NULL;
	ESource *top_source = NULL;
	GList *sources = NULL;
	GList *sources_ptr = NULL;
	gchar *mail_addr = NULL;
	gchar *my_top_uid = NULL;
	const gchar *top_uid = NULL;

	g_return_val_if_fail (E_IS_CAL_BACKEND (backend), NULL);

	registry = e_cal_backend_get_registry (backend);
	g_return_val_if_fail (E_IS_SOURCE_REGISTRY (registry), NULL);

	esource = e_backend_get_source (E_BACKEND (backend));
	top_source = util_calendar_ref_top_source (registry, esource);
	my_top_uid = e_source_dup_uid (top_source);
	g_object_unref (top_source);
	g_return_val_if_fail (my_top_uid != NULL, NULL);

	sources = e_source_registry_list_sources (registry,
	                                          E_SOURCE_EXTENSION_MAIL_IDENTITY);
	sources_ptr = sources;
	while (sources_ptr != NULL) {
		esource = E_SOURCE (sources_ptr->data);
		if (esource == NULL) {
			/* should not normally happen */
			g_warning ("%s()[%u] Got NULL ESource, skipping",
			           __func__, __LINE__);
			goto source_skip;
		}

		top_source = util_calendar_ref_top_source (registry,
		                                           esource);
		if (top_source == NULL) {
			/* should not normally happen */
			g_warning ("%s()[%u] Got NULL top ESource, skipping",
			           __func__, __LINE__);
			goto source_skip;
		}

		top_uid = e_source_get_uid (top_source);
		g_object_unref (top_source);

		if (g_strcmp0 (my_top_uid, top_uid) == 0) {
			/* be sure the esource has the mail identity extension */
			if (! e_source_has_extension (esource, E_SOURCE_EXTENSION_MAIL_IDENTITY)) {
				g_warning ("%s()[%u] ESource %s has no mail identity extension, skipping.",
				           __func__, __LINE__, e_source_get_uid (esource));
				goto source_skip;
			}
			extension = e_source_get_extension (esource,
			                                    E_SOURCE_EXTENSION_MAIL_IDENTITY);
			mail_addr = e_source_mail_identity_dup_address (extension);
		}

		if (mail_addr != NULL)
			break;

	source_skip:

		sources_ptr = g_list_next (sources_ptr);
	}

	if (sources != NULL)
		g_list_free_full (sources, g_object_unref);

	return mail_addr;
}

/**
 * kolab_util_calendar_get_tzid:
 * @comp: An ECalComponent to derive the tzid from.
 * @from: field to get the tzid from.
 *
 * The ECalComponentDateTime Struct from an ECalComponents dtstart field or
 * dtend field is queried for its tzid.
 *
 * Returns: A newly allocated tzid string (g_free() after use)
 *          or NULL if no ID was found.
 */
gchar*
kolab_util_calendar_get_tzid (ECalComponent *comp,
                              ECalComponentField from)
{
	ECalComponentDateTime *dt = NULL;
	gchar *tzid = NULL;

	g_assert (E_IS_CAL_COMPONENT (comp));

	dt = g_new0 (ECalComponentDateTime, 1);
	switch (from) {
	case E_CAL_COMPONENT_FIELD_DTSTART:
		e_cal_component_get_dtstart (comp, dt);
		break;
	case E_CAL_COMPONENT_FIELD_DTEND:
		e_cal_component_get_dtend (comp, dt);
		break;
	case E_CAL_COMPONENT_FIELD_DUE:
		e_cal_component_get_due (comp, dt);
		break;
	default:
		g_warning ("%s()[%u]: TZID from %u not supported.",
		           __func__, __LINE__, from);
		return NULL;
	}

	if (dt == NULL) {
		g_warning ("%s()[%u]: Found ECalComponent without ECalComponentField %u.",
		           __func__, __LINE__, from);
		g_assert_not_reached();
	}

	tzid = g_strdup (dt->tzid);
	e_cal_component_free_datetime (dt);
	g_free (dt);
	return tzid;
} /* kolab_util_calendar_get_tzid () */

gboolean
kolab_util_calendar_toggle_pfb_trigger (KolabSettingsHandler *ksettings,
                                        const gchar *sourcename,
                                        GError **error)
{
	/* Keep running on errors.
	 * If an error occured, internal_state is set to false as indicator.
	 */
	KolabUtilHttpJob *job = NULL;
	GError *tmp_err = NULL;

	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_assert (sourcename != NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	job = util_calendar_create_pfb_trigger (ksettings,
	                                        sourcename,
	                                        &tmp_err);
	if (job == NULL) {
		if (tmp_err != NULL)
			g_propagate_error (error, tmp_err);
		g_warning ("%s()[%u] error: could not create F/B toggle, giving up on %s.",
		           __func__, __LINE__, sourcename);
		return FALSE;
	}
	job->buffer = g_byte_array_new ();
	(void)kolab_util_http_get (job, &tmp_err);
	kolab_util_http_job_free (job);

	if (tmp_err != NULL) {
		g_propagate_error (error, tmp_err);
		return FALSE;
	}
	return TRUE;
} /* kolab_util_calendar_toggle_pfb_trigger () */

KolabUtilHttpJob*
kolab_util_calendar_retrieve_xfb (KolabSettingsHandler *ksettings,
                                  gchar *query,
                                  GError **error)
{
	KolabUtilHttpJob *job = NULL;
	GError *tmp_err = NULL;

	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_assert (query != NULL);
	g_return_val_if_fail (error == NULL || *error == NULL, NULL);

	job = util_calendar_retrieve_xfb (ksettings,
	                                  query,
	                                  TRUE, /* try XFB */
	                                  &tmp_err);
	if (job != NULL)
		goto done;

	if (tmp_err != NULL) {
		g_warning ("%s()[%u] %s",
		           __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}

	job = util_calendar_retrieve_xfb (ksettings,
	                                  query,
	                                  FALSE, /* try SFB */
	                                  &tmp_err);

	if (tmp_err != NULL)
		g_propagate_error (error, tmp_err);

 done:

	return job;
} /* kolab_util_calendar_retrieve_xfb () */

gboolean
kolab_util_calendar_store (ECalComponent *ecalcomp,
                           ECalComponent *ecaltz,
                           ECalComponent *default_tz,
                           ECalBackendSync *backend,
                           KolabMailAccess *koma,
                           GCancellable *cancellable,
                           GError **error)
{
	const gchar *foldername;
	KolabMailHandle *kmh = NULL;
	KolabSettingsHandler *ksettings = NULL;
	KolabMailAccessOpmodeID mode = KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	gboolean do_trigger = FALSE;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_assert (E_IS_CAL_COMPONENT (ecalcomp));
	/* ecaltz may be NULL */
	(void)default_tz; /* FIXME needed? */ /* default_tz may be NULL */
	g_assert (E_IS_CAL_BACKEND_SYNC (backend));
	g_assert (KOLAB_IS_MAIL_ACCESS (koma));
	/* cancellable may be NULL */
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	ksettings = kolab_mail_access_get_settings_handler (koma);
	if (ksettings == NULL) {
		/* FIXME: set GError here */
		return FALSE;
	}

	foldername = kolab_util_backend_get_foldername (E_BACKEND (backend));

	kolab_util_backend_modtime_set_on_ecalcomp (ecalcomp);
	kmh = kolab_mail_handle_new_from_ecalcomponent (ecalcomp,
	                                                ecaltz);
	ok =  kolab_mail_access_store_handle (koma,
	                                      kmh,
	                                      foldername,
	                                      cancellable,
	                                      &tmp_err);
	if (! ok)
		goto cleanup;

	mode = kolab_mail_access_get_opmode (koma,
	                                     &tmp_err);
	if (tmp_err != NULL)
		goto cleanup;
	if (mode < KOLAB_MAIL_ACCESS_OPMODE_ONLINE) {
		/* In OFFLINE state, the triggers are generated during
		 * changing to ONLINE state in e_cal_backend_set_mode ().
		 */
		goto cleanup;
	}

	do_trigger = kolab_mail_access_source_fbtrigger_needed (koma,
	                                                        foldername,
	                                                        &tmp_err);
	if (tmp_err != NULL)
		goto cleanup;

	if (do_trigger == TRUE) {
		kolab_mail_access_password_set_visible (koma, TRUE);
		(void) kolab_util_calendar_toggle_pfb_trigger (ksettings,
		                                               foldername,
		                                               &tmp_err);
		if (tmp_err != NULL) {
			/* if triggering the XFB generation fails,
			 * this is not a critical failure regarding
			 * storing the PIM object
			 */
			g_warning ("%s()[%u] %s",
			           __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
			tmp_err = NULL;
		}
		kolab_mail_access_password_set_visible (koma, FALSE);
	}

 cleanup:
	g_object_unref (ksettings);
	if (tmp_err != NULL) {
		g_propagate_error (error, tmp_err);
		return FALSE;
	}
	return TRUE;
} /* kolab_util_calendar_store () */

/*----------------------------------------------------------------------------*/

/**
 * kolab_util_calendar_extract:
 * @icalcomp: An icalcomponent.
 * @source_type: The data type to process.
 * @ecalcomp: An ECalComponent to return the extracted values to.
 * @tzcomp: An ECaComponent to return contained timezone data.
 *
 * Extract supported information from icalcomponent and return them through
 * ecalcomp and tzcomp.
 *
 * Returns: if the given component type could be extracted successfully into ecalcomp + tzcomp;
 */
gboolean
kolab_util_calendar_extract (icalcomponent *icalcomp,
                             ECalClientSourceType source_type,
                             ECalComponent **ecalcomp,
                             ECalComponent **tzcomp,
                             GError **error)
{
	ECalComponent *comp = NULL;
	ECalComponent *tz = NULL;
	icalcomponent *icalsub = NULL;
	icalcomponent *icaltz = NULL;
	icalcomponent_kind icalkind;
	GError *tmp_err = NULL;

	/* Caution: Let this function return errors
	 *          only which are well-known EDataCal
	 *          errors, since the error set here
	 *          is propagated directly (no mapping)
	 *          in the ECalBackendKolab
	 */

	/* TODO Function argument assertions */

	switch (source_type) {
	case E_CAL_CLIENT_SOURCE_TYPE_EVENTS:
		icalkind = ICAL_VEVENT_COMPONENT;
		break;
	case E_CAL_CLIENT_SOURCE_TYPE_TASKS:
		icalkind = ICAL_VTODO_COMPONENT;
		break;
	case E_CAL_CLIENT_SOURCE_TYPE_MEMOS:
		icalkind = ICAL_VJOURNAL_COMPONENT;
		break;
	default:
		/* can't happen */
		g_assert_not_reached ();
	}

	icalsub = icalcomponent_get_first_component (icalcomp,
	                                             icalkind);
	if (icalsub == NULL) {
		tmp_err = e_data_cal_create_error (InvalidObject, NULL);
		g_propagate_error (error, tmp_err);
		return FALSE;
	}

	comp = e_cal_component_new ();
	e_cal_component_set_icalcomponent (comp, icalsub);
	*ecalcomp = e_cal_component_clone (comp);
	g_object_unref (comp);

	icaltz = icalcomponent_get_first_component (icalcomp,
	                                            ICAL_VTIMEZONE_COMPONENT);
	if (icaltz == NULL) {
		/* no TZ - no problem */
		tzcomp = NULL;
	}
	else {
		/* Create a new ECalComponent from the
		 * ical timezone component.
		 */
		tz = e_cal_component_new ();
		e_cal_component_set_icalcomponent (tz,
		                                   icaltz);
		*tzcomp = e_cal_component_clone (tz);
		g_object_unref (tz);
	}

	return TRUE;
} /* kolab_util_calendar_extract () */

void
kolab_util_calendar_err_to_edb_err (GError **e_err,
                                    const GError *k_err,
                                    const gchar *func,
                                    guint line)
{
	EDataCalCallStatus status = OtherError;
	GError *tmp_err = NULL;

	g_return_if_fail (e_err == NULL || *e_err == NULL);
	g_return_if_fail (k_err != NULL);

	g_warning ("%s()[%u]: '%s', Code %i, Domain '%s'",
	           func, line, k_err->message, k_err->code,
	           g_quark_to_string (k_err->domain));

	if (!e_err)
		return;

	if (g_error_matches (k_err, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
		g_propagate_error (e_err, g_error_copy (k_err));
		return;
	}

	/* TODO
	 *
	 * Need to implement a more elaborated error mapping here.
	 * This function maps Kolab engine errors only. Errors can
	 * originate from the camel/ as well as from the libekolab/
	 * libs. These yield errors in various domains. The following
	 * EDataCalCallStatus (other than OTHER_ERROR) are candidates
	 * for being populated from certain k_err's:
	 *
	 * RepositoryOffline
	 * PermissionDenied
	 * InvalidRange
	 * ObjectNotFound
	 * InvalidObject
	 * ObjectIdAlreadyExists
	 * AuthenticationFailed
	 * AuthenticationRequired
	 * UnsupportedField
	 * UnsupportedMethod
	 * TLSNotAvailable
	 * NoSuchCal
	 * UnknownUser
	 * SearchSizeLimitExceeded
	 * InvalidQuery
	 * QueryRefused
	 * CouldNotCancel
	 * InvalidServerVersion
	 * NotSupported
	 * NotOpened
	 *
	 * Depending on k_err->domain and k_err->code, we can map
	 * to a fitting EDataCalCallStatus. More than one combination
	 * of k_err->domain and k_err->code may map to one specific
	 * EDataCalCallStatus.
	 */

	tmp_err = e_data_cal_create_error (status,
	                                   k_err->message);
	g_propagate_error (e_err, tmp_err);
}
