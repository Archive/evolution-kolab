/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-calendar-cache.h
 *
 *  2011
 *  Copyright  2011  Silvan Marco Fin
 *  <silvan@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_CALENDAR_CACHE_H_
#define _KOLAB_UTIL_CALENDAR_CACHE_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolab/kolab-mail-access.h>
#include <libecal/libecal.h>
#include <libedata-cal/libedata-cal.h>

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

/**
 * SECTION:kolab-util-calendar-cache
 * @short_description: Utility methods for Evolution-Kolab calendar backend implementation with ECalBackendCache side effects.
 * @title: KolabUtilCalendarCache
 *
 * The methods in KolabUtilCalendarCache are supposed to update the
 * ECalBackendCache as a side effect to their primary task or retrieve data
 * from the cache to fulfill its task.
 */


#define KOLAB_TYPE_UTIL_CALENDAR_CACHE             (kolab_util_calendar_cache_get_type ())
#define KOLAB_UTIL_CALENDAR_CACHE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOLAB_TYPE_UTIL_CALENDAR_CACHE, KolabUtilCalendarCache))
#define KOLAB_UTIL_CALENDAR_CACHE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), KOLAB_TYPE_UTIL_CALENDAR_CACHE, KolabUtilCalendarCacheClass))
#define KOLAB_IS_UTIL_CALENDAR_CACHE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOLAB_TYPE_UTIL_CALENDAR_CACHE))
#define KOLAB_IS_UTIL_CALENDAR_CACHE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), KOLAB_TYPE_UTIL_CALENDAR_CACHE))
#define KOLAB_UTIL_CALENDAR_CACHE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), KOLAB_TYPE_UTIL_CALENDAR_CACHE, KolabUtilCalendarCacheClass))

typedef struct _KolabUtilCalendarCacheClass KolabUtilCalendarCacheClass;
typedef struct _KolabUtilCalendarCache KolabUtilCalendarCache;

struct _KolabUtilCalendarCacheClass
{
	GObjectClass parent_class;
};

struct _KolabUtilCalendarCache
{
	GObject parent_instance;
};

GType
kolab_util_calendar_cache_get_type (void) G_GNUC_CONST;

ECalComponent*
kolab_util_calendar_cache_get_tz_by_id (ECalBackendCache *cache,
                                        const gchar *tzid);
ECalComponent*
kolab_util_calendar_cache_get_tz (ECalBackendCache *cache,
                                  ECalComponent *comp);

ECalComponent*
kolab_util_calendar_cache_get_object (ECalBackendSync *backend,
                                      ECalBackendCache *cal_cache,
                                      KolabMailAccess *koma,
                                      const gchar *uid,
                                      gboolean bulk,
                                      GCancellable *cancellable,
                                      GError **error);

ECalComponent*
kolab_util_calendar_cache_remove_instance (ECalBackendCache *cal_cache,
                                           ECalObjModType mod,
                                           ECalComponent *ecalcomp,
                                           const gchar *uid,
                                           const gchar *rid);

gboolean
kolab_util_calendar_cache_assure_uid_on_ecalcomponent (ECalBackendSync *backend,
                                                       ECalBackendCache *cache,
                                                       KolabMailAccess *koma,
                                                       ECalComponent *ecalcomp,
                                                       gboolean bulk,
                                                       GCancellable *cancellable,
                                                       GError **error);

gboolean
kolab_util_calendar_cache_update_on_query (ECalBackendSync *backend,
                                           ECalBackendCache *cache,
                                           KolabMailAccess *koma,
                                           const gchar *query,
                                           GCancellable *cancellable,
                                           GError **error);

gboolean
kolab_util_calendar_cache_update_object (ECalBackendSync *backend,
                                         ECalBackendCache *cache,
                                         KolabMailAccess *koma,
                                         const gchar *uid,
                                         gboolean bulk,
                                         GCancellable *cancellable,
                                         GError **error);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_CALENDAR_CACHE_H_ */

/*----------------------------------------------------------------------------*/
