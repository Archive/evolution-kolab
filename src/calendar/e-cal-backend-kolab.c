/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-cal-backend-kolab.c
 *
 *  Thu Jun 10 18:25:26 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *  and Silvan Marco Fin <silvan@kernelconcepts.de> in 2011
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <gio/gio.h>
#include <glib/gi18n-lib.h>

#include <libecal/libecal.h>

#include <libical/ical.h>

#include <libekolab/e-source-kolab-folder.h>
#include <libekolab/camel-kolab-imapx-settings.h>
#include <libekolab/kolab-mail-access.h>
#include <libekolab/kolab-settings-handler.h>
#include <libekolab/kolab-mail-handle.h>
#include <libekolabutil/kolab-util-http.h>
#include <libekolabutil/kolab-util-glib.h>
#include <libekolabutil/kolab-util-camel.h>
#include <libekolabutil/kolab-util-cal-freebusy.h>

#include "kolab-util-calendar.h"
#include "kolab-util-calendar-cache.h"
#include "e-cal-backend-kolab.h"

/* This forces the GType to be registered in a way that
 * avoids a "statement with no effect" compiler warning.
 * FIXME Use g_type_ensure() once we require GLib 2.34. */
#define REGISTER_TYPE(type) \
	(g_type_class_unref (g_type_class_ref (type)))

/*----------------------------------------------------------------------------*/

static GMutex active_cal_views_lock;

/*----------------------------------------------------------------------------*/
/* forward declarations */

static void
cal_backend_kolab_add_timezone (ECalBackendSync *backend,
                                EDataCal *cal,
                                GCancellable *cancellable,
                                const gchar *tzobject,
                                GError **error);

static void
cal_backend_kolab_open (ECalBackendSync *backend,
                        EDataCal *cal,
                        GCancellable *cancellable,
                        gboolean only_if_exists,
                        GError **error);

/*----------------------------------------------------------------------------*/

#define E_CAL_BACKEND_KOLAB_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), E_TYPE_CAL_BACKEND_KOLAB, ECalBackendKolabPrivate))

G_DEFINE_TYPE (ECalBackendKolab, e_cal_backend_kolab, E_TYPE_CAL_BACKEND_SYNC)

/* private structures for Kolab calendar class ********************************/

/* Private part of the ECalBackendKolab structure */
typedef struct _ECalBackendKolabPrivate ECalBackendKolabPrivate;
struct _ECalBackendKolabPrivate {
	KolabMailAccess *cal_koma;
	ECalBackendCache *cal_cache;
	gchar *user_email;
	ECalComponent *default_zone;
	ECalClientSourceType source_type;
	GError *mode_switch_err;
};

/*----------------------------------------------------------------------------*/
/* internal statics */

static CamelKolabIMAPXSettings *
cal_backend_kolab_get_collection_settings (ECalBackendKolab *backend)
{
	ESource *source;
	ESource *collection;
	ESourceCamel *extension;
	ESourceRegistry *registry;
	CamelSettings *settings;
	const gchar *extension_name;
	const gchar *protocol;

	source = e_backend_get_source (E_BACKEND (backend));
	registry = e_cal_backend_get_registry (E_CAL_BACKEND (backend));

	protocol = KOLAB_CAMEL_PROVIDER_PROTOCOL;
	extension_name = e_source_camel_get_extension_name (protocol);
	e_source_camel_generate_subtype (protocol, CAMEL_TYPE_KOLAB_IMAPX_SETTINGS);

	/* The collection settings live in our parent data source. */
	collection = e_source_registry_find_extension (registry, source, extension_name);
	g_return_val_if_fail (collection != NULL, NULL);

	extension = e_source_get_extension (collection, extension_name);
	settings = e_source_camel_get_settings (extension);

	g_object_unref (collection);

	return CAMEL_KOLAB_IMAPX_SETTINGS (settings);
}

static void
cal_backend_kolab_signal_online_cb (GObject *object)
{
	EBackend *backend = NULL;
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	gboolean online = FALSE;

	g_return_if_fail (E_IS_CAL_BACKEND_KOLAB (object));

	backend = E_BACKEND (object);
	self = E_CAL_BACKEND_KOLAB (object);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	online = e_backend_get_online (backend);

	g_debug ("%s()[%u] backend mode: %i",
	         __func__, __LINE__, online);

	/* FIXME
	 *
	 * This callback is for online state notifications only.
	 * Doing I/O, which can fail, is the wrong thing to do
	 * here since we do not have a proper way to let the user
	 * cancel the I/O. Getting KolabMailAccess online can hang.
	 * Thus, cancellation would be needed.
	 *
	 * e_backend_get_online (backend) could be called in the
	 * "other" operations and the KolabMailAccess state changed
	 * if needed, Problem: KolabMailAccess online/offline state
	 * change involves synchronization with the server, which
	 * may take long time and is not expected by the user if
	 * they e.g. just add a contact or event.
	 *
	 * For now, we'll leave the cancellation issue aside
	 * (we could not have cancelled in 2.30 either), but
	 * at least bind a GError to our backend object, if we
	 * have one. We can then propagate this error in the
	 * first "other" operation the user triggers.
	 *
	 * It has been supposed by upstream to use a global static
	 * GCancellable for the cancellation issue, but it will
	 * not help us here since the Evolution online/offline
	 * toggle button does not seem to expect heavy I/O to
	 * be triggered. Our local GCancellabe would need transfer
	 * to Evo and be registered there so the online/offline
	 * switching in evo-kolab could be cancelled from the
	 * frontend. Moreover, there is no telling how long a
	 * synchronization run with the Kolab server may take,
	 * so we also cannot set a sensible timeout here.
	 *
	 */

	if (priv->cal_koma == NULL) {
		g_warning ("%s()[%u] Backend not yet opened, ignoring online state request",
		           __func__, __LINE__);
		return;
	}

	(void) kolab_util_backend_deploy_mode_by_backend (priv->cal_koma,
	                                                  online,
	                                                  NULL, /* GCancellable (see above ) */
	                                                  &(priv->mode_switch_err));
	if (priv->mode_switch_err) {
		g_warning ("%s()[%u]: Online mode switching error pending, (%s), Domain %s Code %i",
		           __func__, __LINE__,
		           priv->mode_switch_err->message,
		           g_quark_to_string (priv->mode_switch_err->domain),
		           priv->mode_switch_err->code);
		g_error_free (priv->mode_switch_err);
		priv->mode_switch_err = NULL;
	}
}

static gboolean
cal_backend_kolab_notify_opened (ECalBackendKolab *self,
                                 GError **err)
{
	ECalBackend *cbackend = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	KolabMailAccessOpmodeID tmp_mode = KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	GError *tmp_err = NULL;

	g_assert (E_IS_CAL_BACKEND_KOLAB (self));
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);
	cbackend = E_CAL_BACKEND (self);

	tmp_mode = kolab_mail_access_get_opmode (priv->cal_koma, &tmp_err);
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}
	e_backend_set_online (E_BACKEND (self),
	                      tmp_mode == KOLAB_MAIL_ACCESS_OPMODE_ONLINE);
	e_cal_backend_set_writable (cbackend, TRUE);

	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* class functions */

/**
 * cal_backend_kolab_open:
 * @backend: An ECalBackendSync object.
 * @cal: An EDataCal object.
 * @cancellable: A cancellation stack.
 * @only_if_exists: Whether to open the calendar if and only if it already exists
 * or just create it when it does not exist.
 * @err: A GError placeholder.
 *
 * Opens a calendar backend with data from a calendar stored at the specified URI.
 */
static void
cal_backend_kolab_open (ECalBackendSync *backend,
                        EDataCal *cal,
                        GCancellable *cancellable,
                        gboolean only_if_exists,
                        GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	CamelKolabIMAPXSettings *kolab_settings = NULL;
	CamelNetworkSettings *network_settings = NULL;
	KolabSettingsHandler *ksettings = NULL;
	icalcomponent_kind icalkind = ICAL_VEVENT_COMPONENT;
	const gchar *cache_dir = NULL;
	gchar *cache_filename = NULL;
	gchar *servername = NULL;
	gchar *username = NULL;
	const gchar *user_home = NULL;
	gboolean online = FALSE;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */
	(void)only_if_exists; /* FIXME */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	icalkind = e_cal_backend_get_kind (E_CAL_BACKEND (backend));
	/* TODO: this has to be set according to the get_kind() method */
	switch (icalkind) {
	case ICAL_VEVENT_COMPONENT:
		priv->source_type = E_CAL_CLIENT_SOURCE_TYPE_EVENTS;
		break;
	case ICAL_VTODO_COMPONENT:
		priv->source_type = E_CAL_CLIENT_SOURCE_TYPE_TASKS;
		break;
	case ICAL_VJOURNAL_COMPONENT:
		priv->source_type = E_CAL_CLIENT_SOURCE_TYPE_MEMOS;
		break;
	default:
		g_set_error (&tmp_err,
		             KOLAB_BACKEND_ERROR,
		             KOLAB_BACKEND_ERROR_GENERIC,
		             _("Unknown type used in e-cal-backend-kolab initialization"));
		goto exit;
	}

	kolab_settings = cal_backend_kolab_get_collection_settings (self);
	network_settings = CAMEL_NETWORK_SETTINGS (kolab_settings);

	servername = camel_network_settings_dup_host (network_settings);
	username = camel_network_settings_dup_user (network_settings);
	g_debug ("%s()[%u] servername = %s", __func__, __LINE__, servername);
	g_debug ("%s()[%u]   username = %s", __func__, __LINE__, username);

	priv->user_email = kolab_util_calendar_dup_email_address (E_CAL_BACKEND (backend));
	if (priv->user_email == NULL) {
		/* fallback if getting mail address from
		 * ESource failed - synthesized one is just
		 * guessing!
		 */
		if (g_str_has_suffix (username, servername))
			priv->user_email = g_strdup (username);
		else
			priv->user_email = g_strdup_printf ("%s@%s", username, servername);
		g_warning ("%s()[%u] Could not get email address from ESource, synthesized '%s'",
		           __func__, __LINE__, priv->user_email);
	}

	/* Initialize backend cache */
	if (priv->cal_cache != NULL)
		g_object_unref (priv->cal_cache);

	cache_dir = e_cal_backend_get_cache_dir (E_CAL_BACKEND (backend));
	cache_filename = g_build_filename (cache_dir, "cache.xml", NULL);
	priv->cal_cache = e_cal_backend_cache_new (cache_filename);
	g_free (cache_filename);

	ok = e_file_cache_clean (E_FILE_CACHE (priv->cal_cache));
	g_debug (" + Cal cache cleaning %s.", ok ? "was successful" : "FAILED");

	online = e_backend_get_online (E_BACKEND (backend));

	/* Configure settings handler */
	ksettings = kolab_settings_handler_new (kolab_settings,
	                                        E_BACKEND (backend));
	ok = kolab_settings_handler_configure (ksettings,
	                                       KOLAB_FOLDER_CONTEXT_CALENDAR,
	                                       &tmp_err);
	if (! ok)
		goto exit;

	ok = kolab_settings_handler_bringup (ksettings, &tmp_err);
	if (! ok)
		goto exit;

	/* init the HTTP utils */
	user_home = kolab_settings_handler_get_char_field (ksettings,
	                                                   KOLAB_SETTINGS_HANDLER_CHAR_FIELD_USER_HOME_DIR,
	                                                   &tmp_err);
	if (tmp_err != NULL)
		goto exit;
	kolab_util_http_init (user_home);

	/* create new KolabMailAccess instance */
	priv->cal_koma = KOLAB_MAIL_ACCESS (g_object_new (KOLAB_TYPE_MAIL_ACCESS, NULL));

	/* configure and bring up KolabMailAccess instance */
	ok = kolab_mail_access_configure (priv->cal_koma,
	                                  ksettings,
	                                  &tmp_err);
	if (! ok)
		goto exit;

	ok = kolab_mail_access_bringup (priv->cal_koma,
	                                cancellable,
	                                &tmp_err);
	if (! ok)
		goto exit;

	(void) kolab_util_backend_deploy_mode_by_backend (priv->cal_koma,
	                                                  online,
	                                                  cancellable,
	                                                  &tmp_err);
 exit:

	if (ksettings != NULL)
		g_object_unref (ksettings);
	if (servername != NULL)
		g_free (servername);
	if (username != NULL)
		g_free (username);

	/* do we have an error set? if so, propagate and return */
	if (tmp_err != NULL) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}

	/* all good, notify that we're open for business */
	ok = cal_backend_kolab_notify_opened (self, &tmp_err);
	if (! ok) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
	}
}

static void
cal_backend_kolab_refresh (ECalBackendSync *backend,
                           EDataCal *cal,
                           GCancellable *cancellable,
                           GError **error)
{
	/* ECalBackendKolab *self = NULL; */
	/* ECalBackendKolabPrivate *priv = NULL; */

	g_return_if_fail (error == NULL || *error == NULL);
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */

	/* self = E_CAL_BACKEND_KOLAB (backend); */
	/* priv = E_CAL_BACKEND_KOLAB_PRIVATE (self); */

	g_error ("%s: FIXME implement me", __func__);
}

static gchar *
cal_backend_kolab_get_backend_property (ECalBackend *backend,
                                        const gchar *prop_name)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;

	/* this is a modified dupe of the respective mapi function */

	g_return_val_if_fail (prop_name != NULL, NULL);

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	if (g_str_equal (prop_name, CLIENT_BACKEND_PROPERTY_CAPABILITIES)) {
		return g_strjoin (
			",",
			CAL_STATIC_CAPABILITY_NO_ALARM_REPEAT,
			CAL_STATIC_CAPABILITY_NO_AUDIO_ALARMS,
			CAL_STATIC_CAPABILITY_NO_EMAIL_ALARMS,
			CAL_STATIC_CAPABILITY_NO_PROCEDURE_ALARMS,
			CAL_STATIC_CAPABILITY_ONE_ALARM_ONLY,
			CAL_STATIC_CAPABILITY_REMOVE_ALARMS,
			CAL_STATIC_CAPABILITY_NO_THISANDFUTURE,
			CAL_STATIC_CAPABILITY_NO_THISANDPRIOR,
			CAL_STATIC_CAPABILITY_NO_CONV_TO_ASSIGN_TASK,
			CAL_STATIC_CAPABILITY_NO_CONV_TO_RECUR,
			CAL_STATIC_CAPABILITY_HAS_UNACCEPTED_MEETING,
			CAL_STATIC_CAPABILITY_REFRESH_SUPPORTED,
			NULL);
	} else if (g_str_equal (prop_name, CAL_BACKEND_PROPERTY_CAL_EMAIL_ADDRESS)) {
		/* see the comment for user_email in e_cal_backend_kolab_open() ! */
		return g_strdup (priv->user_email);
	} else if (g_str_equal (prop_name, CAL_BACKEND_PROPERTY_ALARM_EMAIL_ADDRESS)) {
		/* We don't support email alarms. This should not have been called. */
		return NULL;
	} else if (g_str_equal (prop_name, CAL_BACKEND_PROPERTY_DEFAULT_OBJECT)) {
		ECalComponent *comp;
		gchar *prop_value;

		comp = e_cal_component_new ();

		switch (e_cal_backend_get_kind (E_CAL_BACKEND (backend))) {
		case ICAL_VEVENT_COMPONENT:
			e_cal_component_set_new_vtype (comp, E_CAL_COMPONENT_EVENT);
			break;
		case ICAL_VTODO_COMPONENT:
			e_cal_component_set_new_vtype (comp, E_CAL_COMPONENT_TODO);
			break;
		case ICAL_VJOURNAL_COMPONENT:
			e_cal_component_set_new_vtype (comp, E_CAL_COMPONENT_JOURNAL);
			break;
		default:
			g_object_unref (comp);
			return NULL;
		}

		prop_value = e_cal_component_get_as_string (comp);

		g_object_unref (comp);

		return prop_value;
	}

	/* Chain up to parent's get_backend_property() method. */
	return E_CAL_BACKEND_CLASS (e_cal_backend_kolab_parent_class)->
		get_backend_property (backend, prop_name);
}

/**
 * cal_backend_kolab_get_object:
 * @backend: An ECalBackendSync object.
 * @cal: An EDataCal object.
 * @cancellable: A cancellation stack.
 * @uid: UID of the object to get.
 * @rid: Recurrence ID of the specific instance to get, or NULL if getting the
 * master object.
 * @calobject: Placeholder for returned object.
 * @err: GError placeholder.
 *
 * Queries a calendar backend for a calendar object based on its unique
 * identifier and its recurrence ID (if a recurrent appointment).
 */
static void
cal_backend_kolab_get_object (ECalBackendSync *backend,
                              EDataCal *cal,
                              GCancellable *cancellable,
                              const gchar *uid,
                              const gchar *rid,
                              gchar **calobj,
                              GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	ECalComponent *ecalcomp = NULL;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */
	(void)rid; /* FIXME */ /* rid may be NULL */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	ecalcomp = kolab_util_calendar_cache_get_object (backend,
	                                                 priv->cal_cache,
	                                                 priv->cal_koma,
	                                                 uid,
	                                                 FALSE,
	                                                 cancellable,
	                                                 &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}
	if (ecalcomp != NULL) {
		*calobj = (gchar *) e_cal_component_get_as_string (ecalcomp);
		g_object_unref (ecalcomp);
	}
}

static void
cal_backend_kolab_get_object_list (ECalBackendSync *backend,
                                   EDataCal *cal,
                                   GCancellable *cancellable,
                                   const gchar *sexp,
                                   GSList **calobjs,
                                   GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	GList *uid_list = NULL;
	GList *it = NULL;
	const gchar *foldername;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */
	/* sexp may be NULL */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	ok = kolab_util_calendar_cache_update_on_query (backend,
	                                                priv->cal_cache,
	                                                priv->cal_koma,
	                                                sexp,
	                                                cancellable,
	                                                &tmp_err);
	if (! ok) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}

	foldername = kolab_util_backend_get_foldername (E_BACKEND (backend));
	uid_list = kolab_mail_access_query_uids (priv->cal_koma,
	                                         foldername,
	                                         sexp,
	                                         &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}

	for (it = uid_list; it != NULL; it = g_list_next (it)) {
		gchar *uid = it->data;
		ECalComponent *ecalcomp = NULL;
		ecalcomp = kolab_util_calendar_cache_get_object (backend,
		                                                 priv->cal_cache,
		                                                 priv->cal_koma,
		                                                 uid,
		                                                 TRUE,
		                                                 cancellable,
		                                                 &tmp_err);
		/* have we been cancelled? */
		if (g_cancellable_is_cancelled (cancellable)) {
			kolab_util_glib_glist_free (uid_list);
			if (ecalcomp != NULL)
				g_object_unref (ecalcomp);
			kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
			g_error_free (tmp_err);
			return;
		}
		/* not cancelled, other error */
		if (tmp_err != NULL) {
			/* TODO Can we sensibly propagate an error here?
			 *      Failure for one does not necessarily imply
			 *      failure for the others
			 */
			g_warning ("%s()[%u]: %s",
			           __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
			tmp_err = NULL;
		}
		if (ecalcomp != NULL) {
			*calobjs = g_slist_append (*calobjs,
			                           e_cal_component_get_as_string (ecalcomp));
			g_object_unref (ecalcomp);
		}
	}

	kolab_util_glib_glist_free (uid_list);
}

static void
cal_backend_kolab_get_free_busy (ECalBackendSync *backend,
                                 EDataCal *cal,
                                 GCancellable *cancellable,
                                 const GSList *users,
                                 time_t start,
                                 time_t end,
                                 GSList **freebusyobjs,
                                 GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	KolabSettingsHandler *ksettings = NULL;
	GSList *it = NULL;
	gboolean have_objects = FALSE;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */
	(void)start; /* FIXME */
	(void)end; /* FIXME */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	ksettings = kolab_mail_access_get_settings_handler (priv->cal_koma);
	if (ksettings == NULL) {
		/* TODO add a translatable string as custom message */
		tmp_err = e_data_cal_create_error (OtherError, NULL);
		goto exit;
	}

	/* If we went through authentication already, we can make the
	 * password visible via ksettings so it can be used in
	 * KolabUtilHttpJob. If we're not authenticated, we're most
	 * probably offline, in which case we cannot fetch XFB anyway
	 */
	kolab_mail_access_password_set_visible (priv->cal_koma, TRUE);

	/* receive the xfb information */
	for (it = (GSList *) users; it != NULL; it = g_slist_next (it)) {
		KolabUtilHttpJob *job = NULL;
		ECalComponent *ecalcomp = NULL;

		/* TODO Can we sensibly propagate an error
		 *      here? Failure for one does not
		 *      necessarily imply failure for all
		 */

		job = kolab_util_calendar_retrieve_xfb (ksettings, (gchar *) it->data, &tmp_err);
		if (tmp_err != NULL)
			goto skip_warn;

		ecalcomp = kolabconv_cal_util_freebusy_ecalcomp_new_from_ics ((gchar *) (job->buffer->data),
		                                                              job->nbytes,
		                                                              &tmp_err);
		kolab_util_http_job_free (job);

		if (tmp_err != NULL)
			goto skip_warn;

		if (ecalcomp == NULL)
			/* this is not an error, but there's no F/B
			 * information available.
			 */
			continue;

		*freebusyobjs = g_slist_append (*freebusyobjs,
		                                g_strdup (e_cal_component_get_as_string (ecalcomp)));
		g_object_unref (ecalcomp);
		have_objects = TRUE;

		continue;

	skip_warn:
		g_warning ("%s()[%u] %s", __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}

 exit:

	/* hide the password again */
	kolab_mail_access_password_set_visible (priv->cal_koma, FALSE);

	if (ksettings != NULL)
		g_object_unref (ksettings);

	if (! have_objects)
		tmp_err = e_data_cal_create_error (ObjectNotFound, NULL);

	if (tmp_err != NULL)
		g_propagate_error (error, tmp_err);
}

/**
 * cal_backend_kolab_create_objects:
 * @backend: (sync) kolab calendar backend object.
 * @cal: An EDataCal object.
 * @cancellable: A cancellation stack
 * @calobjs: Contains the events to be created in the calendar as iCalendar string.
 * @uids: Return values of the created events uid.
 * @new_components: The newly created components, serialized as ECalComponent objects.
 * @err: A GError placeholder.
 *
 * Creates a new calendar entry in the Kolab calendar. This method gets called
 * when Evolution requests a new entry to be saved in the calendar.
 */
static void
cal_backend_kolab_create_objects (ECalBackendSync *backend,
                                  EDataCal *cal,
                                  GCancellable *cancellable,
                                  const GSList *calobjs,
                                  GSList **uids,
                                  GSList **new_components,
                                  GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	ECalComponent *ecalcomp = NULL;
	ECalComponent *tzcomp = NULL;
	const gchar *calobj = NULL;
	const gchar *uid = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */

	if (g_slist_next (calobjs) != NULL) {
		g_propagate_error (error, e_data_cal_create_error (UnsupportedMethod, _("Kolab does not support bulk additions")));
		return;
	}

	calobj = calobjs->data;
	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	/* What do we have here? There is an ECalComponent, unable to hold
	 * timezone information and an calendar entry at the same time.
	 * So Kolab glue code expects two ECalComponents, one for the calendar entry
	 * one for the timezone.
	 */
	ecalcomp = e_cal_component_new_from_string (calobj);
	if (ecalcomp == NULL) {
		g_warning (" + **calobj could not be parsed into ECalComponent: %s",
		           calobj);
		/* TODO add translatable string as custom message */
		tmp_err = e_data_cal_create_error (InvalidObject, NULL);
		g_propagate_error (error, tmp_err);
		return;
	}
	tzcomp = kolab_util_calendar_cache_get_tz (priv->cal_cache,
	                                           ecalcomp);

	if (tzcomp == NULL) {
		gchar *tzid = NULL;
		tzid = kolab_util_calendar_get_tzid (ecalcomp,
		                                     E_CAL_COMPONENT_FIELD_DTSTART);
		/* If tzid is UTC, everything is fine */
		if (g_strcmp0 ("UTC", tzid) == 0) {
			g_free (tzid);
			tzid = NULL;
		}
		if (tzid != NULL) {
			/* TZID is set, but no timezone is available. Currently
			 * this may happen, if an object is copied to an yet unused/empty
			 * calendar.
			 */
			g_free (tzid);
			g_object_unref (ecalcomp);
			tmp_err = e_data_cal_create_error (InvalidObject,
			                                   _("TZID is set, but no timezone is available. Currently this may happen if an object is copied to an empty calendar."));
			g_propagate_error (error, tmp_err);
			return;
		}
	}

	ok = kolab_util_calendar_cache_assure_uid_on_ecalcomponent (backend,
	                                                            priv->cal_cache,
	                                                            priv->cal_koma,
	                                                            ecalcomp,
	                                                            FALSE,
	                                                            cancellable,
	                                                            &tmp_err);
	if (! ok) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		g_object_unref (ecalcomp);
		if (tzcomp != NULL)
			g_object_unref (tzcomp);
		return;
	}

	ok = kolab_util_calendar_store (ecalcomp,
	                                tzcomp,
	                                priv->default_zone,
	                                backend,
	                                priv->cal_koma,
	                                cancellable,
	                                &tmp_err);
	if (! ok) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		g_object_unref (ecalcomp);
		if (tzcomp != NULL)
			g_object_unref (tzcomp);
		return;
	}

	if (tzcomp != NULL)
		g_object_unref (tzcomp);

	/* TODO Check whether we had a collision on the server
	 *      while storing the object (corner case, there
	 *      would need to be a race condition between clients,
	 *      which want to write objects with identical UID,
	 *      between the calls to
	 *      kolab_util_calendar_cache_assure_uid_on_ecalcomponent()
	 *      and kolab_util_calendar_store() in this function
	 *
	 *      In that case we would need to read-back the object
	 *      from the server (can UID have changed...?) and return
	 *      a copy in *new_component (and set *uid) before returning
	 *      from this function
	 */
	e_cal_component_get_uid (ecalcomp, &uid);
	*uids = g_slist_prepend (*uids, g_strdup (uid));
	*new_components = g_slist_prepend (*new_components, (gpointer) ecalcomp);
}

/**
 * cal_backend_kolab_modify_objects:
 * @backend: An ECalBackendSync object.
 * @cal: An EDataCal object.
 * @cancellable: A cancellation stack.
 * @calobjs: Objects to be modified.
 * @mod: Type of modification to be done.
 * @old_components: Placeholder for returning the old objects as it was stored on the
 * backend.
 * @new_components: Placeholder for returning the new objects as it has been stored
 * on the backend.
 * @err: A GError placeholder.
 *
 * Requests the calendar backend to modify an existing object. If the object
 * does not exist on the calendar, it will be issued as new event.
 */
static void
cal_backend_kolab_modify_objects (ECalBackendSync *backend,
                                  EDataCal *cal,
                                  GCancellable *cancellable,
                                  const GSList *calobjs,
                                  ECalObjModType mod,
                                  GSList **old_components,
                                  GSList **new_components,
                                  GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	ECalComponent *ecalcomp = NULL;
	ECalComponent *oldcomp = NULL;
	ECalComponent *ecaltz = NULL;
	const gchar *uid = NULL, *calobj;
	gboolean is_instance = FALSE;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */
	(void)mod; /* FIXME */

	if (calobjs->next) {
		g_propagate_error (error, e_data_cal_create_error (UnsupportedMethod, "Kolab does not support bulk modifications"));
		return;
	}

	calobj = calobjs->data;
	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	/* calobj contains the modifications to be performed.
	 * ecalcomp is the ECalComponent created from calobj.
	 * scenarios:
	 *  a) ecalcomp is no part of a recurring event:
	 *     read oldobj by means of ecalcomps uid
	 *     store ecalcomp and we are done.
	 *  b) ecalcomp is a modification to all instances of a recurring event:
	 *     should be handled like (a)
	 *  c) ecalcomp is a single instance of a recurring event:
	 *     Kolab can not handle detached recurrences. Remove the instance
	 *     from the recurrence and create a new single event für ecalcomp.
	 */

	ecalcomp = e_cal_component_new_from_string (calobj);
	if (ecalcomp == NULL) {
		tmp_err = e_data_cal_create_error (InvalidObject, NULL);
		g_propagate_error (error, tmp_err);
		return;
	}

	e_cal_component_get_uid (ecalcomp, &uid);

	oldcomp = kolab_util_calendar_cache_get_object (backend,
	                                                priv->cal_cache,
	                                                priv->cal_koma,
	                                                uid,
	                                                FALSE,
	                                                cancellable,
	                                                &tmp_err);
	if (tmp_err != NULL) {
		/* TODO: how to handle request to modify non existing objects? */
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		g_object_unref (ecalcomp);
		if (oldcomp != NULL)
			g_object_unref (oldcomp);
		return;
	}

	*old_components = g_slist_append (NULL, oldcomp);
	/* Is this correct? Perhaps _get_tz (..., oldcomp) is needed.
	 * This depends on wether evolution sends the timezone, if modification
	 * to the timezone has been made. In any other case the timezone
	 * information from ecalcomp and oldcomp should be all the same.
	 */
	ecaltz = kolab_util_calendar_cache_get_tz (priv->cal_cache,
	                                           ecalcomp);
	is_instance = e_cal_component_is_instance (ecalcomp);
	if (is_instance) {
		/* Handling of (c) */
		ECalComponent *newcomp = NULL;
		ECalComponent *oldtz = NULL;
		gchar *rid = NULL;

		oldtz = kolab_util_calendar_cache_get_tz (priv->cal_cache,
		                                          oldcomp);
		rid = e_cal_component_get_recurid_as_string (ecalcomp);
		/* This removes the recurrence from ecalcomp. */
		e_cal_component_set_recurid (ecalcomp, NULL);
		/* This removes ecalcomp from the recurrence. */
		newcomp = kolab_util_calendar_cache_remove_instance (priv->cal_cache,
		                                                     E_CAL_OBJ_MOD_THIS,
		                                                     oldcomp,
		                                                     uid,
		                                                     rid);
		ok = kolab_util_calendar_cache_assure_uid_on_ecalcomponent (backend,
		                                                            priv->cal_cache,
		                                                            priv->cal_koma,
		                                                            ecalcomp,
		                                                            TRUE,
		                                                            cancellable,
		                                                            &tmp_err);
		if (! ok) {
			kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
			g_error_free (tmp_err);
			if (oldtz != NULL)
				g_object_unref (oldtz);
			g_object_unref (ecalcomp);
			if (ecaltz != NULL)
				g_object_unref (ecaltz);
			g_object_unref (newcomp);
			g_object_unref (oldcomp);
			g_slist_free (*old_components);
			*old_components = NULL;
			return;
		}

		ok = kolab_util_calendar_store (newcomp,
		                                oldtz,
		                                priv->default_zone,
		                                backend,
		                                priv->cal_koma,
		                                cancellable,
		                                &tmp_err);
		if (! ok) {
			kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
			g_error_free (tmp_err);
			if (oldtz != NULL)
				g_object_unref (oldtz);
			g_object_unref (ecalcomp);
			if (ecaltz != NULL)
				g_object_unref (ecaltz);
			g_object_unref (newcomp);
			g_object_unref (oldcomp);
			g_slist_free (*old_components);
			*old_components = NULL;
			return;
		}
		e_cal_backend_notify_component_modified (E_CAL_BACKEND (backend),
		                                         (*old_components)->data,
		                                         newcomp);
		g_object_unref (newcomp);
		if (oldtz != NULL)
			g_object_unref (oldtz);
	}
	ok = kolab_util_calendar_store (ecalcomp,
	                                ecaltz,
	                                priv->default_zone,
	                                backend,
	                                priv->cal_koma,
	                                cancellable,
	                                &tmp_err);
	if (! ok) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		g_object_unref (ecalcomp);
		return;
	}

	*new_components = g_slist_append (NULL, ecalcomp);
	if (is_instance) {
		e_cal_backend_notify_component_created (E_CAL_BACKEND (backend),
		                                        ecalcomp);
	}

	if (ecaltz != NULL)
		g_object_unref (ecaltz);
}

/**
 * cal_backend_kolab_remove_objects:
 * @backend: An ECalBackendSync object.
 * @cal: An EDataCal object.
 * @cancellable: A cancellation stack.
 * @id: a #GSList of #ECalComponentId-s of the objects to remove.
 * @mod: Type of removal.
 * @old_components: Placeholder for returning the old objects as they were stored on the
 * backend.
 * @new_components: Placeholder for returning the objects after they have been modified (when
 * removing individual instances). If removing the whole object, this will be
 * NULL.
 * @err: A GError placeholder.
 *
 * Removes an (a part of a [recurring] ) object from a calendar backend. The
 * backend will notify all of its clients about the change.
 */
static void
cal_backend_kolab_remove_objects (ECalBackendSync *backend,
                                  EDataCal *cal,
                                  GCancellable *cancellable,
                                  const GSList *ids,
                                  ECalObjModType mod,
                                  GSList **old_components,
                                  GSList **new_components,
                                  GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	ECalComponent *oldcomp = NULL;
	ECalComponent *newcomp = NULL;
	ECalComponent *oldtz = NULL;
	ECalComponent *oc_data = NULL;
	ECalComponent *nc_data = NULL;
	ECalComponentId *id = NULL;
	const gchar *uid, *rid;
	KolabSettingsHandler *ksettings = NULL;
	const gchar *foldername;
	GError *tmp_err = NULL;
	gboolean trigger_needed = FALSE;
	gboolean ok = FALSE;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */
	/* rid may be NULL */

	if (ids->next) {
		g_propagate_error (error, e_data_cal_create_error (UnsupportedMethod, _("Kolab does not support bulk removals")));
		return;
	}

	id = ids->data;
	uid = id->uid;
	rid = id->rid;
	id = NULL;

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	if (rid == NULL)
		mod = E_CAL_OBJ_MOD_ALL;

	foldername = kolab_util_backend_get_foldername (E_BACKEND (backend));

	oldcomp = kolab_util_calendar_cache_get_object (backend,
	                                                priv->cal_cache,
	                                                priv->cal_koma,
	                                                uid,
	                                                FALSE,
	                                                cancellable,
	                                                &tmp_err);
	if (tmp_err != NULL) {
		switch (tmp_err->code) {
		case KOLAB_BACKEND_ERROR_NOTFOUND:
			(void) e_cal_backend_cache_remove_component (priv->cal_cache,
			                                             uid,
			                                             NULL);
			/* Don't bother if this worked */
			id = g_new0 (ECalComponentId, 1);
			id->uid = g_strdup (uid);
			id->rid = g_strdup (rid);
			e_cal_backend_notify_component_removed (E_CAL_BACKEND (backend),
			                                        id,
			                                        NULL,
			                                        NULL);
			g_free (id);
			break;
		default:
			kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		}
		g_error_free (tmp_err);
		return;
	}

	*old_components = g_slist_append (NULL, oldcomp);
	id = e_cal_component_get_id (oldcomp);

	switch (mod) {
	case E_CAL_OBJ_MOD_THIS:
		oldtz = kolab_util_calendar_cache_get_tz (priv->cal_cache,
		                                          oldcomp);
		newcomp = kolab_util_calendar_cache_remove_instance (priv->cal_cache,
		                                                     E_CAL_OBJ_MOD_THIS,
		                                                     oldcomp,
		                                                     uid,
		                                                     rid);
		/* newcomp now contains an exception rule for the rid */
		ok = kolab_util_calendar_store (newcomp,
		                                oldtz,
		                                priv->default_zone,
		                                backend,
		                                priv->cal_koma,
		                                cancellable,
		                                &tmp_err);
		if (oldtz != NULL)
			g_object_unref (oldtz);
		if (! ok) {
			kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
			g_error_free (tmp_err);
			e_cal_component_free_id (id);
			g_object_unref (newcomp);
			g_object_unref (oldcomp);
			g_slist_free (*old_components);
			*old_components = NULL;
			return;
		}
		*new_components = g_slist_append (NULL, newcomp);
		break;
	case E_CAL_OBJ_MOD_THIS_AND_PRIOR: /* not supported by backend, should not be reached */
		g_warning ("%s()[%u]: removing this and prior not supported.",
		           __func__, __LINE__);
		break;
	case E_CAL_OBJ_MOD_THIS_AND_FUTURE: /* not supported by backend, should not be reached */
		g_warning ("%s()[%u]: removing this and future not supported.",
		           __func__, __LINE__);
		break;
	case E_CAL_OBJ_MOD_ALL:
	default:
		ok = kolab_mail_access_delete_by_uid (priv->cal_koma,
		                                      uid,
		                                      foldername,
		                                      cancellable,
		                                      &tmp_err);
		(void) e_cal_backend_cache_remove_component (priv->cal_cache,
		                                             uid,
		                                             NULL);
		if (! ok) {
			kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
			g_error_free (tmp_err);
			e_cal_component_free_id (id);
			g_object_unref (oldcomp);
			g_slist_free (*old_components);
			*old_components = NULL;
			return;
		}
	}

	trigger_needed = kolab_mail_access_source_fbtrigger_needed (priv->cal_koma,
	                                                            foldername,
	                                                            &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		e_cal_component_free_id (id);
		g_object_unref (oldcomp);
		g_slist_free (*old_components);
		*old_components = NULL;
		return;
	}
	if (trigger_needed) {
		ksettings = kolab_mail_access_get_settings_handler (priv->cal_koma);
		ok = kolab_util_calendar_toggle_pfb_trigger (ksettings,
		                                             foldername,
		                                             &tmp_err);
		g_object_unref (ksettings);
		if (! ok) {
			g_warning ("%s()[%u] %s", __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
			tmp_err = NULL;
		}
	}

	if (*old_components != NULL)
		oc_data = (*old_components)->data;
	if (*new_components != NULL)
		nc_data = (*new_components)->data;

	e_cal_backend_notify_component_removed (E_CAL_BACKEND (backend),
	                                        id,
	                                        oc_data,
	                                        nc_data);
	e_cal_component_free_id (id);
}

/**
 * cal_backend_kolab_receive_objects:
 * @backend: An ECalBackendSync object.
 * @cal: An EDataCal object.
 * @cancellable: A cancellation stack.
 * @calobj: iCalendar object to receive.
 * @err: A GError placeholder.
 *
 * Submits a (series of) calendar event(s) through iCalendar object(s).
 */
static void
cal_backend_kolab_receive_objects (ECalBackendSync *backend,
                                   EDataCal *cal,
                                   GCancellable *cancellable,
                                   const gchar *calobj,
                                   GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	ECalComponent *ecalcomp = NULL;
	ECalComponent *tzcomp = NULL;
	icalcomponent *icalcomp = NULL;
	gchar *tzstr = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	/* Workflow:
	 * Incoming calobj is supposed to be a vcalendar string.
	 * KolabMailHandle is the data structure we want to
	 * generate and KolabMailHandle takes an ECalComponent and a timezone
	 * (as ECalComponent). The task at hand: convert calobj into an
	 * icalcomponent ( #1 ). Afterwards process this icalcomponent and extract
	 * the relevant information an ECalComponent can handle ( #2 ). Then
	 * create the ECalComponent, generate a timezone and stuff it
	 * into the KolabMailAccess object. ( #3 )
	 */

	icalcomp = icalparser_parse_string (calobj);
	if (icalcomp == NULL) {
		g_warning ("%s()[%u] calobj could not be parsed into an icalcomponent: %s",
		           __func__, __LINE__, calobj);
		/* TODO add a translatable string as custom error message */
		tmp_err = e_data_cal_create_error (InvalidObject, NULL);
		g_propagate_error (error, tmp_err);
		return;
	}
	/* now this better should be an vcalendar entry */
	if (icalcomponent_isa (icalcomp) != ICAL_VCALENDAR_COMPONENT) {
		g_warning ("%s()[%u] calobj does not represent a vcalendar entry: %s",
		           __func__, __LINE__, calobj);
		icalcomponent_free (icalcomp);
		tmp_err = e_data_cal_create_error (InvalidObject, NULL);
		g_propagate_error (error, tmp_err);
		return;
	}

	ok = kolab_util_calendar_extract (icalcomp,
	                                  priv->source_type,
	                                  &ecalcomp,
	                                  &tzcomp,
	                                  &tmp_err);
	icalcomponent_free (icalcomp);
	if (! ok) {
		g_propagate_error (error, tmp_err);
		return;
	}

	ok = kolab_util_calendar_store (ecalcomp,
	                                tzcomp,
	                                priv->default_zone,
	                                backend,
	                                priv->cal_koma,
	                                cancellable,
	                                &tmp_err);
	if (! ok) {
		kolab_util_calendar_err_to_edb_err (error,
		                                    tmp_err,
		                                    __func__,
		                                    __LINE__);
		g_error_free (tmp_err);
		g_object_unref (ecalcomp);
		if (tzcomp != NULL)
			g_object_unref (tzcomp);
		return;
	}

	if (tzcomp != NULL) {
		tzstr = e_cal_component_get_as_string (tzcomp);
		g_object_unref (tzcomp);
		cal_backend_kolab_add_timezone (backend,
		                                cal,
		                                cancellable,
		                                tzstr,
		                                &tmp_err);
		if (tmp_err != NULL) {
			kolab_util_calendar_err_to_edb_err (error,
			                                    tmp_err,
			                                    __func__,
			                                    __LINE__);
			g_error_free (tmp_err);
			g_object_unref (ecalcomp);
			return;
		}
	}
	e_cal_backend_notify_component_created (E_CAL_BACKEND (backend),
	                                        ecalcomp);
	g_object_unref (ecalcomp);
}

static void
cal_backend_kolab_send_objects (ECalBackendSync *backend,
                                EDataCal *cal,
                                GCancellable *cancellable,
                                const gchar *calobj,
                                GSList **users,
                                gchar **modified_calobj,
                                GError **error)
{
	/* ECalBackendKolab *self = NULL; */
	/* ECalBackendKolabPrivate *priv = NULL; */

	g_return_if_fail (error == NULL || *error == NULL);
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */

	/* self = E_CAL_BACKEND_KOLAB (backend); */
	/* priv = E_CAL_BACKEND_KOLAB_PRIVATE (self); */

	*users = NULL;
	*modified_calobj = g_strdup (calobj);
}

static void
cal_backend_kolab_get_attachment_uris (ECalBackendSync *backend,
                                       EDataCal *cal,
                                       GCancellable *cancellable,
                                       const gchar *uid,
                                       const gchar *rid,
                                       GSList **attachments,
                                       GError **error)
{
	/* ECalBackendKolab *self = NULL; */
	/* ECalBackendKolabPrivate *priv = NULL; */

	g_return_if_fail (error == NULL || *error == NULL);
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */

	/* self = E_CAL_BACKEND_KOLAB (backend); */
	/* priv = E_CAL_BACKEND_KOLAB_PRIVATE (self); */

	g_error ("%s: FIXME implement me", __func__);
}

static void
cal_backend_kolab_discard_alarm (ECalBackendSync *backend,
                                 EDataCal *cal,
                                 GCancellable *cancellable,
                                 const gchar *uid,
                                 const gchar *rid,
                                 const gchar *auid,
                                 GError **error)
{
	/* ECalBackendKolab *self = NULL; */
	/* ECalBackendKolabPrivate *priv = NULL; */

	g_return_if_fail (error == NULL || *error == NULL);
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */

	/* self = E_CAL_BACKEND_KOLAB (backend); */
	/* priv = E_CAL_BACKEND_KOLAB_PRIVATE (self); */

	g_error ("%s: FIXME implement me", __func__);
}

static void
cal_backend_kolab_get_timezone (ECalBackendSync *backend,
                                EDataCal *cal,
                                GCancellable *cancellable,
                                const gchar *tzid,
                                gchar **tzobject,
                                GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	ECalComponent *ecaltz = NULL;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	(void)cancellable; /* unused */ /* cancellable may be NULL */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	ecaltz = kolab_util_calendar_cache_get_tz_by_id (priv->cal_cache,
	                                                 tzid);
	if (ecaltz == NULL) {
		tmp_err = e_data_cal_create_error (ObjectNotFound, NULL);
		g_propagate_error (error, tmp_err);
		return;
	}

	*tzobject = e_cal_component_get_as_string (ecaltz);
	g_object_unref (ecaltz);
}

/**
 * cal_backend_kolab_add_timezone:
 * @backend: An ECalBackendSync object.
 * @cal: An EDataCal object.
 * @cancellable: A cancellation stack.
 * @tzobject: VTIMEZONE object to be added.
 * @err: A GError placeholder.
 *
 * Add a timezone object to the given backend.
 */
static void
cal_backend_kolab_add_timezone (ECalBackendSync *backend,
                                EDataCal *cal,
                                GCancellable *cancellable,
                                const gchar *tzobject,
                                GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	icalcomponent *icalcomp = NULL;
	icaltimezone *icaltz = NULL;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	icalcomp = icalparser_parse_string (tzobject);
	if (icalcomp == NULL) {
		tmp_err = e_data_cal_create_error (InvalidObject, NULL);
		g_propagate_error (error, tmp_err);
		return;
	}

	if (icalcomponent_isa (icalcomp) == ICAL_VTIMEZONE_COMPONENT) {

		icaltz = icaltimezone_new ();
		icaltimezone_set_component (icaltz,
		                            icalcomp);

		if (! e_cal_backend_cache_put_timezone (priv->cal_cache, icaltz)) {
			g_warning ("%s()[%u]: Putting timezone object into cache failed.",
			           __func__, __LINE__);
			icaltimezone_free (icaltz, 1);
			tmp_err = e_data_cal_create_error (OtherError,
			                                   _("Putting timezone object into cache failed."));
			return;
		}
		icaltimezone_free (icaltz, 1);
	}
}

#if 0  /* FIXME Delete this once kolab_mail_access_try_password_sync()
        *       works.  There may be code here that can be reused. */
static void
cal_backend_kolab_authenticate_user (ECalBackendSync *backend,
                                     GCancellable *cancellable,
                                     ECredentials *credentials,
                                     GError **error)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	KolabSettingsHandler *ksettings = NULL;
	const gchar *cred_user = NULL;
	const gchar *cred_pwd  = NULL;
	const gchar *kset_user = NULL;
	gboolean online = FALSE;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	ksettings = kolab_mail_access_get_settings_handler (priv->cal_koma);

	/* warn about a possible inconsistency in user names */
	kset_user = kolab_settings_handler_get_char_field (ksettings,
	                                                   KOLAB_SETTINGS_HANDLER_CHAR_FIELD_KOLAB_USER_NAME,
	                                                   &tmp_err);
	if (tmp_err != NULL) {
		g_object_unref (ksettings);
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}
	cred_user = e_credentials_peek (credentials,
	                                E_CREDENTIALS_KEY_USERNAME);
	if (g_strcmp0 (kset_user, cred_user) != 0) {
		g_warning ("%s()[%u] username from argument and username in "
		           "KolabSettingsHandler do not match: %s vs. %s",
		           __func__, __LINE__, cred_user, kset_user);
	}

	cred_pwd = e_credentials_peek (credentials,
	                               E_CREDENTIALS_KEY_PASSWORD);

	kolab_util_backend_prepare_settings (ksettings,
	                                     NULL,
	                                     NULL,
	                                     NULL,
	                                     cred_pwd,
	                                     NULL,
	                                     NULL);
	g_object_unref (ksettings);

	online = e_backend_get_online (E_BACKEND (backend));
	(void) kolab_util_backend_deploy_mode_by_backend (priv->cal_koma,
	                                                  online,
	                                                  cancellable,
	                                                  &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}

	ok = cal_backend_kolab_notify_opened (self, &tmp_err);
	if (! ok) {
		kolab_util_calendar_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
	}
}
#endif

/**
 * cal_backend_kolab_start_view:
 * @backend: A calendar backend.
 * @view: The view to be started.
 *
 * Starts a new live query on the given backend.
 */
static void
cal_backend_kolab_start_view (ECalBackend *backend,
                              EDataCalView *view)
{
	ECalBackendKolab *self = NULL;
	ECalBackendKolabPrivate *priv = NULL;
	ECalBackendSExp *sexp;
	GList *uid_list = NULL;
	GList *it = NULL;
	GSList *iCal_objects = NULL;
	const gchar *query = NULL;
	const gchar *foldername = NULL;
	GError *tmp_err = NULL;
	GError *view_err = NULL;
	gboolean ok = FALSE;

	g_return_if_fail (E_IS_CAL_BACKEND_KOLAB (backend));
	g_return_if_fail (E_IS_DATA_CAL_VIEW (view));

	g_mutex_lock (&active_cal_views_lock);

	self = E_CAL_BACKEND_KOLAB (backend);
	priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	g_object_ref (view); /* unref()d in stop_view() */

	if (priv->cal_koma == NULL) {
		g_warning ("%s()[%u] Backend not been opened, not updating view",
		           __func__, __LINE__);
		goto exit;
	}

	sexp = e_data_cal_view_get_sexp (view);
	query = e_cal_backend_sexp_text (sexp);
	foldername = kolab_util_backend_get_foldername (E_BACKEND (backend));

	/* try to switch into online mode. if that fails,
	 * we assume there's no network or service available
	 * and continue in offline mode.
	 *
	 * FIXME we need some error reporting here. Going
	 * online may block for any kind of reasons, in which
	 * case the view will never get started...
	 */
	ok = kolab_util_backend_deploy_mode_by_backend (priv->cal_koma,
	                                                TRUE, /* go online */
	                                                NULL, /* FIXME: GCancellable */
	                                                &tmp_err);
	if (! ok) {
		g_warning ("%s()[%u]: %s", __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}

	/* Up to 3.4, we've been synchronizing folder metadata and
	 * PIM payload data with the Kolab server when going online.
	 * This can now be done in a separate synchronization function, which
	 * can be called upon per user request or at any place internally
	 * where it seems fit (e.g. when switching views - but beware,
	 * that operation can become quite bulky and time consuming).
	 * Until we get a synchronize() backend API function, we will
	 * put a sync point here to mimick the previous behaviour.
	 *
	 * We just pretend that going online went okay.
	 */
	ok = kolab_mail_access_synchronize (priv->cal_koma,
	                                    foldername,
	                                    TRUE, /* full sync */
	                                    NULL, /* FIXME: GCancellable */
	                                    &tmp_err);
	if (! ok) {
		g_warning ("%s()[%u]: %s", __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}

	ok = kolab_util_calendar_cache_update_on_query (E_CAL_BACKEND_SYNC (backend),
	                                                priv->cal_cache,
	                                                priv->cal_koma,
	                                                query,
	                                                NULL, /* FIXME: GCancellable */
	                                                &tmp_err);
	if (! ok)
		goto exit;

	uid_list = kolab_mail_access_query_uids (priv->cal_koma,
	                                         foldername,
	                                         query,
	                                         &tmp_err);
	if (tmp_err != NULL)
		goto exit;

	for (it = uid_list; it != NULL; it = g_list_next (it)) {
		gchar *uid = it->data;
		ECalComponent *ecalcomp = NULL;
		ecalcomp = kolab_util_calendar_cache_get_object (E_CAL_BACKEND_SYNC (backend),
		                                                 priv->cal_cache,
		                                                 priv->cal_koma,
		                                                 uid,
		                                                 TRUE,
		                                                 NULL, /* FIXME GCancellable */
		                                                 &tmp_err);
		if (tmp_err != NULL) {
			/* TODO Can we sensibly report a view error here?
			 *      Failure for one object does not necessarily
			 *      mean the next one will also fail. May need
			 *      to differentiate error types here
			 */
			g_warning ("%s()[%u]: %s",
			           __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
			tmp_err = NULL;
		}
		if (ecalcomp != NULL) {
			gboolean do_add = FALSE;
			do_add = e_data_cal_view_component_matches (view,
			                                            ecalcomp);
			if (do_add) {
				iCal_objects = g_slist_append (
					iCal_objects, g_object_ref (ecalcomp));
			}
			g_object_unref (ecalcomp);
		}
	}

	e_data_cal_view_notify_components_added (view, iCal_objects);

 exit:

	if (tmp_err != NULL) {
		kolab_util_calendar_err_to_edb_err (&view_err, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
	}

	e_data_cal_view_notify_complete (view, view_err);

	if (view_err != NULL)
		g_error_free (view_err);

	g_slist_free_full (iCal_objects, (GDestroyNotify) g_object_unref);

	if (uid_list != NULL)
		kolab_util_glib_glist_free (uid_list);

	g_mutex_unlock (&active_cal_views_lock);
}

static void
cal_backend_kolab_stop_view (ECalBackend *backend,
                             EDataCalView *view)
{
	/* ECalBackendKolab *self = NULL; */
	/* ECalBackendKolabPrivate *priv = NULL; */

	g_return_if_fail (E_IS_CAL_BACKEND_KOLAB (backend));
	g_return_if_fail (E_IS_DATA_CAL_VIEW (view));

	/* self = E_CAL_BACKEND_KOLAB (backend); */
	/* priv = E_CAL_BACKEND_KOLAB_PRIVATE (self); */

	g_mutex_lock (&active_cal_views_lock);

	e_data_cal_view_notify_complete (view, NULL);

	g_object_unref (view); /* ref()d in start_view() */

	g_mutex_unlock (&active_cal_views_lock);
}

/*----------------------------------------------------------------------------*/
/* object/class init */

static void
e_cal_backend_kolab_init (ECalBackendKolab *backend)
{
	ECalBackendKolab *self = E_CAL_BACKEND_KOLAB (backend);
	ECalBackendKolabPrivate *priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);
	GError *tmp_err = NULL;

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	priv->cal_koma = NULL;
	priv->cal_cache = NULL;
	priv->user_email = NULL;
	priv->default_zone = NULL;
	priv->source_type = 0;
	priv->mode_switch_err = NULL;

	/* init subsystems (these are no-ops if already called before) */
	kolab_util_glib_init ();

	/* libcamel
	 * Curl init may configure the underlying SSL lib,
	 * but as far as SSL goes, we want Camel to rule here
	 * TODO check whether Camel session needs to be initialized before or after libcurl.
	 */
	if (! kolab_util_camel_init (&tmp_err)) {
		g_error ("%s()[%u]: %s", __func__, __LINE__, tmp_err->message);
		return;
	}

	g_signal_connect (E_BACKEND (backend), "notify::online", G_CALLBACK (cal_backend_kolab_signal_online_cb), NULL);
} /* e_cal_backend_kolab_init () */

static void
e_cal_backend_kolab_dispose (GObject *object)
{
	ECalBackendKolab *self = E_CAL_BACKEND_KOLAB (object);
	ECalBackendKolabPrivate *priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	if (priv->cal_koma != NULL) {
		g_object_unref (priv->cal_koma);
		priv->cal_koma = NULL;
	}
	if (priv->default_zone != NULL) {
		g_object_unref (priv->default_zone);
		priv->default_zone = NULL;
	}
	if (priv->cal_cache != NULL) {
		g_object_unref (priv->cal_cache);
		priv->cal_cache = NULL;
	}

	G_OBJECT_CLASS (e_cal_backend_kolab_parent_class)->dispose (object);
} /* e_cal_backend_kolab_dispose () */

static void
e_cal_backend_kolab_finalize (GObject *object)
{
	ECalBackendKolab *self = E_CAL_BACKEND_KOLAB (object);
	ECalBackendKolabPrivate *priv = E_CAL_BACKEND_KOLAB_PRIVATE (self);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	if (priv->user_email != NULL)
		g_free (priv->user_email);

	if (priv->mode_switch_err != NULL) {
		g_warning ("%s()[%u]: %s",
		           __func__, __LINE__, priv->mode_switch_err->message);
		g_error_free (priv->mode_switch_err);
	}

	G_OBJECT_CLASS (e_cal_backend_kolab_parent_class)->finalize (object);
} /* e_cal_backend_kolab_finalize () */

/**
 * e_cal_backend_kolab_class_init:
 * @klass: An ECalBackendKolabClass type.
 *
 * Class initialisation function for the Kolab backend.
 */
static void
e_cal_backend_kolab_class_init (ECalBackendKolabClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	ECalBackendClass *backend_class = E_CAL_BACKEND_CLASS (klass);
	ECalBackendSyncClass* sync_class = E_CAL_BACKEND_SYNC_CLASS (klass);

	g_type_class_add_private (klass, sizeof (ECalBackendKolabPrivate));

	object_class->dispose = e_cal_backend_kolab_dispose;
	object_class->finalize = e_cal_backend_kolab_finalize;

	/* Backend parent class methods methods not covered in the sync backend part */
	backend_class->get_backend_property = cal_backend_kolab_get_backend_property;
	backend_class->start_view = cal_backend_kolab_start_view;
	backend_class->stop_view = cal_backend_kolab_stop_view;

	/* Sync backend class functions */
	sync_class->open_sync = cal_backend_kolab_open;
	sync_class->refresh_sync = cal_backend_kolab_refresh;
	sync_class->get_object_sync = cal_backend_kolab_get_object;
	sync_class->get_object_list_sync = cal_backend_kolab_get_object_list;
	sync_class->get_free_busy_sync = cal_backend_kolab_get_free_busy;
	sync_class->create_objects_sync = cal_backend_kolab_create_objects;
	sync_class->modify_objects_sync = cal_backend_kolab_modify_objects;
	sync_class->remove_objects_sync = cal_backend_kolab_remove_objects;
	sync_class->receive_objects_sync = cal_backend_kolab_receive_objects;
	sync_class->send_objects_sync = cal_backend_kolab_send_objects;
	sync_class->get_attachment_uris_sync = cal_backend_kolab_get_attachment_uris;
	sync_class->discard_alarm_sync = cal_backend_kolab_discard_alarm;
	sync_class->get_timezone_sync = cal_backend_kolab_get_timezone;
	sync_class->add_timezone_sync = cal_backend_kolab_add_timezone;

	/* Register relevant ESourceExtension types. */
	REGISTER_TYPE (E_TYPE_SOURCE_KOLAB_FOLDER);

} /* e_cal_backend_kolab_class_init () */


/*----------------------------------------------------------------------------*/
