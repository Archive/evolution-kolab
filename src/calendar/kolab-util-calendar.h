/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-calendar.c
 *
 *  2011
 *  Copyright  2011  Silvan Marco Fin
 *  <silvan@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_CALENDAR_H_
#define _KOLAB_UTIL_CALENDAR_H_

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libical/icalcomponent.h>
#include <libecal/libecal.h>
#include <libedata-cal/libedata-cal.h>

#include <libekolabutil/kolab-util-http.h>
#include <libekolab/kolab-settings-handler.h>
#include <libekolab/kolab-mail-access.h>

/*----------------------------------------------------------------------------*/

/**
 * SECTION:kolab-util-calendar
 * @title: KolabUtilCalendar
 * @short_description: Utility methods for Evolution-Kolab calendar backend implementation.
 *
 * The methods in this library don't use the ECalBackendCache.
 */

/*----------------------------------------------------------------------------*/

gchar*
kolab_util_calendar_dup_email_address (ECalBackend *backend);

gchar*
kolab_util_calendar_get_tzid (ECalComponent *comp,
                              ECalComponentField from);

gboolean
kolab_util_calendar_toggle_pfb_trigger (KolabSettingsHandler *ksettings,
                                        const gchar *sourcename,
                                        GError **error);

KolabUtilHttpJob*
kolab_util_calendar_retrieve_xfb (KolabSettingsHandler *ksettings,
                                  gchar *query,
                                  GError **error);

gboolean
kolab_util_calendar_store (ECalComponent *ecalcomp,
                           ECalComponent *ecaltz,
                           ECalComponent *default_tz,
                           ECalBackendSync *backend,
                           KolabMailAccess *koma,
                           GCancellable *cancellable,
                           GError **error);

gboolean
kolab_util_calendar_extract (icalcomponent *icalcomp,
                             ECalClientSourceType source_type,
                             ECalComponent **ecalcomp,
                             ECalComponent **tzcomp,
                             GError **error);

void
kolab_util_calendar_err_to_edb_err (GError **e_err,
                                    const GError *k_err,
                                    const gchar *func,
                                    guint line);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_CALENDAR_H_ */

/*----------------------------------------------------------------------------*/
