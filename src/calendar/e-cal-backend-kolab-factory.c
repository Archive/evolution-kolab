/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-cal-backend-kolab-factory.c
 *
 *  Thu Jun 10 18:25:26 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *  and Silvan Marco Fin <silvan@kernelconcepts.de> in 2011
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n-lib.h>

#include <libedata-cal/libedata-cal.h>
#include <libekolabutil/kolab-util-camel.h>

#include "e-cal-backend-kolab.h"

/*----------------------------------------------------------------------------*/

#define FACTORY_NAME KOLAB_CAMEL_PROVIDER_PROTOCOL

/*----------------------------------------------------------------------------*/

typedef ECalBackendFactory ECalBackendKolabEventsFactory;
typedef ECalBackendFactoryClass ECalBackendKolabEventsFactoryClass;

typedef ECalBackendFactory ECalBackendKolabJournalFactory;
typedef ECalBackendFactoryClass ECalBackendKolabJournalFactoryClass;

typedef ECalBackendFactory ECalBackendKolabTodosFactory;
typedef ECalBackendFactoryClass ECalBackendKolabTodosFactoryClass;

/* Module Entry Points */
void e_module_load (GTypeModule *type_module);
void e_module_unload (GTypeModule *type_module);

/* Forward Declarations */
GType e_cal_backend_kolab_events_factory_get_type (void);
GType e_cal_backend_kolab_journal_factory_get_type (void);
GType e_cal_backend_kolab_todos_factory_get_type (void);

G_DEFINE_DYNAMIC_TYPE (ECalBackendKolabEventsFactory,
                       e_cal_backend_kolab_events_factory,
                       E_TYPE_CAL_BACKEND_FACTORY)

G_DEFINE_DYNAMIC_TYPE (ECalBackendKolabJournalFactory,
                       e_cal_backend_kolab_journal_factory,
                       E_TYPE_CAL_BACKEND_FACTORY)

G_DEFINE_DYNAMIC_TYPE (ECalBackendKolabTodosFactory,
                       e_cal_backend_kolab_todos_factory,
                       E_TYPE_CAL_BACKEND_FACTORY)

static void
e_cal_backend_kolab_events_factory_class_init (ECalBackendFactoryClass *klass)
{
	klass->factory_name = FACTORY_NAME;
	klass->component_kind = ICAL_VEVENT_COMPONENT;
	klass->backend_type = E_TYPE_CAL_BACKEND_KOLAB;
}

static void
e_cal_backend_kolab_events_factory_class_finalize (ECalBackendFactoryClass *klass)
{
	(void)klass;
}

static void
e_cal_backend_kolab_events_factory_init (ECalBackendFactory *factory)
{
	(void)factory;
}

static void
e_cal_backend_kolab_journal_factory_class_init (ECalBackendFactoryClass *klass)
{
	klass->factory_name = FACTORY_NAME;
	klass->component_kind = ICAL_VJOURNAL_COMPONENT;
	klass->backend_type = E_TYPE_CAL_BACKEND_KOLAB;
}

static void
e_cal_backend_kolab_journal_factory_class_finalize (ECalBackendFactoryClass *klass)
{
	(void)klass;
}

static void
e_cal_backend_kolab_journal_factory_init (ECalBackendFactory *factory)
{
	(void)factory;
}

static void
e_cal_backend_kolab_todos_factory_class_init (ECalBackendFactoryClass *klass)
{
	klass->factory_name = FACTORY_NAME;
	klass->component_kind = ICAL_VTODO_COMPONENT;
	klass->backend_type = E_TYPE_CAL_BACKEND_KOLAB;
}

static void
e_cal_backend_kolab_todos_factory_class_finalize (ECalBackendFactoryClass *klass)
{
	(void)klass;
}

static void
e_cal_backend_kolab_todos_factory_init (ECalBackendFactory *factory)
{
	(void)factory;
}

G_MODULE_EXPORT void
e_module_load (GTypeModule *type_module)
{
	bindtextdomain (GETTEXT_PACKAGE, KOLAB_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	
	e_cal_backend_kolab_events_factory_register_type (type_module);
	e_cal_backend_kolab_journal_factory_register_type (type_module);
	e_cal_backend_kolab_todos_factory_register_type (type_module);
}

G_MODULE_EXPORT void
e_module_unload (GTypeModule *type_module)
{
	(void)type_module;
}

/*----------------------------------------------------------------------------*/
