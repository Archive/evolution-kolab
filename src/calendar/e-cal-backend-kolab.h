/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-cal-backend-kolab.h
 *
 *  Thu Jun 10 18:25:26 2010
 *  Copyright  2010  Christian Hilberg <hilberg@kernelconcepts.de>
 *  and Silvan Marco Fin <silvan@kernelconcepts.de> in 2011
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_CAL_BACKEND_KOLAB_H_
#define _E_CAL_BACKEND_KOLAB_H_

/*----------------------------------------------------------------------------*/

#include <glib-object.h>
#include <libedata-cal/libedata-cal.h>

G_BEGIN_DECLS

/**
 * SECTION:e-cal-backend-kolab
 * @short_description: Evolution calendar backend API implementation for Evolution-Kolab plugin.
 * @title: ECalBackendKolab
 */

#define E_TYPE_CAL_BACKEND_KOLAB             (e_cal_backend_kolab_get_type ())
#define E_CAL_BACKEND_KOLAB(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), E_TYPE_CAL_BACKEND_KOLAB, ECalBackendKolab))
#define E_CAL_BACKEND_KOLAB_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), E_TYPE_CAL_BACKEND_KOLAB, ECalBackendKolabClass))
#define E_IS_CAL_BACKEND_KOLAB(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), E_TYPE_CAL_BACKEND_KOLAB))
#define E_IS_CAL_BACKEND_KOLAB_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), E_TYPE_CAL_BACKEND_KOLAB))
#define E_CAL_BACKEND_KOLAB_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), E_TYPE_CAL_BACKEND_KOLAB, ECalBackendKolabClass))

typedef struct _ECalBackendKolabClass ECalBackendKolabClass;
typedef struct _ECalBackendKolab ECalBackendKolab;

struct _ECalBackendKolab
{
	ECalBackendSync parent;
};

struct _ECalBackendKolabClass
{
	ECalBackendSyncClass parent_class;
};

GType e_cal_backend_kolab_get_type (void) G_GNUC_CONST;

/* void e_cal_backend_kolab_set_koma_table (ECalBackendKolab *kolab, GHashTable *koma_objects); */

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _E_CAL_BACKEND_KOLAB_H_ */

/*----------------------------------------------------------------------------*/
