/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-book-backend-kolab.c
 *
 *  2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 * and Silvan Marco Fin <silvan@kernelconcepts.de> in 2011
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n-lib.h>

#include <libekolabutil/camel-system-headers.h>
#include <libekolabutil/kolab-util-camel.h>
#include <libekolabutil/kolab-util-http.h>
#include <libekolabutil/kolab-util-glib.h>

#include <libekolab/e-source-kolab-folder.h>
#include <libekolab/camel-kolab-imapx-settings.h>
#include <libekolab/kolab-types.h>
#include <libekolab/kolab-mail-access.h>
#include <libekolab/kolab-settings-handler.h>

#include "kolab-util-contact.h"
#include "kolab-util-contact-cache.h"

#include "e-book-backend-kolab.h"

/* This forces the GType to be registered in a way that
 * avoids a "statement with no effect" compiler warning.
 * FIXME Use g_type_ensure() once we require GLib 2.34. */
#define REGISTER_TYPE(type)	  \
	(g_type_class_unref (g_type_class_ref (type)))

/*----------------------------------------------------------------------------*/

static GMutex active_book_views_lock;

/*----------------------------------------------------------------------------*/

typedef struct _EBookBackendKolabPrivate EBookBackendKolabPrivate;
struct _EBookBackendKolabPrivate
{
	/* GNOME_Evolution_Addressbook_BookMode	book_mode; */
	KolabMailAccess *book_koma;
	gboolean auth_received;
	GError *mode_switch_err;
};

#define E_BOOK_BACKEND_KOLAB_PRIVATE(o)  (G_TYPE_INSTANCE_GET_PRIVATE ((o), E_TYPE_BOOK_BACKEND_KOLAB, EBookBackendKolabPrivate))


G_DEFINE_TYPE (EBookBackendKolab, e_book_backend_kolab, E_TYPE_BOOK_BACKEND)

/*----------------------------------------------------------------------------*/
/* internal statics */

static CamelKolabIMAPXSettings *
book_backend_kolab_get_collection_settings (EBookBackendKolab *backend)
{
	ESource *source;
	ESource *collection;
	ESourceCamel *extension;
	ESourceRegistry *registry;
	CamelSettings *settings;
	const gchar *extension_name;
	const gchar *protocol;

	source = e_backend_get_source (E_BACKEND (backend));
	registry = e_book_backend_get_registry (E_BOOK_BACKEND (backend));

	protocol = KOLAB_CAMEL_PROVIDER_PROTOCOL;
	extension_name = e_source_camel_get_extension_name (protocol);
	e_source_camel_generate_subtype (protocol, CAMEL_TYPE_KOLAB_IMAPX_SETTINGS);

	/* The collection settings live in our parent data source. */
	collection = e_source_registry_find_extension (registry, source, extension_name);
	g_return_val_if_fail (collection != NULL, NULL);

	extension = e_source_get_extension (collection, extension_name);
	settings = e_source_camel_get_settings (extension);

	g_object_unref (collection);

	return CAMEL_KOLAB_IMAPX_SETTINGS (settings);
}

static void
book_backend_kolab_signal_online_cb (GObject *object)
{
	EBackend *backend = NULL;
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	gboolean online = FALSE;

	g_return_if_fail (E_IS_BOOK_BACKEND_KOLAB (object));

	backend = E_BACKEND (object);
	self = E_BOOK_BACKEND_KOLAB (object);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	online = e_backend_get_online (backend);

	g_debug ("%s()[%u] backend mode: %i",
	         __func__, __LINE__, online);

	/* FIXME
	 *
	 * This callback is for online state notifications only.
	 * Doing I/O, which can fail, is the wrong thing to do
	 * here since we do not have a proper way to let the user
	 * cancel the I/O. Getting KolabMailAccess online can hang.
	 * Thus, cancellation would be needed.
	 *
	 * e_backend_get_online (backend) could be called in the
	 * "other" operations and the KolabMailAccess state changed
	 * if needed, Problem: KolabMailAccess online/offline state
	 * change involves synchronization with the server, which
	 * may take long time and is not expected by the user if
	 * they e.g. just add a contact or event.
	 *
	 * For now, we'll leave the cancellation issue aside
	 * (we could not have cancelled in 2.30 either), but
	 * at least bind a GError to our backend object, if we
	 * have one. We can then propagate this error in the
	 * first "other" operation the user triggers.
	 *
	 * It has been supposed by upstream to use a global static
	 * GCancellable for the cancellation issue, but it will
	 * not help us here since the Evolution online/offline
	 * toggle button does not seem to expect heavy I/O to
	 * be triggered. Our local GCancellabe would need transfer
	 * to Evo and be registered there so the online/offline
	 * switching in evo-kolab could be cancelled from the
	 * frontend. Moreover, there is no telling how long a
	 * synchronization run with the Kolab server may take,
	 * so we also cannot set a sensible timeout here.
	 *
	 */

	if (priv->book_koma == NULL) {
		g_warning ("%s()[%u] Backend not been opened, ignoring online state request",
		           __func__, __LINE__);
		return;
	}

	(void) kolab_util_backend_deploy_mode_by_backend (priv->book_koma,
	                                                  online,
	                                                  NULL, /* GCancellable (see above) */
	                                                  &(priv->mode_switch_err));
	if (priv->mode_switch_err) {
		g_warning ("%s()[%u]: Online mode switching error pending, (%s), Domain %s Code %i",
		           __func__, __LINE__,
		           priv->mode_switch_err->message,
		           g_quark_to_string (priv->mode_switch_err->domain),
		           priv->mode_switch_err->code);
		g_error_free (priv->mode_switch_err);
		priv->mode_switch_err = NULL;
	}
}

static gboolean
book_backend_kolab_notify_opened (EBookBackendKolab *self,
                                  GError **err)
{
	EBookBackend *bbackend = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	KolabMailAccessOpmodeID tmp_mode = KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	GError *tmp_err = NULL;

	g_assert (E_IS_BOOK_BACKEND_KOLAB (self));
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);
	bbackend = E_BOOK_BACKEND (self);

	tmp_mode = kolab_mail_access_get_opmode (priv->book_koma, &tmp_err);
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	e_backend_set_online (E_BACKEND (self),
	                      tmp_mode == KOLAB_MAIL_ACCESS_OPMODE_ONLINE);
	e_book_backend_set_writable (bbackend, TRUE);

	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* class functions */

static gboolean
book_backend_kolab_open_sync (EBookBackend *backend,
                              GCancellable *cancellable,
                              GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	CamelKolabIMAPXSettings *kolab_settings = NULL;
	CamelNetworkSettings *network_settings = NULL;
	KolabSettingsHandler *ksettings = NULL;
	gchar *servername = NULL;
	gchar *username = NULL;
	const gchar *user_home = NULL;
	gboolean online = FALSE;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	kolab_settings = book_backend_kolab_get_collection_settings (self);
	network_settings = CAMEL_NETWORK_SETTINGS (kolab_settings);

	servername = camel_network_settings_dup_host (network_settings);
	username = camel_network_settings_dup_user (network_settings);
	g_debug ("%s()[%u] servername = %s", __func__, __LINE__, servername);
	g_debug ("%s()[%u]   username = %s", __func__, __LINE__, username);

	online = e_backend_get_online (E_BACKEND (backend));

	/* init subsystems (these are no-ops if already called before) */
	kolab_util_glib_init ();

	/* libcamel
	 * Curl init may configure the underlying SSL lib,
	 * but as far as SSL goes, we want Camel to rule here
	 * TODO check whether Camel session needs to be initialized before or after libcurl.
	 */
	ok = kolab_util_camel_init (&tmp_err);
	if (! ok)
		goto exit;

	/* Configure settings handler */
	ksettings = kolab_settings_handler_new (kolab_settings,
	                                        E_BACKEND (backend));
	ok = kolab_settings_handler_configure (ksettings,
	                                       KOLAB_FOLDER_CONTEXT_CONTACT,
	                                       &tmp_err);
	if (! ok)
		goto exit;

	ok = kolab_settings_handler_bringup (ksettings, &tmp_err);
	if (! ok)
		goto exit;

	/* init the HTTP utils */
	user_home = kolab_settings_handler_get_char_field (ksettings,
	                                                   KOLAB_SETTINGS_HANDLER_CHAR_FIELD_USER_HOME_DIR,
	                                                   &tmp_err);
	if (tmp_err != NULL)
		goto exit;
	kolab_util_http_init (user_home);

	/* create new KolabMailAccess instance */
	priv->book_koma = KOLAB_MAIL_ACCESS (g_object_new (KOLAB_TYPE_MAIL_ACCESS, NULL));

	/* configure and bring up KolabMailAccess instance */
	ok = kolab_mail_access_configure (priv->book_koma,
	                                  ksettings,
	                                  &tmp_err);
	if (! ok)
		goto exit;

	ok = kolab_mail_access_bringup (priv->book_koma,
	                                cancellable,
	                                &tmp_err);
	if (! ok)
		goto exit;

	(void) kolab_util_backend_deploy_mode_by_backend (priv->book_koma,
	                                                  online,
	                                                  cancellable,
	                                                  &tmp_err);
 exit:

	if (ksettings != NULL)
		g_object_unref (ksettings);
	if (servername != NULL)
		g_free (servername);
	if (username != NULL)
		g_free (username);

	/* do we have an error set? if so, propagate and return */
	if (tmp_err != NULL) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	/* all good, notify that we're open for business */
	ok = book_backend_kolab_notify_opened (self, &tmp_err);
	if (! ok) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	return TRUE;
}

static gboolean
book_backend_kolab_refresh_sync (EBookBackend *backend,
                                 GCancellable *cancellable,
                                 GError **error)
{
#if 0
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);
#endif
	g_warning ("%s: FIXME implement me", __func__);

	return TRUE;
}

static gboolean
book_backend_kolab_create_contacts_sync (EBookBackend *backend,
                                         const gchar * const *vcards,
                                         GQueue *out_contacts,
                                         GCancellable *cancellable,
                                         GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	EContact *econtact = NULL;
	gboolean ok = FALSE;
	guint ii, length;
	GError *tmp_err = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	length = g_strv_length ((gchar **) vcards);

	for (ii = 0; ii < length; ii++) {
		econtact = e_contact_new_from_vcard (vcards[ii]);
		if (econtact == NULL) {
			g_warning ("%s()[%u] error creating contact from vcard:\n%s",
			           __func__, __LINE__, vcards[ii]);
			tmp_err = e_data_book_create_error (E_DATA_BOOK_STATUS_OTHER_ERROR, NULL);
			goto exit;
		}

		ok = kolab_util_contact_cache_assure_uid_on_econtact (backend,
		                                                      priv->book_koma,
		                                                      econtact,
		                                                      FALSE,
		                                                      cancellable,
		                                                      &tmp_err);
		if (! ok)
			goto exit;

		ok = kolab_util_contact_store (econtact,
		                               backend,
		                               priv->book_koma,
		                               cancellable,
		                               &tmp_err);
		if (! ok)
			goto exit;

		g_queue_push_tail (out_contacts, econtact);
	}

 exit:

	if (tmp_err != NULL) {
		if (econtact != NULL)
			g_object_unref (econtact);
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	return TRUE;
}

static gboolean
book_backend_kolab_remove_contacts_sync (EBookBackend *backend,
                                         const gchar * const *uids,
                                         GCancellable *cancellable,
                                         GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	const gchar *foldername;
	gboolean ok = FALSE;
	guint ii, length;
	GError *tmp_err = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	foldername = kolab_util_backend_get_foldername (E_BACKEND (backend));

	length = g_strv_length ((gchar **) uids);

	for (ii = 0; ii < length; ii++) {
		ok = kolab_mail_access_delete_by_uid (priv->book_koma,
		                                      uids[ii],
		                                      foldername,
		                                      cancellable,
		                                      &tmp_err);
		if (! ok) {
			if (tmp_err->code != KOLAB_BACKEND_ERROR_NOTFOUND)
				goto exit;

			g_warning ("%s()[%u]: %s", __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
			tmp_err = NULL;
		}
	}

 exit:
	if (tmp_err != NULL) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	return TRUE;
}

static gboolean
book_backend_kolab_modify_contacts_sync (EBookBackend *backend,
                                         const gchar * const *vcards,
                                         GQueue *out_contacts,
                                         GCancellable *cancellable,
                                         GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	EContact *econtact = NULL;
	gboolean ok = FALSE;
	guint ii, length;
	GError *tmp_err = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	length = g_strv_length ((gchar **) vcards);

	for (ii = 0; ii < length; ii++) {
		econtact = e_contact_new_from_vcard (vcards[ii]);
		if (econtact == NULL) {
			g_warning ("%s()[%u] error creating contact from vcard:\n%s",
			           __func__, __LINE__, vcards[ii]);
			tmp_err = e_data_book_create_error (E_DATA_BOOK_STATUS_OTHER_ERROR, NULL);
			goto exit;
		}

		ok = kolab_util_contact_store (econtact,
		                               backend,
		                               priv->book_koma,
		                               cancellable,
		                               &tmp_err);
		if (! ok)
			goto exit;

		g_queue_push_tail (out_contacts, econtact);
	}

 exit:
	if (tmp_err != NULL) {
		if (econtact != NULL)
			g_object_unref (econtact);
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	return TRUE;
}

static EContact *
book_backend_kolab_get_contact_sync (EBookBackend *backend,
                                     const gchar *uid,
                                     GCancellable *cancellable,
                                     GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	EContact *econtact = NULL;
	GError *tmp_err = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	econtact = kolab_util_contact_cache_get_object (backend,
	                                                priv->book_koma,
	                                                uid,
	                                                FALSE,
	                                                cancellable,
	                                                &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return NULL;
	}

	return econtact;
}

static gboolean
book_backend_kolab_get_contact_list_sync (EBookBackend *backend,
                                          const gchar *query,
                                          GQueue *out_contacts,
                                          GCancellable *cancellable,
                                          GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	GList *econtact_list = NULL;
	GList *it = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	g_debug ("%s()[%u] called.", __func__, __LINE__);
	g_debug (" + query: %s", query);

	/* Update the cache to contain all data requested. */
	ok = kolab_util_contact_cache_update_on_query (backend,
	                                               priv->book_koma,
	                                               query,
	                                               cancellable,
	                                               &tmp_err);
	if (! ok) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	/* Fetch information from BackendCache */;
	econtact_list = kolab_util_contact_cache_get_contacts (backend,
	                                                       priv->book_koma,
	                                                       query,
	                                                       cancellable,
	                                                       &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	for (it = econtact_list; it != NULL; it = g_list_next (it))
		g_queue_push_tail (out_contacts, it->data);

	g_list_free (econtact_list);

	return TRUE;
}

static gboolean
book_backend_kolab_get_contact_list_uids_sync (EBookBackend *backend,
                                               const gchar *query,
                                               GQueue *out_uids,
                                               GCancellable *cancellable,
                                               GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	GList *econtact_list = NULL;
	GList *it = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	/* TODO This function currently works the same way as
	 *      e_book_backend_kolab_get_contact_list_sync, since
	 *      we need to update the local cache information
	 *      when querying UIDs from KolabMailAccess.
	 *
	 *      Once this is issue is resolved, we can simply
	 *      query the KolabMailAccess instance for UIDs,
	 *      which is a far less i/o intensive operation
	 *      than actually querying the EContact objects
	 *      (the latter involves reading Kolab mail data
	 *      from the disk cache and conversion to EContact)
	 */

	/* Update the cache to contain all data requested. */
	ok = kolab_util_contact_cache_update_on_query (backend,
	                                               priv->book_koma,
	                                               query,
	                                               cancellable,
	                                               &tmp_err);
	if (! ok) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	/* Fetch information from BackendCache */;
	econtact_list = kolab_util_contact_cache_get_contacts (backend,
	                                                       priv->book_koma,
	                                                       query,
	                                                       cancellable,
	                                                       &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return FALSE;
	}

	for (it = econtact_list; it != NULL; it = g_list_next (it)) {
		EContact *econtact = it->data;
		gchar *uid = e_contact_get (econtact,
		                            E_CONTACT_UID);
		g_queue_push_tail (out_uids, uid);
		g_object_unref (econtact);
	}

	g_list_free (econtact_list);

	return TRUE;
}

static gchar *
book_backend_kolab_get_backend_property (EBookBackend *backend,
                                         const gchar *prop_name)
{
	g_return_val_if_fail (prop_name != NULL, NULL);

	if (g_str_equal (prop_name, CLIENT_BACKEND_PROPERTY_CAPABILITIES)) {
		return g_strdup ("net,bulk-removes,contact-lists,do-initial-query");
	} else if (g_str_equal (prop_name, BOOK_BACKEND_PROPERTY_REQUIRED_FIELDS)) {
		return g_strdup (e_contact_field_name (E_CONTACT_FILE_AS));
	} else if (g_str_equal (prop_name, BOOK_BACKEND_PROPERTY_SUPPORTED_FIELDS)) {
		gchar *prop_value;

		GSList *fields = kolab_utils_contact_get_supported_fields ();
		prop_value = e_data_book_string_slist_to_comma_string (fields);
		g_slist_free (fields);

		return prop_value;
	}

	/* Chain up to parent's get_backend_property() method. */
	return E_BOOK_BACKEND_CLASS (e_book_backend_kolab_parent_class)->
		get_backend_property (backend, prop_name);
}

#if 0  /* FIXME Delete this once kolab_mail_access_try_password_sync()
        *       works.  There may be code here that can be reused. */
static void
book_backend_kolab_authenticate_user (EBookBackend *backend,
                                      GCancellable *cancellable,
                                      ECredentials *credentials,
                                      GError **error)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	KolabSettingsHandler *ksettings = NULL;
	const gchar *cred_user = NULL;
	const gchar *cred_pwd  = NULL;
	const gchar *kset_user = NULL;
	gboolean online = FALSE;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_if_fail (error == NULL || *error == NULL);
	/* cancellable may be NULL */

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	ksettings = kolab_mail_access_get_settings_handler (priv->book_koma);

	/* warn about a possible inconsistency in user names */
	kset_user = kolab_settings_handler_get_char_field (ksettings,
	                                                   KOLAB_SETTINGS_HANDLER_CHAR_FIELD_KOLAB_USER_NAME,
	                                                   &tmp_err);
	if (tmp_err != NULL) {
		g_object_unref (ksettings);
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}
	cred_user = e_credentials_peek (credentials,
	                                E_CREDENTIALS_KEY_USERNAME);
	if (g_strcmp0 (kset_user, cred_user) != 0) {
		g_warning ("%s()[%u] username from argument and username in "
		           "KolabSettingsHandler do not match: %s vs. %s",
		           __func__, __LINE__, cred_user, kset_user);
	}

	cred_pwd = e_credentials_peek (credentials,
	                               E_CREDENTIALS_KEY_PASSWORD);

	kolab_util_backend_prepare_settings (ksettings,
	                                     NULL,
	                                     NULL,
	                                     NULL,
	                                     cred_pwd,
	                                     NULL,
	                                     NULL);
	g_object_unref (ksettings);

	priv->auth_received = TRUE;

	online = e_backend_get_online (E_BACKEND (backend));
	(void) kolab_util_backend_deploy_mode_by_backend (priv->book_koma,
	                                                  online,
	                                                  cancellable,
	                                                  &tmp_err);
	if (tmp_err != NULL) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
		return;
	}

	ok = book_backend_kolab_notify_opened (self, &tmp_err);
	if (! ok) {
		kolab_util_contact_err_to_edb_err (error, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
	}

#if 0 /* FIXME old */
	g_debug (" + user: %s\n + auth_method: %s",
	         user, auth_method);
	g_debug ("%s()[%u]    username = %s",
	         __func__, __LINE__, user);
	g_debug ("%s()[%u] auth_method = %s",
	         __func__, __LINE__, auth_method);
	password_placeholder = g_strdup (passwd);
	password_placeholder = g_strcanon (password_placeholder, "", '*');
	g_debug ("%s()[%u]    password = %s",
	         __func__, __LINE__, password_placeholder);
	g_free (password_placeholder);

	ksettings = kolab_mail_access_get_settings_handler (priv->book_koma);
	tmp_user = kolab_settings_handler_get_char_field (ksettings,
	                                                  KOLAB_SETTINGS_HANDLER_CHAR_FIELD_KOLAB_USER_NAME,
	                                                  &error);
	if (error != NULL) {
		g_warning ("%s()[%u]: %s",
		           __func__, __LINE__, error->message);
		g_error_free (error);
		error = NULL;
	}
	if (g_strcmp0 (tmp_user, user) != 0)
		g_warning (" + username from argument and username in "
		           "KolabSettingsHandler do not match: %s vs. %s",
		           tmp_user, user);

	kolab_util_backend_prepare_settings (ksettings,
	                                     NULL,
	                                     NULL,
	                                     NULL,
	                                     passwd,
	                                     NULL,
	                                     NULL);

	priv->auth_received = TRUE;
	ok = kolab_util_backend_deploy_mode_by_backend (priv->book_koma,
	                                                priv->book_mode);

	return GNOME_Evolution_Addressbook_Success;
#endif /* FIXME */
}
#endif

static void
book_backend_kolab_start_view (EBookBackend *backend,
                               EDataBookView *book_view)
{
	EBookBackendKolab *self = NULL;
	EBookBackendKolabPrivate *priv = NULL;
	EBookBackendSExp *sexp;
	GList *econtact_list = NULL;
	GList *it = NULL;
	const gchar *query = NULL;
	const gchar *foldername = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;
	GError *notify_err = NULL;

	g_return_if_fail (E_IS_BOOK_BACKEND_KOLAB (backend));
	g_return_if_fail (E_IS_DATA_BOOK_VIEW (book_view));

	g_mutex_lock (&active_book_views_lock);

	self = E_BOOK_BACKEND_KOLAB (backend);
	priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	g_object_ref (book_view); /* unref()d in stop_view() */

	if (priv->book_koma == NULL) {
		g_warning ("%s()[%u] Backend not been opened, not updating view",
		           __func__, __LINE__);
		goto exit;
	}

	sexp = e_data_book_view_get_sexp (book_view);
	query = e_book_backend_sexp_text (sexp);
	foldername = kolab_util_backend_get_foldername (E_BACKEND (backend));

	/* try to switch into online mode. if that fails,
	 * we assume there's no network or service available
	 * and continue in offline mode.
	 *
	 * FIXME we need some error reporting here. Going
	 * online may block for any kind of reasons, in which
	 * case the view will never get started...
	 */
	ok = kolab_util_backend_deploy_mode_by_backend (priv->book_koma,
	                                                TRUE, /* go online */
	                                                NULL, /* FIXME: GCancellable */
	                                                &tmp_err);
	if (! ok) {
		g_warning ("%s()[%u]: %s", __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}

	/* Up to 3.4, we've been synchronizing folder metadata and
	 * PIM payload data with the Kolab server when going online.
	 * This can now be done in a separate synchronization function, which
	 * can be called upon per user request or at any place internally
	 * where it seems fit (e.g. when switching views - but beware,
	 * that operation can become quite bulky and time consuming).
	 * Until we get a synchronize() backend API function, we will
	 * put a sync point here to mimick the previous behaviour.
	 *
	 * We just pretend that going online went okay.
	 */
	ok = kolab_mail_access_synchronize (priv->book_koma,
	                                    foldername,
	                                    TRUE, /* full sync */
	                                    NULL, /* FIXME: GCancellable */
	                                    &tmp_err);
	if (! ok) {
		g_warning ("%s()[%u]: %s", __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}

	/* First update the BackendCache to the current situation. */
	ok = kolab_util_contact_cache_update_on_query (backend,
	                                               priv->book_koma,
	                                               query,
	                                               NULL, /* FIXME GCancellable */
	                                               &tmp_err);
	if (! ok)
		goto exit;

	econtact_list = kolab_util_contact_cache_get_contacts (backend,
	                                                       priv->book_koma,
	                                                       query,
	                                                       NULL, /* FIXME GCancellable */
	                                                       &tmp_err);
	if (tmp_err != NULL)
		goto exit;

	/* Then deliver requests from there. */
	for (it = econtact_list; it != NULL; it = g_list_next (it)) {
		EContact *econtact = E_CONTACT (it->data);
		e_data_book_view_notify_update (book_view, econtact);
		g_object_unref (econtact);
	}

 exit:

	if (tmp_err != NULL) {
		kolab_util_contact_err_to_edb_err (&notify_err, tmp_err, __func__, __LINE__);
		g_error_free (tmp_err);
	}

	e_data_book_view_notify_complete (book_view, notify_err);

	if (notify_err != NULL)
		g_error_free (notify_err);

	if (econtact_list != NULL)
		g_list_free (econtact_list);

	g_mutex_unlock (&active_book_views_lock);
}

static void
book_backend_kolab_stop_view (EBookBackend *backend,
                              EDataBookView *book_view)
{
	g_return_if_fail (E_IS_BOOK_BACKEND_KOLAB (backend));
	g_return_if_fail (E_IS_DATA_BOOK_VIEW (book_view));

	g_mutex_lock (&active_book_views_lock);

	e_data_book_view_notify_complete (book_view, NULL);

	g_object_unref (book_view); /* ref()d in start_view() */

	g_mutex_unlock (&active_book_views_lock);
}

/*----------------------------------------------------------------------------*/
/* object/class init */

static void
e_book_backend_kolab_init (EBookBackendKolab *backend)
{
	EBookBackendKolabPrivate *priv = E_BOOK_BACKEND_KOLAB_PRIVATE (backend);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	priv->book_koma = NULL;
	priv->auth_received = FALSE;
	priv->mode_switch_err = NULL;

	g_signal_connect (E_BACKEND (backend), "notify::online", G_CALLBACK (book_backend_kolab_signal_online_cb), NULL);
} /* e_book_backend_kolab_init () */

static void
e_book_backend_kolab_dispose (GObject *object)
{
	EBookBackend *self = E_BOOK_BACKEND (object);
	EBookBackendKolabPrivate *priv = E_BOOK_BACKEND_KOLAB_PRIVATE (self);

	g_debug ("%s()[%u] called.", __func__, __LINE__);
	if (priv->book_koma != NULL) {
		g_object_unref (priv->book_koma);
		priv->book_koma = NULL;
	}

	G_OBJECT_CLASS (e_book_backend_kolab_parent_class)->dispose (object);
}

static void
e_book_backend_kolab_finalize (GObject *object)
{
	EBookBackendKolabPrivate *priv = E_BOOK_BACKEND_KOLAB_PRIVATE (object);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	if (priv->mode_switch_err != NULL) {
		g_warning ("%s()[%u]: %s",
		           __func__, __LINE__, priv->mode_switch_err->message);
		g_error_free (priv->mode_switch_err);
	}

	G_OBJECT_CLASS (e_book_backend_kolab_parent_class)->finalize (object);
} /* e_book_backend_kolab_finalize () */

static void
e_book_backend_kolab_class_init (EBookBackendKolabClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	EBookBackendClass *backend_class = E_BOOK_BACKEND_CLASS (klass);

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	g_type_class_add_private (klass, sizeof (EBookBackendKolabPrivate));

	object_class->dispose = e_book_backend_kolab_dispose;
	object_class->finalize = e_book_backend_kolab_finalize;

	/* Backend parent class methods */
	backend_class->get_backend_property = book_backend_kolab_get_backend_property;
	backend_class->open_sync = book_backend_kolab_open_sync;
	backend_class->refresh_sync = book_backend_kolab_refresh_sync;
	backend_class->create_contacts_sync = book_backend_kolab_create_contacts_sync;
	backend_class->remove_contacts_sync = book_backend_kolab_remove_contacts_sync;
	backend_class->modify_contacts_sync = book_backend_kolab_modify_contacts_sync;
	backend_class->get_contact_sync = book_backend_kolab_get_contact_sync;
	backend_class->get_contact_list_sync = book_backend_kolab_get_contact_list_sync;
	backend_class->get_contact_list_uids_sync = book_backend_kolab_get_contact_list_uids_sync;
	backend_class->start_view = book_backend_kolab_start_view;
	backend_class->stop_view = book_backend_kolab_stop_view;

	/* Register relevant ESourceExtension types. */
	REGISTER_TYPE (E_TYPE_SOURCE_KOLAB_FOLDER);

} /* e_book_backend_kolab_class_init () */


/*----------------------------------------------------------------------------*/
/* public API */


/*----------------------------------------------------------------------------*/
