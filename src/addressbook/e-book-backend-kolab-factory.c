/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-book-backend-factory-kolab.c
 *
 *  Thu Jun 10 17:21:15 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 * and Silvan Marco Fin <silvan@kernelconcepts.de> in 2011
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <glib-object.h>
#include <glib/gi18n-lib.h>

#include <libedata-book/libedata-book.h>
#include <libekolabutil/kolab-util-camel.h>

#include "e-book-backend-kolab.h"

/*----------------------------------------------------------------------------*/

#define FACTORY_NAME KOLAB_CAMEL_PROVIDER_PROTOCOL

/*----------------------------------------------------------------------------*/

/* factory types */
typedef EBookBackendFactory EBookBackendKolabFactory;
typedef EBookBackendFactoryClass EBookBackendKolabFactoryClass;

/* module entry points */
void e_module_load (GTypeModule *type_module);
void e_module_unload (GTypeModule *type_module);

/* forward declarations */
GType e_book_backend_kolab_factory_get_type (void);

G_DEFINE_DYNAMIC_TYPE (EBookBackendKolabFactory,
                       e_book_backend_kolab_factory,
                       E_TYPE_BOOK_BACKEND_FACTORY)

static void
e_book_backend_kolab_factory_class_init (EBookBackendFactoryClass *klass)
{
	klass->factory_name = FACTORY_NAME;
	klass->backend_type = E_TYPE_BOOK_BACKEND_KOLAB;
}

static void
e_book_backend_kolab_factory_class_finalize (EBookBackendFactoryClass *klass)
{
	(void)klass;
}

static void
e_book_backend_kolab_factory_init (EBookBackendFactory *factory)
{
	(void)factory;
}

G_MODULE_EXPORT void
e_module_load (GTypeModule *type_module)
{
	bindtextdomain (GETTEXT_PACKAGE, KOLAB_LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	
	e_book_backend_kolab_factory_register_type (type_module);
}

G_MODULE_EXPORT void
e_module_unload (GTypeModule *type_module)
{
	(void)type_module;
}

/*----------------------------------------------------------------------------*/
