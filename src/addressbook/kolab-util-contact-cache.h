/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-contact-cache.h
 *
 *  2011
 *  Copyright  2011 Silvan Marco Fin
 *  <silvan@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_CONTACT_CACHE_H_
#define _KOLAB_UTIL_CONTACT_CACHE_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gio/gio.h>

#include <libekolab/kolab-mail-access.h>

#include <libedata-book/libedata-book.h>

/*----------------------------------------------------------------------------*/

gboolean
kolab_util_contact_cache_assure_uid_on_econtact (EBookBackend *backend,
                                                 KolabMailAccess *koma,
                                                 EContact *econtact,
                                                 gboolean bulk,
                                                 GCancellable *cancellable,
                                                 GError **error);

EContact*
kolab_util_contact_cache_get_object (EBookBackend *backend,
                                     KolabMailAccess *koma,
                                     const gchar *uid,
                                     gboolean bulk,
                                     GCancellable *cancellable,
                                     GError **error);
gboolean
kolab_util_contact_cache_update_object (EBookBackend *backend,
                                        KolabMailAccess *koma,
                                        const gchar *uid,
                                        gboolean bulk,
                                        GCancellable *cancellable,
                                        GError **error);

gboolean
kolab_util_contact_cache_update_on_query (EBookBackend *backend,
                                          KolabMailAccess *koma,
                                          const gchar *query,
                                          GCancellable *cancellable,
                                          GError **error);

GList*
kolab_util_contact_cache_get_contacts (EBookBackend *backend,
                                       KolabMailAccess *koma,
                                       const gchar *query,
                                       GCancellable *cancellable,
                                       GError **error);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_CONTACT_CACHE_H_ */

/*----------------------------------------------------------------------------*/
