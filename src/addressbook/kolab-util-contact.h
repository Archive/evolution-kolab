/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-contact.h
 *
 *  2011
 *  Copyright  2011  Silvan Marco Fin
 *  <silvan@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_CONTACT_H_
#define _KOLAB_UTIL_CONTACT_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gio/gio.h>
#include <glib-object.h>

#include <libebook/libebook.h>
#include <libedata-book/libedata-book.h>

#include <libekolab/kolab-mail-access.h>

#include "e-book-backend-kolab.h"

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

gboolean
kolab_util_contact_has_id (EContact *contact);

void
kolab_util_contact_gen_uid_if_none (EContact **contact);

gboolean
kolab_util_contact_store (EContact *econtact,
                          EBookBackend *backend,
                          KolabMailAccess *koma,
                          GCancellable *cancellable,
                          GError **error);

void
kolab_util_contact_err_to_edb_err (GError **e_err,
                                   const GError *k_err,
                                   const gchar *func,
                                   guint line);

GSList*
kolab_utils_contact_get_supported_fields (void);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_CONTACT_H_ */

/*----------------------------------------------------------------------------*/
