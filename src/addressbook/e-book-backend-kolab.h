/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-book-backend-kolab.h
 *
 *  2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 * and Silvan Marco Fin <silvan@kernelconcepts.de> in 2011
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_BOOK_BACKEND_KOLAB_H_
#define _E_BOOK_BACKEND_KOLAB_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libedata-book/libedata-book.h>

/*----------------------------------------------------------------------------*/

/* Standard GObject macros */
#define E_TYPE_BOOK_BACKEND_KOLAB	  \
	(e_book_backend_kolab_get_type ())
#define E_BOOK_BACKEND_KOLAB(obj)	  \
	(G_TYPE_CHECK_INSTANCE_CAST \
	 ((obj), E_TYPE_BOOK_BACKEND_KOLAB, EBookBackendKolab))
#define E_BOOK_BACKEND_KOLAB_CLASS(cls)	  \
	(G_TYPE_CHECK_CLASS_CAST \
	 ((cls), E_TYPE_BOOK_BACKEND_KOLAB, EBookBackendKolabClass))
#define E_IS_BOOK_BACKEND_KOLAB(obj)	  \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	 ((obj), E_TYPE_BOOK_BACKEND_KOLAB))
#define E_IS_BOOK_BACKEND_KOLAB_CLASS(cls)	  \
	(G_TYPE_CHECK_CLASS_TYPE \
	 ((cls), E_TYPE_BOOK_BACKEND_KOLAB))
#define E_BOOK_BACKEND_KOLAB_GET_CLASS(obj)	  \
	(G_TYPE_INSTANCE_GET_CLASS \
	 ((obj), E_TYPE_BOOK_BACKEND_KOLAB, EBookBackendKolabClass))

G_BEGIN_DECLS

typedef struct _EBookBackendKolab EBookBackendKolab;
struct _EBookBackendKolab {
	EBookBackend parent_object;
};

typedef struct _EBookBackendKolabClass EBookBackendKolabClass;
struct _EBookBackendKolabClass {
	EBookBackendClass parent_class;
};

GType
e_book_backend_kolab_get_type (void) G_GNUC_CONST;

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _E_BOOK_BACKEND_KOLAB_H_ */

/*----------------------------------------------------------------------------*/
