/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-folder-metadata.c
 *
 *  Fri Feb 10 11:25:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <config.h>

#include <glib/gi18n-lib.h>
#include <glib-object.h>

#include <libekolab/camel-kolab-imapx-store.h>
#include <libekolab/e-source-kolab-folder.h>
#include <libekolab/kolab-util-backend.h>

#include <libekolabutil/kolab-util-error.h>
#include <libekolabutil/kolab-util-folder.h>

#include "e-kolab-plugin-util.h"
#include "e-kolab-folder-metadata.h"

/*----------------------------------------------------------------------------*/
/* internal statics (non-UI) */


/*----------------------------------------------------------------------------*/
/* internal statics (UI) */

static void
kolab_folder_metadata_ui_create_folder_type_map (KolabFolderMetaUIData *uidata)
{
	GHashTable *map = NULL;

	g_assert (uidata != NULL);
	g_assert (uidata->widgets != NULL);

	map = g_hash_table_new_full (g_direct_hash,
	                             g_direct_equal,
	                             NULL,  /* radio button addresses as keys */
	                             NULL); /* numeric Kolab folder types as values */;

	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_type[0],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_TYPE_EMAIL));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_type[1],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_TYPE_EVENT));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_type[2],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_TYPE_NOTE));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_type[3],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_TYPE_TASK));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_type[4],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_TYPE_CONTACT));

	if (uidata->widgets->folder_type_map != NULL)
		g_hash_table_destroy (uidata->widgets->folder_type_map);
	uidata->widgets->folder_type_map = map;
}

static void
kolab_folder_metadata_ui_syncstrategy_fillbox (KolabFolderMetaUIData *uidata)
{
	GtkComboBoxText *box = NULL;
	const gchar *strategy = NULL;
	KolabSyncStrategyID ii = 0;

	g_assert (uidata != NULL);
	g_assert (uidata->widgets != NULL);

	box = GTK_COMBO_BOX_TEXT (uidata->widgets->cbox_syncstrategy);

	for (ii = 0; ii < KOLAB_SYNC_LAST_STRATEGY; ii++) {
		strategy = kolab_util_backend_get_sync_strategy_desc (ii);
		gtk_combo_box_text_insert_text (box,
		                                (gint) ii,
		                                strategy);
	}

	gtk_combo_box_set_active (GTK_COMBO_BOX (box),
	                          KOLAB_SYNC_STRATEGY_DEFAULT);
}

static KolabSyncStrategyID
kolab_folder_metadata_ui_syncstrategy_get (KolabFolderMetaUIData *uidata)
{
	KolabSyncStrategyID strategy = KOLAB_SYNC_STRATEGY_DEFAULT;
	ESource *source = NULL;
	ESourceResource *resource = NULL;
	gboolean is_kolab = FALSE;

	g_return_val_if_fail (uidata != NULL, KOLAB_SYNC_STRATEGY_DEFAULT);
	g_return_val_if_fail (E_IS_SHELL_VIEW (uidata->shell_view), KOLAB_SYNC_STRATEGY_DEFAULT);

	is_kolab = e_kolab_plugin_util_ui_get_selected_source (uidata->shell_view,
	                                                       &source);
	g_return_val_if_fail (is_kolab, KOLAB_SYNC_STRATEGY_DEFAULT);

	resource = E_SOURCE_RESOURCE (e_source_get_extension (source,
	                                                      E_SOURCE_EXTENSION_KOLAB_FOLDER));
	strategy = e_source_kolab_folder_get_sync_strategy (E_SOURCE_KOLAB_FOLDER (resource));

	g_object_unref (source);

	return strategy;
}

static gboolean
kolab_folder_metadata_ui_syncstrategy_set (KolabFolderMetaUIData *uidata,
                                           GCancellable *cancellable,
                                           GError **err)
{
	ESource *source = NULL;
	ESourceResource *resource = NULL;
	gboolean is_kolab = FALSE;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	g_return_val_if_fail (E_IS_SHELL_VIEW (uidata->shell_view), FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	is_kolab = e_kolab_plugin_util_ui_get_selected_source (uidata->shell_view,
	                                                       &source);
	if (! is_kolab) {
		g_warning ("%s()[%u] expected Kolab source, got something else, skipping",
		           __func__, __LINE__);
		goto exit;
	}

	resource = E_SOURCE_RESOURCE (e_source_get_extension (source,
	                                                      E_SOURCE_EXTENSION_KOLAB_FOLDER));
	e_source_kolab_folder_set_sync_strategy (E_SOURCE_KOLAB_FOLDER (resource),
	                                         uidata->metadata->strategy);
	ok = e_source_write_sync (source,
	                          cancellable,
	                          &tmp_err);
 exit:
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	if (source != NULL)
		g_object_unref (source);

	return ok;
}

static void
kolab_folder_metadata_ui_foldertype_cb (GtkRadioButton *btn,
                                        gpointer userdata)
{
	KolabFolderMetaUIData *uidata = NULL;
	KolabFolderTypeID foldertype = KOLAB_FOLDER_TYPE_INVAL;
	GtkWidget *ok_btn = NULL;
	gpointer type = NULL;

	g_return_if_fail (GTK_IS_RADIO_BUTTON (btn));
	g_return_if_fail (userdata != NULL);

	uidata = (KolabFolderMetaUIData *) userdata;
	g_return_if_fail (uidata->widgets != NULL);

	type = g_hash_table_lookup (uidata->widgets->folder_type_map,
	                            (gpointer) btn);
	if (type != NULL)
		foldertype = GPOINTER_TO_UINT (type);

	if ((foldertype > KOLAB_FOLDER_TYPE_INVAL) &&
	    (foldertype < KOLAB_FOLDER_LAST_TYPE)) {
		uidata->metadata->foldertype = foldertype;
		uidata->changed_metadata = TRUE;
	}

	ok_btn = e_kolab_plugin_util_ui_dialog_ref_button (uidata->dialog,
	                                                   GTK_STOCK_OK,
	                                                   TRUE);
	g_return_if_fail (GTK_IS_BUTTON (ok_btn));
	gtk_widget_set_sensitive (ok_btn, TRUE);
	g_object_unref (ok_btn);
}

static void
kolab_folder_metadata_ui_syncstrategy_cb (GtkComboBoxText *box,
                                          gpointer userdata)
{
	GtkWidget *ok_btn = NULL;
	KolabFolderMetaUIData *uidata = NULL;
	gint active = 0;

	g_return_if_fail (GTK_IS_COMBO_BOX_TEXT (box));
	g_return_if_fail (userdata != NULL);

	uidata = (KolabFolderMetaUIData *) userdata;
	g_return_if_fail (uidata->metadata != NULL);

	active = gtk_combo_box_get_active (GTK_COMBO_BOX (box));
	uidata->metadata->strategy = (KolabFolderTypeID) active;
	uidata->changed_syncstrategy = TRUE;

	ok_btn = e_kolab_plugin_util_ui_dialog_ref_button (uidata->dialog,
	                                                   GTK_STOCK_OK,
	                                                   TRUE);
	g_return_if_fail (GTK_IS_BUTTON (ok_btn));
	gtk_widget_set_sensitive (ok_btn, TRUE);
	g_object_unref (ok_btn);
}

static void
kolab_folder_metadata_ui_show_all_cb (GtkToggleButton *btn,
                                      gpointer userdata)
{
	KolabFolderMetaUIData *uidata = NULL;
	GtkWidget *ok_btn = NULL;

	g_return_if_fail (GTK_IS_TOGGLE_BUTTON (btn));
	g_return_if_fail (userdata != NULL);

	uidata = (KolabFolderMetaUIData *) userdata;
	g_return_if_fail (uidata->metadata != NULL);

	uidata->metadata->show_all = !(uidata->metadata->show_all);
	uidata->changed_visibility = TRUE;

	ok_btn = e_kolab_plugin_util_ui_dialog_ref_button (uidata->dialog,
	                                                   GTK_STOCK_OK,
	                                                   TRUE);
	g_return_if_fail (GTK_IS_BUTTON (ok_btn));
	gtk_widget_set_sensitive (ok_btn, TRUE);
	g_object_unref (ok_btn);
}

/*----------------------------------------------------------------------------*/
/* API functions (non-UI) */


/*----------------------------------------------------------------------------*/
/* API functions (UI) */

KolabFolderMetaUIData*
e_kolab_folder_metadata_ui_new (void)
{
	KolabFolderMetaUIData *uidata = g_new0 (KolabFolderMetaUIData, 1);
	GtkWidget *widget = NULL;
	GtkWidget *grid = NULL;
	GtkWidget *btn_loco = NULL;
	GtkWidget *btn = NULL;
	gint ii = 0;

	/* documenting initial settings */
	uidata->shell_view = NULL;
	uidata->shell_context = KOLAB_FOLDER_CONTEXT_INVAL;
	uidata->alert_bar = NULL;
	uidata->dialog = NULL;
	uidata->foldername = NULL;
	uidata->sourcename = NULL;
	uidata->changed_metadata = FALSE;
	uidata->changed_syncstrategy = FALSE;
	uidata->changed_visibility = FALSE;

	uidata->widgets = g_new0 (KolabFolderMetaUIWidgets, 1);
	uidata->metadata = kolab_data_folder_metadata_new ();

	grid = gtk_grid_new ();
	gtk_orientable_set_orientation (GTK_ORIENTABLE (grid), GTK_ORIENTATION_VERTICAL);
	gtk_container_set_border_width (GTK_CONTAINER (grid), 16);
	uidata->widgets->container = grid;

	/* Folder type radio button group */

	widget = gtk_frame_new (C_("Kolab Folder Metadata",
	                           "Folder Type (Annotation)"));
	gtk_container_set_border_width (GTK_CONTAINER (widget), 6);
	gtk_container_add (GTK_CONTAINER (uidata->widgets->container), widget);
	uidata->widgets->frame_type_select = widget;

	grid = gtk_grid_new ();
	gtk_orientable_set_orientation (GTK_ORIENTABLE (grid), GTK_ORIENTATION_VERTICAL);
	gtk_grid_set_column_spacing (GTK_GRID (grid), 2);
	gtk_container_set_border_width (GTK_CONTAINER (grid), 16);
	gtk_container_add (GTK_CONTAINER (uidata->widgets->frame_type_select), grid);

	/* mail */
	btn_loco = gtk_radio_button_new_with_label (NULL, C_("Kolab Folder Type",
	                                                     "Mail"));
	uidata->widgets->radio_btn_type[0] = btn_loco;
	uidata->widgets->radio_group = gtk_radio_button_get_group (GTK_RADIO_BUTTON (btn_loco));
	gtk_container_add (GTK_CONTAINER (grid), btn_loco);

	/* calendar */
	btn = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (btn_loco),
	                                                   C_("Kolab Folder Type",
	                                                      "Calendar"));
	uidata->widgets->radio_btn_type[1] = btn;
	gtk_container_add (GTK_CONTAINER (grid), btn);

	/* memos */
	btn = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (btn_loco),
	                                                   C_("Kolab Folder Type",
	                                                      "Memos"));
	uidata->widgets->radio_btn_type[2] = btn;
	gtk_container_add (GTK_CONTAINER (grid), btn);

	/* tasks */
	btn = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (btn_loco),
	                                                   C_("Kolab Folder Type",
	                                                      "Tasks"));
	uidata->widgets->radio_btn_type[3] = btn;
	gtk_container_add (GTK_CONTAINER (grid), btn);

	/* contacts */
	btn = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (btn_loco),
	                                                   C_("Kolab Folder Type",
	                                                      "Contacts"));
	uidata->widgets->radio_btn_type[4] = btn;
	gtk_container_add (GTK_CONTAINER (grid), btn);

	kolab_folder_metadata_ui_create_folder_type_map (uidata);

	/* folder options */

	widget = gtk_frame_new (C_("Kolab Folder Metadata",
	                           "Kolab Folder Options"));
	gtk_container_set_border_width (GTK_CONTAINER (widget), 6);
	gtk_container_add (GTK_CONTAINER (uidata->widgets->container), widget);
	uidata->widgets->frame_options = widget;

	grid = gtk_grid_new ();
	gtk_orientable_set_orientation (GTK_ORIENTABLE (grid), GTK_ORIENTATION_VERTICAL);
	gtk_container_set_border_width (GTK_CONTAINER (grid), 4);
	gtk_container_add (GTK_CONTAINER (uidata->widgets->frame_options), grid);

	/* option: folder sync conflict resolution strategy */
	widget = gtk_label_new (C_("Sync Conflict Resolution",
	                           "Strategy for resolving PIM conflicts when synchronizing\n"
	                           "with the Kolab server"));
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0f, 0.66f);
	gtk_misc_set_padding (GTK_MISC (widget), 4, 4);
	gtk_container_add (GTK_CONTAINER (grid), widget);
	widget = gtk_combo_box_text_new ();
	uidata->widgets->cbox_syncstrategy = widget;
	gtk_container_add (GTK_CONTAINER (grid), uidata->widgets->cbox_syncstrategy);
	kolab_folder_metadata_ui_syncstrategy_fillbox (uidata);

	/* show all folders */

	widget = gtk_check_button_new ();
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (widget), 4);
	uidata->widgets->chk_btn_show_all = widget;
	widget = gtk_label_new (C_("Kolab Folder Options",
	                           "Show all PIM folders in this Kolab account"));
	gtk_container_add (GTK_CONTAINER (uidata->widgets->chk_btn_show_all), widget);
	gtk_container_add (GTK_CONTAINER (uidata->widgets->container), uidata->widgets->chk_btn_show_all);

	/* signals */

	/* folder type radio buttons */
	for (ii = 0; ii < KOLAB_FOLDER_META_UI_NUM_TYPES; ii++) {
		uidata->widgets->radio_btn_handler_id[ii] =
			g_signal_connect (G_OBJECT (uidata->widgets->radio_btn_type[ii]),
			                  "toggled",
			                  G_CALLBACK (kolab_folder_metadata_ui_foldertype_cb),
			                  uidata);
	}

	/* sync strategy combo box */
	uidata->widgets->cbox_syncstrategy_id =
		g_signal_connect (G_OBJECT (uidata->widgets->cbox_syncstrategy),
		                  "changed",
		                  G_CALLBACK (kolab_folder_metadata_ui_syncstrategy_cb),
		                  uidata);

	/* "show all folders" switch */
	uidata->widgets->chk_btn_show_all_handler_id =
		g_signal_connect (G_OBJECT (uidata->widgets->chk_btn_show_all),
		                  "toggled",
		                  G_CALLBACK (kolab_folder_metadata_ui_show_all_cb),
		                  uidata);
	return uidata;
}

void
e_kolab_folder_metadata_ui_free (KolabFolderMetaUIData *uidata)
{
	if (uidata == NULL)
		return;

	/* the actual widgets will have been deleted already,
	 * so just deleting the struct shell here
	 */
	if (uidata->widgets != NULL) {
		if (uidata->widgets->folder_type_map != NULL)
			g_hash_table_destroy (uidata->widgets->folder_type_map);
		g_free (uidata->widgets);
	}

	kolab_data_folder_metadata_free (uidata->metadata);

	if (uidata->foldername != NULL)
		g_free (uidata->foldername);
	if (uidata->sourcename != NULL)
		g_free (uidata->sourcename);

	g_free (uidata);
}

void
e_kolab_folder_metadata_ui_update_from_uidata (KolabFolderMetaUIData *uidata)
{
	GtkWidget *widget = NULL;
	gulong handler_id = 0;
	guint ii = 0;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (E_IS_SHELL_VIEW (uidata->shell_view));
	g_return_if_fail (E_IS_ALERT_BAR (uidata->alert_bar));
	g_return_if_fail (uidata->metadata != NULL);
	g_return_if_fail (uidata->widgets != NULL);

	/* When updating the state of the UI here,
	 * we need to always make sure not to trigger
	 * signals which we may have handlers for.
	 * Updating the UI this way is meant to happen
	 * before the user actually uses it.
	 */

	/* radio button group (current folder type) */
	for (ii = 0; ii < KOLAB_FOLDER_META_UI_NUM_TYPES; ii++) {
		g_signal_handler_block (uidata->widgets->radio_btn_type[ii],
		                        uidata->widgets->radio_btn_handler_id[ii]);
	}

	switch (uidata->metadata->foldertype) {

	case KOLAB_FOLDER_TYPE_EVENT:
	case KOLAB_FOLDER_TYPE_EVENT_DEFAULT:
		ii = 1;
		break;

	case KOLAB_FOLDER_TYPE_NOTE:
	case KOLAB_FOLDER_TYPE_NOTE_DEFAULT:
		ii = 2;
		break;

	case KOLAB_FOLDER_TYPE_TASK:
	case KOLAB_FOLDER_TYPE_TASK_DEFAULT:
		ii = 3;
		break;

	case KOLAB_FOLDER_TYPE_CONTACT:
	case KOLAB_FOLDER_TYPE_CONTACT_DEFAULT:
		ii = 4;
		break;

	default:
		/* all else is regarded email */
		ii = 0;
	}

	widget = uidata->widgets->radio_btn_type[ii];
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), TRUE);

	for (ii = 0; ii < KOLAB_FOLDER_META_UI_NUM_TYPES; ii++) {
		g_signal_handler_unblock (uidata->widgets->radio_btn_type[ii],
		                          uidata->widgets->radio_btn_handler_id[ii]);
	}

	/* PIM options */

	/* option: sync conflict strategy */
	widget = uidata->widgets->cbox_syncstrategy;
	g_return_if_fail (GTK_IS_COMBO_BOX_TEXT (widget));
	handler_id = uidata->widgets->cbox_syncstrategy_id;
	g_signal_handler_block (widget, handler_id);
	gtk_combo_box_set_active (GTK_COMBO_BOX (widget),
	                          (gint) uidata->metadata->strategy);
	g_signal_handler_unblock (widget, handler_id);

	/* whether to show all PIM folders in email view */

	widget = uidata->widgets->chk_btn_show_all;
	g_return_if_fail (GTK_IS_CHECK_BUTTON (widget));
	handler_id = uidata->widgets->chk_btn_show_all_handler_id;

	g_signal_handler_block (widget, handler_id);
	if (uidata->metadata->show_all)
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget),
		                              TRUE);
	else
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget),
		                              FALSE);
	g_signal_handler_unblock (widget, handler_id);
}

gboolean
e_kolab_folder_metadata_ui_query_store (KolabFolderMetaUIData *uidata,
                                        GCancellable *cancellable,
                                        GError **err)
{
	CamelKolabIMAPXStore *kstore = NULL;
	KolabSyncStrategyID strategy = KOLAB_SYNC_STRATEGY_DEFAULT;
	gchar *selected_path = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_return_val_if_fail (E_IS_SHELL_VIEW (uidata->shell_view), FALSE);
	g_return_val_if_fail (uidata->metadata != NULL, FALSE);

	if (uidata->shell_context != KOLAB_FOLDER_CONTEXT_EMAIL)
		goto skip_email;

	/* If we cannot get the store here, it means
	 * that the store associated with the selected
	 * path is not a CamelKolabIMAPXStore. This
	 * should not happen at this point (if the store
	 * in question is not a Kolab store, then no
	 * Kolab folder options context menu entry should
	 * have been shown).
	 */
	ok = e_kolab_plugin_util_ui_get_selected_store (uidata->shell_view,
	                                                &kstore,
	                                                &selected_path,
	                                                &tmp_err);
	if (! ok)
		goto exit;

	/* FIXME we have the foldername duped in uidata->foldername */
	uidata->metadata->foldername = selected_path;
	uidata->metadata->show_all =
		camel_kolab_imapx_store_get_show_all_folders (kstore);
	uidata->metadata->foldertype =
		camel_kolab_imapx_store_get_folder_type (kstore,
		                                         selected_path,
		                                         TRUE,
		                                         cancellable,
		                                         &tmp_err);
 skip_email:

	if (! ((uidata->shell_context == KOLAB_FOLDER_CONTEXT_CALENDAR) ||
	       (uidata->shell_context == KOLAB_FOLDER_CONTEXT_CONTACT)))
		goto exit;

	uidata->metadata->foldername = g_strdup (uidata->foldername);
	uidata->metadata->foldertype =
		e_kolab_plugin_util_ui_get_shell_type (uidata->shell_view);

	strategy = kolab_folder_metadata_ui_syncstrategy_get (uidata);
	uidata->metadata->strategy = strategy;

 exit:

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	if (kstore != NULL)
		g_object_unref (kstore);

	return ok;
}

gboolean
e_kolab_folder_metadata_ui_write_store (KolabFolderMetaUIData *uidata,
                                        GCancellable *cancellable,
                                        GError **err)
{
	CamelKolabIMAPXStore *kstore = NULL;
	gchar *selected_path = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_return_val_if_fail (E_IS_SHELL_VIEW (uidata->shell_view), FALSE);
	g_return_val_if_fail (uidata->metadata != NULL, FALSE);

	if (uidata->shell_context != KOLAB_FOLDER_CONTEXT_EMAIL)
		goto email_skip;

	if (! (uidata->changed_metadata || uidata->changed_visibility))
		return TRUE;

	ok = e_kolab_plugin_util_ui_get_selected_store (uidata->shell_view,
	                                                &kstore,
	                                                &selected_path,
	                                                &tmp_err);
	if (! ok)
		goto exit;

	if (g_strcmp0 (uidata->foldername, selected_path) != 0)
		g_warning ("%s()[%u] foldername change: stored '%s' vs. current '%s'",
		           __func__, __LINE__, uidata->foldername, selected_path);

	if (! uidata->metadata)
		goto metadata_skip;

	ok = camel_kolab_imapx_store_set_folder_type (kstore,
	                                              uidata->foldername,
	                                              uidata->metadata->foldertype,
	                                              cancellable,
	                                              &tmp_err);
	if (! ok)
		goto exit;

 metadata_skip:

	if (! uidata->changed_visibility)
		goto exit;

	ok = camel_kolab_imapx_store_set_show_all_folders (kstore,
	                                                   uidata->metadata->show_all,
	                                                   cancellable,
	                                                   &tmp_err);
 email_skip:

	if (! ((uidata->shell_context == KOLAB_FOLDER_CONTEXT_CALENDAR) ||
	       (uidata->shell_context == KOLAB_FOLDER_CONTEXT_CONTACT)))
		goto exit;

	ok = kolab_folder_metadata_ui_syncstrategy_set (uidata,
	                                                cancellable,
	                                                &tmp_err);
 exit:

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	if (kstore != NULL)
		g_object_unref (kstore);

	if (selected_path != NULL)
		g_free (selected_path);

	return ok;
}

/*----------------------------------------------------------------------------*/
