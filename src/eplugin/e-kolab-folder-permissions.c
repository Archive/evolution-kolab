/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-folder-permissions.c
 *
 *  Fri Feb 10 11:27:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <config.h>

#include <glib/gi18n-lib.h>

#include <e-util/e-util.h>

#include <libekolab/camel-kolab-imapx-store.h>

#include <libekolabutil/kolab-util-error.h>
#include <libekolabutil/kolab-util-folder.h>

#include "e-kolab-plugin-util.h"
#include "e-kolab-folder-permissions.h"

/*----------------------------------------------------------------------------*/

enum {
	KOLAB_PERM_TREE_VIEW_COL_NAME = 0,
	KOLAB_PERM_TREE_VIEW_COL_LEVEL,
	KOLAB_PERM_TREE_VIEW_LAST_COL
};

/*----------------------------------------------------------------------------*/
/* internal statics (non-UI) */

/*----------------------------------------------------------------------------*/
/* internal statics (UI) */

static void
kolab_folder_permissions_ui_create_folder_perms_map (KolabFolderPermUIData *uidata)
{
	GHashTable *map = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);

	map = g_hash_table_new_full (g_direct_hash,
	                             g_direct_equal,
	                             NULL,  /* radio button addresses as keys */
	                             NULL); /* numeric Kolab folder perms as values */;

	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_perm[0],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_PERM_NOCHANGE));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_perm[1],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_PERM_READ));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_perm[2],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_PERM_APPEND));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_perm[3],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_PERM_WRITE));
	g_hash_table_insert (map,
	                     uidata->widgets->radio_btn_perm[4],
	                     GUINT_TO_POINTER (KOLAB_FOLDER_PERM_ALL));

	if (uidata->widgets->folder_perm_map != NULL)
		g_hash_table_destroy (uidata->widgets->folder_perm_map);
	uidata->widgets->folder_perm_map = map;
}

static void
kolab_folder_permissions_ui_create_perms_btn_lbl_array (KolabFolderPermUIData *uidata)
{
	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (uidata->widgets->radio_btn_perm_lbl != NULL);

	uidata->widgets->radio_btn_perm_lbl[KOLAB_FOLDER_PERM_NOCHANGE] =
		g_strdup (C_("Kolab Folder Permissions",
		             "Keep the current permission settings"));
	uidata->widgets->radio_btn_perm_lbl[KOLAB_FOLDER_PERM_READ] =
		g_strdup (C_("Kolab Folder Permissions",
		             "Read"));
	uidata->widgets->radio_btn_perm_lbl[KOLAB_FOLDER_PERM_APPEND] =
		g_strdup (C_("Kolab Folder Permissions",
		             "Append"));
	uidata->widgets->radio_btn_perm_lbl[KOLAB_FOLDER_PERM_WRITE] =
		g_strdup (C_("Kolab Folder Permissions",
		             "Write"));
	uidata->widgets->radio_btn_perm_lbl[KOLAB_FOLDER_PERM_ALL] =
		g_strdup (C_("Kolab Folder Permissions",
		             "All"));
}

static void
kolab_folder_permissions_ui_get_active_perm (KolabFolderPermUIData *uidata,
                                             KolabFolderPermID *perm_id,
                                             GtkRadioButton **active_btn)
{
	GtkToggleButton *cur_btn = NULL;
	KolabFolderPermID ii = 0;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (uidata->widgets->radio_btn_perm != NULL);
	/* perm_id may be NULL */
	if (active_btn != NULL)
		g_return_if_fail (*active_btn == NULL);

	for (ii = 0; ii < KOLAB_FOLDER_LAST_PERM; ii++) {
		cur_btn = GTK_TOGGLE_BUTTON (uidata->widgets->radio_btn_perm[ii]);
		if (gtk_toggle_button_get_active (cur_btn)) {
			if (active_btn != NULL)
				*active_btn = GTK_RADIO_BUTTON (cur_btn);
			if (perm_id != NULL)
				*perm_id = ii;
			break;
		}
	}
}

static gboolean
kolab_folder_permissions_ui_treeview_get_selected (KolabFolderPermUIData *uidata,
                                                   gchar **access_id,
                                                   gchar **rights)
{
	GtkTreeView *treeview = NULL;
	GtkTreeSelection *treesel = NULL;
	GtkTreeModel *treemodel = NULL;
	GtkTreeIter treeiter;
	gboolean have_sel = FALSE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	g_return_val_if_fail (uidata->widgets != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TREE_VIEW (uidata->widgets->treeview), FALSE);
	g_return_val_if_fail (access_id != NULL && *access_id == NULL, FALSE);
	g_return_val_if_fail (rights != NULL && *rights == NULL, FALSE);

	treeview = GTK_TREE_VIEW (uidata->widgets->treeview);
	treesel = gtk_tree_view_get_selection (treeview);
	g_return_val_if_fail (GTK_IS_TREE_SELECTION (treesel), FALSE);

	have_sel = gtk_tree_selection_get_selected (treesel,
	                                            &treemodel,
	                                            &treeiter);
	if (! have_sel)
		return FALSE;

	g_return_val_if_fail (GTK_IS_TREE_MODEL (treemodel), FALSE);

	gtk_tree_model_get (treemodel, &treeiter,
	                    0, access_id,
	                    1, rights,
	                    -1);
	g_return_val_if_fail (*access_id != NULL, FALSE);
	/* rights may be NULL */

	return TRUE;
}

static gboolean
kolab_folder_permissions_ui_add_edit_have_changes (KolabFolderPermUIData *uidata,
                                                   KolabFolderPermID perm_id)
{
	GtkComboBoxText *entry = NULL;
	gchar *text = NULL;
	gboolean have_changes = FALSE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	g_return_val_if_fail (uidata->widgets != NULL, FALSE);
	g_return_val_if_fail (uidata->widgets->edit_entry_box != NULL, FALSE);

	if (uidata->editing) {
		have_changes =
			(perm_id != KOLAB_FOLDER_PERM_NOCHANGE);
	} else {
		entry = GTK_COMBO_BOX_TEXT (uidata->widgets->edit_entry_box);
		text = gtk_combo_box_text_get_active_text (entry);
		have_changes =
			((perm_id != KOLAB_FOLDER_PERM_NOCHANGE) &&
			 (text != NULL) &&
			 (g_strcmp0 (text, "") != 0));
		if (text != NULL)
			g_free (text);
	}

	return have_changes;
}

static void
kolab_folder_permissions_ui_add_edit_update_ok_buttons (KolabFolderPermUIData *uidata,
                                                        KolabFolderPermID perm_id)
{
	GtkDialog *edit_dialog = NULL;
	gboolean activate_ok_btn = FALSE;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (GTK_IS_DIALOG (uidata->dialog));
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_DIALOG (uidata->widgets->edit_dialog));

	edit_dialog = GTK_DIALOG (uidata->widgets->edit_dialog);

	activate_ok_btn =
		kolab_folder_permissions_ui_add_edit_have_changes (uidata,
		                                                   perm_id);

	if (activate_ok_btn) {
		e_kolab_plugin_util_ui_dialog_set_button_sensitive (uidata->dialog,
		                                                    GTK_STOCK_OK,
		                                                    TRUE,
		                                                    TRUE);
		e_kolab_plugin_util_ui_dialog_set_button_sensitive (edit_dialog,
		                                                    GTK_STOCK_OK,
		                                                    TRUE,
		                                                    TRUE);
	} else {
		/* The "Kolab Folder Permissions..." dialog button,
		 * once activated, is not deactivated here, since we
		 * may have some changes pending in the Metadata tab.
		 * Hence, we only deactivate the add/edit sub-dialog
		 * button as long as we do not have any changes pending
		 * in the add/edit dialog
		 */
		e_kolab_plugin_util_ui_dialog_set_button_sensitive (edit_dialog,
		                                                    GTK_STOCK_OK,
		                                                    TRUE,
		                                                    FALSE);
	}
}

static void
kolab_folder_permissions_ui_folderperm_cb (GtkRadioButton *btn,
                                           gpointer userdata)
{
	KolabFolderPermUIData *uidata = NULL;
	KolabFolderPermID perm_id = KOLAB_FOLDER_PERM_NOCHANGE;
	gpointer perm = NULL;

	g_return_if_fail (GTK_IS_RADIO_BUTTON (btn));
	g_return_if_fail (userdata != NULL);

	uidata = (KolabFolderPermUIData *) userdata;
	g_return_if_fail (uidata->widgets != NULL);

	perm = g_hash_table_lookup (uidata->widgets->folder_perm_map,
	                            (gpointer) btn);
	if (perm != NULL)
		perm_id = GPOINTER_TO_UINT (perm);

	kolab_folder_permissions_ui_add_edit_update_ok_buttons (uidata,
	                                                        perm_id);
}

static void
kolab_folder_permissions_ui_edit_entry_box_cb (GtkComboBoxText *box,
                                               gpointer userdata)
{
	KolabFolderPermUIData *uidata = NULL;
	GtkRadioButton *btn = NULL;

	g_return_if_fail (GTK_IS_COMBO_BOX_TEXT (box));
	g_return_if_fail (userdata != NULL);

	uidata = (KolabFolderPermUIData *) userdata;

	kolab_folder_permissions_ui_get_active_perm (uidata,
	                                             NULL,
	                                             &btn);
	kolab_folder_permissions_ui_folderperm_cb (btn,
	                                           userdata);
}

static void
kolab_folder_permissions_ui_update_add_dialog (KolabFolderPermUIData *uidata)
{
	GtkComboBoxText *entrybox = NULL;
	CamelImapxAclEntry *entry = NULL;
	GList *acl = NULL;
	GList *acl_ptr = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (uidata->permissions != NULL);

	/* when editing an existing ACL entry, we show
	 * the current rights string for the selected
	 * access_id, otherwise we hide it
	 */
	gtk_widget_hide (uidata->widgets->access_rights);

	/* when adding a new ACL entry, the "Keep the
	 * current permission settings" selection is
	 * meaningless, so we hide it here
	 */
	gtk_widget_hide (uidata->widgets->radio_btn_perm[KOLAB_FOLDER_PERM_NOCHANGE]);

	g_return_if_fail (GTK_IS_COMBO_BOX_TEXT (uidata->widgets->edit_entry_box));
	entrybox = GTK_COMBO_BOX_TEXT (uidata->widgets->edit_entry_box);

	acl = uidata->permissions->acl;
	acl_ptr = acl;
	while (acl_ptr != NULL) {
		entry = (CamelImapxAclEntry *) acl_ptr->data;
		if (entry == NULL) {
			g_warning ("%s[%u] got NULL CamelImapxAclEntry",
			           __func__, __LINE__);
			goto skip;
		}

		if (entry->access_id == NULL) {
			g_warning ("%s[%u] got NULL CamelImapxAclEntry.access_id",
			           __func__, __LINE__);
			goto skip;
		}

		gtk_combo_box_text_append_text (entrybox,
		                                entry->access_id);

	skip:
		acl_ptr = g_list_next (acl_ptr);
	}
}

static void
kolab_folder_permissions_ui_update_edit_dialog (KolabFolderPermUIData *uidata)
{
	GtkComboBoxText *entrybox = NULL;
	gchar *access_id = NULL;
	gchar *rights = NULL;
	gboolean ok = FALSE;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);

	/* when editing an existing ACL entry, we use the
	 * dropdown combo box for displaying the clicked
	 * access_id only
	 */
	gtk_widget_set_sensitive (uidata->widgets->edit_entry_box,
	                          FALSE);

	/* get the access_id and rights string from the tree selection */
	ok = kolab_folder_permissions_ui_treeview_get_selected (uidata,
	                                                        &access_id,
	                                                        &rights);
	if (! ok)
		return;

	/* update dialog */
	entrybox = GTK_COMBO_BOX_TEXT (uidata->widgets->edit_entry_box);
	gtk_combo_box_text_insert_text (entrybox, 0, access_id);
	gtk_combo_box_set_active (GTK_COMBO_BOX (entrybox), 0);
	gtk_label_set_text (GTK_LABEL (uidata->widgets->access_rights_lbl),
	                    rights);

	g_free (access_id);
	if (rights != NULL)
		g_free (rights);
}

static void
kolab_folder_permissions_ui_update_add_edit_dialog (KolabFolderPermUIData *uidata)
{
	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);

	/* when the add/edit dialog opens, we will have the existing
	 * access_id list read in already, so we can deactivate the
	 * "Retrieve" button for now (could be used for an in-place
	 * re-query later on or simply removed)
	 */
	gtk_widget_set_sensitive (uidata->widgets->edit_btn_retrieve,
	                          FALSE);

	if (uidata->editing)
		kolab_folder_permissions_ui_update_edit_dialog (uidata);
	else
		kolab_folder_permissions_ui_update_add_dialog (uidata);
}

static void
kolab_folder_permissions_ui_update_treeview (KolabFolderPermUIData *uidata)
{
	GtkTreeView *treeview = NULL;
	GtkListStore *liststore = NULL;
	GList *acl_ptr = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->permissions != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_TREE_VIEW (uidata->widgets->treeview));

	treeview = GTK_TREE_VIEW (uidata->widgets->treeview);
	liststore = GTK_LIST_STORE (gtk_tree_view_get_model (treeview));

	gtk_list_store_clear (liststore);

	acl_ptr = uidata->permissions->acl;
	while (acl_ptr != NULL) {
		CamelImapxAclEntry *entry = NULL;
		gchar *access_id = NULL;
		gchar *rights = NULL;

		entry = (CamelImapxAclEntry *) acl_ptr->data;
		if (entry == NULL)
			goto skip;
		if (entry->rights == NULL)
			goto skip;

		access_id = entry->access_id;
		rights = entry->rights;

		gtk_list_store_insert_with_values (liststore,
		                                   NULL, /* GtkTreeIter not needed */
		                                   -1,   /* append to liststore    */
		                                   0, g_strdup (access_id),
		                                   1, g_strdup (rights),
		                                   -1);
	skip:
		acl_ptr = g_list_next (acl_ptr);
	}
}

static void
kolab_folder_permissions_ui_update_from_dialog (KolabFolderPermUIData *uidata,
                                                const gchar *access_id,
                                                const gchar *rights)
{
	CamelImapxAclEntry *entry = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (access_id != NULL);
	/* rights may be NULL */

	/* update payload data */
	entry = camel_imapx_acl_entry_new (access_id,
	                                   rights,
	                                   NULL);
	(void) camel_imapx_acl_list_update_from_entry (&(uidata->permissions->acl),
	                                               entry,
	                                               NULL);
	camel_imapx_acl_entry_free (entry);

	/* update treeview */
	kolab_folder_permissions_ui_update_treeview (uidata);
	uidata->changed = TRUE;
}

static void
kolab_folder_permissions_ui_add_edit_dialog_response_cb (GObject *dialog,
                                                         gint response_id,
                                                         gpointer *userdata)
{
	KolabFolderPermUIData *uidata = NULL;
	KolabFolderPermID perm_id = KOLAB_FOLDER_PERM_NOCHANGE;
	GtkComboBoxText *entry = NULL;
	gchar *access_id = NULL;
	const gchar *rights = NULL;
	const gchar *prev_rights = NULL;
	gchar *new_rights = NULL;
	GError *tmp_err = NULL;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (userdata != NULL);

	if (response_id != GTK_RESPONSE_OK) {
		gtk_widget_destroy (GTK_WIDGET (dialog));
		return;
	}

	uidata = (KolabFolderPermUIData *) userdata;

	/* ACL access_id */
	entry = GTK_COMBO_BOX_TEXT (uidata->widgets->edit_entry_box);
	access_id = gtk_combo_box_text_get_active_text (entry);

	/* currently active permission id */
	kolab_folder_permissions_ui_get_active_perm (uidata,
	                                             &perm_id,
	                                             NULL);

	/* corresponding rights string */
	rights = kolab_util_folder_perm_get_string (perm_id);
	g_return_if_fail (rights != NULL);

	/* previously set rights */
	prev_rights = camel_imapx_acl_list_get_rights (uidata->permissions->acl,
	                                               access_id,
	                                               &tmp_err);
	if (tmp_err != NULL)
		goto exit;

	/* merge current and previous rights */
	new_rights = camel_imapx_acl_rights_merge (prev_rights,
	                                           rights,
	                                           &tmp_err);
	if (tmp_err != NULL)
		goto exit;

	/* set new access_id,rights */
	kolab_folder_permissions_ui_update_from_dialog (uidata,
	                                                access_id,
	                                                new_rights);

	if (new_rights != NULL)
		g_free (new_rights);

 exit:

	if (tmp_err != NULL) {
		e_kolab_plugin_util_ui_alert_bar_add_error (uidata->alert_bar,
		                                            tmp_err);
		g_error_free (tmp_err);

	}
	if (access_id != NULL)
		g_free (access_id);

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void
kolab_folder_permissions_ui_add_edit_dialog_perm_button_add_to_grid (KolabFolderPermUIData *uidata,
                                                                     GtkGrid *grid,
                                                                     GtkWidget *btn,
                                                                     gint row)
{
	GtkWidget *widget = NULL;
	gchar *tmp = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (uidata->widgets->radio_btn_perm != NULL);
	g_return_if_fail (GTK_IS_GRID (grid));
	g_return_if_fail (GTK_IS_RADIO_BUTTON (btn));

	uidata->widgets->radio_btn_perm[row] = btn;
	gtk_grid_attach (grid, btn, 0, row, 1, 1);
	tmp = g_strdup_printf ("(%s)",
	                       kolab_util_folder_perm_get_string (row));
	widget = gtk_label_new (tmp);
	g_free (tmp);
	gtk_misc_set_alignment (GTK_MISC (widget), 0.0, 0.5);
	gtk_grid_attach (grid, widget, 1, row, 1, 1);
}

static void
kolab_folder_permissions_ui_add_edit_dialog (KolabFolderPermUIData *uidata)
{
	GtkWidget *dialog = NULL;
	GtkWidget *content = NULL;
	GtkWidget *hbox = NULL;
	GtkWidget *label = NULL;
	GtkWidget *entry = NULL;
	GtkWidget *frame = NULL;
	GtkWidget *btn_loco = NULL;
	GtkWidget *btn = NULL;
	GtkWidget *grid = NULL;
	GtkWidget *widget = NULL;
	GtkGrid *ggrid = NULL;
	const gchar *title = NULL;
	gchar *tmp = NULL;
	gint row = 0;
	gint ii = 0;

	g_assert (uidata != NULL);
	g_assert (uidata->widgets != NULL);

	if (uidata->editing) {
		title = C_("Kolab Folder Permissions",
		           "Edit Kolab Folder Permissions...");
	} else {
		title = C_("Kolab Folder Permissions",
		           "Add Kolab Folder Permissions...");
	}

	dialog = gtk_dialog_new_with_buttons (title,
	                                      NULL, /* parent */
	                                      GTK_DIALOG_DESTROY_WITH_PARENT|GTK_DIALOG_MODAL,
	                                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
	                                      GTK_STOCK_OK, GTK_RESPONSE_OK,
	                                      NULL);

	content = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
	gtk_container_set_border_width (GTK_CONTAINER (content), 6);

	widget = e_kolab_plugin_util_ui_selected_folder_widget (uidata->foldername,
	                                                        uidata->sourcename);
	gtk_container_add (GTK_CONTAINER (content), widget);

	widget = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
	gtk_widget_set_margin_bottom (widget, 4);
	gtk_container_add (GTK_CONTAINER (content), widget);

	hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 6);
	label = gtk_label_new (C_("Kolab Folder Permissions", "User ID:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	entry = gtk_combo_box_text_new_with_entry ();
	gtk_combo_box_set_popup_fixed_width (GTK_COMBO_BOX (entry), FALSE);
	uidata->widgets->edit_entry_box = entry;
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
	btn = gtk_button_new_with_label (C_("Kolab Folder Permissions",
	                                    "Retrieve..."));
	uidata->widgets->edit_btn_retrieve = btn;
	gtk_box_pack_start (GTK_BOX (hbox), btn, FALSE, FALSE, 0);
	gtk_container_add (GTK_CONTAINER (content), hbox);

	frame = gtk_frame_new (C_("Kolab Folder Permissions",
	                          "Current Folder Permissions"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), 2);
	gtk_widget_set_margin_bottom (frame, 4);
	uidata->widgets->access_rights = frame;
	label = gtk_label_new (C_("Kolab Folder Permissions",
	                          "(No access rights retrieved from server)"));
	gtk_widget_set_margin_left (label, 4);
	gtk_widget_set_margin_right (label, 4);
	gtk_widget_set_margin_top (label, 2);
	gtk_widget_set_margin_bottom (label, 2);
	uidata->widgets->access_rights_lbl = label;
	gtk_container_add (GTK_CONTAINER (frame), label);
	gtk_container_add (GTK_CONTAINER (content), frame);

	frame = gtk_frame_new (C_("Kolab Folder Permissions",
	                          "IMAP Folder Permissions"));
	gtk_container_add (GTK_CONTAINER (content), frame);

	grid = gtk_grid_new ();
	ggrid = GTK_GRID (grid);
	gtk_orientable_set_orientation (GTK_ORIENTABLE (grid), GTK_ORIENTATION_VERTICAL);
	gtk_grid_set_column_spacing (ggrid, 5);
	gtk_grid_set_row_homogeneous (ggrid, TRUE);
	gtk_grid_set_column_homogeneous (ggrid, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (grid), 16);
	gtk_container_add (GTK_CONTAINER (frame), grid);

	/* permission types radio button list */

	kolab_folder_permissions_ui_create_perms_btn_lbl_array (uidata);
	row = 0;

	tmp = uidata->widgets->radio_btn_perm_lbl[row]; /* do not free() */
	btn_loco = gtk_radio_button_new_with_label (uidata->widgets->radio_group,
	                                            tmp);
	uidata->widgets->radio_btn_perm[row] = btn_loco;
	gtk_grid_attach (ggrid, btn_loco, 0, row, 2, 1);

	for (row = 1; row < KOLAB_FOLDER_PERM_UI_NUM_PERMS; row++) {
		tmp = uidata->widgets->radio_btn_perm_lbl[row]; /* do not free() */
		btn = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (btn_loco),
		                                                   tmp);
		kolab_folder_permissions_ui_add_edit_dialog_perm_button_add_to_grid (uidata,
		                                                                     ggrid,
		                                                                     btn,
		                                                                     row);
	}

	kolab_folder_permissions_ui_create_folder_perms_map (uidata);

	/* signals */

	/* folder perms radio buttons */
	for (ii = 0; ii < KOLAB_FOLDER_PERM_UI_NUM_PERMS; ii++) {
		uidata->widgets->radio_btn_handler_id[ii] =
			g_signal_connect (G_OBJECT (uidata->widgets->radio_btn_perm[ii]),
			                  "toggled",
			                  G_CALLBACK (kolab_folder_permissions_ui_folderperm_cb),
			                  uidata);
	}

	/* access_id combo box */
	g_signal_connect (G_OBJECT (uidata->widgets->edit_entry_box),
	                  "changed",
	                  G_CALLBACK (kolab_folder_permissions_ui_edit_entry_box_cb),
	                  uidata);

	/* dialog */
	g_signal_connect (dialog,
	                  "response",
	                  G_CALLBACK (kolab_folder_permissions_ui_add_edit_dialog_response_cb),
	                  uidata);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog),
	                                   GTK_RESPONSE_OK,
	                                   FALSE);

	uidata->widgets->edit_dialog = dialog;

	gtk_widget_show_all (content);

	kolab_folder_permissions_ui_update_add_edit_dialog (uidata);

	gtk_widget_show (dialog);
}

static void
kolab_folder_permissions_ui_add_dialog_cb (KolabFolderPermUIData *uidata)
{
	g_assert (uidata != NULL);

	uidata->editing = FALSE;
	kolab_folder_permissions_ui_add_edit_dialog (uidata);
}

static void
kolab_folder_permissions_ui_edit_dialog_cb (KolabFolderPermUIData *uidata)
{
	gchar *access_id = NULL;
	gchar *rights = NULL;
	gboolean ok = FALSE;

	g_assert (uidata != NULL);

	/* check whether we have a valid treeview selection */
	ok = kolab_folder_permissions_ui_treeview_get_selected (uidata,
	                                                        &access_id,
	                                                        &rights);
	if (! ok)
		return;

	g_free (access_id);
	g_free (rights);

	/* show edit dialog */
	uidata->editing = TRUE;
	kolab_folder_permissions_ui_add_edit_dialog (uidata);
}

static void
kolab_folder_permissions_ui_acl_remove_cb (KolabFolderPermUIData *uidata)
{
	CamelImapxAclEntry *entry = NULL;
	gchar *access_id = NULL;
	gchar *rights = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (uidata != NULL);

	/* remove selected entry from payload data */
	ok = kolab_folder_permissions_ui_treeview_get_selected (uidata,
	                                                        &access_id,
	                                                        &rights);
	if (! ok)
		return;

	entry = camel_imapx_acl_entry_new (access_id, NULL, &tmp_err);

	if (access_id != NULL)
		g_free (access_id);
	if (rights != NULL)
		g_free (rights);

	if (entry == NULL)
		goto exit;

	ok = camel_imapx_acl_list_update_from_entry (&(uidata->permissions->acl),
	                                             entry,
	                                             &tmp_err);
	camel_imapx_acl_entry_free (entry);

	if (! ok)
		goto exit;

	/* update treeview */
	kolab_folder_permissions_ui_update_treeview (uidata);
	uidata->changed = TRUE;
	e_kolab_plugin_util_ui_dialog_set_button_sensitive (uidata->dialog,
	                                                    GTK_STOCK_OK,
	                                                    TRUE,
	                                                    TRUE);
 exit:

	if (tmp_err != NULL) {
		g_warning ("%s()[%u] %s",
		           __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
	}
}

static GtkWidget*
kolab_folder_permissions_ui_create_tree_view (KolabFolderPermUIData *uidata)
{
	GtkTreeView *tree_view = NULL;
	GtkTreeModel *tree_model = NULL;
	GtkTreeSelection *selection = NULL;
	GtkCellRenderer *renderer = NULL;
	GtkTreeViewColumn *column = NULL;

	g_assert (uidata != NULL);
	g_assert (uidata->widgets != NULL);

	tree_model = GTK_TREE_MODEL (gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_STRING));
	tree_view = GTK_TREE_VIEW (gtk_tree_view_new_with_model (tree_model));
	gtk_tree_view_set_grid_lines (tree_view, GTK_TREE_VIEW_GRID_LINES_BOTH);
	gtk_tree_view_set_rubber_banding (tree_view, TRUE);
	gtk_tree_view_columns_autosize (tree_view);
	gtk_scrollable_set_vscroll_policy (GTK_SCROLLABLE (tree_view), GTK_SCROLL_NATURAL);
	gtk_scrollable_set_hscroll_policy (GTK_SCROLLABLE (tree_view), GTK_SCROLL_NATURAL);

	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "editable", FALSE, NULL);
	gtk_tree_view_insert_column_with_attributes (tree_view,
	                                             -1,
	                                             C_("Kolab Folder Permissions",
	                                                "User ID"),
	                                             renderer,
	                                             "text",
	                                             KOLAB_PERM_TREE_VIEW_COL_NAME,
	                                             NULL);
	column = gtk_tree_view_get_column (tree_view, KOLAB_PERM_TREE_VIEW_COL_NAME);
	gtk_tree_view_column_set_expand (column, TRUE);

	renderer = gtk_cell_renderer_text_new ();
	g_object_set (renderer, "editable", FALSE, NULL);
	gtk_tree_view_insert_column_with_attributes (tree_view,
	                                             -1,
	                                             C_("Kolab Folder Permissions",
	                                                "Permissions"),
	                                             renderer,
	                                             "text",
	                                             KOLAB_PERM_TREE_VIEW_COL_LEVEL,
	                                             NULL);

	selection = gtk_tree_view_get_selection (tree_view);
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
#if 0
	/* FIXME connect signal to callback */
	g_signal_connect (selection, "changed", G_CALLBACK (folder_permissions_tree_selection_changed_cb), widgets);
#endif
	gtk_tree_view_set_headers_visible (tree_view, TRUE);
	gtk_tree_view_set_headers_clickable (tree_view, FALSE);
	gtk_tree_view_set_fixed_height_mode (tree_view, TRUE);

	return GTK_WIDGET (tree_view);
}

/*----------------------------------------------------------------------------*/
/* API functions (non-UI) */


/*----------------------------------------------------------------------------*/
/* API functions (UI) */

KolabFolderPermUIData*
e_kolab_folder_permissions_ui_new (void)
{
	KolabFolderPermUIData *uidata = g_new0 (KolabFolderPermUIData, 1);
	GtkGrid *grid = NULL;
	GtkWidget *vbox = NULL;
	GtkWidget *myrights = NULL;
	GtkWidget *myrights_lbl = NULL;
	GtkWidget *treeview = NULL;
	gint row = 0;

	/* documenting initial settings */
	uidata->shell_view = NULL;
	uidata->shell_context = KOLAB_FOLDER_CONTEXT_INVAL;
	uidata->alert_bar = NULL;
	uidata->dialog = NULL;
	uidata->foldername = NULL;
	uidata->sourcename = NULL;
	uidata->editing = FALSE;
	uidata->changed = FALSE;

	uidata->permissions = kolab_data_folder_permissions_new ();
	uidata->widgets = g_new0 (KolabFolderPermUIWidgets, 1);

	uidata->widgets->container = gtk_frame_new (C_("Kolab Folder Permissions",
	                                               "IMAP Folder Permissions"));
	gtk_container_set_border_width (GTK_CONTAINER (uidata->widgets->container), 6);
	/* gtk_container_set_resize_mode (uidata->widgets->container, GTK_RESIZE_QUEUE); */

	grid = GTK_GRID (gtk_grid_new ());
	gtk_grid_set_row_homogeneous (grid, FALSE);
	gtk_grid_set_row_spacing (grid, 6);
	gtk_grid_set_column_homogeneous (grid, FALSE);
	gtk_grid_set_column_spacing (grid, 6);
	gtk_container_set_border_width (GTK_CONTAINER (grid), 16);
	/* gtk_container_set_resize_mode (GTK_CONTAINER (grid), GTK_RESIZE_QUEUE); */
	gtk_container_add (GTK_CONTAINER (uidata->widgets->container), GTK_WIDGET (grid));

	row = 0;

	myrights = gtk_frame_new (C_("Kolab Folder Permissions",
	                             "My Folder Permissions"));
	gtk_container_set_border_width (GTK_CONTAINER (myrights), 2);
	gtk_widget_set_margin_bottom (myrights, 4);
	uidata->widgets->myrights = myrights;
	myrights_lbl = gtk_label_new (C_("Kolab Folder Permissions",
	                                 "(No access rights retrieved from server)"));
	gtk_widget_set_margin_left (myrights_lbl, 4);
	gtk_widget_set_margin_right (myrights_lbl, 4);
	gtk_widget_set_margin_top (myrights_lbl, 2);
	gtk_widget_set_margin_bottom (myrights_lbl, 2);
	uidata->widgets->myrights_lbl = myrights_lbl;
	gtk_container_add (GTK_CONTAINER (myrights), myrights_lbl);
	gtk_grid_attach (GTK_GRID (grid), myrights, 0, row, 2, 1);

	row++;

	/* permissions tree view */
	treeview = kolab_folder_permissions_ui_create_tree_view (uidata);
	/* gtk_container_set_resize_mode (GTK_CONTAINER (treeview), GTK_RESIZE_QUEUE); */
	uidata->widgets->treeview = treeview;
	gtk_grid_attach (GTK_GRID (grid), treeview, 0, row, 1, 1);

	/* FIXME remove me, display testing */
	/* add_some_content_to_treeview (GTK_TREE_VIEW (treeview)); */

	/* add/edit/delete stock buttons */
	vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 12);
	uidata->widgets->btn_add = gtk_button_new_from_stock (GTK_STOCK_ADD);
	gtk_box_pack_start (GTK_BOX (vbox), uidata->widgets->btn_add, FALSE, FALSE, 0);
	uidata->widgets->btn_edit = gtk_button_new_from_stock (GTK_STOCK_EDIT);
	gtk_box_pack_start (GTK_BOX (vbox), uidata->widgets->btn_edit, FALSE, FALSE, 0);
	uidata->widgets->btn_remove = gtk_button_new_from_stock (GTK_STOCK_REMOVE);
	gtk_box_pack_start (GTK_BOX (vbox), uidata->widgets->btn_remove, FALSE, FALSE, 0);
	gtk_grid_attach (GTK_GRID (grid), vbox, 1, row, 1, 1);

	g_signal_connect_swapped (G_OBJECT (uidata->widgets->btn_add),
	                          "clicked",
	                          G_CALLBACK (kolab_folder_permissions_ui_add_dialog_cb),
	                          uidata);
	g_signal_connect_swapped (G_OBJECT (uidata->widgets->btn_edit),
	                          "clicked",
	                          G_CALLBACK (kolab_folder_permissions_ui_edit_dialog_cb),
	                          uidata);
	g_signal_connect_swapped (G_OBJECT (uidata->widgets->btn_remove),
	                          "clicked",
	                          G_CALLBACK (kolab_folder_permissions_ui_acl_remove_cb),
	                          uidata);
	return uidata;
}

void
e_kolab_folder_permissions_ui_free (KolabFolderPermUIData *uidata)
{
	gint ii = 0;
	gchar *tmp = NULL;

	if (uidata == NULL)
		return;

	/* the actual widgets will have been deleted already,
	 * so just deleting the struct shell and supplemental
	 * data structures for the widgets here
	 */
	if (uidata->widgets != NULL) {
		if (uidata->widgets->folder_perm_map != NULL)
			g_hash_table_destroy (uidata->widgets->folder_perm_map);
		for (ii = 0; ii < KOLAB_FOLDER_PERM_UI_NUM_PERMS; ii++) {
			tmp = uidata->widgets->radio_btn_perm_lbl[ii];
			if (tmp)
				g_free (tmp);
		}
		g_free (uidata->widgets);
	}

	kolab_data_folder_permissions_free (uidata->permissions);
	if (uidata->foldername != NULL)
		g_free (uidata->foldername);
	if (uidata->sourcename != NULL)
		g_free (uidata->sourcename);

	g_free (uidata);
}

void
e_kolab_folder_permissions_ui_update_from_uidata (KolabFolderPermUIData *uidata)
{
	GtkWidget *label = NULL;
	gpointer data = NULL;
	gchar *rights = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->permissions != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_LABEL (uidata->widgets->myrights_lbl));

	kolab_folder_permissions_ui_update_treeview (uidata);

	if (uidata->permissions->myrights != NULL) {
		label = uidata->widgets->myrights_lbl;
		data = uidata->permissions->myrights->data;
		rights = NULL;

		if (data != NULL)
			rights = ((CamelImapxAclEntry *) data)->rights;

		gtk_label_set_text (GTK_LABEL (label),
		                    rights);
	}
}

gboolean
e_kolab_folder_permissions_ui_query_store (KolabFolderPermUIData *uidata,
                                           GCancellable *cancellable,
                                           GError **err)
{
	CamelKolabIMAPXStore *kstore = NULL;
	gchar *selected_path = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_return_val_if_fail (E_IS_SHELL_VIEW (uidata->shell_view), FALSE);
	g_return_val_if_fail (uidata->permissions != NULL, FALSE);
	g_return_val_if_fail (uidata->permissions->acl == NULL, FALSE);

	if (uidata->shell_context != KOLAB_FOLDER_CONTEXT_EMAIL)
		goto exit; /* ACL can be done from email view only */

	/* If we cannot get the store here, it means
	 * that the store associated with the selected
	 * path is not a CamelKolabIMAPXStore. This
	 * should not happen at this point (if the store
	 * in question is not a Kolab store, then no
	 * Kolab folder options context menu entry should
	 * have been shown).
	 */
	ok = e_kolab_plugin_util_ui_get_selected_store (uidata->shell_view,
	                                                &kstore,
	                                                &selected_path,
	                                                &tmp_err);
	if (! ok)
		goto exit;

	uidata->permissions->acl =
		camel_kolab_imapx_store_get_folder_permissions (kstore,
		                                                selected_path,
		                                                FALSE, /* general ACL (no myrights) */
		                                                cancellable,
		                                                &tmp_err);
	if (tmp_err != NULL)
		goto exit;

	uidata->permissions->myrights =
		camel_kolab_imapx_store_get_folder_permissions (kstore,
		                                                selected_path,
		                                                TRUE, /* myrights only */
		                                                cancellable,
		                                                &tmp_err);
 exit:

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	if (selected_path != NULL)
		g_free (selected_path);

	if (kstore != NULL)
		g_object_unref (kstore);

	return ok;
}

gboolean
e_kolab_folder_permissions_ui_write_store (KolabFolderPermUIData *uidata,
                                           GCancellable *cancellable,
                                           GError **err)
{
	CamelKolabIMAPXStore *kstore = NULL;
	gchar *selected_path = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_return_val_if_fail (E_IS_SHELL_VIEW (uidata->shell_view), FALSE);
	g_return_val_if_fail (uidata->permissions != NULL, FALSE);

	if (uidata->shell_context != KOLAB_FOLDER_CONTEXT_EMAIL)
		goto exit; /* ACL can be done from email view only */

	if (! uidata->changed)
		return TRUE;

	ok = e_kolab_plugin_util_ui_get_selected_store (uidata->shell_view,
	                                                &kstore,
	                                                &selected_path,
	                                                &tmp_err);
	if (! ok)
		goto exit;

	if (g_strcmp0 (uidata->foldername, selected_path) != 0)
		g_warning ("%s()[%u] foldername change: stored '%s' vs. current '%s'",
		           __func__, __LINE__, uidata->foldername, selected_path);

	ok = camel_kolab_imapx_store_set_folder_permissions (kstore,
	                                                     uidata->foldername,
	                                                     uidata->permissions->acl,
	                                                     cancellable,
	                                                     &tmp_err);
 exit:

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	if (kstore != NULL)
		g_object_unref (kstore);

	if (selected_path != NULL)
		g_free (selected_path);

	return ok;
}

/*----------------------------------------------------------------------------*/
