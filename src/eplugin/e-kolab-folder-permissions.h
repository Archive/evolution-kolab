/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-folder-permissions.h
 *
 *  Fri Feb 10 11:23:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_KOLAB_FOLDER_PERMISSIONS_H_
#define _E_KOLAB_FOLDER_PERMISSIONS_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gtk/gtk.h>

#include <e-util/e-util.h>
#include <shell/e-shell-view.h>

#include <libekolab/kolab-data-folder-permissions.h>
#include <libekolab/kolab-data-imap-account.h>

#include <libekolabutil/kolab-util-folder.h>

/*----------------------------------------------------------------------------*/

#define KOLAB_FOLDER_PERM_UI_NUM_PERMS 5

/*----------------------------------------------------------------------------*/

typedef struct _KolabFolderPermUIWidgets KolabFolderPermUIWidgets;
struct _KolabFolderPermUIWidgets {
	GtkWidget *container;
	/* sub-widgets of container */
	GtkWidget *myrights;
	GtkWidget *myrights_lbl;
	GtkWidget *treeview;
	GtkWidget *btn_add;
	GtkWidget *btn_edit;
	GtkWidget *btn_remove;
	/* add/edit dialog */
	GtkWidget *edit_dialog;
	/* sub-widgets of add/edit dialog */
	GtkWidget *edit_entry_box;
	GtkWidget *edit_btn_retrieve;
	GtkWidget *access_rights;
	GtkWidget *access_rights_lbl;
	GHashTable *folder_perm_map;
	GSList *radio_group;
	gulong radio_btn_handler_id[KOLAB_FOLDER_PERM_UI_NUM_PERMS];
	GtkWidget *radio_btn_perm[KOLAB_FOLDER_PERM_UI_NUM_PERMS];
	gchar *radio_btn_perm_lbl[KOLAB_FOLDER_PERM_UI_NUM_PERMS];
};

typedef struct _KolabFolderPermUIData KolabFolderPermUIData;
struct _KolabFolderPermUIData {
	EShellView *shell_view;
	KolabFolderContextID shell_context;
	EAlertBar *alert_bar;
	GtkDialog *dialog;
	KolabFolderPermUIWidgets *widgets;
	KolabDataFolderPermissions *permissions;
	gchar *foldername;
	gchar *sourcename;
	gboolean editing;
	gboolean changed;
};

/*----------------------------------------------------------------------------*/

KolabFolderPermUIData*
e_kolab_folder_permissions_ui_new (void);

void
e_kolab_folder_permissions_ui_free (KolabFolderPermUIData *uidata);

void
e_kolab_folder_permissions_ui_update_from_uidata (KolabFolderPermUIData *uidata);

gboolean
e_kolab_folder_permissions_ui_query_store (KolabFolderPermUIData *uidata,
                                           GCancellable *cancellable,
                                           GError **err);

gboolean
e_kolab_folder_permissions_ui_write_store (KolabFolderPermUIData *uidata,
                                           GCancellable *cancellable,
                                           GError **err);

/*----------------------------------------------------------------------------*/

#endif /* _E_KOLAB_FOLDER_PERMISSIONS_H_ */

/*----------------------------------------------------------------------------*/
