/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-plugin-ui.h
 *
 *  Fri Feb 10 17:20:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_KOLAB_PLUGIN_UI_H_
#define _E_KOLAB_PLUGIN_UI_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gtk/gtk.h>

#include <shell/e-shell-view.h>

/*----------------------------------------------------------------------------*/

gboolean
e_kolab_plugin_ui_init_mail (GtkUIManager *ui_manager,
                             EShellView *shell_view);

gboolean
e_kolab_plugin_ui_init_calendar (GtkUIManager *ui_manager,
                                 EShellView *shell_view);

gboolean
e_kolab_plugin_ui_init_tasks (GtkUIManager *ui_manager,
                              EShellView *shell_view);

gboolean
e_kolab_plugin_ui_init_memos (GtkUIManager *ui_manager,
                              EShellView *shell_view);

gboolean
e_kolab_plugin_ui_init_contacts (GtkUIManager *ui_manager,
                                 EShellView *shell_view);

/*----------------------------------------------------------------------------*/

#endif /* _E_KOLAB_PLUGIN_UI_H_ */

/*----------------------------------------------------------------------------*/
