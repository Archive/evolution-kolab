/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-plugin.c
 *
 *  Wed Feb 08 16:22:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#define d(x)

/*----------------------------------------------------------------------------*/

#include <config.h>

#include <libekolab/e-source-kolab-folder.h>

#include "e-kolab-plugin-types.h"
#include "e-kolab-plugin-ui.h"
#include "e-kolab-plugin.h"

#include "e-kolab-backend-sync-conflict.h"

/*----------------------------------------------------------------------------*/
/* plugin */

gint
e_plugin_lib_enable (EPlugin *epl,
                     gint enable)
{
	GType mytype = 0;

	g_return_val_if_fail (E_IS_PLUGIN (epl), 0);

	/* register kolab folder source type */
	mytype = e_source_kolab_folder_get_type ();
	g_debug ("%s(): %s registered",
	         __func__, g_type_name (mytype));

	if (enable) {
		g_debug ("%s(): Kolab plugin enabled", __func__);
	} else {
		g_debug ("%s(): Kolab plugin disabled", __func__);
	}

	/* FIXME
	 *
	 * This needs to be removed entirely. It just serves
	 * as a showcase for the sync conflict resolution
	 * dialog until we have the evo<->e-d-s extra communication
	 * established so the dialog can be shown on backend request.
	 *
	 * This call does not belong here!
	 */
#if 0
	e_kolab_backend_sync_ui_conflict_cb (NULL);
#endif

	return 0;
}

/*----------------------------------------------------------------------------*/
/* Kolab folder UI extensions init */

gboolean
e_kolab_plugin_init_mail (GtkUIManager *ui_manager,
                          EShellView *shell_view)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (GTK_IS_UI_MANAGER (ui_manager), FALSE);
	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), FALSE);

	ok = e_kolab_plugin_ui_init_mail (ui_manager,
	                                  shell_view);
	return ok;
}

gboolean
e_kolab_plugin_init_calendar (GtkUIManager *ui_manager,
                              EShellView *shell_view)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (GTK_IS_UI_MANAGER (ui_manager), FALSE);
	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), FALSE);

	ok = e_kolab_plugin_ui_init_calendar (ui_manager,
	                                      shell_view);
	return ok;
}

gboolean
e_kolab_plugin_init_tasks (GtkUIManager *ui_manager,
                           EShellView *shell_view)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (GTK_IS_UI_MANAGER (ui_manager), FALSE);
	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), FALSE);

	ok = e_kolab_plugin_ui_init_tasks (ui_manager,
	                                   shell_view);
	return ok;
}

gboolean
e_kolab_plugin_init_memos (GtkUIManager *ui_manager,
                           EShellView *shell_view)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (GTK_IS_UI_MANAGER (ui_manager), FALSE);
	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), FALSE);

	ok = e_kolab_plugin_ui_init_memos (ui_manager,
	                                   shell_view);
	return ok;
}

gboolean
e_kolab_plugin_init_contacts (GtkUIManager *ui_manager,
                              EShellView *shell_view)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (GTK_IS_UI_MANAGER (ui_manager), FALSE);
	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), FALSE);

	ok = e_kolab_plugin_ui_init_contacts (ui_manager,
	                                      shell_view);
	return ok;
}

/*----------------------------------------------------------------------------*/
