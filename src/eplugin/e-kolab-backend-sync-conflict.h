/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-backend-sync-conflict.h
 *
 *  Fri Feb 20 22:32:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_KOLAB_BACKEND_SYNC_CONFLICT_H_
#define _E_KOLAB_BACKEND_SYNC_CONFLICT_H_

/*----------------------------------------------------------------------------*/

#include <shell/e-shell-view.h>

/*----------------------------------------------------------------------------*/

void
e_kolab_backend_sync_ui_conflict_cb (EShellView *shell_view);

/*----------------------------------------------------------------------------*/

#endif /* _E_KOLAB_BACKEND_SYNC_CONFLICT_H_ */

/*----------------------------------------------------------------------------*/
