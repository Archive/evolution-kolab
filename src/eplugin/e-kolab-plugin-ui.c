/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-plugin-ui.c
 *
 *  Fri Feb 10 17:21:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <config.h>

#include <glib/gi18n-lib.h>

#include <e-util/e-util.h>

#include <libekolabutil/kolab-util-folder.h>

#include <libekolab/camel-kolab-imapx-store.h>
#include <libekolab/kolab-data-imap-account.h>
#include <libekolab/kolab-util-backend.h>

#include "e-kolab-folder-metadata.h"
#include "e-kolab-folder-permissions.h"
#include "e-kolab-plugin-util.h"
#include "e-kolab-plugin-ui.h"

/*----------------------------------------------------------------------------*/

/* how many menu entries are defined; all calendar/tasks/memos/contacts
   actions should have same count */
#define KOLAB_CONTEXT_NUM_ENTRIES 1

static void kolab_plugin_ui_action_kolab_properties_cb (GtkAction *action, EShellView *shell_view);

static GtkActionEntry mail_folder_context_entries[] = {
	{ "kolab-mail-folder-properties",
	  GTK_STOCK_PROPERTIES,
	  N_("Kolab Folder Properties..."),
	  NULL,
	  N_("Edit Kolab mail folder properties"),
	  G_CALLBACK (kolab_plugin_ui_action_kolab_properties_cb) }
};

static GtkActionEntry calendar_context_entries[] = {
	{ "kolab-calendar-folder-properties",
	  GTK_STOCK_PROPERTIES,
	  N_("Kolab Folder Properties..."),
	  NULL,
	  N_("Edit Kolab calendar folder properties"),
	  G_CALLBACK (kolab_plugin_ui_action_kolab_properties_cb) }
};

static GtkActionEntry memos_context_entries[] = {
	{ "kolab-memos-folder-properties",
	  GTK_STOCK_PROPERTIES,
	  N_("Kolab Folder Properties..."),
	  NULL,
	  N_("Edit Kolab memos folder properties"),
	  G_CALLBACK (kolab_plugin_ui_action_kolab_properties_cb) }
};

static GtkActionEntry tasks_context_entries[] = {
	{ "kolab-tasks-folder-properties",
	  GTK_STOCK_PROPERTIES,
	  N_("Kolab Folder Properties..."),
	  NULL,
	  N_("Edit Kolab Tasks folder properties"),
	  G_CALLBACK (kolab_plugin_ui_action_kolab_properties_cb) }
};

static GtkActionEntry contacts_context_entries[] = {
	{ "kolab-contacts-folder-properties",
	  GTK_STOCK_PROPERTIES,
	  N_("Kolab Folder Properties..."),
	  NULL,
	  N_("Edit Kolab contacts folder properties"),
	  G_CALLBACK (kolab_plugin_ui_action_kolab_properties_cb) }
};

/*----------------------------------------------------------------------------*/
/* UI data structures */

#define  E_KOLAB_PROP_DLG_UIDATA "e-kolab-prop-dlg-uidata"

typedef struct _KolabFolderPropUIWidgets KolabFolderPropUIWidgets;
struct _KolabFolderPropUIWidgets {
	GtkWidget *dialog;
	/* sub-widgets of dialog */
	GtkWidget *alert_bar;
	GtkWidget *selected_folder;
	GtkWidget *notebook;
};

typedef struct _KolabFolderPropUIData KolabFolderPropUIData;
struct _KolabFolderPropUIData {
	EShellView *shell_view;
	KolabFolderContextID shell_context;
	KolabFolderPropUIWidgets *widgets;
	KolabFolderMetaUIData *meta_ui_data;
	KolabFolderPermUIData *perm_ui_data;
	KolabDataImapAccount *account;
};

static KolabFolderPropUIData*
kolab_folder_prop_ui_data_new ()
{
	KolabFolderPropUIData *uidata = NULL;

	uidata = g_new0 (KolabFolderPropUIData, 1);
	uidata->shell_view = NULL;
	uidata->widgets = g_new0 (KolabFolderPropUIWidgets, 1);
	uidata->meta_ui_data = NULL;
	uidata->perm_ui_data = NULL;
	uidata->account = NULL;

	return uidata;
}

static void
kolab_folder_prop_ui_data_free (KolabFolderPropUIData *uidata)
{
	if (uidata == NULL)
		return;

	/* the actual widgets will have been deleted already,
	 * so just deleting the struct shell here
	 */
	if (uidata->widgets != NULL)
		g_free (uidata->widgets);

	e_kolab_folder_metadata_ui_free (uidata->meta_ui_data);
	e_kolab_folder_permissions_ui_free (uidata->perm_ui_data);
	kolab_data_imap_account_free (uidata->account);

	g_free (uidata);
}

static void
kolab_folder_prop_ui_data_destroy (gpointer data)
{
	if (data == NULL)
		return;

	kolab_folder_prop_ui_data_free ((KolabFolderPropUIData *)data);
}

/*----------------------------------------------------------------------------*/
/* internal statics (non-UI) */


/*----------------------------------------------------------------------------*/
/* internal statics (UI) */

static void
kolab_folder_prop_ui_alertbar_visible (KolabFolderPropUIData *uidata,
                                       gboolean visible)
{
	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (E_IS_ALERT_BAR (uidata->widgets->alert_bar));

	if (visible)
		gtk_widget_show (uidata->widgets->alert_bar);
	else
		gtk_widget_hide (uidata->widgets->alert_bar);
}

static void
kolab_folder_prop_ui_alertbar_add_error (KolabFolderPropUIData *uidata,
                                         GError *err)
{
	EAlertBar *alert_bar = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);

	alert_bar = E_ALERT_BAR (uidata->widgets->alert_bar);
	g_return_if_fail (alert_bar);

	e_kolab_plugin_util_ui_alert_bar_add_error (alert_bar,
	                                            err);
}

static void
kolab_folder_prop_ui_notebook_enable (KolabFolderPropUIData *uidata,
                                      gboolean enable)
{
	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_NOTEBOOK (uidata->widgets->notebook));

	gtk_widget_set_sensitive (uidata->widgets->notebook,
	                          enable);
}

static void
kolab_folder_prop_ui_response_cb (GObject *dialog,
                                  gint response_id,
                                  gpointer userdata)
{
	KolabFolderPropUIData *uidata = NULL;
	GtkWidget *ok_btn = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_if_fail (dialog != NULL);
	g_return_if_fail (userdata != NULL);

	if (response_id != GTK_RESPONSE_OK) {
		gtk_widget_destroy (GTK_WIDGET (dialog));
		return;
	}

	uidata = (KolabFolderPropUIData *) userdata;

	/* we try with folder permissions first, since if it will
	 * succeed, the following actions may be affected by the
	 * newly set permissions
	 */

	ok = e_kolab_folder_permissions_ui_write_store (uidata->perm_ui_data,
	                                                NULL, /* FIXME add cancellation stack */
	                                                &tmp_err);
	if (! ok)
		goto exit;

	/* Instead of aborting after unsuccessful permission setting,
	 * should we just dump the error into the EAlertBar and move
	 * on? Alerts can be stacked in the EAlertBar, so we could
	 * try whether either operation succeeds, and in the event of
	 * a double failure just accumulate two errors in the EAlertBar.
	 * Downside: If there are two errors, the earlier one may not
	 * be visible unless the latter is closed.
	 */

	ok = e_kolab_folder_metadata_ui_write_store (uidata->meta_ui_data,
	                                             NULL, /* FIXME add cancellation stack */
	                                             &tmp_err);
 exit:
	if (tmp_err != NULL) {
		/* dump GError into EAlertBar */
		kolab_folder_prop_ui_alertbar_add_error (uidata, tmp_err);
		g_error_free (tmp_err);
		/* deactivate OK button (need to cancel or try again) */
		ok_btn = e_kolab_plugin_util_ui_dialog_ref_button (GTK_DIALOG (uidata->widgets->dialog),
		                                                   GTK_STOCK_OK,
		                                                   TRUE);
		g_return_if_fail (GTK_IS_BUTTON (ok_btn));
		gtk_widget_set_sensitive (ok_btn, FALSE);
		g_object_unref (ok_btn);
		return;
	}

	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static KolabFolderPropUIData*
kolab_folder_prop_ui_dialog_data_new (EShellView *shell_view)
{
	KolabFolderPropUIData *uidata = NULL;
	KolabFolderContextID context = KOLAB_FOLDER_CONTEXT_INVAL;
	gchar *foldername = NULL;
	gchar *sourcename = NULL;
	GError *tmp_err = NULL;

	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), NULL);

	context = e_kolab_plugin_util_ui_get_shell_context (shell_view);
	g_return_val_if_fail (context != KOLAB_FOLDER_CONTEXT_INVAL, NULL);

	foldername = e_kolab_plugin_util_ui_get_selected_foldername (shell_view,
	                                                             &tmp_err);
	if (tmp_err != NULL) {
		/* TODO this should not happen.
		 *
		 * If the selected folder is NULL here, it means
		 * it is no Kolab folder node. There should not
		 * be a context menu entry in this case in the
		 * first place.
		 */
		g_warning ("%s()[%u]: %s",
		           __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		return NULL;
	}

	/* sourcename may be NULL */
	sourcename = e_kolab_plugin_util_ui_get_selected_sourcename (shell_view);

	uidata = kolab_folder_prop_ui_data_new ();
	uidata->shell_view = shell_view; /* FIXME ref the view? */
	uidata->shell_context = context;
	uidata->widgets->alert_bar = e_alert_bar_new ();
	uidata->widgets->notebook = gtk_notebook_new ();

	uidata->widgets->selected_folder =
		e_kolab_plugin_util_ui_selected_folder_widget (foldername,
		                                               sourcename);

	uidata->widgets->dialog =
		gtk_dialog_new_with_buttons (_("Edit Kolab Folder Properties..."),
		                             NULL, /* parent */
		                             GTK_DIALOG_DESTROY_WITH_PARENT|GTK_DIALOG_MODAL,
		                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		                             GTK_STOCK_OK, GTK_RESPONSE_OK,
		                             NULL);
	gtk_window_set_resizable (GTK_WINDOW (uidata->widgets->dialog), FALSE);

	uidata->meta_ui_data = e_kolab_folder_metadata_ui_new ();
	uidata->meta_ui_data->shell_view = shell_view;
	uidata->meta_ui_data->shell_context = context;
	uidata->meta_ui_data->alert_bar = E_ALERT_BAR (uidata->widgets->alert_bar);
	uidata->meta_ui_data->dialog = GTK_DIALOG (uidata->widgets->dialog);
	uidata->meta_ui_data->foldername = g_strdup (foldername);
	uidata->meta_ui_data->sourcename = g_strdup (sourcename);

	uidata->perm_ui_data = e_kolab_folder_permissions_ui_new ();
	uidata->perm_ui_data->shell_view = shell_view;
	uidata->perm_ui_data->shell_context = context;
	uidata->perm_ui_data->alert_bar = E_ALERT_BAR (uidata->widgets->alert_bar);
	uidata->perm_ui_data->dialog = GTK_DIALOG (uidata->widgets->dialog);
	uidata->perm_ui_data->foldername = g_strdup (foldername);
	uidata->perm_ui_data->sourcename = g_strdup (sourcename);

	g_free (foldername);
	if (sourcename != NULL)
		g_free (sourcename);

	uidata->account = kolab_data_imap_account_new ();

	return uidata;
}

static void
kolab_folder_prop_ui_dialog_update_sensitivity (KolabFolderPropUIData *uidata)
{
	GtkWidget *widget = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_DIALOG (uidata->widgets->dialog));
	g_return_if_fail (GTK_IS_NOTEBOOK (uidata->widgets->notebook));
	g_return_if_fail (uidata->meta_ui_data);
	g_return_if_fail (uidata->meta_ui_data->widgets);
	g_return_if_fail (uidata->perm_ui_data);
	g_return_if_fail (uidata->perm_ui_data->widgets);

	if (uidata->shell_context != KOLAB_FOLDER_CONTEXT_EMAIL)
		goto skip_email;

	/* in email view, we disable the PIM folder options */
	widget = uidata->meta_ui_data->widgets->frame_options;
	gtk_widget_set_sensitive (widget, FALSE);

 skip_email:

	if (! ((uidata->shell_context == KOLAB_FOLDER_CONTEXT_CALENDAR) ||
	       (uidata->shell_context == KOLAB_FOLDER_CONTEXT_CONTACT)))
		goto skip_pim;

	/* in PIM view, we disable all IMAP stuff (for which we
	 * need the CamelKolabIMAPXProvider, which is only available
	 * in email view)
	 */
	widget = uidata->meta_ui_data->widgets->frame_type_select;
	gtk_widget_set_sensitive (widget, FALSE);
	widget = uidata->meta_ui_data->widgets->chk_btn_show_all;
	gtk_widget_set_sensitive (widget, FALSE);
	widget = uidata->perm_ui_data->widgets->container;
	gtk_widget_set_sensitive (widget, FALSE);

 skip_pim:

	return;
}

static void
kolab_folder_prop_ui_dialog_assemble (KolabFolderPropUIData *uidata)
{
	GtkWidget *content = NULL;
	GtkNotebook *notebook = NULL;

	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_DIALOG (uidata->widgets->dialog));
	g_return_if_fail (E_IS_ALERT_BAR (uidata->widgets->alert_bar));

	content = gtk_dialog_get_content_area (GTK_DIALOG (uidata->widgets->dialog));
	gtk_container_add (GTK_CONTAINER (content), uidata->widgets->alert_bar);
	gtk_container_add (GTK_CONTAINER (content), uidata->widgets->selected_folder);
	notebook = GTK_NOTEBOOK (uidata->widgets->notebook);
	gtk_notebook_set_show_border (notebook, TRUE);
	gtk_container_add (GTK_CONTAINER (content),
	                   uidata->widgets->notebook);

	/* set notebook content */

	/* folder metadata notebook page */
	gtk_notebook_append_page (notebook,
	                          uidata->meta_ui_data->widgets->container,
	                          NULL);
	gtk_notebook_set_tab_label_text (notebook,
	                                 uidata->meta_ui_data->widgets->container,
	                                 C_("Kolab Folder Properties",
	                                    "IMAP Metadata"));

	/* folder permissions notebook page */
	gtk_notebook_append_page (notebook,
	                          uidata->perm_ui_data->widgets->container,
	                          NULL);
	gtk_notebook_set_tab_label_text (notebook,
	                                 uidata->perm_ui_data->widgets->container,
	                                 C_("Kolab Folder Properties",
	                                    "IMAP Access Control"));
	gtk_widget_show_all (content);
	kolab_folder_prop_ui_dialog_update_sensitivity (uidata);
}

static void
kolab_folder_prop_ui_dialog_visible (KolabFolderPropUIData *uidata,
                                     gboolean visible)
{
	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_DIALOG (uidata->widgets->dialog));

	if (visible)
		gtk_widget_show (uidata->widgets->dialog);
	else
		gtk_widget_hide (uidata->widgets->dialog);
}

static void
kolab_folder_prop_ui_dialog_update_from_uidata (KolabFolderPropUIData *uidata)
{
	g_return_if_fail (uidata != NULL);
	g_return_if_fail (uidata->widgets != NULL);
	g_return_if_fail (GTK_IS_DIALOG (uidata->widgets->dialog));

	e_kolab_folder_metadata_ui_update_from_uidata (uidata->meta_ui_data);
	e_kolab_folder_permissions_ui_update_from_uidata (uidata->perm_ui_data);
}

static gboolean
kolab_folder_prop_ui_query_metadata (KolabFolderPropUIData *uidata,
                                     GCancellable *cancellable,
                                     GError **err)
{
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = e_kolab_folder_metadata_ui_query_store (uidata->meta_ui_data,
	                                             cancellable,
	                                             &tmp_err);
	if (tmp_err != NULL)
		g_propagate_error (err, tmp_err);

	return ok;
}

static gboolean
kolab_folder_prop_ui_query_acl (KolabFolderPropUIData *uidata,
                                GCancellable *cancellable,
                                GError **err)
{
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (uidata != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = e_kolab_folder_permissions_ui_query_store (uidata->perm_ui_data,
	                                                cancellable,
	                                                &tmp_err);
	if (tmp_err != NULL)
		g_propagate_error (err, tmp_err);

	return ok;
}

static void
kolab_plugin_ui_action_kolab_properties_cb (GtkAction *action,
                                            EShellView *shell_view)
{
	KolabFolderPropUIData *uidata = NULL;
	GObject *dialog = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_return_if_fail (action != NULL);
	g_return_if_fail (E_IS_SHELL_VIEW (shell_view));

	/* create widgets and payload data structures */
	uidata = kolab_folder_prop_ui_dialog_data_new (shell_view);
	g_return_if_fail (uidata != NULL);

	/* assemble dialog */
	kolab_folder_prop_ui_dialog_assemble (uidata);

	/* connect response callback */
	dialog = G_OBJECT (uidata->widgets->dialog);
	g_signal_connect (dialog, "response", G_CALLBACK (kolab_folder_prop_ui_response_cb), uidata);
	g_object_set_data_full (dialog, E_KOLAB_PROP_DLG_UIDATA, uidata, kolab_folder_prop_ui_data_destroy);

	kolab_folder_prop_ui_notebook_enable (uidata, FALSE);

	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_CANCEL);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog),
	                                   GTK_RESPONSE_OK,
	                                   FALSE);

	kolab_folder_prop_ui_alertbar_visible (uidata, FALSE);
	kolab_folder_prop_ui_dialog_visible (uidata, TRUE);

	/* TODO
	 *
	 * - add an EActivity bar and a cancellation stack
	 *   so we can cancel getting the info as well as
	 *   cancel setting the new options later on
	 *
	 */

	ok = kolab_folder_prop_ui_query_metadata (uidata,
	                                          NULL, /* add cancellation stack here */
	                                          &tmp_err);
	if (! ok)
		goto exit;

	ok = kolab_folder_prop_ui_query_acl (uidata,
	                                     NULL, /* add cancellation stack here */
	                                     &tmp_err);
	if (! ok)
		goto exit;

	kolab_folder_prop_ui_dialog_update_from_uidata (uidata);
	kolab_folder_prop_ui_notebook_enable (uidata, TRUE);

 exit:
	if (tmp_err != NULL) {
		kolab_folder_prop_ui_alertbar_add_error (uidata, tmp_err);
		g_error_free (tmp_err);
	}
}

static void
kolab_plugin_ui_enable_actions (GtkActionGroup *action_group,
                                const GtkActionEntry *entries,
                                guint n_entries,
                                gboolean can_show,
                                gboolean is_online)
{
	guint ii = 0;

	g_return_if_fail (action_group != NULL);
	g_return_if_fail (entries != NULL);

	for (ii = 0; ii < n_entries; ii++) {
		GtkAction *action;

		action = gtk_action_group_get_action (action_group, entries[ii].name);
		if (!action)
			continue;

		gtk_action_set_visible (action, can_show);
		if (can_show)
			gtk_action_set_sensitive (action, is_online);
	}
}

static void
kolab_plugin_ui_update_mail_entries_cb (EShellView *shell_view,
                                        GtkActionEntry *entries)
{
	EShellWindow *shell_window = NULL;
	GtkActionGroup *action_group = NULL;
	GtkUIManager *ui_manager = NULL;
	gchar *selected_path = NULL;
	gboolean is_kolab_account_node = FALSE;
	gboolean is_kolab_folder_node = FALSE;
	gboolean is_online = FALSE;

	g_assert (E_IS_SHELL_VIEW (shell_view));
	g_assert (entries != NULL);

	selected_path = e_kolab_plugin_util_ui_get_selected_path (shell_view,
	                                                          &is_kolab_account_node,
	                                                          &is_kolab_folder_node,
	                                                          NULL);
	g_free (selected_path);

	shell_window = e_shell_view_get_shell_window (shell_view);
	ui_manager = e_shell_window_get_ui_manager (shell_window);
	action_group = e_lookup_action_group (ui_manager, "mail");

	if (is_kolab_account_node || is_kolab_folder_node) {
		EShellBackend *backend = NULL;
		CamelSession *session = NULL;

		backend = e_shell_view_get_shell_backend (shell_view);
		g_object_get (G_OBJECT (backend), "session", &session, NULL);

		is_online = session && camel_session_get_online (session);

		if (session)
			g_object_unref (session);
	}

	kolab_plugin_ui_enable_actions (action_group,
	                                mail_folder_context_entries,
	                                KOLAB_CONTEXT_NUM_ENTRIES,
	                                is_kolab_folder_node,
	                                is_online);
}

static void
kolab_plugin_ui_update_source_entries_cb (EShellView *shell_view,
                                          GtkActionEntry *entries)
{
	GtkActionGroup *action_group = NULL;
	EShell *shell = NULL;
	EShellWindow *shell_window = NULL;
	const gchar *group = NULL;
	gboolean is_kolab_source = FALSE;
	gboolean is_online = FALSE;

	g_return_if_fail (E_IS_SHELL_VIEW (shell_view));
	g_return_if_fail (entries != NULL);

	group = e_kolab_plugin_util_group_name_from_action_entry_name (entries->name);
	g_return_if_fail (group != NULL);

	is_kolab_source = e_kolab_plugin_util_ui_get_selected_source (shell_view,
	                                                              NULL);
	shell_window = e_shell_view_get_shell_window (shell_view);
	shell = e_shell_window_get_shell (shell_window);

	/* this is a shortcut. we should actually query
	 * the online state from the backend
	 */
	is_online = shell && e_shell_get_online (shell);
	action_group = e_shell_window_get_action_group (shell_window, group);

	kolab_plugin_ui_enable_actions (action_group,
	                                entries,
	                                KOLAB_CONTEXT_NUM_ENTRIES,
	                                is_kolab_source,
	                                is_online);
}

static void
kolab_plugin_ui_setup_mail_actions (EShellView *shell_view)
{
	EShellWindow *shell_window;
	GtkActionGroup *action_group;

	g_assert (E_IS_SHELL_VIEW (shell_view));
	g_return_if_fail (G_N_ELEMENTS (mail_folder_context_entries) == KOLAB_CONTEXT_NUM_ENTRIES);

	shell_window = e_shell_view_get_shell_window (shell_view);
	action_group = e_shell_window_get_action_group (shell_window, "mail");

	/* Add actions to the "mail" action group. */
	e_action_group_add_actions_localized (action_group,
	                                      GETTEXT_PACKAGE,
	                                      mail_folder_context_entries,
	                                      KOLAB_CONTEXT_NUM_ENTRIES,
	                                      shell_view);

	/* Decide whether we want this option to be visible or not */
	g_signal_connect (shell_view,
	                  "update-actions",
	                  G_CALLBACK (kolab_plugin_ui_update_mail_entries_cb),
	                  shell_view);

	g_object_unref (action_group);
}

static void
kolab_plugin_ui_setup_source_actions (EShellView *shell_view,
                                      GtkActionEntry *entries,
                                      guint n_entries)
{
	EShellWindow *shell_window;
	const gchar *group;

	g_assert (E_IS_SHELL_VIEW (shell_view));
	g_return_if_fail (entries != NULL);
	g_return_if_fail (n_entries == KOLAB_CONTEXT_NUM_ENTRIES);

	group = e_kolab_plugin_util_group_name_from_action_entry_name (entries->name);
	g_return_if_fail (group != NULL);

	shell_window = e_shell_view_get_shell_window (shell_view);

	e_action_group_add_actions_localized (e_shell_window_get_action_group (shell_window, group),
	                                      GETTEXT_PACKAGE,
	                                      entries,
	                                      KOLAB_CONTEXT_NUM_ENTRIES,
	                                      shell_view);

	g_signal_connect (shell_view,
	                  "update-actions",
	                  G_CALLBACK (kolab_plugin_ui_update_source_entries_cb),
	                  entries);
}

/*----------------------------------------------------------------------------*/
/* API functions (non-UI) */


/*----------------------------------------------------------------------------*/
/* API functions (UI) */

gboolean
e_kolab_plugin_ui_init_mail (GtkUIManager *ui_manager,
                             EShellView *shell_view)
{
	g_assert (GTK_IS_UI_MANAGER (ui_manager));
	g_assert (E_IS_SHELL_VIEW (shell_view));

	kolab_plugin_ui_setup_mail_actions (shell_view);
	return TRUE;
}

gboolean
e_kolab_plugin_ui_init_calendar (GtkUIManager *ui_manager,
                                 EShellView *shell_view)
{
	g_assert (GTK_IS_UI_MANAGER (ui_manager));
	g_assert (E_IS_SHELL_VIEW (shell_view));

	kolab_plugin_ui_setup_source_actions (shell_view,
	                                      calendar_context_entries,
	                                      G_N_ELEMENTS (calendar_context_entries));
	return TRUE;
}

gboolean
e_kolab_plugin_ui_init_tasks (GtkUIManager *ui_manager,
                              EShellView *shell_view)
{
	g_assert (GTK_IS_UI_MANAGER (ui_manager));
	g_assert (E_IS_SHELL_VIEW (shell_view));

	kolab_plugin_ui_setup_source_actions (shell_view,
	                                      tasks_context_entries,
	                                      G_N_ELEMENTS (tasks_context_entries));
	return TRUE;
}

gboolean
e_kolab_plugin_ui_init_memos (GtkUIManager *ui_manager,
                              EShellView *shell_view)
{
	g_assert (GTK_IS_UI_MANAGER (ui_manager));
	g_assert (E_IS_SHELL_VIEW (shell_view));

	kolab_plugin_ui_setup_source_actions (shell_view,
	                                      memos_context_entries,
	                                      G_N_ELEMENTS (memos_context_entries));
	return TRUE;
}

gboolean
e_kolab_plugin_ui_init_contacts (GtkUIManager *ui_manager,
                                 EShellView *shell_view)
{
	g_assert (GTK_IS_UI_MANAGER (ui_manager));
	g_assert (E_IS_SHELL_VIEW (shell_view));

	kolab_plugin_ui_setup_source_actions (shell_view,
	                                      contacts_context_entries,
	                                      G_N_ELEMENTS (contacts_context_entries));
	return TRUE;
}

/*----------------------------------------------------------------------------*/
