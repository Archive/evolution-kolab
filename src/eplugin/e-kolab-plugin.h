/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-plugin.h
 *
 *  Wed Feb 08 16:24:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_KOLAB_PLUGIN_H_
#define _E_KOLAB_PLUGIN_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gtk/gtk.h>

#include <e-util/e-util.h>
#include <shell/e-shell-view.h>

/*----------------------------------------------------------------------------*/

gint
e_plugin_lib_enable (EPlugin *epl,
                     gint enable);

gboolean
e_kolab_plugin_calendar_check (EPlugin *epl,
                               EConfigHookPageCheckData *data);

void
e_kolab_plugin_calendar_commit (EPlugin *epl,
                                EConfigTarget *target);

GtkWidget*
e_kolab_plugin_calendar_create (EPlugin *epl,
                                EConfigHookItemFactoryData *data);

gboolean
e_kolab_plugin_addressbook_check (EPlugin *epl,
                                  EConfigHookPageCheckData *data);

void
e_kolab_plugin_addressbook_commit (EPlugin *epl,
                                   EConfigTarget *target);

GtkWidget*
e_kolab_plugin_addressbook_create (EPlugin *epl,
                                   EConfigHookItemFactoryData *data);

gboolean
e_kolab_plugin_mail_account_check (EPlugin *epl,
                                   EConfigHookPageCheckData *data);

void
e_kolab_plugin_mail_account_commit (EPlugin *epl,
                                    EConfigTarget *target);

GtkWidget*
e_kolab_plugin_mail_account_setup (EPlugin *epl,
                                   EConfigHookItemFactoryData *data);

gboolean
e_kolab_plugin_init_mail (GtkUIManager *ui_manager,
                          EShellView *shell_view);

gboolean
e_kolab_plugin_init_calendar (GtkUIManager *ui_manager,
                              EShellView *shell_view);

gboolean
e_kolab_plugin_init_tasks (GtkUIManager *ui_manager,
                           EShellView *shell_view);

gboolean
e_kolab_plugin_init_memos (GtkUIManager *ui_manager,
                           EShellView *shell_view);

gboolean
e_kolab_plugin_init_contacts (GtkUIManager *ui_manager,
                              EShellView *shell_view);

/*----------------------------------------------------------------------------*/

#endif /* _E_KOLAB_PLUGIN_H_ */

/*----------------------------------------------------------------------------*/
