/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-folder-metadata.h
 *
 *  Fri Feb 10 11:16:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_KOLAB_FOLDER_METADATA_H_
#define _E_KOLAB_FOLDER_METADATA_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gtk/gtk.h>

#include <e-util/e-util.h>
#include <shell/e-shell-view.h>

#include <libekolab/kolab-data-folder-metadata.h>

/*----------------------------------------------------------------------------*/

#define KOLAB_FOLDER_META_UI_NUM_TYPES 5

/*----------------------------------------------------------------------------*/

typedef struct _KolabFolderMetaUIWidgets KolabFolderMetaUIWidgets;
struct _KolabFolderMetaUIWidgets {
	GtkWidget *container;
	/* sub-widgets of container - folder type */
	GtkWidget *frame_type_select;
	GHashTable *folder_type_map;
	GSList *radio_group;
	gulong radio_btn_handler_id[KOLAB_FOLDER_META_UI_NUM_TYPES];
	GtkWidget *radio_btn_type[KOLAB_FOLDER_META_UI_NUM_TYPES];
	/* sub-widgets of container - folder options */
	GtkWidget *frame_options;
	GtkWidget *cbox_syncstrategy;
	gulong cbox_syncstrategy_id;
	/* sub-widgets of container - misc */
	GtkWidget *chk_btn_show_all;
	gulong chk_btn_show_all_handler_id;
};

typedef struct _KolabFolderMetaUIData KolabFolderMetaUIData;
struct _KolabFolderMetaUIData {
	EShellView *shell_view;
	KolabFolderContextID shell_context;
	EAlertBar *alert_bar;
	GtkDialog *dialog;
	KolabFolderMetaUIWidgets *widgets;
	KolabDataFolderMetadata *metadata;
	gchar *foldername;
	gchar *sourcename;
	gboolean changed_metadata;
	gboolean changed_syncstrategy;
	gboolean changed_visibility;
};

/*----------------------------------------------------------------------------*/

KolabFolderMetaUIData*
e_kolab_folder_metadata_ui_new (void);

void
e_kolab_folder_metadata_ui_free (KolabFolderMetaUIData *uidata);

void
e_kolab_folder_metadata_ui_update_from_uidata (KolabFolderMetaUIData *uidata);

gboolean
e_kolab_folder_metadata_ui_query_store (KolabFolderMetaUIData *uidata,
                                        GCancellable *cancellable,
                                        GError **err);
gboolean
e_kolab_folder_metadata_ui_write_store (KolabFolderMetaUIData *uidata,
                                        GCancellable *cancellable,
                                        GError **err);

/*----------------------------------------------------------------------------*/

#endif /* _E_KOLAB_FOLDER_METADATA_H_ */

/*----------------------------------------------------------------------------*/
