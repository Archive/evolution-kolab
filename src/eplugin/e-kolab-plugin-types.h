/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-plugin-types.h
 *
 *  Thu Feb 16 17:35:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ***************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_KOLAB_PLUGIN_TYPES_H_
#define _E_KOLAB_PLUGIN_TYPES_H_

/*----------------------------------------------------------------------------*/

typedef enum {
	KOLAB_WIDGET_TYPE_CALENDAR = 0,
	KOLAB_WIDGET_TYPE_CONTACT,
	KOLAB_WIDGET_LAST_TYPE
} KolabWidgetTypeID;

/*----------------------------------------------------------------------------*/

#endif /* _E_KOLAB_PLUGIN_TYPES_H_ */

/*----------------------------------------------------------------------------*/
