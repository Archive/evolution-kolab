/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-plugin-util.c
 *
 *  Sat Jun 02 15:28:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <config.h>

#include <glib/gi18n-lib.h>

#include <mail/em-config.h>
#include <mail/em-folder-tree.h>

#include <shell/e-shell-sidebar.h>
#include <shell/e-shell-window.h>

#include <libekolab/e-source-kolab-folder.h>
#include <libekolabutil/kolab-util-camel.h>
#include <libekolabutil/kolab-util-error.h>

#include "e-kolab-plugin-util.h"

/*----------------------------------------------------------------------------*/

#define KOLAB_E_ERROR_SIMPLE_SYSTEM_ERROR "system:simple-error"

/*----------------------------------------------------------------------------*/
/* internal statics (non-UI) */


/*----------------------------------------------------------------------------*/
/* internal statics (UI) */

static gchar*
kolab_plugin_util_ui_path_from_mail_view (EShellView *shell_view,
                                          gboolean *is_kolab_account_node,
                                          gboolean *is_kolab_folder_node,
                                          GError **err)
{
	CamelKolabIMAPXStore *kstore = NULL;
	gchar *selected_path = NULL;
	gboolean have_kolab = FALSE;
	GError *tmp_err = NULL;

	g_assert (E_IS_SHELL_VIEW (shell_view));

	have_kolab = e_kolab_plugin_util_ui_get_selected_store (shell_view,
	                                                        &kstore,
	                                                        &selected_path,
	                                                        &tmp_err);
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return NULL;
	}

	if (have_kolab) {
		*is_kolab_account_node = !selected_path || !*selected_path;
		*is_kolab_folder_node = !*is_kolab_account_node;
		g_object_unref (kstore);
	}

	return selected_path;
}

static gchar*
kolab_plugin_util_ui_path_from_pim_view (EShellView *shell_view,
                                         gboolean *is_kolab_account_node,
                                         gboolean *is_kolab_folder_node)
{
	const gchar *selected_path = NULL;
	ESource *source = NULL;
	ESourceResource *resource = NULL;
	gboolean is_source = FALSE;

	g_assert (E_IS_SHELL_VIEW (shell_view));

	*is_kolab_account_node = FALSE;
	*is_kolab_folder_node = FALSE;

	is_source = e_kolab_plugin_util_ui_get_selected_source (shell_view,
	                                                        &source);
	if (! is_source)
		goto exit;

	if (! e_source_has_extension (source, E_SOURCE_EXTENSION_KOLAB_FOLDER)) {
		g_warning ("%s()[%u] ESource of Kolab backend has no Kolab Folder Extension",
		           __func__, __LINE__);
		goto exit;
	}

	resource = E_SOURCE_RESOURCE (e_source_get_extension (source,
	                                                      E_SOURCE_EXTENSION_KOLAB_FOLDER));

	selected_path = e_source_resource_get_identity (resource);

	if (selected_path == NULL) {
		g_warning ("%s()[%u] selected path is NULL", __func__, __LINE__);
		goto exit;
	}

	*is_kolab_folder_node = TRUE;

 exit:
	return g_strdup (selected_path);
}

/*----------------------------------------------------------------------------*/
/* API functions (non-UI) */


const gchar*
e_kolab_plugin_util_group_name_from_action_entry_name (const gchar *entry_name)
{
	const gchar *group = NULL;

	if (entry_name == NULL)
		return NULL;

	if (strstr (entry_name, "calendar") != NULL)
		group = "calendar";
	else if (strstr (entry_name, "tasks") != NULL)
		group = "tasks";
	else if (strstr (entry_name, "memos") != NULL)
		group = "memos";
	else if (strstr (entry_name, "contacts") != NULL)
		group = "contacts";
	else
		g_return_val_if_reached (NULL);

	return group;
}

/*----------------------------------------------------------------------------*/
/* API functions (UI) */

GtkWidget*
e_kolab_plugin_util_ui_label_new (const gchar *text,
                                  gboolean add_bottom_space)
{
	GtkWidget *label = NULL;

	if (text == NULL)
		return NULL;

	label = gtk_label_new (text);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

	if (add_bottom_space)
		gtk_widget_set_margin_bottom (label, 5);

	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 1.0);

	return label;
}

GtkWidget*
e_kolab_plugin_util_ui_selected_folder_widget (const gchar *foldername,
                                               const gchar *sourcename)
{
	GtkGrid *grid = NULL;
	GtkWidget *label = NULL;
	gchar *labeltext = NULL;
	gint row = 0;

	g_return_val_if_fail (foldername != NULL, NULL);
	/* sourcname may be NULL */

	grid = GTK_GRID (gtk_grid_new ());
	gtk_grid_set_row_homogeneous (grid, FALSE);
	gtk_grid_set_row_spacing (grid, 6);
	gtk_grid_set_column_homogeneous (grid, FALSE);
	gtk_grid_set_column_spacing (grid, 16);
	gtk_container_set_border_width (GTK_CONTAINER (grid), 8);

	if (sourcename != NULL) {
		labeltext = g_strconcat ("<b>",
		                         C_("Kolab Folder Properties",
		                            "Selected Resource:"),
		                         "</b>",
		                         NULL);
		label = e_kolab_plugin_util_ui_label_new (labeltext, FALSE);
		g_free (labeltext);
		gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
		gtk_grid_attach (grid, label, 0, row, 1, 1);
		label = e_kolab_plugin_util_ui_label_new (sourcename, FALSE);
		gtk_grid_attach (grid, label, 1, row, 1, 1);
		row++;
	}

	labeltext = g_strconcat ("<b>",
	                         C_("Kolab Folder Properties",
	                            "Selected Folder:"),
	                         "</b>",
	                         NULL);
	label = e_kolab_plugin_util_ui_label_new (labeltext, FALSE);
	g_free (labeltext);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_grid_attach (grid, label, 0, row, 1, 1);
	label = e_kolab_plugin_util_ui_label_new (foldername, FALSE);
	gtk_grid_attach (grid, label, 1, row, 1, 1);

	return GTK_WIDGET (grid);
}

GtkWidget*
e_kolab_plugin_util_ui_dialog_ref_button (GtkDialog *dialog,
                                          const gchar *button_label,
                                          gboolean stock_check)
{
	GtkWidget *action_area = NULL;
	GList *btn_lst = NULL;
	GList *btn_lst_ptr = NULL;
	GtkWidget *btn = NULL;

	g_return_val_if_fail (GTK_IS_DIALOG (dialog), NULL);
	g_return_val_if_fail (button_label != NULL, NULL);

	action_area = gtk_dialog_get_action_area (dialog);
	g_return_val_if_fail (GTK_IS_BUTTON_BOX (action_area), NULL);

	btn_lst = gtk_container_get_children (GTK_CONTAINER (action_area));

	btn_lst_ptr = btn_lst;
	while (btn_lst_ptr != NULL) {
		GtkButton *btn_cand = NULL;
		const gchar *label = NULL;

		if (! GTK_IS_BUTTON (btn_lst_ptr->data)) {
			g_warning ("%s()[%u] GtkButtonBox contains non-GtkButton child, skipping",
			           __func__, __LINE__);
			continue;
		}

		btn_cand = GTK_BUTTON (btn_lst_ptr->data);

		if (stock_check && (! gtk_button_get_use_stock (btn_cand)))
			continue;

		label = gtk_button_get_label (btn_cand);
		if (g_strcmp0 (label, button_label) == 0) {
			btn = g_object_ref (btn_cand);
			break;
		}

		btn_lst_ptr = g_list_next (btn_lst_ptr);
	}

	if (btn_lst != NULL)
		g_list_free (btn_lst);

	return btn;
}

void
e_kolab_plugin_util_ui_dialog_set_button_sensitive (GtkDialog *dialog,
                                                    const gchar *button_label,
                                                    gboolean stock_check,
                                                    gboolean sensitive)
{
	GtkWidget *btn = NULL;

	g_return_if_fail (GTK_IS_DIALOG (dialog));
	g_return_if_fail (button_label != NULL);

	btn = e_kolab_plugin_util_ui_dialog_ref_button (dialog,
	                                                button_label,
	                                                stock_check);
	g_return_if_fail (GTK_IS_BUTTON (btn));
	gtk_widget_set_sensitive (btn, sensitive);
	g_object_unref (btn);
}

KolabFolderTypeID
e_kolab_plugin_util_ui_get_shell_type (EShellView *shell_view)
{
	KolabFolderTypeID ftype = KOLAB_FOLDER_TYPE_INVAL;
	const gchar *vname = NULL;

	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), KOLAB_FOLDER_TYPE_INVAL);

	vname = e_shell_view_get_name (shell_view);
	g_return_val_if_fail (vname != NULL, KOLAB_FOLDER_TYPE_INVAL);

	if (g_strcmp0 (vname, "mail") == 0)
		ftype = KOLAB_FOLDER_TYPE_EMAIL;
	else if (g_strcmp0 (vname, "addressbook") == 0)
		ftype = KOLAB_FOLDER_TYPE_CONTACT;
	else if (g_strcmp0 (vname, "calendar") == 0)
		ftype = KOLAB_FOLDER_TYPE_EVENT;
	else if (g_strcmp0 (vname, "tasks") == 0)
		ftype = KOLAB_FOLDER_TYPE_TASK;
	else if (g_strcmp0 (vname, "memos") == 0)
		ftype = KOLAB_FOLDER_TYPE_NOTE;
	else
		ftype = KOLAB_FOLDER_TYPE_INVAL;

	return ftype;
}

KolabFolderContextID
e_kolab_plugin_util_ui_get_shell_context (EShellView *shell_view)
{
	KolabFolderContextID context = KOLAB_FOLDER_CONTEXT_INVAL;
	KolabFolderTypeID ftype = KOLAB_FOLDER_TYPE_INVAL;

	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), KOLAB_FOLDER_CONTEXT_INVAL);

	ftype = e_kolab_plugin_util_ui_get_shell_type (shell_view);
	context = kolab_util_folder_type_map_to_context_id (ftype);

	return context;
}

gboolean
e_kolab_plugin_util_ui_get_selected_source (EShellView *shell_view,
                                            ESource **selected_source)
{
	ESource *source = NULL;
	EShellSidebar *shell_sidebar = NULL;
	ESourceSelector *selector = NULL;
	ESourceBackend *extension = NULL;
	const gchar *extension_name = NULL;
	const gchar *backend_name = NULL;
	gboolean is_kolab = FALSE;

	g_return_val_if_fail (shell_view != NULL, FALSE);
	g_return_val_if_fail (selected_source == NULL || *selected_source == NULL, FALSE);

	shell_sidebar = e_shell_view_get_shell_sidebar (shell_view);
	g_return_val_if_fail (shell_sidebar != NULL, FALSE);

	g_object_get (shell_sidebar, "selector", &selector, NULL);
	g_return_val_if_fail (selector != NULL, FALSE);

	source = e_source_selector_ref_primary_selection (selector);
	if (source == NULL)
		goto exit;

	extension_name = e_source_selector_get_extension_name (selector);
	if (extension_name == NULL)
		goto exit;

	extension = e_source_get_extension (source, extension_name);
	if (extension == NULL)
		goto exit;

	backend_name = e_source_backend_get_backend_name (extension);
	if (backend_name == NULL)
		goto exit;

	/* check whether we have a Kolab ESource selected */
	is_kolab = g_str_has_prefix (backend_name,  KOLAB_CAMEL_PROVIDER_PROTOCOL);

 exit:
	g_object_unref (selector);

	if (selected_source)
		*selected_source = source;
	else if (source)
		g_object_unref (source);

	return is_kolab;
}

gboolean
e_kolab_plugin_util_ui_get_selected_store (EShellView *shell_view,
                                           CamelKolabIMAPXStore **kstore,
                                           gchar **selected_path,
                                           GError **err)
{
	EShellSidebar *shell_sidebar = NULL;
	EMFolderTree *folder_tree = NULL;
	CamelStore *store = NULL;
	CamelProvider *provider = NULL;
	gchar *path = NULL;
	gboolean have_sel = FALSE;
	gboolean ok = FALSE;

	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), FALSE);
	g_return_val_if_fail (kstore != NULL && *kstore == NULL, FALSE);
	g_return_val_if_fail (selected_path != NULL && *selected_path == NULL, FALSE);

	shell_sidebar = e_shell_view_get_shell_sidebar (shell_view);
	g_return_val_if_fail (E_IS_SHELL_SIDEBAR (shell_sidebar), FALSE);

	g_object_get (shell_sidebar, "folder-tree", &folder_tree, NULL);
	g_return_val_if_fail (EM_IS_FOLDER_TREE (folder_tree), FALSE);

	have_sel = (em_folder_tree_get_selected (folder_tree, &store, &path) ||
	            em_folder_tree_store_root_selected (folder_tree, &store));

	if (! have_sel)
		goto exit;

	if (store == NULL)
		goto exit;

	provider = camel_service_get_provider (CAMEL_SERVICE (store));

	if (provider == NULL)
		goto exit;

	if (g_ascii_strcasecmp (provider->protocol, KOLAB_CAMEL_PROVIDER_PROTOCOL) == 0)
		ok = TRUE;
 exit:
	if (ok) {
		*selected_path = path;
		*kstore = CAMEL_KOLAB_IMAPX_STORE (store);
	} else {
		g_set_error (err,
		             KOLAB_CAMEL_KOLAB_ERROR,
		             KOLAB_CAMEL_KOLAB_ERROR_GENERIC,
		             _("Could not get the Kolab store from shell view!"));
		if (path != NULL)
			g_free (path);
		if (store != NULL)
			g_object_unref (store);
	}

	g_object_unref (folder_tree);

	return ok;
}

gchar*
e_kolab_plugin_util_ui_get_selected_path (EShellView *shell_view,
                                          gboolean *is_kolab_account_node,
                                          gboolean *is_kolab_folder_node,
                                          GError **err)
{
	KolabFolderContextID context = KOLAB_FOLDER_CONTEXT_INVAL;
	gchar *sel_path = NULL;

	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), NULL);

	context = e_kolab_plugin_util_ui_get_shell_context (shell_view);
	g_return_val_if_fail (context != KOLAB_FOLDER_CONTEXT_INVAL, NULL);

	if (context == KOLAB_FOLDER_CONTEXT_EMAIL)
		sel_path = kolab_plugin_util_ui_path_from_mail_view (shell_view,
		                                                     is_kolab_account_node,
		                                                     is_kolab_folder_node,
		                                                     err);
	else
		sel_path = kolab_plugin_util_ui_path_from_pim_view (shell_view,
		                                                    is_kolab_account_node,
		                                                    is_kolab_folder_node);
	return sel_path;
}

gchar*
e_kolab_plugin_util_ui_get_selected_sourcename (EShellView *shell_view)
{
	ESource *source = NULL;
	gboolean is_source = FALSE;
	const gchar *view_name = NULL;
	gchar *sourcename = NULL;

	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), NULL);

	view_name = e_shell_view_get_name (shell_view);
	if (g_strcmp0 (view_name, "mail") == 0)
		return NULL;

	is_source = e_kolab_plugin_util_ui_get_selected_source (shell_view,
	                                                        &source);
	if (is_source) {
		sourcename = e_source_dup_uid (source);
		g_object_unref (source);
	}

	return sourcename;
}

gchar*
e_kolab_plugin_util_ui_get_selected_foldername (EShellView *shell_view,
                                                GError **err)
{
	gchar *foldername = NULL;
	gboolean is_kolab_account_node = FALSE;
	gboolean is_kolab_folder_node = FALSE;
	GError *tmp_err = NULL;

	g_return_val_if_fail (E_IS_SHELL_VIEW (shell_view), NULL);

	foldername = e_kolab_plugin_util_ui_get_selected_path (shell_view,
	                                                       &is_kolab_account_node,
	                                                       &is_kolab_folder_node,
	                                                       &tmp_err);
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return NULL;
	}

	if (is_kolab_account_node || (! is_kolab_folder_node)) {
		if (foldername != NULL)
			g_free (foldername);
		return NULL;
	}

	return foldername;
}

EAlert*
e_kolab_plugin_util_ui_alert_new_from_gerror (GError *err)
{
	EAlert *alert = NULL;

	g_return_val_if_fail (err != NULL, NULL);
	g_return_val_if_fail (err->message != NULL, NULL);

	alert = e_alert_new (KOLAB_E_ERROR_SIMPLE_SYSTEM_ERROR,
	                     err->message,
	                     NULL);
	return alert;
}

void
e_kolab_plugin_util_ui_alert_bar_add_error (EAlertBar *alert_bar,
                                            GError *err)
{
	EAlert *alert = NULL;

	g_return_if_fail (E_IS_ALERT_BAR (alert_bar));

	if (err == NULL)
		return;

	alert = e_kolab_plugin_util_ui_alert_new_from_gerror (err);

	if (alert == NULL)
		return;

	e_alert_bar_add_alert (alert_bar, alert);
}

/*----------------------------------------------------------------------------*/
