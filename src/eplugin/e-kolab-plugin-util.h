/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            e-kolab-plugin-util.h
 *
 *  Sat Jun 02 15:28:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _E_KOLAB_PLUGIN_UTIL_H_
#define _E_KOLAB_PLUGIN_UTIL_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gtk/gtk.h>

#include <e-util/e-util.h>
#include <shell/e-shell-view.h>

#include <libekolab/camel-kolab-imapx-store.h>

/*----------------------------------------------------------------------------*/

const gchar*
e_kolab_plugin_util_group_name_from_action_entry_name (const gchar *entry_name);

GtkWidget*
e_kolab_plugin_util_ui_label_new (const gchar *text,
                                  gboolean add_bottom_space);

GtkWidget*
e_kolab_plugin_util_ui_selected_folder_widget (const gchar *foldername,
                                               const gchar *sourcename);

GtkWidget*
e_kolab_plugin_util_ui_dialog_ref_button (GtkDialog *dialog,
                                          const gchar *button_label,
                                          gboolean stock_check);

void
e_kolab_plugin_util_ui_dialog_set_button_sensitive (GtkDialog *dialog,
                                                    const gchar *button_label,
                                                    gboolean stock_check,
                                                    gboolean sensitive);

KolabFolderTypeID
e_kolab_plugin_util_ui_get_shell_type (EShellView *shell_view);

KolabFolderContextID
e_kolab_plugin_util_ui_get_shell_context (EShellView *shell_view);

gboolean
e_kolab_plugin_util_ui_get_selected_source (EShellView *shell_view,
                                            ESource **selected_source);

gboolean
e_kolab_plugin_util_ui_get_selected_store (EShellView *shell_view,
                                           CamelKolabIMAPXStore **kstore,
                                           gchar **selected_path,
                                           GError **err);

gchar*
e_kolab_plugin_util_ui_get_selected_path (EShellView *shell_view,
                                          gboolean *is_kolab_account_node,
                                          gboolean *is_kolab_folder_node,
                                          GError **err);

gchar*
e_kolab_plugin_util_ui_get_selected_sourcename (EShellView *shell_view);

gchar*
e_kolab_plugin_util_ui_get_selected_foldername (EShellView *shell_view,
                                                GError **err);

EAlert*
e_kolab_plugin_util_ui_alert_new_from_gerror (GError *err);

void
e_kolab_plugin_util_ui_alert_bar_add_error (EAlertBar *alert_bar,
                                            GError *err);

/*----------------------------------------------------------------------------*/

#endif /* _E_KOLAB_PLUGIN_UTIL_H_ */

/*----------------------------------------------------------------------------*/
