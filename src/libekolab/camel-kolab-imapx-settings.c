/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-settings.c
 *
 *  2011-12-02, 19:11:12
 *  Copyright 2011, Christian Hilberg
 *  <hilberg@unix-ag.org>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "camel-kolab-imapx-settings.h"

#include <libedataserver/libedataserver.h>

#include <libekolab/kolab-enumtypes.h>

/*----------------------------------------------------------------------------*/

struct _CamelKolabIMAPXSettingsPrivate {
	GMutex property_lock;
	gchar *pkcs11_pin;
};

enum {
	PROP_0,
	PROP_PKCS11_PIN
};

#define CAMEL_KOLAB_IMAPX_SETTINGS_PRIVATE(obj)  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), CAMEL_TYPE_KOLAB_IMAPX_SETTINGS, CamelKolabIMAPXSettingsPrivate))

G_DEFINE_TYPE (CamelKolabIMAPXSettings, camel_kolab_imapx_settings, CAMEL_TYPE_IMAPX_SETTINGS)

/*----------------------------------------------------------------------------*/
/* object/class init */

static void
camel_kolab_imapx_settings_set_property (GObject *object,
                                         guint property_id,
                                         const GValue *value,
                                         GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_PKCS11_PIN:
			camel_kolab_imapx_settings_set_pkcs11_pin (
				CAMEL_KOLAB_IMAPX_SETTINGS (object),
				g_value_get_string (value));
			return;
		default:
			break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
camel_kolab_imapx_settings_get_property (GObject *object,
                                         guint property_id,
                                         GValue *value,
                                         GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_PKCS11_PIN:
			g_value_set_string (
				value,
				camel_kolab_imapx_settings_get_pkcs11_pin (
				CAMEL_KOLAB_IMAPX_SETTINGS (object)));
			return;
		default:
			break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
camel_kolab_imapx_settings_finalize (GObject *object)
{
	CamelKolabIMAPXSettingsPrivate *priv;

	priv = CAMEL_KOLAB_IMAPX_SETTINGS_PRIVATE (object);

	g_mutex_clear (&(priv->property_lock));

	g_free (priv->pkcs11_pin);

	/* Chain up to parent's finalize() method. */
	G_OBJECT_CLASS (camel_kolab_imapx_settings_parent_class)->finalize (object);
}

static void
camel_kolab_imapx_settings_class_init (CamelKolabIMAPXSettingsClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	g_type_class_add_private (klass, sizeof (CamelKolabIMAPXSettingsPrivate));

	object_class->set_property = camel_kolab_imapx_settings_set_property;
	object_class->get_property = camel_kolab_imapx_settings_get_property;
	object_class->finalize = camel_kolab_imapx_settings_finalize;

	g_object_class_install_property (
		object_class,
		PROP_PKCS11_PIN,
		g_param_spec_string (
			"pkcs11-pin",
			"PKCS #11 PIN",
			"PKCS #11 certificate PIN",
			NULL,
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT |
			G_PARAM_STATIC_STRINGS));
}

static void
camel_kolab_imapx_settings_init (CamelKolabIMAPXSettings *self)
{
	self->priv = CAMEL_KOLAB_IMAPX_SETTINGS_PRIVATE (self);
	g_mutex_init (&(self->priv->property_lock));
}

/*----------------------------------------------------------------------------*/
/* API functions */

CamelURL *
camel_kolab_imapx_settings_build_url (CamelKolabIMAPXSettings *settings)
{
	CamelURL *url = NULL;
	CamelNetworkSettings *network_settings = NULL;
	CamelNetworkSecurityMethod method;
	const gchar *host = NULL;
	const gchar *user = NULL;
	guint16 port = 0;

	g_return_val_if_fail (CAMEL_IS_KOLAB_IMAPX_SETTINGS (settings), NULL);

	network_settings = CAMEL_NETWORK_SETTINGS (settings);

	host = camel_network_settings_get_host (network_settings);
	port = camel_network_settings_get_port (network_settings);
	user = camel_network_settings_get_user (network_settings);
	method = camel_network_settings_get_security_method (network_settings);

	url = g_new0 (CamelURL, 1);

	switch (method) {
		case CAMEL_NETWORK_SECURITY_METHOD_SSL_ON_ALTERNATE_PORT:
		case CAMEL_NETWORK_SECURITY_METHOD_STARTTLS_ON_STANDARD_PORT:
			camel_url_set_protocol (url, "https");
			break;
		default:
			camel_url_set_protocol (url, "http");
			break;
	}

	camel_url_set_host (url, host);
	camel_url_set_port (url, port);
	camel_url_set_user (url, user);

	return url;
}

const gchar *
camel_kolab_imapx_settings_get_pkcs11_pin (CamelKolabIMAPXSettings *settings)
{
	g_return_val_if_fail (CAMEL_IS_KOLAB_IMAPX_SETTINGS (settings), NULL);

	return settings->priv->pkcs11_pin;
}

gchar *
camel_kolab_imapx_settings_dup_pkcs11_pin (CamelKolabIMAPXSettings *settings)
{
	const gchar *protected;
	gchar *duplicate;

	g_return_val_if_fail (CAMEL_IS_KOLAB_IMAPX_SETTINGS (settings), NULL);

	g_mutex_lock (&(settings->priv->property_lock));

	protected = camel_kolab_imapx_settings_get_pkcs11_pin (settings);
	duplicate = g_strdup (protected);

	g_mutex_unlock (&(settings->priv->property_lock));

	return duplicate;
}

void
camel_kolab_imapx_settings_set_pkcs11_pin (CamelKolabIMAPXSettings *settings,
                                           const gchar *pkcs11_pin)
{
	g_return_if_fail (CAMEL_IS_KOLAB_IMAPX_SETTINGS (settings));

	g_mutex_lock (&(settings->priv->property_lock));

	g_free (settings->priv->pkcs11_pin);
	settings->priv->pkcs11_pin = e_util_strdup_strip (pkcs11_pin);

	g_mutex_unlock (&(settings->priv->property_lock));

	g_object_notify (G_OBJECT (settings), "pkcs11-pin");
}

/*----------------------------------------------------------------------------*/
