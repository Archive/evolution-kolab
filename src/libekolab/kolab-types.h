/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-types.h
 *
 *  Thu Feb 03 20:19:20 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_TYPES_H_
#define _KOLAB_TYPES_H_

#include <glib.h>

/*----------------------------------------------------------------------------*/

/* transport layer security */
typedef enum {
	KOLAB_TLS_VARIANT_NONE = 0,
	KOLAB_TLS_VARIANT_SSL,
	KOLAB_TLS_VARIANT_STARTTLS,
	KOLAB_TLS_LAST_VARIANT,                                   /*< skip >*/
	KOLAB_TLS_FIRST_VARIANT = KOLAB_TLS_VARIANT_NONE,         /*< skip >*/
	KOLAB_TLS_VARIANT_DEFAULT = KOLAB_TLS_VARIANT_STARTTLS    /*< skip >*/
} KolabTLSVariantID;

#define KOLAB_TLS_DESC_VARIANT_NONE "No encryption"
#define KOLAB_TLS_DESC_VARIANT_SSL  "SSL encryption"
#define KOLAB_TLS_DESC_VARIANT_TLS  "TLS encryption"

#define KOLAB_TLS_VARIANT_PROP "kolab-encryption-method"
#define KOLAB_PORT_NUMBER_PROP "kolab-port-number"

/* PIM data conflict resolution variants */
typedef enum {
	KOLAB_SYNC_STRATEGY_NEWEST = 0,	/* use latest entry (timestamp)	*/
	KOLAB_SYNC_STRATEGY_SERVER,	/* always use server data set	*/
	KOLAB_SYNC_STRATEGY_CLIENT,	/* always use client data set	*/
	KOLAB_SYNC_STRATEGY_DUPE,	/* create duplicate (new UID)	*/
	KOLAB_SYNC_LAST_STRATEGY,                                 /*< skip >*/
	KOLAB_SYNC_FIRST_STRATEGY = KOLAB_SYNC_STRATEGY_NEWEST,   /*< skip >*/
	KOLAB_SYNC_STRATEGY_DEFAULT = KOLAB_SYNC_STRATEGY_NEWEST  /*< skip >*/
} KolabSyncStrategyID;

#define KOLAB_STRATEGY_DESC_NEWEST "Take Newer (last modified)"
#define KOLAB_STRATEGY_DESC_SERVER "Take Remote (server-side)"
#define KOLAB_STRATEGY_DESC_CLIENT "Take Local (client-side)"
#define KOLAB_STRATEGY_DESC_DUPE   "Take Both (resulting in two different, parallel entries)"

typedef enum {
	KOLAB_PKCS11_INFRASTRUCTURE_DISABLE,
	KOLAB_PKCS11_INFRASTRUCTURE_ENABLE,
	KOLAB_PKCS11_INFRASTRUCTURE_LAST,                                          /*< skip >*/
	KOLAB_PKCS11_INFRASTRUCTURE_FIRST = KOLAB_PKCS11_INFRASTRUCTURE_DISABLE,   /*< skip >*/
	KOLAB_PKCS11_INFRASTRUCTURE_DEFAULT = KOLAB_PKCS11_INFRASTRUCTURE_DISABLE  /*< skip >*/
} KolabReqPkcs11;

#define KOLAB_REQ_PKCS11_PROP	"kolab-require-pkcs11-infrastructure"
#define KOLAB_PKCS11_PIN_PROP	"kolab-pkcs11-pin"

/* Evolution-Kolab introduces client certificate secured connections to services
 * on a Kolab server. It utilizes certificates from PKCS #11 tokens. Access to
 * PKCS #11 tokens is secured by means of a PIN. Intention of the concepts of
 * client certificates and security tokens is, to increase security. This
 * implementation serves as a feasability study concerning PKCS #11 deployment
 * and the implementation of Kolab EPlugin stores the PIN in plain within the
 * associated  esource entry. THIS IS BY NO MEANS SECURE! It is intended as a
 * simple workaround of limitations of current (2.30) implementation of
 * evolution. DO NOT USE IN PRODUCTION ENVIRONMENTS!
 */

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_TYPES_H_ */

/*----------------------------------------------------------------------------*/
