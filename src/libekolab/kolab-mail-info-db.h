/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-info-db.h
 *
 *  Wed Jan 26 11:42:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 
/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-mail-info-db
 * @short_description: the Kolab2 PIM metadata database
 * @title: KolabMailInfoDb
 * @section_id:
 * @see_also: #KolabMailSynchronizer, #KolabMailSideCache, #KolabMailImapClient, #KolabMailSummary, #KolabFolderSummary
 * @stability: unstable
 * 
 * This class gathers information about Kolab mail objects and folders and
 * stores the info in a persistent manner (SQLite-DB). The information gathered
 * and stored for PIM emails includes
 * <itemizedlist>
 *   <listitem>Kolab UID, this is the primary key</listitem>
 *   <listitem>IMAP UID (if available)</listitem>
 *   <listitem>location of the mail object
 *     <itemizedlist>
 *       <listitem>in the #KolabMailImapClient disk cache</listitem>
 *       <listitem>in the #KolabMailSideCache disk cache</listitem>
 *       <listitem>both locations (means sync situation)</listitem>
 *     </itemizedlist>
 *   </listitem>
 *   <listitem>the folder name (i.e. calendar name)</listitem>
 *   <listitem>start date, end date (for quick search)</listitem>
 *   <listitem>alarms, recurrences, ... (for quick search)</listitem>
 *   <listitem>...</listitem>
 * </itemizedlist>
 *
 * For more detail on which metadata is stored per PIM object, see
 * #KolabMailSummary. For folders, refer to #KolabFolderSummary.
 *
 * The information stored via this class is updated by
 * #KolabMailSynchronizer, which has access to #KolabMailSideCache and
 * #KolabMailImapClient. The #KolabMailSynchronizer itself is operated by
 * #KolabMailAccess.
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_MAIL_INFO_DB_H_
#define _KOLAB_MAIL_INFO_DB_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include "kolab-settings-handler.h"
#include "kolab-mail-summary.h"
#include "kolab-folder-summary.h"
#include "kolab-backend-types.h"
#include "kolab-mail-info-db-record.h"

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

#define KOLAB_TYPE_MAIL_INFO_DB             (kolab_mail_info_db_get_type ())
#define KOLAB_MAIL_INFO_DB(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOLAB_TYPE_MAIL_INFO_DB, KolabMailInfoDb))
#define KOLAB_MAIL_INFO_DB_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), KOLAB_TYPE_MAIL_INFO_DB, KolabMailInfoDbClass))
#define KOLAB_IS_MAIL_INFO_DB(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOLAB_TYPE_MAIL_INFO_DB))
#define KOLAB_IS_MAIL_INFO_DB_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), KOLAB_TYPE_MAIL_INFO_DB))
#define KOLAB_MAIL_INFO_DB_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), KOLAB_TYPE_MAIL_INFO_DB, KolabMailInfoDbClass))

typedef struct _KolabMailInfoDbClass KolabMailInfoDbClass;
typedef struct _KolabMailInfoDb KolabMailInfoDb;

struct _KolabMailInfoDbClass
{
	GObjectClass parent_class;
};

struct _KolabMailInfoDb
{
	GObject parent_instance;
};

GType kolab_mail_info_db_get_type (void) G_GNUC_CONST;

/*----------------------------------------------------------------------------*/

gboolean kolab_mail_info_db_configure  (KolabMailInfoDb *self, KolabSettingsHandler *ksettings, GError **err);
gboolean kolab_mail_info_db_bringup (KolabMailInfoDb *self, GError **err);
gboolean kolab_mail_info_db_shutdown (KolabMailInfoDb *self, GError **err);

GList* kolab_mail_info_db_query_uids (KolabMailInfoDb *self, const gchar *foldername, const gchar *sexp, gboolean sidecache_only, gboolean include_deleted, GError **err);
GList* kolab_mail_info_db_query_changed_uids (KolabMailInfoDb *self, const gchar *foldername, const gchar *sexp, gboolean unset_changed, GError **err);

GList* kolab_mail_info_db_query_foldernames (KolabMailInfoDb *self, GError **err);
GList* kolab_mail_info_db_query_foldernames_anon (gpointer self, GError **err);
gboolean kolab_mail_info_db_exists_foldername (KolabMailInfoDb *self, const gchar *foldername, GError **err);

KolabFolderSummary* kolab_mail_info_db_query_folder_summary (KolabMailInfoDb *self, const gchar *foldername, GError **err);
gboolean kolab_mail_info_db_update_folder_summary (KolabMailInfoDb *self, const KolabFolderSummary *summary, GError **err);
gboolean kolab_mail_info_db_remove_folder (KolabMailInfoDb *self, const gchar *foldername, GError **err);

KolabMailSummary* kolab_mail_info_db_query_mail_summary (KolabMailInfoDb *self, const gchar *uid, const gchar *foldername, GError **err);

KolabMailInfoDbRecord* kolab_mail_info_db_query_record (KolabMailInfoDb *self, const gchar *uid, const gchar *foldername, GError **err);
gboolean kolab_mail_info_db_update_record (KolabMailInfoDb *self, const KolabMailInfoDbRecord *record, const gchar *foldername, GError **err);
gboolean kolab_mail_info_db_remove_record (KolabMailInfoDb *self, const gchar *uid, const gchar *foldername, GError **err);

gboolean kolab_mail_info_db_transaction_start (KolabMailInfoDb *self, GError **err);
gboolean kolab_mail_info_db_transaction_commit (KolabMailInfoDb *self, GError **err);
gboolean kolab_mail_info_db_transaction_abort (KolabMailInfoDb *self, GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_MAIL_INFO_DB_H_ */

/*----------------------------------------------------------------------------*/
