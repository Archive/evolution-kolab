/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-store.c
 *
 *  2011-12-05, 19:11:27
 *  Copyright 2011, Christian Hilberg
 *  <hilberg@unix-ag.org>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>

#include "camel-imapx-extd-store.h"
#include "camel-imapx-extd-store-friend.h"

/*----------------------------------------------------------------------------*/

static GInitableIface *parent_initable_iface = NULL;
static CamelNetworkServiceInterface *parent_service_iface = NULL;
static CamelSubscribableInterface *parent_subscribable_iface = NULL;

static CamelServiceClass *parent_service_class = NULL;
static CamelStoreClass *parent_store_class = NULL;

/*----------------------------------------------------------------------------*/

/* forward declarations */
static gboolean imapx_extd_store_register_capability_flags (CamelIMAPXExtdStore *self);
static void imapx_extd_store_initable_init (GInitableIface *interface);
static void imapx_extd_store_network_service_init (CamelNetworkServiceInterface *interface);
static void imapx_extd_store_subscribable_init (CamelSubscribableInterface *interface);

/* externs */
extern CamelServiceAuthType camel_imapx_password_authtype;

typedef struct _CamelIMAPXExtdStorePrivate CamelIMAPXExtdStorePrivate;
struct _CamelIMAPXExtdStorePrivate {
	CamelImapxMetadata *md; /* raw annotation data (different from CamelKolabImapxMetadata) */
	CamelImapxAcl *acl; /* IMAP ACL data */
	guint32 imapx_capa_flag_ids[CAMEL_IMAPX_EXTD_STORE_CAPA_LAST_FLAG];
};

#define CAMEL_IMAPX_EXTD_STORE_PRIVATE(obj)  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), CAMEL_TYPE_IMAPX_EXTD_STORE, CamelIMAPXExtdStorePrivate))

G_DEFINE_TYPE_WITH_CODE (CamelIMAPXExtdStore,
                         camel_imapx_extd_store,
                         CAMEL_TYPE_IMAPX_STORE,
                         G_IMPLEMENT_INTERFACE (G_TYPE_INITABLE,
                                                imapx_extd_store_initable_init)
                         G_IMPLEMENT_INTERFACE (CAMEL_TYPE_NETWORK_SERVICE,
                                                imapx_extd_store_network_service_init)
                         G_IMPLEMENT_INTERFACE (CAMEL_TYPE_SUBSCRIBABLE,
                                                imapx_extd_store_subscribable_init))

/*----------------------------------------------------------------------------*/
/* object init */

static void
camel_imapx_extd_store_init (CamelIMAPXExtdStore *self)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;
	CamelIMAPXStore *istore = NULL;
	CamelIMAPXConnManager *cm = NULL;
	gint ii = 0;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));

	istore = CAMEL_IMAPX_STORE (self);
	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	priv->md = camel_imapx_metadata_new (CAMEL_IMAPX_METADATA_PROTO_INVAL,
	                                     FALSE);
	for (ii = 0; ii < CAMEL_IMAPX_EXTD_STORE_CAPA_LAST_FLAG; ii++)
		priv->imapx_capa_flag_ids[ii] = 0;
	priv->acl = camel_imapx_acl_new (FALSE);

	/* remove existing conn manager.
	 * should not normally be necessary
	 */
	if (istore->con_man != NULL) {
		camel_imapx_conn_manager_close_connections (istore->con_man);
		g_object_unref (istore->con_man);
	}

	/* create conn manager, which will create
	 * a CamelIMAPXServer for us
	 */
	cm = camel_imapx_conn_manager_new (CAMEL_STORE (self));
	istore->con_man = CAMEL_IMAPX_CONN_MANAGER (cm);
}

static void
camel_imapx_extd_store_dispose (GObject *object)
{
	CamelIMAPXExtdStore *self = NULL;
	CamelIMAPXStore *istore = NULL;
	GCancellable *cancellable = NULL;
	GError *tmp_err = NULL;

	self = CAMEL_IMAPX_EXTD_STORE (object);
	istore = CAMEL_IMAPX_STORE (self);

	/* disconnect service and unref the connection manager.
	 * see imapx_store_dispose() in camel-imapx-store.c
	 */
	if (istore->con_man != NULL) {
		cancellable = g_cancellable_new ();
		camel_service_disconnect_sync (CAMEL_SERVICE (self),
		                               TRUE,
		                               cancellable,
		                               &tmp_err);

		/* this part will now be skipped
		 * in the parent's dispose() function
		 */
		g_object_unref (istore->con_man);
		istore->con_man = NULL;

		if (tmp_err != NULL) {
			g_warning ("%s()[%u]: %s",
			           __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
			tmp_err = NULL;
		}

		(void)g_cancellable_set_error_if_cancelled (cancellable,
		                                            &tmp_err);
		if (tmp_err != NULL) {
			g_warning ("%s()[%u]: %s",
			           __func__, __LINE__, tmp_err->message);
			g_error_free (tmp_err);
			tmp_err = NULL;
		}

		/* do we need to do anything else before
		 * unref()ing here (e.g. reset())?
		 */
		g_object_unref (cancellable);
	}

	G_OBJECT_CLASS (camel_imapx_extd_store_parent_class)->dispose (object);
}

static void
camel_imapx_extd_store_finalize (GObject *object)
{
	CamelIMAPXExtdStore *self = NULL;
	CamelIMAPXExtdStorePrivate *priv = NULL;

	self = CAMEL_IMAPX_EXTD_STORE (object);
	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	if (priv->md != NULL)
		camel_imapx_metadata_free (priv->md);
	if (priv->acl != NULL)
		camel_imapx_acl_free (priv->acl);

	G_OBJECT_CLASS (camel_imapx_extd_store_parent_class)->finalize (object);
}

/*----------------------------------------------------------------------------*/
/* internal statics */

static void
imapx_extd_store_set_capa_flag_id (CamelIMAPXExtdStore *self,
                                   camel_imapx_extd_store_capa_flag_t flag,
                                   guint32 id)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (flag < CAMEL_IMAPX_EXTD_STORE_CAPA_LAST_FLAG);

	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	priv->imapx_capa_flag_ids[flag] = id;
}

static gboolean
imapx_extd_store_register_capability_flags (CamelIMAPXExtdStore *self)
{
	guint32 capa_flag_id = 0;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));

	/* Registering the capability flags must be done
	 * before CamelIMAPXServer init. The ConnManager
	 * hands us out an inited and already connected
	 * CamelIMAPXServer instance, which will already
	 * have seen the server capabilities (and it will
	 * have ignored the extended ones it did not know
	 * about then). Hence, we need to make known to the
	 * IMAPX utils the new flags before creating the
	 * first CamelIMAPXServer instance. The registration
	 * is not per CamelIMAPXServer, but global.
	 * It is safe to call imapx_register_capability()
	 * multiple times (if a capa is already registered, its
	 * id is returned immediately).
	 */

	/* ANNOTATEMORE server capability flag */
	capa_flag_id = imapx_register_capability (IMAPX_IMAP_TOKEN_ANNOTATEMORE);
	g_return_val_if_fail (capa_flag_id > 0, FALSE);
	imapx_extd_store_set_capa_flag_id (self,
	                                   CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ANNOTATEMORE,
	                                   capa_flag_id);

	/* (register METADATA server capability flag here) */

	/* ACL server capability flag */
	capa_flag_id = imapx_register_capability (IMAPX_IMAP_TOKEN_ACL);
	g_return_val_if_fail (capa_flag_id > 0, FALSE);
	imapx_extd_store_set_capa_flag_id (self,
	                                   CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ACL,
	                                   capa_flag_id);
	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* class functions */

static CamelFolderInfo*
imapx_extd_store_create_folder_sync (CamelStore *self,
                                     const gchar *parentname,
                                     const gchar *foldername,
                                     GCancellable *cancellable,
                                     GError **err)
{
	CamelFolderInfo *fi = NULL;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	/* parentname may be NULL */
	g_assert (foldername != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	fi = parent_store_class->create_folder_sync (self,
	                                             parentname,
	                                             foldername,
	                                             cancellable,
	                                             err);
	/* the folder annotation needs to be set by the
	 * class which uses CamelIMAPXExtdStore (i.e.,
	 * CamelKolabIMAPXStore)
	 */

	return fi;
}

static gboolean
imapx_extd_store_delete_folder_sync (CamelStore *self,
                                     const gchar *foldername,
                                     GCancellable *cancellable,
                                     GError **err)
{
	CamelIMAPXExtdStore *myself = NULL;
	CamelIMAPXExtdStorePrivate *priv = NULL;

	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (foldername != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = parent_store_class->delete_folder_sync (self,
	                                             foldername,
	                                             cancellable,
	                                             err);
	if (ok) {
		myself = CAMEL_IMAPX_EXTD_STORE (self);
		priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (myself);

		/* delete folder metadata entry from our
		 * hash table so we will not return a wrong
		 * folder annotation if a folder with the
		 * same name but different annotation gets
		 * created later on
		 */
		(void) camel_imapx_metadata_remove_metadata (priv->md,
		                                             foldername);
	}

	return ok;
}

static gboolean
imapx_extd_store_rename_folder_sync (CamelStore *self,
                                     const gchar *oldname,
                                     const gchar *newname,
                                     GCancellable *cancellable,
                                     GError **err)
{
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (oldname != NULL);
	g_assert (newname != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = parent_store_class->rename_folder_sync (self,
	                                             oldname,
	                                             newname,
	                                             cancellable,
	                                             err);

	/* FIXME update annotation data on server */
	g_warning ("%s()[%u] FIXME update annotation data on server",
	           __func__, __LINE__);

	return ok;
}

static gboolean
imapx_extd_store_synchronize_sync (CamelStore *self,
                                   gboolean expunge,
                                   GCancellable *cancellable,
                                   GError **err)
{
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = parent_store_class->synchronize_sync (self,
	                                           expunge,
	                                           cancellable,
	                                           err);
	return ok;
}

static CamelIMAPXServer*
imapx_extd_store_get_server (CamelIMAPXStore *self,
                             const gchar *foldername,
                             GCancellable *cancellable,
                             GError **err)
{
	CamelIMAPXServer *server = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	/* foldername may be NULL */
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	server = camel_imapx_conn_manager_get_connection (self->con_man,
	                                                  foldername,
	                                                  cancellable,
	                                                  &tmp_err);

	if (server == NULL) {
		g_propagate_error (err, tmp_err);
		return NULL;
	}

	g_return_val_if_fail (CAMEL_IS_IMAPX_SERVER (server), NULL);

	/* this can be called multiple times,
	 * which is okay (init() just returns
	 * TRUE if already inited)
	 */
	ok = camel_imapx_extd_server_init (server,
	                                   cancellable,
	                                   &tmp_err);
	if (! ok) {
		g_propagate_error (err, tmp_err);
		g_object_unref (server);
		return NULL;
	}

	return server;
}

static camel_imapx_metadata_proto_t
imapx_extd_store_metadata_get_proto (CamelIMAPXExtdStore *self)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;
	camel_imapx_metadata_proto_t proto = CAMEL_IMAPX_METADATA_PROTO_INVAL;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));

	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	if (priv->md == NULL) {
		/* TODO implement online folder annotation query
		 *      in case there is no metadata as yet
		 *      (decide the protocol here, depending on
		 *      the IMAP server's untagged response)
		 */
		g_warning ("%s: FIXME implement online annotation protocol query",
		           __func__);
		return CAMEL_IMAPX_METADATA_PROTO_INVAL; /* FIXME */
	}

	proto = camel_imapx_metadata_get_proto (priv->md);

	if (proto != CAMEL_IMAPX_METADATA_PROTO_INVAL)
		return proto;

	/* Set the protocol type according to the
	 * announced server capability, if we have
	 * one advertised by the server (we assume
	 * that the server advertises either one
	 * or none, but not both)
	 */

	/* ANNOTATEMORE */
	if (priv->imapx_capa_flag_ids[CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ANNOTATEMORE]) {
		proto = CAMEL_IMAPX_METADATA_PROTO_ANNOTATEMORE;
		goto skip;
	}

	/* METADATA */
	if (priv->imapx_capa_flag_ids[CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_METADATA]) {
		proto = CAMEL_IMAPX_METADATA_PROTO_METADATA;
		goto skip;
	}

 skip:
	if (proto != CAMEL_IMAPX_METADATA_PROTO_INVAL) {
		if (! camel_imapx_metadata_set_proto (priv->md, proto))
			proto = CAMEL_IMAPX_METADATA_PROTO_INVAL;
	}

	return proto;
}

static CamelImapxMetadata*
imapx_extd_store_get_metadata (CamelIMAPXExtdStore *self,
                               CamelImapxMetadataSpec *spec,
                               gboolean do_resect,
                               GCancellable *cancellable,
                               GError **err)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;
	CamelIMAPXServer *is = NULL;
	CamelImapxMetadata *md = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (spec != NULL);
	g_assert (spec->mailbox_name != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	is = imapx_extd_store_get_server (CAMEL_IMAPX_STORE (self),
	                                  spec->mailbox_name,
	                                  cancellable,
	                                  &tmp_err);
	if (is == NULL) {
		g_propagate_error (err, tmp_err);
		return NULL;
	}

	ok = camel_imapx_extd_server_get_metadata (is,
	                                           spec,
	                                           cancellable,
	                                           &tmp_err);
	if (! ok) {
		g_propagate_error (err, tmp_err);
		return NULL;
	}

	/* FIXME
	 *
	 * we would now need to wait for the IMAP untagged
	 * response, so the handler function can process it
	 * and put the result into the metadata hashtables.
	 *
	 */

	/* only if do_resect==TRUE we do return the metadata,
	 * otherwise we only get it from the server and leave
	 * the result inside this object for later resection
	 */
	if (do_resect)
		md = camel_imapx_metadata_resect (priv->md);

	return md;
}

static gboolean
imapx_extd_store_set_metadata (CamelIMAPXExtdStore *self,
                               CamelImapxMetadata *md,
                               GCancellable *cancellable,
                               GError **err)
{
	/* CamelIMAPXExtdStorePrivate *priv = NULL; */
	CamelIMAPXServer *is = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (md != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self); */

	is = imapx_extd_store_get_server (CAMEL_IMAPX_STORE (self),
	                                  NULL,
	                                  cancellable,
	                                  &tmp_err);
	if (is == NULL) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	ok = camel_imapx_extd_server_set_metadata (is,
	                                           md,
	                                           cancellable,
	                                           &tmp_err);
	if (! ok) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	/* TODO should we update local metadata information
	 *      right away here?
	 *
	 *      Pro: This will be needed for supporting
	 *           non-Kolab IMAP servers anyway (but
	 *           could be limited to that use case)
	 *
	 *      Con: If we read-back the information from
	 *           the server on need, we can be sure it
	 *           has previously been successfully set
	 */

	return TRUE;
}

static CamelImapxAcl*
imapx_extd_store_get_acl (CamelIMAPXExtdStore *self,
                          CamelImapxAclSpec *spec,
                          gboolean do_resect,
                          GCancellable *cancellable,
                          GError **err)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;
	CamelIMAPXServer *is = NULL;
	CamelImapxAcl *acl = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (spec != NULL);
	g_assert (spec->mbox_name != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	is = imapx_extd_store_get_server (CAMEL_IMAPX_STORE (self),
	                                  NULL,
	                                  cancellable,
	                                  &tmp_err);
	if (is == NULL)
		goto exit;

	if (spec->type & CAMEL_IMAPX_ACL_TYPE_GENERAL) {
		ok = camel_imapx_extd_server_get_acl (is,
		                                      spec->mbox_name,
		                                      cancellable,
		                                      &tmp_err);
		if (! ok)
			goto exit;
	}

	if (spec->type & CAMEL_IMAPX_ACL_TYPE_MYRIGHTS) {
		ok = camel_imapx_extd_server_get_myrights (is,
		                                           spec->mbox_name,
		                                           cancellable,
		                                           &tmp_err);
		if (! ok)
			goto exit;
	}

	if (do_resect)
		acl = camel_imapx_acl_resect (priv->acl);

 exit:
	if (tmp_err != NULL)
		g_propagate_error (err, tmp_err);

	return acl;
}

static gboolean
imapx_extd_store_set_acl (CamelIMAPXExtdStore *self,
                          const gchar *foldername,
                          const GList *entries,
                          GCancellable *cancellable,
                          GError **err)
{
	CamelIMAPXServer *is = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (foldername != NULL);
	/* entries may be NULL */
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	is = imapx_extd_store_get_server (CAMEL_IMAPX_STORE (self),
	                                  NULL,
	                                  cancellable,
	                                  &tmp_err);
	if (is == NULL)
		goto exit;

	ok = camel_imapx_extd_server_set_acl (is,
	                                      foldername,
	                                      entries,
	                                      cancellable,
	                                      &tmp_err);
 exit:
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	return ok;
}

/*----------------------------------------------------------------------------*/
/* interface functions */

static gboolean
imapx_extd_store_initable_initialize (GInitable *initable,
                                      GCancellable *cancellable,
                                      GError **err)
{
	CamelIMAPXExtdStore *self = NULL;
	gboolean ok = FALSE;

	g_assert (G_IS_INITABLE (initable));
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	self = CAMEL_IMAPX_EXTD_STORE (initable);

	/* register IMAPX capability flags (to be done before
	 * instantiating the first CamelIMAPXServer)
	 */
	ok = imapx_extd_store_register_capability_flags (self);
	if (! ok) {
		g_set_error (err,
		             CAMEL_IMAPX_ERROR,
		             1, /* FIXME define and add a sensible code here */
		             _("Cannot register extended IMAP capability flags"));
		return FALSE;
	}

	/* chain up to parent interface's init() method. */
	ok = parent_initable_iface->init (initable,
	                                  cancellable,
	                                  err);
	return ok;
}

static const gchar*
imapx_extd_store_get_service_name (CamelNetworkService *service,
                                   CamelNetworkSecurityMethod method)
{
	const gchar *sn = NULL;

	g_assert (CAMEL_IS_NETWORK_SERVICE (service));

	/* use parent function for now */
	sn = parent_service_iface->get_service_name (service,
	                                             method);

	return sn;
}

static guint16
imapx_extd_store_get_default_port (CamelNetworkService *service,
                                   CamelNetworkSecurityMethod method)
{
	guint16 port = 0;

	g_assert (CAMEL_IS_NETWORK_SERVICE (service));

	/* use parent function for now */
	port = parent_service_iface->get_default_port (service,
	                                               method);

	return port;
}

static gboolean
imapx_extd_store_folder_is_subscribed (CamelSubscribable *subscribable,
                                       const gchar *foldername)
{
	gboolean subscribed = FALSE;

	g_assert (CAMEL_IS_SUBSCRIBABLE (subscribable));
	g_assert (foldername != NULL);

	/* use parent function for now */
	subscribed = parent_subscribable_iface->folder_is_subscribed (subscribable,
	                                                              foldername);

	return subscribed;
}

static gboolean
imapx_extd_store_subscribe_folder_sync (CamelSubscribable *subscribable,
                                        const gchar *foldername,
                                        GCancellable *cancellable,
                                        GError **err)
{
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_SUBSCRIBABLE (subscribable));
	g_assert (foldername != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* use parent function for now */
	ok = parent_subscribable_iface->subscribe_folder_sync (subscribable,
	                                                       foldername,
	                                                       cancellable,
	                                                       err);
	return ok;
}

static gboolean
imapx_extd_store_unsubscribe_folder_sync (CamelSubscribable *subscribable,
                                          const gchar *foldername,
                                          GCancellable *cancellable,
                                          GError **err)
{
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_SUBSCRIBABLE (subscribable));
	g_assert (foldername != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* use parent function for now */
	ok = parent_subscribable_iface->unsubscribe_folder_sync (subscribable,
	                                                         foldername,
	                                                         cancellable,
	                                                         err);
	return ok;
}

static void
imapx_extd_store_initable_init (GInitableIface *interface)
{
	parent_initable_iface = g_type_interface_peek_parent (interface);
	interface->init = imapx_extd_store_initable_initialize;
}

static void
imapx_extd_store_network_service_init (CamelNetworkServiceInterface *interface)
{
	g_assert (CAMEL_IS_NETWORK_SERVICE_INTERFACE (interface));

	parent_service_iface = g_type_interface_peek_parent (interface);
	interface->get_service_name = imapx_extd_store_get_service_name;
	interface->get_default_port = imapx_extd_store_get_default_port;
}

static void
imapx_extd_store_subscribable_init (CamelSubscribableInterface *interface)
{
	g_assert (CAMEL_IS_SUBSCRIBABLE_INTERFACE (interface));

	parent_subscribable_iface = g_type_interface_peek_parent (interface);
	interface->folder_is_subscribed = imapx_extd_store_folder_is_subscribed;
	interface->subscribe_folder_sync = imapx_extd_store_subscribe_folder_sync;
	interface->unsubscribe_folder_sync = imapx_extd_store_unsubscribe_folder_sync;
}

/*----------------------------------------------------------------------------*/
/* class init */

static void
camel_imapx_extd_store_class_init (CamelIMAPXExtdStoreClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	CamelStoreClass *store_class = CAMEL_STORE_CLASS (klass);

	parent_service_class = CAMEL_SERVICE_CLASS (camel_imapx_extd_store_parent_class);
	parent_store_class = CAMEL_STORE_CLASS (camel_imapx_extd_store_parent_class);

	g_type_class_add_private (klass, sizeof (CamelIMAPXExtdStorePrivate));

	object_class->dispose = camel_imapx_extd_store_dispose;
	object_class->finalize = camel_imapx_extd_store_finalize;

	store_class->create_folder_sync = imapx_extd_store_create_folder_sync;
	store_class->delete_folder_sync = imapx_extd_store_delete_folder_sync;
	store_class->rename_folder_sync = imapx_extd_store_rename_folder_sync;
	store_class->synchronize_sync = imapx_extd_store_synchronize_sync;

	klass->get_server = imapx_extd_store_get_server;
	klass->metadata_get_proto = imapx_extd_store_metadata_get_proto;
	klass->get_metadata = imapx_extd_store_get_metadata;
	klass->set_metadata = imapx_extd_store_set_metadata;
	klass->get_acl = imapx_extd_store_get_acl;
	klass->set_acl = imapx_extd_store_set_acl;
}

/*----------------------------------------------------------------------------*/
/* API functions */

CamelIMAPXServer*
camel_imapx_extd_store_get_server (CamelIMAPXStore *self,
                                   const gchar *foldername,
                                   GCancellable *cancellable,
                                   GError **err)
{
	CamelIMAPXServer *server = NULL;
	CamelIMAPXExtdStoreClass *klass = NULL;

	g_return_val_if_fail (CAMEL_IS_IMAPX_EXTD_STORE (self), NULL);

	klass = CAMEL_IMAPX_EXTD_STORE_GET_CLASS (self);
	server = klass->get_server (self,
	                            foldername,
	                            cancellable,
	                            err);
	return server;
}

camel_imapx_metadata_proto_t
camel_imapx_extd_store_metadata_get_proto (CamelIMAPXExtdStore *self)
{
	CamelIMAPXExtdStoreClass *klass = NULL;

	g_return_val_if_fail (CAMEL_IS_IMAPX_EXTD_STORE (self), CAMEL_IMAPX_METADATA_PROTO_INVAL);

	klass = CAMEL_IMAPX_EXTD_STORE_GET_CLASS (self);
	return klass->metadata_get_proto (self);
}

CamelImapxMetadata*
camel_imapx_extd_store_get_metadata (CamelIMAPXExtdStore *self,
                                     CamelImapxMetadataSpec *spec,
                                     gboolean do_resect,
                                     GCancellable *cancellable,
                                     GError **err)
{
	CamelIMAPXExtdStoreClass *klass = NULL;

	g_return_val_if_fail (CAMEL_IS_IMAPX_EXTD_STORE (self), NULL);

	klass = CAMEL_IMAPX_EXTD_STORE_GET_CLASS (self);
	return klass->get_metadata (self,
	                            spec,
	                            do_resect,
	                            cancellable,
	                            err);
}

gboolean
camel_imapx_extd_store_set_metadata (CamelIMAPXExtdStore *self,
                                     CamelImapxMetadata *md,
                                     GCancellable *cancellable,
                                     GError **err)
{
	CamelIMAPXExtdStoreClass *klass = NULL;

	g_return_val_if_fail (CAMEL_IS_IMAPX_EXTD_STORE (self), FALSE);

	klass = CAMEL_IMAPX_EXTD_STORE_GET_CLASS (self);
	return klass->set_metadata (self,
	                            md,
	                            cancellable,
	                            err);
}

CamelImapxAcl*
camel_imapx_extd_store_get_acl (CamelIMAPXExtdStore *self,
                                CamelImapxAclSpec *spec,
                                gboolean do_resect,
                                GCancellable *cancellable,
                                GError **err)
{
	CamelIMAPXExtdStoreClass *klass = NULL;

	g_return_val_if_fail (CAMEL_IS_IMAPX_EXTD_STORE (self), FALSE);

	klass = CAMEL_IMAPX_EXTD_STORE_GET_CLASS (self);
	return klass->get_acl (self,
	                       spec,
	                       do_resect,
	                       cancellable,
	                       err);
}

gboolean
camel_imapx_extd_store_set_acl (CamelIMAPXExtdStore *self,
                                const gchar *foldername,
                                const GList *entries,
                                GCancellable *cancellable,
                                GError **err)
{
	CamelIMAPXExtdStoreClass *klass = NULL;

	g_return_val_if_fail (CAMEL_IS_IMAPX_EXTD_STORE (self), FALSE);

	klass = CAMEL_IMAPX_EXTD_STORE_GET_CLASS (self);
	return klass->set_acl (self,
	                       foldername,
	                       entries,
	                       cancellable,
	                       err);
}

/*----------------------------------------------------------------------------*/
/* "friend" API */

CamelImapxMetadata*
camel_imapx_extd_store_get_md_table (CamelIMAPXExtdStore *self)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));

	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	/* TODO does this need to be thread-safe? */

	return priv->md;
}

CamelImapxAcl*
camel_imapx_extd_store_get_acl_table (CamelIMAPXExtdStore *self)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));

	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	/* TODO does this need to be thread-safe? */

	return priv->acl;
}

guint32
camel_imapx_extd_store_get_capa_flag_id (CamelIMAPXExtdStore *self,
                                         camel_imapx_extd_store_capa_flag_t flag)
{
	CamelIMAPXExtdStorePrivate *priv = NULL;

	g_assert (CAMEL_IS_IMAPX_EXTD_STORE (self));
	g_assert (flag < CAMEL_IMAPX_EXTD_STORE_CAPA_LAST_FLAG);

	priv = CAMEL_IMAPX_EXTD_STORE_PRIVATE (self);

	return priv->imapx_capa_flag_ids[flag];
}

/*----------------------------------------------------------------------------*/
