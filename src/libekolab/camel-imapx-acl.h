/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-acl.h
 *
 *  2012-07-30, 19:09:28
 *  Copyright 2012, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/
/* ACL (RFC 4314) */

#ifndef _CAMEL_IMAPX_ACL_H_
#define _CAMEL_IMAPX_ACL_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gio/gio.h>

#include <libekolabutil/camel-system-headers.h>
#include <libekolabutil/kolab-util-glib.h>

/*----------------------------------------------------------------------------*/

#define IMAPX_IMAP_TOKEN_ACL       "ACL"
#define IMAPX_IMAP_TOKEN_MYRIGHTS  "MYRIGHTS"
#define IMAPX_IMAP_TOKEN_SETACL    "SETACL"
#define IMAPX_IMAP_TOKEN_GETACL    "GETACL"
#define IMAPX_IMAP_TOKEN_DELETEACL "DELETEACL"

/*----------------------------------------------------------------------------*/

typedef enum {
	CAMEL_IMAPX_ACL_TYPE_NONE     = 0,
	CAMEL_IMAPX_ACL_TYPE_MYRIGHTS = 1 << 0,
	CAMEL_IMAPX_ACL_TYPE_GENERAL  = 1 << 1
} CamelImapxAclType;

/*----------------------------------------------------------------------------*/

typedef struct _CamelImapxAclSpec CamelImapxAclSpec;
struct _CamelImapxAclSpec {
	gchar *mbox_name;
	CamelImapxAclType type;
};

typedef struct _CamelImapxAclEntry CamelImapxAclEntry;
struct _CamelImapxAclEntry {
	gchar *access_id;
	gchar *rights;
};

typedef struct _CamelImapxAclCmd CamelImapxAclCmd;
struct _CamelImapxAclCmd {
	gchar *token;
	gchar *command;
};

typedef struct _CamelImapxAcl CamelImapxAcl;
struct _CamelImapxAcl {
	GHashTable *myrights; /* MYRIGHTS per folder */
	GHashTable *mboxes;   /* ACL per folder      */
	GMutex lock;
};

/*----------------------------------------------------------------------------*/

CamelImapxAclSpec*
camel_imapx_acl_spec_new (const gchar *mbox_name,
                          CamelImapxAclType type);

void
camel_imapx_acl_spec_free (CamelImapxAclSpec *spec);

CamelImapxAclEntry*
camel_imapx_acl_entry_new (const gchar *access_id,
                           const gchar *rights,
                           GError **err);

CamelImapxAclEntry*
camel_imapx_acl_entry_clone (const CamelImapxAclEntry *entry,
                             GError **err);

void
camel_imapx_acl_entry_free (CamelImapxAclEntry *entry);

CamelImapxAclCmd*
camel_imapx_acl_cmd_new (const gchar *token,
                         const gchar *command);

void
camel_imapx_acl_cmd_free (CamelImapxAclCmd *cmd);

CamelImapxAcl*
camel_imapx_acl_new (gboolean locked);

void
camel_imapx_acl_free (CamelImapxAcl *acl);

CamelImapxAcl*
camel_imapx_acl_resect (CamelImapxAcl *acl);

gboolean
camel_imapx_acl_validate (CamelImapxAcl *acl,
                          GError **err);

gchar*
camel_imapx_acl_get_myrights (CamelImapxAcl *acl,
                              const gchar *mbox_name);

gboolean
camel_imapx_acl_update_myrights (CamelImapxAcl *acl,
                                 const gchar *mbox_name,
                                 const gchar *rights,
                                 GError **err);

gboolean
camel_imapx_acl_update_from_acl (CamelImapxAcl *acl,
                                 CamelImapxAcl *src_acl,
                                 GError **err);

gboolean
camel_imapx_acl_update_from_list (CamelImapxAcl *acl,
                                  const gchar *mbox_name,
                                  const GList *entries,
                                  GError **err);

gboolean
camel_imapx_acl_update_acl_from_server_response (CamelImapxAcl *acl,
                                                 CamelIMAPXStream *is,
                                                 GCancellable *cancellable,
                                                 GError **err);
gboolean
camel_imapx_acl_update_myrights_from_server_response (CamelImapxAcl *acl,
                                                      CamelIMAPXStream *is,
                                                      GCancellable *cancellable,
                                                      GError **err);

GList*
camel_imapx_acl_get_as_list (CamelImapxAcl *acl,
                             const gchar *mbox_name);

GList*
camel_imapx_acl_list_clone (GList *entries,
                            GError **err);

const gchar*
camel_imapx_acl_list_get_rights (const GList *entries,
                                 const gchar *access_id,
                                 GError **err);

gboolean
camel_imapx_acl_list_remove_entry (GList **entries,
                                   const gchar *access_id,
                                   GError **err);

gboolean
camel_imapx_acl_list_update_from_entry (GList **entries,
                                        const CamelImapxAclEntry *entry,
                                        GError **err);

void
camel_imapx_acl_list_free (GList *entries);

gchar*
camel_imapx_acl_rights_merge (const gchar *oldrights,
                              const gchar *newrights,
                              GError **err);

GList*
camel_imapx_acl_commandlist_new (const GList *entries,
                                 const gchar *foldername,
                                 GError **err);

void
camel_imapx_acl_commandlist_free (GList *commands);

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_IMAPX_ACL_H_ */

/*----------------------------------------------------------------------------*/
