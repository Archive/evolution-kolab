/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-data-folder-metadata.h
 *
 *  Fri Feb 17 17:07:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_DATA_FOLDER_METADATA_H_
#define _KOLAB_DATA_FOLDER_METADATA_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include <libekolabutil/kolab-util-folder.h>

#include "kolab-types.h"

/*----------------------------------------------------------------------------*/

typedef struct _KolabDataFolderMetadata KolabDataFolderMetadata;
struct _KolabDataFolderMetadata {
	gchar *foldername;
	KolabFolderTypeID foldertype;
	KolabSyncStrategyID strategy;
	gboolean show_all; /* whether to unhide PIM folders */
};

/*----------------------------------------------------------------------------*/

KolabDataFolderMetadata*
kolab_data_folder_metadata_new (void);

KolabDataFolderMetadata*
kolab_data_folder_metadata_clone (const KolabDataFolderMetadata *srcdata);

void
kolab_data_folder_metadata_free (KolabDataFolderMetadata *data);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_DATA_FOLDER_METADATA_H_ */

/*----------------------------------------------------------------------------*/
