/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-server.h
 *
 *  2011-11-28, 20:16:38
 *  Copyright 2011, Christian Hilberg
 *  <hilberg@unix-ag.org>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_IMAPX_EXTD_SERVER_H_
#define _CAMEL_IMAPX_EXTD_SERVER_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolabutil/camel-system-headers.h>

/* CamelIMAPXExtdServer protocol extensions */
#include "camel-imapx-extd-server-acl.h"
#include "camel-imapx-extd-server-metadata.h"

/*----------------------------------------------------------------------------*/

gboolean
camel_imapx_extd_server_init (CamelIMAPXServer *is,
                              GCancellable *cancellable,
                              GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* CAMEL_IMAPX_EXTD_SERVER_H_ */

/*----------------------------------------------------------------------------*/
