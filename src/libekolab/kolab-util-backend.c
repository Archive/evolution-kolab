/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-backend.c
 *
 *  Mon Jan 17 11:16:52 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <config.h>

#include <string.h>
#include <time.h>

#include <glib/gi18n-lib.h>

#include <libical/ical.h>

#include <libekolabutil/camel-system-headers.h>
#include <libekolabutil/kolab-util-camel.h>
#include <libekolabutil/kolab-util.h>

#include "e-source-kolab-folder.h"
#include "kolab-mail-access.h"

#include "kolab-util-backend.h"

/*----------------------------------------------------------------------------*/

static const gchar *kolab_sync_strategy_desc[KOLAB_SYNC_LAST_STRATEGY] = {
	NC_("Sync Conflict Resolution", KOLAB_STRATEGY_DESC_NEWEST),
	NC_("Sync Conflict Resolution", KOLAB_STRATEGY_DESC_SERVER),
	NC_("Sync Conflict Resolution", KOLAB_STRATEGY_DESC_CLIENT),
	NC_("Sync Conflict Resolution", KOLAB_STRATEGY_DESC_DUPE)
};

static const gchar *kolab_tls_variant_desc[KOLAB_TLS_LAST_VARIANT] = {
	NC_("Security", KOLAB_TLS_DESC_VARIANT_NONE),
	NC_("Security", KOLAB_TLS_DESC_VARIANT_SSL),
	NC_("Security", KOLAB_TLS_DESC_VARIANT_TLS)
};

/*----------------------------------------------------------------------------*/
/* local statics */

/*----------------------------------------------------------------------------*/
/* GError for libekolabbackend */

GQuark
kolab_util_backend_error_quark (void)
{
	static GQuark quark = 0;

	if (G_UNLIKELY (quark == 0)) {
		const gchar *string = "kolab-backend-error-quark";
		quark = g_quark_from_static_string (string);
	}

	return quark;
}

/*----------------------------------------------------------------------------*/

/**
 * kolab_util_backend_get_foldername:
 * @backend: an #EBackend
 *
 * Returns the Kolab foldername for @backend.
 *
 * Returns: the foldername for @backend
 **/
const gchar *
kolab_util_backend_get_foldername (EBackend *backend)
{
	ESource *source;
	ESourceResource *extension;
	const gchar *extension_name;

	g_return_val_if_fail (E_IS_BACKEND (backend), NULL);

	source = e_backend_get_source (backend);
	extension_name = E_SOURCE_EXTENSION_KOLAB_FOLDER;
	extension = e_source_get_extension (source, extension_name);

	return e_source_resource_get_identity (extension);
}

/**
 * kolab_util_backend_get_relative_path_from_uri:
 * @uri: An URI string
 *
 * Extracts the path from the given uri. Leading "/" are removed.
 *
 * Returns: A newly allocated string containing the uri, or NULL.
 */
gchar *
kolab_util_backend_get_relative_path_from_uri (const gchar *uri)
{
	CamelURL *c_url = NULL;
	gchar *tmp = NULL;
	gchar *path = NULL;
	GError *tmp_err = NULL;

	c_url = camel_url_new (uri, &tmp_err);
	if (c_url == NULL) {
		g_warning ("%s()[%u] error: %s", __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		return NULL;
	}
	tmp = g_strdup (c_url->path);
	camel_url_free (c_url);
	if (tmp[0] == KOLAB_PATH_SEPARATOR) {
		path = g_strdup (tmp+1);
		g_free (tmp);
	}
	else {
		path = tmp;
	}

	return path;
} /* kolab_util_backend_get_relative_path_from_uri () */

gchar*
kolab_util_backend_get_protocol_from_uri (const gchar *uri)
{
	CamelURL *c_url = NULL;
	gchar *proto = NULL;
	GError *tmp_err = NULL;

	c_url = camel_url_new (uri, &tmp_err);
	if (c_url == NULL) {
		g_warning ("%s()[%u] error: %s", __func__, __LINE__, tmp_err->message);
		g_error_free (tmp_err);
		return NULL;
	}
	proto = g_strdup (c_url->protocol);
	camel_url_free (c_url);

	return proto;
}

static gint
kolab_util_misc_generic_integer_from_property (const gchar *prop_str,
                                               gint DEFAULT_VALUE)
{
	guint64 tmp_value;
	gint prop_value;
	gchar *endptr = NULL;

	if (prop_str == NULL) /* Empty property -> use DEFAULT_VALUE. */
		return DEFAULT_VALUE;

	tmp_value = g_ascii_strtoull (prop_str, &endptr, 10);
	if ((tmp_value == 0) && (endptr == prop_str)) {
		/* This is a conversion error. Use DEFAULT_VALUE instead. */
		prop_value = DEFAULT_VALUE;
	} else if (tmp_value > G_MAXINT) {
		/* Overflow, handle gracefully: defaulting to DEFAULT_VALUE.
		 * Any other error leads to sync_value being zero which we
		 * assume to be the default value anyway */
		prop_value = DEFAULT_VALUE;
	} else
		prop_value = (gint) tmp_value;
	return prop_value;
} /* kolab_util_misc_generic_integer_from_property () */

KolabSyncStrategyID
kolab_util_misc_sync_value_from_property (const gchar *sync_prop)
{
	KolabSyncStrategyID sid = KOLAB_SYNC_STRATEGY_DEFAULT;

	if (sync_prop == NULL)
		return KOLAB_SYNC_STRATEGY_DEFAULT;

	sid = kolab_util_misc_generic_integer_from_property (sync_prop,
	                                                     KOLAB_SYNC_STRATEGY_DEFAULT);
	return sid;
} /* kolab_util_misc_sync_value_from_property () */

KolabTLSVariantID
kolab_util_misc_tls_variant_from_property (const gchar *tls_variant)
{
	KolabTLSVariantID tvid = KOLAB_TLS_VARIANT_DEFAULT;

	if (tls_variant == NULL)
		return KOLAB_TLS_VARIANT_DEFAULT;

	tvid = kolab_util_misc_generic_integer_from_property (tls_variant,
	                                                      KOLAB_TLS_VARIANT_DEFAULT);
	return tvid;
} /* kolab_util_misc_tls_variant_from_property () */

gint
kolab_util_misc_port_number_from_property (const gchar *port_number_prop)
{
	gint port = 0;

	if (port_number_prop == NULL)
		return -1;

	port = kolab_util_misc_generic_integer_from_property (port_number_prop, -1);
	if (port > 99999) {
		g_warning ("%s()[%u] configured port number exceeds 5 digits, clamping to 99999!",
		           __func__, __LINE__);
		port = 99999;
	}
	if (port < 0) {
		g_warning ("%s()[%u] configured port number is negative, clamping to 0!",
		           __func__, __LINE__);
		port = 0;
	}

	return port;
}

KolabReqPkcs11
kolab_util_misc_req_pkcs11_from_property (const gchar *req_pkcs11_prop)
{
	KolabReqPkcs11 pkreq = KOLAB_PKCS11_INFRASTRUCTURE_DEFAULT;

	if (req_pkcs11_prop == NULL)
		return KOLAB_PKCS11_INFRASTRUCTURE_DEFAULT;

	pkreq = kolab_util_misc_generic_integer_from_property (req_pkcs11_prop,
	                                                       KOLAB_PKCS11_INFRASTRUCTURE_DEFAULT);
	return pkreq;
}

void
kolab_util_backend_koma_table_cleanup_cb (gpointer data,
                                          GObject *object,
                                          gboolean is_last_ref)
{
	KolabMailAccess *koma = KOLAB_MAIL_ACCESS (object);
	GHashTable *koma_table = (GHashTable *) data;
	const gchar *host = NULL;
	const gchar *user = NULL;
	gchar *user_at_host = NULL;
	KolabSettingsHandler *ksettings = NULL;
	CamelKolabIMAPXSettings *camel_settings;
	CamelNetworkSettings *network_settings;
	/* gboolean ok; */
	g_debug ("%s()[%u] called.", __func__, __LINE__);

	g_assert (data != NULL);
	g_assert (G_IS_OBJECT (object));
	(void)is_last_ref;

	ksettings = kolab_mail_access_get_settings_handler (koma);
	camel_settings = kolab_settings_handler_get_camel_settings (ksettings);

	network_settings = CAMEL_NETWORK_SETTINGS (camel_settings);
	host = camel_network_settings_get_host (network_settings);
	user = camel_network_settings_get_user (network_settings);

	user_at_host = g_strdup_printf ("%s@%s", user, host);
	(void)g_hash_table_remove (koma_table, user_at_host);

	g_free (user_at_host);
	g_object_remove_toggle_ref (object,
	                            kolab_util_backend_koma_table_cleanup_cb,
	                            data);
} /* kolab_util_backend_koma_table_cleanup_cb () */

void
kolab_util_backend_modtime_set_on_econtact (EContact *econtact)
{
	time_t rawtime;
	struct tm *ts = NULL;
	gchar *buf = NULL;

	g_assert (E_IS_CONTACT (econtact));

	time (&rawtime);
	ts = gmtime (&rawtime);
	buf = g_new0 (gchar, 21);
	strftime (buf, 21, "%Y-%m-%dT%H:%M:%SZ", ts); /* same as in contact-i-to-e.c */
	e_contact_set (econtact, E_CONTACT_REV, buf);
	g_free (buf);
} /* kolab_util_backend_modtime_set_on_econtact () */

void
kolab_util_backend_modtime_set_on_ecalcomp (ECalComponent *ecalcomp)
{
	struct icaltimetype itt;

	g_assert (E_IS_CAL_COMPONENT (ecalcomp));

	itt = icaltime_current_time_with_zone (NULL); /* need UTC here, hence NULL timezone */
	e_cal_component_set_last_modified (ecalcomp, &itt);
} /* kolab_util_backend_modtime_set_on_ecalcomp () */

gboolean
kolab_util_backend_sqlite_db_new_open (KolabUtilSqliteDb **kdb,
                                       KolabSettingsHandler *ksettings,
                                       const gchar *dbfilename,
                                       GError **err)
{
	const gchar *dbpath = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_assert ((kdb != NULL) && (*kdb == NULL));
	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_assert (dbfilename != NULL);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	dbpath = kolab_settings_handler_get_char_field (ksettings,
	                                                KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_ACCOUNT_DIR,
	                                                &tmp_err);
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}
	if (dbpath == NULL) {
		g_set_error (err,
		             KOLAB_BACKEND_ERROR,
		             KOLAB_BACKEND_ERROR_CAMEL,
		             _("Could not get Camel storage path for internal database"));
		return FALSE;
	}
	*kdb = kolab_util_sqlite_db_new ();
	ok = kolab_util_sqlite_db_open (*kdb,
	                                dbpath,
	                                dbfilename,
	                                &tmp_err);
	if (! ok) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* mapping of description IDs to description strings */

const gchar*
kolab_util_backend_get_sync_strategy_desc (KolabSyncStrategyID id)
{
	g_assert (id < KOLAB_SYNC_LAST_STRATEGY);

	return gettext (kolab_sync_strategy_desc[id]);
}

const gchar*
kolab_util_backend_get_tls_variant_desc (KolabTLSVariantID id)
{
	g_assert (id < KOLAB_TLS_LAST_VARIANT);

	return gettext (kolab_tls_variant_desc[id]);
}

gchar*
kolab_util_backend_account_uid_new_from_settings (KolabSettingsHandler *ksettings,
                                                  GError **err)
{
	CamelKolabIMAPXSettings *camel_settings;
	CamelNetworkSettings *network_settings;
	const gchar *host = NULL;
	const gchar *user = NULL;
	const gchar *esource_uid = NULL;
	gchar *account_uid = NULL;
	gchar *tmp_str = NULL;
	GError *tmp_err = NULL;

	g_assert (KOLAB_IS_SETTINGS_HANDLER (ksettings));
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	esource_uid = kolab_settings_handler_get_char_field (ksettings,
	                                                     KOLAB_SETTINGS_HANDLER_CHAR_FIELD_ESOURCE_UID,
	                                                     &tmp_err);
	if (tmp_err != NULL) {
		g_warning ("%s: %s", __func__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}

	if (esource_uid != NULL) {
		account_uid = g_strdup (esource_uid);
		goto done;
	}

	/* fallback in case we don't have an ESource UID set */

	camel_settings = kolab_settings_handler_get_camel_settings (ksettings);

	network_settings = CAMEL_NETWORK_SETTINGS (camel_settings);
	host = camel_network_settings_get_host (network_settings);
	user = camel_network_settings_get_user (network_settings);

	tmp_str = g_strconcat (user, "@", host, NULL);

	account_uid = g_strconcat (KOLAB_CAMEL_PROVIDER_PROTOCOL, "__", tmp_str, NULL);
	g_free (tmp_str);

 done:

	return account_uid;
}

KolabMailAccessOpmodeID
kolab_util_backend_deploy_mode_by_koma (KolabMailAccess *koma,
                                        KolabMailAccessOpmodeID koma_mode,
                                        GCancellable *cancellable,
                                        GError **error)
{
	KolabMailAccessOpmodeID tmp_mode = KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	if (koma != NULL) g_assert (KOLAB_IS_MAIL_ACCESS (koma));
	g_assert (koma_mode < KOLAB_MAIL_ACCESS_LAST_OPMODE);
	/* cancellable may be NULL */
	g_return_val_if_fail (error == NULL || *error == NULL, KOLAB_MAIL_ACCESS_OPMODE_INVAL);

	if (koma == NULL) {
		g_debug ("%s()[%u] error: KolabMailAccess object not existent.",
		         __func__, __LINE__);
		/* FIXME set GError */
		return KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	}

	/* get current KolabMailAccess operational mode */
	tmp_mode = kolab_mail_access_get_opmode (koma, &tmp_err);
	if (tmp_err != NULL) {
		g_propagate_error (error, tmp_err);
		return KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	}
	if (tmp_mode < KOLAB_MAIL_ACCESS_OPMODE_OFFLINE) {
		g_debug ("%s()[%u] KolabMailAccess object not ready, deferring.",
		         __func__, __LINE__);
		return tmp_mode;
	}

	/* set new KolabMailAccess operational mode */
	ok = kolab_mail_access_set_opmode (koma,
	                                   koma_mode,
	                                   cancellable,
	                                   &tmp_err);
	if (! ok) {
		g_propagate_error (error, tmp_err);
		return KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	}

	return koma_mode;
}

gboolean
kolab_util_backend_deploy_mode_by_backend (KolabMailAccess *koma,
                                           gboolean online,
                                           GCancellable *cancellable,
                                           GError **error)
{
	KolabMailAccessOpmodeID koma_mode = KOLAB_MAIL_ACCESS_OPMODE_OFFLINE;
	KolabMailAccessOpmodeID tmp_mode = KOLAB_MAIL_ACCESS_OPMODE_INVAL;
	GError *tmp_err = NULL;

	g_debug ("%s()[%u] called.", __func__, __LINE__);

	g_return_val_if_fail (KOLAB_IS_MAIL_ACCESS (koma), FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (error == NULL || *error == NULL, KOLAB_MAIL_ACCESS_OPMODE_INVAL);

	if (online)
		koma_mode = KOLAB_MAIL_ACCESS_OPMODE_ONLINE;

	tmp_mode = kolab_util_backend_deploy_mode_by_koma (koma,
	                                                   koma_mode,
	                                                   cancellable,
	                                                   &tmp_err);
	if (tmp_err != NULL) {
		g_propagate_error (error, tmp_err);
		return FALSE;
	}

	if (tmp_mode != koma_mode) {
		g_warning ("%s()[%u] KolabMailAccess did not set its GError on failure",
		           __func__, __LINE__);
		g_set_error (error,
		             KOLAB_BACKEND_ERROR,
		             KOLAB_BACKEND_ERROR_INTERNAL,
		             _("Kolab engine did not switch into requested operational mode"));
		return FALSE;
	}

	return TRUE;
}
