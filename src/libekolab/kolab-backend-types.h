/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-backend-types.h
 *
 *  Mon Feb 14 10:43:52 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 
/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-backend-types
 * @short_description: Common Kolab backend types
 * @title: kolab-backend-types
 * @section_id:
 * @see_also:
 * @stability: unstable
 *
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_BACKEND_TYPES_H_
#define _KOLAB_BACKEND_TYPES_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

/*----------------------------------------------------------------------------*/
/* KolabMailHandle and friends */

/* where a pim object / folder is cached
 * - not cached
 * - CamelKolabIMAPXStore
 * - KolabMailSideCache
 * - both
 */
typedef enum {
	KOLAB_OBJECT_CACHE_LOCATION_NONE	= 0,
	KOLAB_OBJECT_CACHE_LOCATION_IMAP	= 1 << 0,
	KOLAB_OBJECT_CACHE_LOCATION_SIDE	= 1 << 1,

	KOLAB_OBJECT_CACHE_LOCATION_INVAL       = 1 << 8
} KolabObjectCacheLocation;

/* cache status for an object
 */
typedef enum {
	KOLAB_OBJECT_CACHE_STATUS_NONE		= 0,
	KOLAB_OBJECT_CACHE_STATUS_DIRTY		= 1 << 0,
	KOLAB_OBJECT_CACHE_STATUS_DELETED       = 1 << 1,
	KOLAB_OBJECT_CACHE_STATUS_CHANGED       = 1 << 2,

	KOLAB_OBJECT_CACHE_STATUS_INVAL		= 1 << 8
} KolabObjectCacheStatus;

/*----------------------------------------------------------------------------*/
/* KolabMailAccess */

/* operational mode - MUST be kept in-order */
typedef enum {
	KOLAB_MAIL_ACCESS_OPMODE_INVAL = 0, /* MUST start with 0 */
	KOLAB_MAIL_ACCESS_OPMODE_SHUTDOWN,
	KOLAB_MAIL_ACCESS_OPMODE_NEW,
	KOLAB_MAIL_ACCESS_OPMODE_CONFIGURED,
	KOLAB_MAIL_ACCESS_OPMODE_OFFLINE,
	KOLAB_MAIL_ACCESS_OPMODE_ONLINE,
	KOLAB_MAIL_ACCESS_LAST_OPMODE
} KolabMailAccessOpmodeID;

/*----------------------------------------------------------------------------*/
/* KolabMailMimeBuilder */

typedef struct _KolabMailMimeBuilderHeaderInfo KolabMailMimeBuilderHeaderInfo;
struct _KolabMailMimeBuilderHeaderInfo {
	gchar *kolab_uid;
	gchar *from_name;
	gchar *from_addr;
};

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_BACKEND_TYPES_H_ */

/*----------------------------------------------------------------------------*/
