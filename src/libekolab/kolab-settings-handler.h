/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-settings-handler.h
 *
 *  Wed Dec 22 13:46:54 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-settings-handler
 * @short_description: aggregate Evolution settings data
 * @title: KolabSettingsHandler
 * @section_id:
 * @see_also: #KolabMailAccess
 * @stability: unstable
 *
 * This class is handles all Kolab (account) settings for the
 * Kolab backends. This means (mostly) reading the settings information created by
 * the Evolution frontend and encapsulating operations concerning
 * #ESource classes. One instance of this class will live in each backend
 * process and is passed to #KolabMailAccess (and by #KolabMailAccess, on to
 * all of it's subordinate objects).
 *
 */

/*----------------------------------------------------------------------------*/

/* TODO Create a common base class for
 *      - KolabSettingsHandler
 *      - KolabMailSummary
 *      - KolabFolderSummary
 *      containing the Char/Uint/Int/Bool field infrastructure
 *      and getters/setters
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_SETTINGS_HANDLER_H_
#define _KOLAB_SETTINGS_HANDLER_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>

#include <libebackend/libebackend.h>

#include <libekolabutil/kolab-util-folder.h>
#include <libekolab/camel-kolab-imapx-settings.h>

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

#define KOLAB_TYPE_SETTINGS_HANDLER             (kolab_settings_handler_get_type ())
#define KOLAB_SETTINGS_HANDLER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOLAB_TYPE_SETTINGS_HANDLER, KolabSettingsHandler))
#define KOLAB_SETTINGS_HANDLER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), KOLAB_TYPE_SETTINGS_HANDLER, KolabSettingsHandlerClass))
#define KOLAB_IS_SETTINGS_HANDLER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOLAB_TYPE_SETTINGS_HANDLER))
#define KOLAB_IS_SETTINGS_HANDLER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), KOLAB_TYPE_SETTINGS_HANDLER))
#define KOLAB_SETTINGS_HANDLER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), KOLAB_TYPE_SETTINGS_HANDLER, KolabSettingsHandlerClass))

typedef struct _KolabSettingsHandler KolabSettingsHandler;
typedef struct _KolabSettingsHandlerClass KolabSettingsHandlerClass;
typedef struct _KolabSettingsHandlerPrivate KolabSettingsHandlerPrivate;

struct _KolabSettingsHandlerClass
{
	GObjectClass parent_class;
};

struct _KolabSettingsHandler
{
	GObject parent_instance;
	KolabSettingsHandlerPrivate *priv;
};

typedef enum {
	KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_DATA_DIR = 0,
	KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CACHE_DIR,
	KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CONFIG_DIR,
	KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_ACCOUNT_DIR,
	KOLAB_SETTINGS_HANDLER_CHAR_FIELD_USER_HOME_DIR,
	KOLAB_SETTINGS_HANDLER_CHAR_FIELD_ESOURCE_UID,
	KOLAB_SETTINGS_HANDLER_CHAR_FIELD_KOLAB_USER_PASSWORD,
	KOLAB_SETTINGS_HANDLER_CHAR_LAST_FIELD
} KolabSettingsHandlerCharFieldID;

typedef enum {
	KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_CONTEXT = 0,
	KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_SYNCSTRATEGY,
	KOLAB_SETTINGS_HANDLER_UINT_LAST_FIELD
} KolabSettingsHandlerUintFieldID;

typedef enum {
	KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_IMAP_PORT = 0,
	KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_IMAPS_PORT,
	KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_HTTP_PORT,
	KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_HTTPS_PORT,
	KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_LDAP_PORT,
	KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_LDAPS_PORT,
	KOLAB_SETTINGS_HANDLER_INT_LAST_FIELD
} KolabSettingsHandlerIntFieldID;

typedef enum {
	KOLAB_SETTINGS_HANDLER_BOOL_FIELD_NONE = 0, /* FIXME replace */
	KOLAB_SETTINGS_HANDLER_BOOL_LAST_FIELD
} KolabSettingsHandlerBoolFieldID;

GType kolab_settings_handler_get_type (void) G_GNUC_CONST;

KolabSettingsHandler*
kolab_settings_handler_new (CamelKolabIMAPXSettings *camel_settings,
                            EBackend *e_backend);

gboolean
kolab_settings_handler_configure (KolabSettingsHandler *self,
                                  KolabFolderContextID context,
                                  GError **err);

gboolean
kolab_settings_handler_bringup (KolabSettingsHandler *self,
                                GError **err);

gboolean
kolab_settings_handler_shutdown (KolabSettingsHandler *self,
                                 GError **err);

CamelKolabIMAPXSettings*
kolab_settings_handler_get_camel_settings (KolabSettingsHandler *self);

EBackend*
kolab_settings_handler_get_e_backend (KolabSettingsHandler *self);

gboolean
kolab_settings_handler_set_char_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerCharFieldID field_id,
                                       gchar *value,
                                       GError **err);

const gchar*
kolab_settings_handler_get_char_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerCharFieldID field_id,
                                       GError **err);

gboolean
kolab_settings_handler_set_uint_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerUintFieldID field_id,
                                       guint value,
                                       GError **err);

guint
kolab_settings_handler_get_uint_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerUintFieldID field_id,
                                       GError **err);

gboolean
kolab_settings_handler_set_int_field (KolabSettingsHandler *self,
                                      KolabSettingsHandlerIntFieldID field_id,
                                      gint value,
                                      GError **err);

gint
kolab_settings_handler_get_int_field (KolabSettingsHandler *self,
                                      KolabSettingsHandlerIntFieldID field_id,
                                      GError **err);

gboolean
kolab_settings_handler_set_bool_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerBoolFieldID field_id,
                                       gboolean value,
                                       GError **err);

gboolean
kolab_settings_handler_get_bool_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerBoolFieldID field_id,
                                       GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_SETTINGS_HANDLER_H_ */

/*----------------------------------------------------------------------------*/
