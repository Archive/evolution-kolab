/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-metadata-db.h
 *
 *  Mon Oct 11 12:37:05 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_KOLAB_IMAPX_METADATA_DB_H_
#define _CAMEL_KOLAB_IMAPX_METADATA_DB_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <sqlite3.h>

/*----------------------------------------------------------------------------*/

typedef struct _CamelKolabImapxMetadataDb CamelKolabImapxMetadataDb;
struct _CamelKolabImapxMetadataDb {
	sqlite3  *db;	/* sqlite3 DB handle */
	gchar    *path; /* path to the sqlite3 metadata db file */
	gint	 ctr;	/* aux counter, usable with callback functions */
};

/*----------------------------------------------------------------------------*/

struct _CamelKolabImapxFolderMetadata;

/*----------------------------------------------------------------------------*/

CamelKolabImapxMetadataDb*
camel_kolab_imapx_metadata_db_new (void);

gboolean
camel_kolab_imapx_metadata_db_free (CamelKolabImapxMetadataDb *mdb,
                                    GError **err);

gboolean
camel_kolab_imapx_metadata_db_open (CamelKolabImapxMetadataDb *mdb,
                                    const gchar *cachepath,
                                    GError **err);

gboolean
camel_kolab_imapx_metadata_db_init (CamelKolabImapxMetadataDb *mdb,
                                    GError **err);

gboolean
camel_kolab_imapx_metadata_db_close (CamelKolabImapxMetadataDb *mdb,
                                     GError **err);

gboolean
camel_kolab_imapx_metadata_db_folder_update (CamelKolabImapxMetadataDb *mdb,
                                             const gchar *foldername,
                                             const struct _CamelKolabImapxFolderMetadata *kfmd,
                                             GError **err);

gboolean
camel_kolab_imapx_metadata_db_update (CamelKolabImapxMetadataDb *mdb,
                                      GHashTable *kolab_metadata,
                                      GError **err);

struct _CamelKolabImapxFolderMetadata*
camel_kolab_imapx_metadata_db_lookup (CamelKolabImapxMetadataDb *mdb,
                                      const gchar *foldername,
                                      GError **err);

gboolean
camel_kolab_imapx_metadata_db_remove_folder (CamelKolabImapxMetadataDb *mdb,
                                             const gchar *foldername,
                                             GError **err);

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_KOLAB_IMAPX_METADATA_DB_H_ */

/*----------------------------------------------------------------------------*/
