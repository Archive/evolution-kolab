/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-session.h
 *
 *  Tue Aug 10 15:04:38 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/*
 * The CamelSession class for Kolab access. To be instantiated once for
 * each IMAPX CamelKolabIMAPXProvider we have. Within EDS, this is one for
 * address book and one for calendar access. Within Evolution, a CamelSession
 * object is already instantiated and will be used for the CamelKolabIMAPXProvider
 *
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_KOLAB_SESSION_H_
#define _CAMEL_KOLAB_SESSION_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libebackend/libebackend.h>

#include <libekolabutil/camel-system-headers.h>

/*----------------------------------------------------------------------------*/

#define CAMEL_KOLAB_SESSION_PROP_DATA_DIR  "user-data-dir"
#define CAMEL_KOLAB_SESSION_PROP_CACHE_DIR "user-cache-dir"

/*----------------------------------------------------------------------------*/
/* Standard GObject macros */

#define CAMEL_TYPE_KOLAB_SESSION	  \
	(camel_kolab_session_get_type ())
#define CAMEL_KOLAB_SESSION(obj)	  \
	(G_TYPE_CHECK_INSTANCE_CAST \
	 ((obj), CAMEL_TYPE_KOLAB_SESSION, CamelKolabSession))
#define CAMEL_KOLAB_SESSION_CLASS(klass)	  \
	(G_TYPE_CHECK_CLASS_CAST \
	 ((klass), CAMEL_TYPE_KOLAB_SESSION, CamelKolabSessionClass))
#define CAMEL_IS_KOLAB_SESSION(obj)	  \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	 ((obj), CAMEL_TYPE_KOLAB_SESSION))
#define CAMEL_IS_KOLAB_SESSION_CLASS(klass)	  \
	(G_TYPE_CHECK_CLASS_TYPE \
	 ((klass), CAMEL_TYPE_KOLAB_SESSION))
#define CAMEL_KOLAB_SESSION_GET_CLASS(obj)	  \
	(G_TYPE_INSTANCE_GET_CLASS \
	 ((obj), CAMEL_TYPE_KOLAB_SESSION, CamelKolabSessionClass))

G_BEGIN_DECLS

typedef struct _CamelKolabSession CamelKolabSession;
typedef struct _CamelKolabSessionClass CamelKolabSessionClass;
typedef struct _CamelKolabSessionPrivate CamelKolabSessionPrivate;

struct _CamelKolabSession {
	CamelSession parent;
	CamelKolabSessionPrivate *priv;
};


struct _CamelKolabSessionClass {
	CamelSessionClass parent_class;
	/* TODO check what else is needed here */
};

GType
camel_kolab_session_get_type (void);

CamelKolabSession *
camel_kolab_session_new (EBackend *backend,
                         const gchar *user_data_dir,
                         const gchar *user_cache_dir);

EBackend *
camel_kolab_session_ref_backend (CamelKolabSession *self);

gboolean
camel_kolab_session_bringup (CamelKolabSession *self,
                             GCancellable *cancellable,
                             GError **err);

gboolean
camel_kolab_session_shutdown (CamelKolabSession *self,
                              GCancellable *cancellable,
                              GError **err);

void
camel_kolab_session_set_token_pin (CamelKolabSession *self,
                                   const gchar *pin);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_KOLAB_SESSION_H_ */

/*----------------------------------------------------------------------------*/
