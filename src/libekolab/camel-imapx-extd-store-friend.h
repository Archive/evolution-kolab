/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-store-friend.h
 *
 *  2012-05-24, 18:34:30
 *  Copyright 2012, Christian Hilberg
 *  <hilberg@unix-ag.org>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_IMAPX_EXTD_STORE_FRIEND_H_
#define _CAMEL_IMAPX_EXTD_STORE_FRIEND_H_

/*----------------------------------------------------------------------------*/

typedef enum {
	CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ANNOTATEMORE,
	CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_METADATA,
	CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ACL,
	CAMEL_IMAPX_EXTD_STORE_CAPA_LAST_FLAG
} camel_imapx_extd_store_capa_flag_t;

/*----------------------------------------------------------------------------*/

CamelImapxMetadata*
camel_imapx_extd_store_get_md_table (CamelIMAPXExtdStore *self);

CamelImapxAcl*
camel_imapx_extd_store_get_acl_table (CamelIMAPXExtdStore *self);

guint32
camel_imapx_extd_store_get_capa_flag_id (CamelIMAPXExtdStore *self,
                                         camel_imapx_extd_store_capa_flag_t flag);

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_IMAPX_EXTD_STORE_FRIEND_H_ */

/*----------------------------------------------------------------------------*/
