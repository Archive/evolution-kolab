/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef E_SOURCE_KOLAB_FOLDER_H
#define E_SOURCE_KOLAB_FOLDER_H

#include <libedataserver/libedataserver.h>

#include <libekolab/kolab-types.h>

/* Standard GObject macros */
#define E_TYPE_SOURCE_KOLAB_FOLDER \
	(e_source_kolab_folder_get_type ())
#define E_SOURCE_KOLAB_FOLDER(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), E_TYPE_SOURCE_KOLAB_FOLDER, ESourceKolabFolder))
#define E_SOURCE_KOLAB_FOLDER_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), E_TYPE_SOURCE_KOLAB_FOLDER, ESourceKolabFolderClass))
#define E_IS_SOURCE_KOLAB_FOLDER(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), E_TYPE_SOURCE_KOLAB_FOLDER))
#define E_IS_SOURCE_KOLAB_FOLDER_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), E_TYPE_SOURCE_KOLAB_FOLDER))
#define E_SOURCE_KOLAB_FOLDER_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), E_TYPE_SOURCE_KOLAB_FOLDER, ESourceKolabFolderClass))

/**
 * E_SOURCE_EXTENSION_KOLAB_FOLDER:
 *
 * Pass the extension name to e_source_get_extension() to access
 * #ESourceKolabFolder.  This is also used as a group name in key files.
 **/
#define E_SOURCE_EXTENSION_KOLAB_FOLDER "Kolab Folder"

G_BEGIN_DECLS

typedef struct _ESourceKolabFolder ESourceKolabFolder;
typedef struct _ESourceKolabFolderClass ESourceKolabFolderClass;
typedef struct _ESourceKolabFolderPrivate ESourceKolabFolderPrivate;

/**
 * ESourceKolabFolder:
 *
 * Contains only private data that should be read and manipulated using the
 * functions below.
 **/
struct _ESourceKolabFolder {
	ESourceResource parent;
	ESourceKolabFolderPrivate *priv;
};

struct _ESourceKolabFolderClass {
	ESourceResourceClass parent_class;
};

GType		e_source_kolab_folder_get_type
					(void) G_GNUC_CONST;
KolabSyncStrategyID
		e_source_kolab_folder_get_sync_strategy
					(ESourceKolabFolder *extension);
void		e_source_kolab_folder_set_sync_strategy
					(ESourceKolabFolder *extension,
					 KolabSyncStrategyID sync_strategy);

G_END_DECLS

#endif /* E_SOURCE_KOLAB_FOLDER_H */

