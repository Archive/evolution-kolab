/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-data-folder-permissions.c
 *
 *  Fri Feb 17 17:19:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include "camel-imapx-acl.h"

#include "kolab-data-folder-permissions.h"

/*----------------------------------------------------------------------------*/

KolabDataFolderPermissions*
kolab_data_folder_permissions_new (void)
{
	KolabDataFolderPermissions *data = NULL;

	data = g_new0 (KolabDataFolderPermissions, 1);
	data->acl = NULL;
	data->myrights = NULL;

	return data;
}

KolabDataFolderPermissions*
kolab_data_folder_permissions_clone (const KolabDataFolderPermissions *srcdata,
                                     GError **err)
{
	KolabDataFolderPermissions *data = NULL;
	GError *tmp_err = NULL;

	if (srcdata == NULL)
		return NULL;

	data = g_new0 (KolabDataFolderPermissions, 1);

	data->acl = camel_imapx_acl_list_clone (srcdata->acl,
	                                        &tmp_err);
	if (tmp_err != NULL)
		goto exit;

	data->myrights = camel_imapx_acl_list_clone (srcdata->myrights,
	                                             &tmp_err);

 exit:
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		kolab_data_folder_permissions_free (data);
		return NULL;
	}

	return data;
}

void
kolab_data_folder_permissions_free (KolabDataFolderPermissions *data)
{
	if (data == NULL)
		return;

	camel_imapx_acl_list_free (data->acl);
	camel_imapx_acl_list_free (data->myrights);
	g_free (data);
}

/*----------------------------------------------------------------------------*/
