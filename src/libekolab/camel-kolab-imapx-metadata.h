/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-metadata.h
 *
 *  Tue Oct 19 18:58:03 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/
/* ANNOTATEMORE / METADATA (RFC 5464) (Kolab specifics) */

#ifndef _CAMEL_KOLAB_IMAPX_METADATA_H_
#define _CAMEL_KOLAB_IMAPX_METADATA_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include <libekolabutil/kolab-util-folder.h>

#include "camel-imapx-metadata.h"
#include "camel-kolab-imapx-metadata-db.h"

/*----------------------------------------------------------------------------*/

typedef struct _CamelKolabImapxMetadata CamelKolabImapxMetadata;
struct _CamelKolabImapxMetadata {
	CamelKolabImapxMetadataDb  *mdb;	/* persistent DB */
	GHashTable *kolab_metadata;		/* for CamelKolabFolderMetadata */
};

typedef struct _CamelKolabImapxFolderMetadata CamelKolabImapxFolderMetadata;
struct _CamelKolabImapxFolderMetadata {
	/* folder name kept as key in CamelKolabImapxMetadata kolab_metadata  */
	KolabFolderTypeID folder_type;
};

/*----------------------------------------------------------------------------*/

CamelKolabImapxMetadata*
camel_kolab_imapx_metadata_new (void);

void
camel_kolab_imapx_metadata_free (CamelKolabImapxMetadata *kmd);

gboolean
camel_kolab_imapx_metadata_init (CamelKolabImapxMetadata *kmd,
                                 const gchar *cachepath,
                                 GError **err);

gboolean
camel_kolab_imapx_metadata_shutdown (CamelKolabImapxMetadata *kmd,
                                     GError **err);

gboolean
camel_kolab_imapx_metadata_remove (CamelKolabImapxMetadata *kmd,
                                   const gchar *foldername,
                                   GError **err);

void
camel_kolab_imapx_metadata_update (CamelKolabImapxMetadata *kmd,
                                   CamelImapxMetadata *md,
                                   camel_imapx_metadata_proto_t proto);

CamelKolabImapxFolderMetadata*
camel_kolab_imapx_folder_metadata_new (void);

gboolean
camel_kolab_imapx_folder_metadata_free (CamelKolabImapxFolderMetadata *kfmd);

void
camel_kolab_imapx_folder_metadata_gdestroy (gpointer data);

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_KOLAB_IMAPX_METADATA_H_ */

/*----------------------------------------------------------------------------*/
