/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-mime-builder.h
 *
 *  Wed Jan 26 11:42:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-mail-mime-builder
 * @short_description: deal with MIME message parts and whole MIME messages
 * @title: KolabMailInfoDb
 * @section_id:
 * @see_also: #KolabMailAccess, #KolabMailSynchronizer, #KolabMailImapClient, #KolabMailSideCache
 * @stability: unstable
 *
 * This class constructs MIME messages (or message parts) from #Kolab_conv_mail
 * data and vice versa. It also contains facility for data checksumming.
 *
 * The MIME messages are read from and fed to the #KolabMailImapClient as well
 * as the #KolabMailSideCache. They (need to) carry header information which are
 * not part of Kolab_conv_mail, so header info needs to be (re)constructed from
 * email data. In the backends, we do operate mainly on "subject" MIME header
 * information, which carries the Kolab-UID, and the IMAP-UID for finding messages
 * in #KolabMailImapClient quickly.
 *
 * Serialized MIME message parts are read from and fed to #KolabMailSideCache. This is
 * used for newly created PIM mails when in offline mode. No MIME header information
 * needs to be stored, it will be created on-the-fly once the #KolabMailSideCache
 * contents gets synced in online mode.
 */

/*----------------------------------------------------------------------------*/

/* TODO use GMime or CamelMime internally?
 *      - need to be able to read/convert/create CamelMimeMessage<->Kolab_conv_mail
 *      - serialized MIME message needs to be base64-encoded
 *      - would be nice not to let Camel data format creep into
 *        KolabMailSideCache DB (but does serialized GMimeMessage differ from
 *        serialized CamelMimeMessage?)
 *
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_MAIL_MIME_BUILDER_H_
#define _KOLAB_MAIL_MIME_BUILDER_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolabconv/main/src/kolab-conv.h>
#include <libekolabutil/camel-system-headers.h>

#include "kolab-backend-types.h"
#include "kolab-settings-handler.h"

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

#define KOLAB_TYPE_MAIL_MIME_BUILDER             (kolab_mail_mime_builder_get_type ())
#define KOLAB_MAIL_MIME_BUILDER(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOLAB_TYPE_MAIL_MIME_BUILDER, KolabMailMimeBuilder))
#define KOLAB_MAIL_MIME_BUILDER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), KOLAB_TYPE_MAIL_MIME_BUILDER, KolabMailMimeBuilderClass))
#define KOLAB_IS_MAIL_MIME_BUILDER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOLAB_TYPE_MAIL_MIME_BUILDER))
#define KOLAB_IS_MAIL_MIME_BUILDER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), KOLAB_TYPE_MAIL_MIME_BUILDER))
#define KOLAB_MAIL_MIME_BUILDER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), KOLAB_TYPE_MAIL_MIME_BUILDER, KolabMailMimeBuilderClass))

typedef struct _KolabMailMimeBuilderClass KolabMailMimeBuilderClass;
typedef struct _KolabMailMimeBuilder KolabMailMimeBuilder;

struct _KolabMailMimeBuilderClass
{
	GObjectClass parent_class;
};

struct _KolabMailMimeBuilder
{
	GObject parent_instance;
};

GType
kolab_mail_mime_builder_get_type (void) G_GNUC_CONST;

gboolean
kolab_mail_mime_builder_configure (KolabMailMimeBuilder *self,
                                   KolabSettingsHandler *ksettings,
                                   GError **err);

gboolean
kolab_mail_mime_builder_bringup (KolabMailMimeBuilder *self,
                                 GError **err);

gboolean
kolab_mail_mime_builder_shutdown (KolabMailMimeBuilder *self,
                                  GError **err);

Kolab_conv_mail*
kolab_mail_mime_builder_conv_new_from_camel (KolabMailMimeBuilder *self,
                                             const CamelMimeMessage *message,
                                             GCancellable *cancellable,
                                             GError **err);

CamelMimeMessage*
kolab_mail_mime_builder_camel_new_from_conv (KolabMailMimeBuilder *self,
                                             const Kolab_conv_mail *kconvmail,
                                             GCancellable *cancellable,
                                             GError **err);

gboolean
kolab_mail_mime_builder_camel_set_header (KolabMailMimeBuilder *self,
                                          CamelMimeMessage *message,
                                          const KolabMailMimeBuilderHeaderInfo *headerinfo,
                                          CamelMimeMessage *orig_message,
                                          GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_MAIL_MIME_BUILDER_H_ */

/*----------------------------------------------------------------------------*/
