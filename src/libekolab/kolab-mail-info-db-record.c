/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-record.c
 *
 *  Fri Mar 04 14:31:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include "kolab-mail-info-db-record.h"

/*----------------------------------------------------------------------------*/
/* new/free/clone... */

KolabMailInfoDbRecord*
kolab_mail_info_db_record_new (void)
{
	gint ii = 0;
	KolabMailInfoDbRecord *record = g_new0 (KolabMailInfoDbRecord, 1);

	/* standard mail summary */
	record->summary = NULL;

	/* extra data */
	for (ii = 0; ii < KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD; ii++)
		record->rdata_char[ii] = NULL;
	for (ii = 0; ii < KOLAB_MAIL_INFO_DB_RECORD_UINT_LAST_FIELD; ii++)
		record->rdata_uint[ii] = 0;

	/* documenting folder context/type initial setting */
	record->rdata_uint[KOLAB_MAIL_INFO_DB_RECORD_UINT_FIELD_IMAP_FOLDER_TYPE] \
		= KOLAB_FOLDER_TYPE_INVAL;
	record->rdata_uint[KOLAB_MAIL_INFO_DB_RECORD_UINT_FIELD_IMAP_FOLDER_CONTEXT] \
		= KOLAB_FOLDER_CONTEXT_INVAL;

	return record;
}

KolabMailInfoDbRecord*
kolab_mail_info_db_record_clone (const KolabMailInfoDbRecord *record)
{
	gint ii = 0;
	KolabMailInfoDbRecord *new_record = NULL;

	if (record == NULL)
		return NULL;

	new_record = kolab_mail_info_db_record_new ();

	/* standard mail summary */
	new_record->summary = kolab_mail_summary_clone (record->summary);

	/* extra data */
	for (ii = 0; ii < KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD; ii++)
		new_record->rdata_char[ii] = g_strdup(record->rdata_char[ii]);
	for (ii = 0; ii < KOLAB_MAIL_INFO_DB_RECORD_UINT_LAST_FIELD; ii++)
		new_record->rdata_uint[ii] = record->rdata_uint[ii];

	return new_record;
}

void
kolab_mail_info_db_record_free (KolabMailInfoDbRecord *record)
{
	gint ii = 0;

	if (record == NULL)
		return;

	kolab_mail_summary_free (record->summary);
	for (ii = 0; ii < KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD; ii++) {
		if (record->rdata_char[ii] != NULL)
			g_free (record->rdata_char[ii]);
	}

	g_free (record);
}

void
kolab_mail_info_db_record_gdestroy (gpointer data)
{
	KolabMailInfoDbRecord *record = NULL;

	if (data == NULL)
		return;

	record = (KolabMailInfoDbRecord *)data;
	kolab_mail_info_db_record_free (record);
}

gboolean
kolab_mail_info_db_record_equal (const KolabMailInfoDbRecord *record1,
                                 const KolabMailInfoDbRecord *record2)
{
	guint ii =  0;

	if ((record1 == NULL) && (record2 == NULL))
		return TRUE;
	if ((record1 == NULL) && (record2 != NULL))
		return FALSE;
	if ((record1 != NULL) && (record2 == NULL))
		return FALSE;

	if (! kolab_mail_summary_equal (record1->summary, record2->summary))
		return FALSE;

	for (ii = 0; ii < KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD; ii++) {
		if (g_strcmp0 (record1->rdata_char[ii], record2->rdata_char[ii]) != 0)
			return FALSE;
	}
	for (ii = 0; ii < KOLAB_MAIL_INFO_DB_RECORD_UINT_LAST_FIELD; ii++) {
		if (record1->rdata_uint[ii] != record2->rdata_uint[ii])
			return FALSE;
	}

	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* getters/setters */

void
kolab_mail_info_db_record_set_char_field (KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordCharFieldID field_id,
                                          gchar *value)
{
	g_assert (record != NULL);
	g_assert (field_id < KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD);

	if (record->rdata_char[field_id] != NULL)
		g_free (record->rdata_char[field_id]);

	record->rdata_char[field_id] = value; /* no copy! */
}

const gchar*
kolab_mail_info_db_record_get_char_field (const KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordCharFieldID field_id)
{
	g_assert (record != NULL);
	g_assert (field_id < KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD);

	return record->rdata_char[field_id]; /* no copy! */
}

void
kolab_mail_info_db_record_set_uint_field (KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordUintFieldID field_id,
                                          guint value)
{
	g_assert (record != NULL);
	g_assert (field_id < KOLAB_MAIL_INFO_DB_RECORD_UINT_LAST_FIELD);

	record->rdata_uint[field_id] = value;
}

guint
kolab_mail_info_db_record_get_uint_field (const KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordUintFieldID field_id)
{
	g_assert (record != NULL);
	g_assert (field_id < KOLAB_MAIL_INFO_DB_RECORD_UINT_LAST_FIELD);

	return record->rdata_uint[field_id];
}

/*----------------------------------------------------------------------------*/
/* debugging */

void
kolab_mail_info_db_record_dump (const KolabMailInfoDbRecord *record)
{
	const KolabMailSummary *summary = NULL;

	g_debug ("\n%s: **** InfoDB Record Begin ****", __func__);

	if (record == NULL) {
		g_debug ("%s: Record is NULL", __func__);
		goto db_record_skip;
	}

	summary = record->summary;

	if (summary == NULL) {
		g_debug ("%s: Record summary is NULL", __func__);
		goto db_record_skip;
	}

	g_debug ("\n%s: Summary data:", __func__);
	g_debug ("%s: Kolab UID: (%s) IMAP UID: (%s)",
	         __func__,
	         kolab_mail_summary_get_char_field (summary,
	                                            KOLAB_MAIL_SUMMARY_CHAR_FIELD_KOLAB_UID),
	         kolab_mail_summary_get_char_field (summary,
	                                            KOLAB_MAIL_SUMMARY_CHAR_FIELD_IMAP_UID)
	         );
	g_debug ("%s: Folder Type: (%i) Context: (%i) Cache Location: (%i) Status: (%i)",
	         __func__,
	         kolab_mail_summary_get_uint_field (summary,
	                                            KOLAB_MAIL_SUMMARY_UINT_FIELD_FOLDER_TYPE),
	         kolab_mail_summary_get_uint_field (summary,
	                                            KOLAB_MAIL_SUMMARY_UINT_FIELD_FOLDER_CONTEXT),
	         kolab_mail_summary_get_uint_field (summary,
	                                            KOLAB_MAIL_SUMMARY_UINT_FIELD_CACHE_LOCATION),
	         kolab_mail_summary_get_uint_field (summary,
	                                            KOLAB_MAIL_SUMMARY_UINT_FIELD_CACHE_STATUS)
	         );
	g_debug ("%s: Complete: (%i)",
	         __func__,
	         kolab_mail_summary_get_bool_field (summary,
	                                            KOLAB_MAIL_SUMMARY_BOOL_FIELD_COMPLETE)
	         );

	g_debug ("\n%s: Extra data:", __func__);
	g_debug ("%s: IMAP SyncUID: (%s) Folder Name: (%s) Type: (%u) Context: (%u)",
	         __func__,
	         record->rdata_char[KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_UID_SYNC],
	         record->rdata_char[KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_FOLDER_NAME],
	         record->rdata_uint[KOLAB_MAIL_INFO_DB_RECORD_UINT_FIELD_IMAP_FOLDER_TYPE],
	         record->rdata_uint[KOLAB_MAIL_INFO_DB_RECORD_UINT_FIELD_IMAP_FOLDER_CONTEXT]);
	g_debug ("%s: Checksums: IMAP: (%s) Sync: (%s) Side: (%s)",
	         __func__,
	         record->rdata_char[KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_CHECKSUM],
	         record->rdata_char[KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_CHECKSUM_SYNC],
	         record->rdata_char[KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_SIDE_CHECKSUM]);

 db_record_skip:
	g_debug ("\n%s: **** InfoDB Record End ****\n", __func__);
}

/*----------------------------------------------------------------------------*/
