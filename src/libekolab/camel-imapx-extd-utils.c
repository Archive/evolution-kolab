/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-utils.c
 *
 *  2012-09-28, 13:44:28
 *  Copyright 2012, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include "camel-imapx-extd-utils.h"

/*----------------------------------------------------------------------------*/

gboolean
camel_imapx_extd_utils_command_run (CamelIMAPXServer *is,
                                    const gchar *cmd_token,
                                    const gchar *cmd,
                                    GCancellable *cancellable,
                                    GError **err)
{
	static GMutex runner_lock;
	CamelIMAPXCommand *ic = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_return_val_if_fail (cmd_token != NULL, FALSE);
	g_return_val_if_fail (cmd != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_mutex_lock (&runner_lock);

	ic = camel_imapx_command_new (is, cmd_token, NULL, cmd);
	/* TODO set some job details like priority? */

	/* run IMAP command */
	ok = camel_imapx_server_command_run (is,
	                                     ic,
	                                     cancellable,
	                                     &tmp_err);
	camel_imapx_command_unref (ic);

	if (! ok)
		g_propagate_error (err, tmp_err);

	g_mutex_unlock (&runner_lock);

	return ok;
}

/*----------------------------------------------------------------------------*/

