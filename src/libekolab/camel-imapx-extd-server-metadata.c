/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-server-metadata.c
 *
 *  2012-07-27, 11:02:38
 *  Copyright 2013, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include "camel-imapx-extd-store.h"
#include "camel-imapx-extd-store-friend.h"
#include "camel-imapx-extd-server.h"
#include "camel-imapx-extd-utils.h"

#include "camel-imapx-extd-server-metadata.h"

/*----------------------------------------------------------------------------*/

static gboolean
imapx_extd_server_untagged_annotation (CamelIMAPXServer *is,
                                       CamelIMAPXStream *stream,
                                       GCancellable *cancellable,
                                       GError **err);

static const CamelIMAPXUntaggedRespHandlerDesc desc_annotation = {
	IMAPX_IMAP_TOKEN_ANNOTATION,           /* untagged_response     */
	imapx_extd_server_untagged_annotation, /* handler               */
	NULL,                                  /* next_response         */
	TRUE                                   /* skip_stream_when_done */
};

/*----------------------------------------------------------------------------*/

static gboolean
imapx_extd_server_untagged_annotation (CamelIMAPXServer *is,
                                       CamelIMAPXStream *stream,
                                       GCancellable *cancellable,
                                       GError **err)
{
	static GMutex handler_lock;
	CamelIMAPXStore *store = NULL;
	CamelIMAPXExtdStore *estore = NULL;
	CamelImapxMetadata *md = NULL;
	guint32 capa = 0;
	guint32 capa_flag_id = 0;
	GError *tmp_err = NULL;
	gboolean parse_and_add_ok = FALSE;
	gboolean success = FALSE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_mutex_lock (&handler_lock);

	store = camel_imapx_server_ref_store (is);
	estore = CAMEL_IMAPX_EXTD_STORE (store);

	/* capability check */
	capa_flag_id =
		camel_imapx_extd_store_get_capa_flag_id (estore,
		                                         CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ANNOTATEMORE);
	capa = is->cinfo->capa & capa_flag_id;
	if (! capa) {
		g_set_error (err,
		             CAMEL_IMAPX_ERROR,
		             1, /* FIXME define and add a sensible code here */
		             _("Got ANNOTATION response but server did not advertise ANNOTATEMORE capability"));
		goto exit;
	}

	md = camel_imapx_extd_store_get_md_table (estore);
	parse_and_add_ok =
		camel_imapx_metadata_add_from_server_response (md,
		                                               stream,
		                                               cancellable,
		                                               &tmp_err);

	if (! parse_and_add_ok) {
		g_propagate_error (err, tmp_err);
		goto exit;
	}

	success = TRUE;

exit:
	g_object_unref (store);

	g_mutex_unlock (&handler_lock);

	return success;
}

/*----------------------------------------------------------------------------*/

KolabGConstList*
camel_imapx_extd_server_metadata_get_handler_descriptors (void)
{
	KolabGConstList *list = NULL;
	list = kolab_util_glib_gconstlist_prepend (list,
	                                           (gconstpointer)(&desc_annotation));
	return list;
}

gboolean
camel_imapx_extd_server_get_metadata (CamelIMAPXServer *is,
                                      CamelImapxMetadataSpec *spec,
                                      GCancellable *cancellable,
                                      GError **err)
{
	gchar *cmd = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_assert (spec != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* TODO move GETANNOTATION string to -metadata.[hc]
	 *      (add a function to return proper string depending
	 *      on protocol type)
	 */
	cmd = g_strdup_printf ("%s \"%s\" \"%s\" \"%s\"",
	                       IMAPX_IMAP_TOKEN_GETANNOTATION,
	                       spec->mailbox_name,
	                       spec->entry_name,
	                       spec->attrib_name);

	/* run GETANNOTATION command */
	ok = camel_imapx_extd_utils_command_run (is,
	                                         IMAPX_IMAP_TOKEN_GETANNOTATION,
	                                         cmd,
	                                         cancellable,
	                                         &tmp_err);
	g_free (cmd);

	if (! ok)
		g_propagate_error (err, tmp_err);

	return ok;
}

gboolean
camel_imapx_extd_server_set_metadata (CamelIMAPXServer *is,
                                      CamelImapxMetadata *md,
                                      GCancellable *cancellable,
                                      GError **err)
{
	static GMutex setter_lock;
	GSList *commands = NULL;
	GSList *cur_cmd = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_assert (md != NULL);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_mutex_lock (&setter_lock);

	commands = camel_imapx_metadata_new_commandlist (md);

	if (commands == NULL)
		goto exit;

	cur_cmd = commands;
	while (cur_cmd != NULL) {
		/* TODO move SETANNOTATION string to -metadata.[hc]
		 *      (add a function to return proper string depending
		 *      on protocol type)
		 */
		ok = camel_imapx_extd_utils_command_run (is,
		                                         IMAPX_IMAP_TOKEN_SETANNOTATION,
		                                         (gchar *) cur_cmd->data,
		                                         cancellable,
		                                         &tmp_err);
		if (! ok)
			goto exit;

		cur_cmd = g_slist_next (cur_cmd);
	}

 exit:
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}
	if (commands != NULL)
		g_slist_free (commands);

	g_mutex_unlock (&setter_lock);

	return ok;
}

/*----------------------------------------------------------------------------*/
