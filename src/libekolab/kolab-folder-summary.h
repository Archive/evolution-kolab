/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-folder-summary.h
 *
 *  Mon Feb 14 11:43:25 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-folder-summary
 * @short_description: Summary information for a Kolab PIM folder
 * @title: KolabFolderSummary
 * @section_id:
 * @see_also: #KolabMailInfoDb, #KolabMailSummary
 * @stability: unstable
 *
 * A #KolabFolderSummary is stored permanently in the #KolabMailInfoDb for
 * each known Kolab PIM folder.
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_FOLDER_SUMMARY_H_
#define _KOLAB_FOLDER_SUMMARY_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

/*----------------------------------------------------------------------------*/

typedef enum {
	KOLAB_FOLDER_SUMMARY_CHAR_FIELD_FOLDERNAME = 0,
	/* LAST */
	KOLAB_FOLDER_SUMMARY_CHAR_LAST_FIELD
} KolabFolderSummaryCharFieldID;

typedef enum {
	/* Kolab intern */
	KOLAB_FOLDER_SUMMARY_UINT_FIELD_CACHE_LOCATION = 0,	/* KolabObjectCacheLocation */
	KOLAB_FOLDER_SUMMARY_UINT_FIELD_CACHE_STATUS,		/* KolabObjectCacheStatus */
	KOLAB_FOLDER_SUMMARY_UINT_FIELD_FOLDER_TYPE,		/* KolabFolderTypeID */
	KOLAB_FOLDER_SUMMARY_UINT_FIELD_FOLDER_CONTEXT,		/* KolabFolderContextID */
	/* LAST */
	KOLAB_FOLDER_SUMMARY_UINT_LAST_FIELD
}  KolabFolderSummaryUintFieldID;

typedef enum {
	KOLAB_FOLDER_SUMMARY_UINT64_FIELD_UIDVALIDITY = 0,	/* IMAP folder uidvalidity (always updated) */
	KOLAB_FOLDER_SUMMARY_UINT64_FIELD_UIDVALIDITY_SYNC,	/* IMAP folder uidvalidity (updated only after sync) */
	/* LAST */
	KOLAB_FOLDER_SUMMARY_UINT64_LAST_FIELD
} KolabFolderSummaryUint64FieldID;

typedef enum {
	/* LAST */
	KOLAB_FOLDER_SUMMARY_INT_LAST_FIELD
} KolabFolderSummaryIntFieldID;

typedef enum {
	/* LAST */
	KOLAB_FOLDER_SUMMARY_BOOL_LAST_FIELD
} KolabFolderSummaryBoolFieldID;

/* SQLiteDB column names for KolabFolderSummary */
enum {
	KOLAB_FOLDER_SUMMARY_SQLCOL_CHAR_FIELD_FOLDERNAME = 0,
	KOLAB_FOLDER_SUMMARY_SQLCOL_UINT_FIELD_CACHE_LOCATION,
	KOLAB_FOLDER_SUMMARY_SQLCOL_UINT_FIELD_CACHE_STATUS,
	KOLAB_FOLDER_SUMMARY_SQLCOL_UINT_FIELD_FOLDER_TYPE,
	KOLAB_FOLDER_SUMMARY_SQLCOL_UINT_FIELD_FOLDER_CONTEXT,
	KOLAB_FOLDER_SUMMARY_SQLCOL_UINT64_FIELD_UIDVALIDITY,
	KOLAB_FOLDER_SUMMARY_SQLCOL_UINT64_FIELD_UIDVALIDITY_SYNC,
	/* LAST */
	KOLAB_FOLDER_SUMMARY_LAST_SQLCOL
};

typedef struct _KolabFolderSummary KolabFolderSummary;
struct _KolabFolderSummary
{
	gchar   *sdata_char[KOLAB_FOLDER_SUMMARY_CHAR_LAST_FIELD];
	guint    sdata_uint[KOLAB_FOLDER_SUMMARY_UINT_LAST_FIELD];
	guint64  sdata_uint64[KOLAB_FOLDER_SUMMARY_UINT64_LAST_FIELD];
#if 0
	gint     sdata_int[KOLAB_FOLDER_SUMMARY_INT_LAST_FIELD];
	gboolean sdata_bool[KOLAB_FOLDER_SUMMARY_BOOL_LAST_FIELD];
#endif
};

KolabFolderSummary* kolab_folder_summary_new (void);
KolabFolderSummary* kolab_folder_summary_clone (const KolabFolderSummary *summary);
void kolab_folder_summary_free (KolabFolderSummary *summary);
void kolab_folder_summary_gdestroy (gpointer data);

gboolean kolab_folder_summary_check (const KolabFolderSummary *summary);

void kolab_folder_summary_set_char_field (KolabFolderSummary *summary, KolabFolderSummaryCharFieldID field_id, gchar *value);
const gchar* kolab_folder_summary_get_char_field (const KolabFolderSummary *summary, KolabFolderSummaryCharFieldID field_id);

void kolab_folder_summary_set_uint_field (KolabFolderSummary *summary, KolabFolderSummaryUintFieldID field_id, guint value);
guint kolab_folder_summary_get_uint_field (const KolabFolderSummary *summary, KolabFolderSummaryUintFieldID field_id);

void kolab_folder_summary_set_uint64_field (KolabFolderSummary *summary, KolabFolderSummaryUint64FieldID field_id, guint64 value);
guint64 kolab_folder_summary_get_uint64_field (const KolabFolderSummary *summary, KolabFolderSummaryUint64FieldID field_id);

#if 0
void kolab_folder_summary_set_int_field (KolabFolderSummary *summary, KolabFolderSummaryIntFieldID field_id, gint value);
gint kolab_folder_summary_get_int_field (const KolabFolderSummary *summary, KolabFolderSummaryIntFieldID field_id);

void kolab_folder_summary_set_bool_field (KolabFolderSummary *summary, KolabFolderSummaryBoolFieldID field_id, gboolean value);
gboolean kolab_folder_summary_get_bool_field (const KolabFolderSummary *summary, KolabFolderSummaryBoolFieldID field_id);
#endif

void kolab_folder_summary_dump (const KolabFolderSummary *summary);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_FOLDER_SUMMARY_H_ */

/*----------------------------------------------------------------------------*/
