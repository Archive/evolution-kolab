/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-backend.h
 *
 *  Mon Jan 17 11:16:52 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_BACKEND_H_
#define _KOLAB_UTIL_BACKEND_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gio/gio.h>

#include <libebook/libebook.h>
#include <libecal/libecal.h>
#include <libebackend/libebackend.h>

#include <libekolabutil/camel-system-headers.h>
#include <libekolabutil/kolab-util-sqlite.h>

#include "kolab-settings-handler.h"
#include "kolab-types.h"
#include "kolab-backend-types.h"

/*----------------------------------------------------------------------------*/

struct _KolabMailAccess;

/*----------------------------------------------------------------------------*/
/* GError for libekolabbackend */

#define KOLAB_BACKEND_ERROR (kolab_util_backend_error_quark ())

typedef enum {
	KOLAB_BACKEND_ERROR_GENERIC = 0,	/* lazy fallback error */
	KOLAB_BACKEND_ERROR_INTERNAL,		/* internal (programming) error */
	KOLAB_BACKEND_ERROR_NOTFOUND,		/* any object was not found */
	KOLAB_BACKEND_ERROR_SYNC_NOTSTORED,     /* object not stored due to sync conflict */
	KOLAB_BACKEND_ERROR_CONTEXT_MISUSE,     /* calendar/contacts mixup */
	KOLAB_BACKEND_ERROR_DATATYPE_EVOLUTION, /* Evo data type error */
	KOLAB_BACKEND_ERROR_DATATYPE_KOLAB,     /* Kolab data type error */
	KOLAB_BACKEND_ERROR_DATATYPE_INTERNAL,  /* Backend data type error */
	KOLAB_BACKEND_ERROR_HANDLE_INCOMPLETE,  /* not-yet-completed KolabMailHandle */
	KOLAB_BACKEND_ERROR_STATE_WRONG_FOR_OP, /* wrong state for requested operation */
	KOLAB_BACKEND_ERROR_INFODB_GENERIC,     /* for cumulated InfoDb errors */
	KOLAB_BACKEND_ERROR_INFODB_NOFOLDER ,   /* item destination folder unknown */
	KOLAB_BACKEND_ERROR_INFODB_EXISTS,      /* item to be added already exists */
	KOLAB_BACKEND_ERROR_CAMEL	        /* Camel system error */
} KolabBackendError;

GQuark kolab_util_backend_error_quark (void) G_GNUC_CONST;

/*----------------------------------------------------------------------------*/

const gchar *kolab_util_backend_get_foldername (EBackend *backend);
gchar *kolab_util_backend_get_relative_path_from_uri (const gchar *uri);
gchar *kolab_util_backend_get_protocol_from_uri (const gchar *uri);
KolabSyncStrategyID kolab_util_misc_sync_value_from_property (const gchar *sync_prop);
KolabTLSVariantID kolab_util_misc_tls_variant_from_property (const gchar *tls_variant);
gint kolab_util_misc_port_number_from_property (const gchar *port_number_prop);
KolabReqPkcs11 kolab_util_misc_req_pkcs11_from_property (const gchar *req_pkcs11_prop);
void kolab_util_backend_koma_table_cleanup_cb (gpointer data, GObject *object, gboolean is_last_ref);

void kolab_util_backend_modtime_set_on_econtact (EContact *econtact);
void kolab_util_backend_modtime_set_on_ecalcomp (ECalComponent *ecalcomp);

gboolean kolab_util_backend_sqlite_db_new_open (KolabUtilSqliteDb **kdb, KolabSettingsHandler *ksettings, const gchar *dbfilename, GError **err);

const gchar* kolab_util_backend_get_sync_strategy_desc (KolabSyncStrategyID id);
const gchar* kolab_util_backend_get_tls_variant_desc (KolabTLSVariantID id);

gchar* kolab_util_backend_account_uid_new_from_settings (KolabSettingsHandler *ksettings, GError **err);

KolabMailAccessOpmodeID
kolab_util_backend_deploy_mode_by_koma (struct _KolabMailAccess *koma,
                                        KolabMailAccessOpmodeID koma_mode,
                                        GCancellable *cancellable,
                                        GError **error);

gboolean
kolab_util_backend_deploy_mode_by_backend (struct _KolabMailAccess *koma,
                                           gboolean online,
                                           GCancellable *cancellable,
                                           GError **error);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_BACKEND_H_ */

/*----------------------------------------------------------------------------*/
