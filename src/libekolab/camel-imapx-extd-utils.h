/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-utils.h
 *
 *  2012-09-28, 13:44:28
 *  Copyright 2012, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_IMAPX_EXTD_UTILS_H_
#define _CAMEL_IMAPX_EXTD_UTILS_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolabutil/camel-system-headers.h>

/*----------------------------------------------------------------------------*/

gboolean
camel_imapx_extd_utils_command_run (CamelIMAPXServer *is,
                                    const gchar *cmd_token,
                                    const gchar *cmd,
                                    GCancellable *cancellable,
                                    GError **err);

/*----------------------------------------------------------------------------*/

#endif /* CAMEL_IMAPX_EXTD_UTILS_H_ */

/*----------------------------------------------------------------------------*/
