/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-acl.c
 *
 *  2012-07-30, 19:09:28
 *  Copyright 2012, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/
/* ACL (RFC 4314) */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <glib/gi18n-lib.h>

#include <libekolabutil/camel-system-headers.h>
#include <libekolabutil/kolab-util-camel.h>

#include "camel-imapx-acl.h"

/*----------------------------------------------------------------------------*/

#define CAMEL_IMAPX_ACL_KNOWN_RIGHTS "lrswipkxteacd"

/*----------------------------------------------------------------------------*/

static void
imapx_acl_entry_free (CamelImapxAclEntry *entry)
{
	if (entry == NULL)
		return;

	if (entry->access_id != NULL)
		g_free (entry->access_id);

	if (entry->rights != NULL)
		g_free (entry->rights);

	g_free (entry);
}

static gboolean
imapx_acl_entry_validate_access_id (const gchar *access_id,
                                    GError **err)
{
	const gchar *id_ptr = NULL;
	gunichar uc;
	gboolean ok = TRUE;

	g_return_val_if_fail (access_id != NULL, FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* access_id is required to be valid UTF-8 */
	ok = g_utf8_validate (access_id, -1, NULL);
	if (! ok)
		goto exit;

	/* access_id may not contain whitespace
	 * and non-printables
	 */
	id_ptr = access_id;
	while (id_ptr != NULL) {
		uc = g_utf8_get_char (id_ptr);
		if (! g_unichar_isgraph (uc)) {
			ok = FALSE;
			goto exit;
		}
		id_ptr = g_utf8_next_char (id_ptr);
		if (g_strcmp0 (id_ptr, "") == 0)
			break;
	}

	/* add more checks here */

 exit:

	if (! ok) {
		g_set_error (err,
		             CAMEL_ERROR,
		             CAMEL_ERROR_GENERIC,
		             _("Invalid IMAP ACL AccessID"));
	}

	return ok;
}

static gboolean
imapx_acl_entry_validate_rights (const gchar *rights,
                                 GError **err)
{
	const gchar *rights_ptr = NULL;
	gboolean ok = TRUE;

	/* rights may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	if (rights == NULL)
		return TRUE;

	/* if non-NULL, rights is required to be alphanumeric */
	rights_ptr = rights;
	while (rights_ptr != NULL) {
		if (! g_ascii_isalnum (rights_ptr[0])) {
			ok = FALSE;
			goto exit;
		}
		rights_ptr++;
		if (g_strcmp0 (rights_ptr, "") == 0)
			break;
	}

	/* add more checks here */

 exit:

	if (! ok) {
		g_set_error (err,
		             CAMEL_ERROR,
		             CAMEL_ERROR_GENERIC,
		             _("Invalid IMAP ACL Rights"));
	}

	return ok;
}

static gboolean
imapx_acl_entry_validate (const CamelImapxAclEntry *entry,
                          GError **err)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (entry != NULL, FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = imapx_acl_entry_validate_access_id (entry->access_id,
	                                         err);
	if (! ok)
		return FALSE;

	ok = imapx_acl_entry_validate_rights (entry->rights,
	                                      err);
	return ok;
}

static void
imapx_acl_entry_gdestroy (gpointer data)
{
	CamelImapxAclEntry *entry = NULL;

	if (data == NULL)
		return;

	entry = (CamelImapxAclEntry *) data;
	imapx_acl_entry_free (entry);
}

static CamelImapxAclCmd*
imapx_acl_cmd_new (const gchar *token,
                   const gchar *command)
{
	CamelImapxAclCmd *cmd = NULL;

	g_return_val_if_fail (token != NULL, NULL);
	g_return_val_if_fail (command != NULL, NULL);

	cmd = g_new0 (CamelImapxAclCmd, 1);
	cmd->token = g_strdup (token);
	cmd->command = g_strdup (command);

	return cmd;
}

static void
imapx_acl_cmd_free (CamelImapxAclCmd *cmd)
{
	if (cmd == NULL)
		return;

	if (cmd->token != NULL)
		g_free (cmd->token);
	if (cmd->command != NULL)
		g_free (cmd->command);

	g_free (cmd);
}

static void
imapx_acl_cmd_gdestroy (gpointer data)
{
	CamelImapxAclCmd *cmd = NULL;

	if (data == NULL)
		return;

	cmd = (CamelImapxAclCmd *) data;
	imapx_acl_cmd_free (cmd);
}

static GHashTable*
imapx_acl_myrights_table_new (void)
{
	GHashTable *tbl = NULL;

	tbl = g_hash_table_new_full (g_str_hash,
	                             g_str_equal,
	                             g_free,
	                             g_free);
	return tbl;
}

static GHashTable*
imapx_acl_entries_table_new (void)
{
	GHashTable *tbl = NULL;

	tbl = g_hash_table_new_full (g_str_hash,
	                             g_str_equal,
	                             g_free,
	                             g_free);
	return tbl;
}

static GHashTable*
imapx_acl_mboxes_table_new (void)
{
	GHashTable *tbl = NULL;

	tbl = g_hash_table_new_full (g_str_hash,
	                             g_str_equal,
	                             g_free,
	                             (GDestroyNotify) g_hash_table_destroy);
	return tbl;
}

static gboolean
imapx_acl_validate (CamelImapxAcl *acl,
                    GError **err)
{
	GHashTable *entries = NULL;
	GList *keys = NULL;
	GList *keys_ptr = NULL;
	GList *entries_keys = NULL;
	GList *entries_keys_ptr = NULL;
	gpointer tmp = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_return_val_if_fail (acl != NULL, FALSE);
	g_return_val_if_fail (acl->mboxes != NULL, FALSE);
	g_return_val_if_fail (acl->myrights != NULL, FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* caller should hold acl->lock */

	/* MYRIGHTS */
	keys = g_hash_table_get_keys (acl->myrights);
	keys_ptr = keys;
	while (keys_ptr != NULL) {
		tmp = g_hash_table_lookup (acl->myrights, keys_ptr->data);

		ok = imapx_acl_entry_validate_rights ((const gchar *) tmp,
		                                      &tmp_err);
		if (! ok)
			goto exit;

		keys_ptr = g_list_next (keys_ptr);
	}
	g_list_free (keys);

	/* ACL */
	keys = g_hash_table_get_keys (acl->mboxes);
	keys_ptr = keys;
	while (keys_ptr != NULL) {
		entries = g_hash_table_lookup (acl->mboxes,
		                               keys_ptr->data);
		if (entries == NULL) {
			g_set_error (&tmp_err,
			             CAMEL_IMAPX_ERROR,
			             0, /* FIXME define sensible error number */
			             _("Invalid internal IMAP ACL datastructure"));
			goto exit;
		}

		/* mbox rights entries */
		entries_keys = g_hash_table_get_keys (entries);
		entries_keys_ptr = entries_keys;
		while (entries_keys_ptr != NULL) {
			ok = imapx_acl_entry_validate_access_id ((const gchar *) (entries_keys_ptr->data),
			                                         &tmp_err);
			if (! ok) {
				g_list_free (entries_keys);
				goto exit;
			}

			tmp = g_hash_table_lookup (entries,
			                           entries_keys_ptr->data);
			ok = imapx_acl_entry_validate_rights ((const gchar *) tmp,
			                                      &tmp_err);
			if (! ok) {
				g_list_free (entries_keys);
				goto exit;
			}

			entries_keys_ptr = g_list_next (entries_keys_ptr);
		}
		g_list_free (entries_keys);

		keys_ptr = g_list_next (keys_ptr);
	}

 exit:

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	g_list_free (keys);

	return ok;
}

static gchar*
imapx_acl_rights_get_unknown (const gchar *rights)
{
	gchar *residue = NULL;
	gchar **set = NULL;

	if (rights == NULL)
		return NULL;

	set = g_strsplit_set (rights,
	                      CAMEL_IMAPX_ACL_KNOWN_RIGHTS,
	                      -1);
	residue = g_strjoinv (NULL, set);
	g_strfreev (set);

	return residue;
}

static void
imapx_acl_commandlist_free (GList *commands)
{
	if (commands == NULL)
		return;

	g_list_free_full (commands,
	                  imapx_acl_cmd_gdestroy);
}

/*----------------------------------------------------------------------------*/

CamelImapxAclSpec*
camel_imapx_acl_spec_new (const gchar *mbox_name,
                          CamelImapxAclType type)
{
	CamelImapxAclSpec *spec = NULL;

	g_return_val_if_fail (mbox_name != NULL, NULL);
	g_return_val_if_fail (type > 0, NULL);

	spec = g_new0 (CamelImapxAclSpec, 1);
	spec->mbox_name = g_strdup (mbox_name);
	spec->type = type;

	return spec;
}

void
camel_imapx_acl_spec_free (CamelImapxAclSpec *spec)
{
	if (spec == NULL)
		return;

	if (spec->mbox_name != NULL)
		g_free (spec->mbox_name);

	g_free (spec);
}

CamelImapxAclEntry*
camel_imapx_acl_entry_new (const gchar *access_id,
                           const gchar *rights,
                           GError **err)
{
	CamelImapxAclEntry *entry = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	/* access_id may be NULL (for MYRIGHTS) */
	/* rights may be NULL (for no rights) */
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	if ((access_id != NULL) && (err != NULL)) {
		ok = imapx_acl_entry_validate_access_id (access_id,
		                                         &tmp_err);
		if (! ok)
			goto exit;
	}

	if ((rights != NULL) && (err != NULL)) {
		ok = imapx_acl_entry_validate_rights (rights,
		                                      &tmp_err);
		if (! ok)
			goto exit;
	}

	entry = g_new0 (CamelImapxAclEntry, 1);
	entry->access_id = g_strdup (access_id);
	entry->rights = g_strdup (rights);

 exit:
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return NULL;
	}

	return entry;
}

CamelImapxAclEntry*
camel_imapx_acl_entry_clone (const CamelImapxAclEntry *entry,
                             GError **err)
{
	CamelImapxAclEntry *new_entry = NULL;

	if (entry == NULL)
		return NULL;

	new_entry = camel_imapx_acl_entry_new (entry->access_id,
	                                       entry->rights,
	                                       err);
	return new_entry;
}

void
camel_imapx_acl_entry_free (CamelImapxAclEntry *entry)
{
	imapx_acl_entry_free (entry);
}

CamelImapxAclCmd*
camel_imapx_acl_cmd_new (const gchar *token,
                         const gchar *command)
{
	return imapx_acl_cmd_new (token, command);
}

void
camel_imapx_acl_cmd_free (CamelImapxAclCmd *cmd)
{
	imapx_acl_cmd_free (cmd);
}

CamelImapxAcl*
camel_imapx_acl_new (gboolean locked)
{
	CamelImapxAcl *acl = g_new0 (CamelImapxAcl, 1);
	g_mutex_init (&(acl->lock));

	if (locked)
		g_mutex_lock (&(acl->lock));

	/* foldername:myrights */
	acl->myrights = imapx_acl_myrights_table_new ();

	/* foldername:access_id:rights */
	acl->mboxes = imapx_acl_mboxes_table_new ();

	return acl;
}

void
camel_imapx_acl_free (CamelImapxAcl *acl)
{
	if (acl == NULL)
		return;

	while (! g_mutex_trylock (&(acl->lock)));

	if (acl->myrights != NULL)
		g_hash_table_destroy (acl->myrights);

	if (acl->mboxes != NULL)
		g_hash_table_destroy (acl->mboxes);

	g_mutex_unlock (&(acl->lock));
	g_mutex_clear (&(acl->lock));

	g_free (acl);
}

CamelImapxAcl*
camel_imapx_acl_resect (CamelImapxAcl *acl)
{
	CamelImapxAcl *tmp_acl = NULL;
	GHashTable *myrights = NULL;
	GHashTable *mboxes = NULL;

	if (acl == NULL)
		return NULL;

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	tmp_acl = camel_imapx_acl_new (FALSE);

	myrights = acl->myrights;
	mboxes = acl->mboxes;

	acl->myrights = tmp_acl->myrights;
	acl->mboxes = tmp_acl->mboxes;

	tmp_acl->myrights = myrights;
	tmp_acl->mboxes = mboxes;

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return tmp_acl;
}

gboolean
camel_imapx_acl_validate (CamelImapxAcl *acl,
                          GError **err)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (acl != NULL, FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	ok = imapx_acl_validate (acl, err);

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return ok;
}

gchar*
camel_imapx_acl_get_myrights (CamelImapxAcl *acl,
                              const gchar *mbox_name)
{
	gchar *r = NULL;
	gchar *rights = NULL;

	g_return_val_if_fail (acl != NULL, NULL);
	g_return_val_if_fail (acl->myrights != NULL, NULL);
	g_return_val_if_fail (mbox_name != NULL, NULL);

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	r = g_hash_table_lookup (acl->myrights,
	                         mbox_name);
	rights = g_strdup (r);

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return rights;
}

gboolean
camel_imapx_acl_update_myrights (CamelImapxAcl *acl,
                                 const gchar *mbox_name,
                                 const gchar *rights,
                                 GError **err)
{
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (acl != NULL, FALSE);
	g_return_val_if_fail (acl->myrights != NULL, FALSE);
	g_return_val_if_fail (mbox_name != NULL, FALSE);
	/* rights may be NULL (used for deletion) */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	if (rights != NULL) {
		ok = imapx_acl_entry_validate_rights (rights,
		                                      &tmp_err);
		if (! ok) {
			g_propagate_error (err, tmp_err);
			return FALSE;
		}
	}

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	if (rights != NULL) {
		g_hash_table_replace (acl->myrights,
		                      g_strdup (mbox_name),
		                      g_strdup (rights));
	} else {
		g_hash_table_remove (acl->myrights,
		                     mbox_name);
	}

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return TRUE;
}

gboolean
camel_imapx_acl_update_from_acl (CamelImapxAcl *acl,
                                 CamelImapxAcl *src_acl,
                                 GError **err)
{
	GList *mbox_names = NULL;
	GList *mbox_names_ptr = NULL;
	GList *access_ids = NULL;
	GList *access_ids_ptr = NULL;
	GHashTable *src_entries = NULL;
	GHashTable *new_entries = NULL;
	gchar *rights = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (acl != NULL, FALSE);
	g_return_val_if_fail (acl->mboxes != NULL, FALSE);
	g_return_val_if_fail (acl->myrights != NULL, FALSE);
	/* src_acl may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	if (src_acl == NULL)
		return TRUE;

	/* (acquire src_acl lock) */
	g_mutex_lock (&(src_acl->lock));

	ok = imapx_acl_validate (src_acl, &tmp_err);
	if (! ok)
		goto src_acl_skip;

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	if (src_acl->mboxes == NULL)
		goto acl_skip;

	/* update general ACL */
	mbox_names = g_hash_table_get_keys (src_acl->mboxes);
	mbox_names_ptr = mbox_names;
	while (mbox_names_ptr != NULL) {

		src_entries = g_hash_table_lookup (src_acl->mboxes,
		                                   mbox_names_ptr->data);
		if (src_entries == NULL) {
			/* should not happen */
			g_warning ("%s()[%u] NULL ACL source entries table for '%s'",
			           __func__, __LINE__, (gchar *) mbox_names_ptr->data);
			(void) g_hash_table_remove (acl->mboxes,
			                            mbox_names_ptr->data);
			goto skip;
		}

		new_entries = imapx_acl_entries_table_new ();
		access_ids = g_hash_table_get_keys (src_entries);
		access_ids_ptr = access_ids;
		while (access_ids_ptr != NULL) {
			rights = g_hash_table_lookup (src_entries,
			                              access_ids_ptr->data);
			g_hash_table_insert (new_entries,
			                     g_strdup ((gchar *) access_ids_ptr->data),
			                     g_strdup (rights));

			access_ids_ptr = g_list_next (access_ids_ptr);
		}
		g_list_free (access_ids);

		g_hash_table_replace (acl->mboxes,
		                      g_strdup ((gchar *) mbox_names_ptr->data),
		                      new_entries);

	skip:
		mbox_names_ptr = g_list_next (mbox_names_ptr);
	}
	g_list_free (mbox_names);

 acl_skip:

	if (src_acl->myrights == NULL)
		goto myrights_skip;

	/* update MYRIGHTS */
	mbox_names = g_hash_table_get_keys (src_acl->myrights);
	mbox_names_ptr = mbox_names;
	while (mbox_names_ptr) {
		rights = g_hash_table_lookup (src_acl->myrights,
		                              mbox_names_ptr->data);
		g_hash_table_replace (acl->myrights,
		                      g_strdup ((gchar *) mbox_names_ptr->data),
		                      g_strdup (rights));
		mbox_names_ptr = g_list_next (mbox_names_ptr);
	}
	g_list_free (mbox_names);

 myrights_skip:

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

 src_acl_skip:

	/* (release src_acl lock) */
	g_mutex_unlock (&(src_acl->lock));

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	return TRUE;
}

gboolean
camel_imapx_acl_update_from_list (CamelImapxAcl *acl,
                                  const gchar *mbox_name,
                                  const GList *entries,
                                  GError **err)
{
	const CamelImapxAclEntry *entry = NULL;
	const GList *entries_ptr = NULL;
	GHashTable *new_entries_tbl = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (acl != NULL, FALSE);
	g_return_val_if_fail (acl->mboxes != NULL, FALSE);
	g_return_val_if_fail (mbox_name != NULL, FALSE);
	/* entries may be NULL (used for deletion) */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* We replace the whole ACL for the
	 * named mailbox with the list contents
	 * instead of doing some merge operation
	 */

	if (entries == NULL)
		goto skip;

	new_entries_tbl = imapx_acl_entries_table_new ();
	entries_ptr = entries;
	while (entries_ptr != NULL) {
		entry = (const CamelImapxAclEntry *) entries_ptr->data;

		ok = imapx_acl_entry_validate (entry,
		                               &tmp_err);
		if (! ok) {
			g_propagate_error (err, tmp_err);
			g_hash_table_destroy (new_entries_tbl);
			return FALSE;
		}

		g_hash_table_insert (new_entries_tbl,
		                     g_strdup (entry->access_id),
		                     g_strdup (entry->rights));

		entries_ptr = g_list_next (entries_ptr);
	}

 skip:

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	if (new_entries_tbl != NULL) {
		g_hash_table_replace (acl->mboxes,
		                      g_strdup (mbox_name),
		                      new_entries_tbl);
	} else {
		g_hash_table_remove (acl->mboxes,
		                     mbox_name);
	}

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return TRUE;
}

gboolean
camel_imapx_acl_update_acl_from_server_response (CamelImapxAcl *acl,
                                                 CamelIMAPXStream *is,
                                                 GCancellable *cancellable,
                                                 GError **err)
{
	gint tok = 0;
	guchar *token = NULL;
	gchar *mbox_name = NULL;
	gchar *access_id = NULL;
	gchar *rights = NULL;
	GHashTable *entries_tbl = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;
	gboolean at_end = FALSE;

	g_return_val_if_fail (acl != NULL, FALSE);
	g_return_val_if_fail (acl->mboxes != NULL, FALSE);
	g_return_val_if_fail (CAMEL_IS_IMAPX_STREAM (is), FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	/* mailbox name */
	tok = camel_imapx_stream_astring (is, &token, cancellable, &tmp_err);
	if (tmp_err != NULL) {
		g_warning ("%s()[%u] stream error, token %u: %s",
		           __func__, __LINE__, tok, tmp_err->message);
		goto exit;
	}

	mbox_name = g_strdup ((gchar *) token);

	/* Where to dump the ACL entries into.
	 * If we have a cached ACL table for a folder, we
	 * need to drop it even if reading from the server
	 * fails (better not to show any data than to show
	 * incorrect or obsoleted data)
	 */
	entries_tbl = imapx_acl_entries_table_new ();
	g_hash_table_replace (acl->mboxes,
	                      g_strdup (mbox_name),
	                      entries_tbl);

	at_end = kolab_util_camel_imapx_stream_eos (is,
	                                            cancellable,
	                                            &tmp_err);
	if (tmp_err != NULL)
		goto exit;
	if (at_end)
		goto skip;

	/* access_id,rights pairs */
	while (TRUE) {
		tok = camel_imapx_stream_astring (is, &token, cancellable, &tmp_err);
		if (tmp_err != NULL) {
			g_warning ("%s()[%u] stream error, token %u: %s",
			           __func__, __LINE__, tok, tmp_err->message);
			goto exit;
		}
		access_id = g_strdup ((gchar *) token);
		tok = camel_imapx_stream_astring (is, &token, cancellable, &tmp_err);
		if (tmp_err != NULL) {
			g_warning ("%s()[%u] stream error, token %u: %s",
			           __func__, __LINE__, tok, tmp_err->message);
			goto exit;
		}
		rights = g_strdup ((gchar *) token);

		if (! imapx_acl_entry_validate_access_id (access_id, &tmp_err))
			goto exit;
		if (! imapx_acl_entry_validate_rights (rights, &tmp_err))
			goto exit;

		g_hash_table_replace (entries_tbl, access_id, rights);
		access_id = NULL;
		rights = NULL;

		at_end = kolab_util_camel_imapx_stream_eos (is,
		                                            cancellable,
		                                            &tmp_err);
		if (tmp_err != NULL)
			goto exit;
		if (at_end)
			break;
	}

 skip:

	ok = TRUE;

 exit:
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		if (access_id != NULL)
			g_free (access_id);
		if (rights != NULL)
			g_free (rights);
	}

	if (mbox_name != NULL)
		g_free (mbox_name);

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return ok;
}

gboolean
camel_imapx_acl_update_myrights_from_server_response (CamelImapxAcl *acl,
                                                      CamelIMAPXStream *is,
                                                      GCancellable *cancellable,
                                                      GError **err)
{
	gint tok = 0;
	guchar *token = NULL;
	gchar *mbox_name = NULL;
	gchar *rights = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (acl != NULL, FALSE);
	g_return_val_if_fail (acl->myrights != NULL, FALSE);
	g_return_val_if_fail (CAMEL_IS_IMAPX_STREAM (is), FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	/* read mailbox name */
	tok = camel_imapx_stream_astring (is, &token, cancellable, &tmp_err);
	if (tmp_err != NULL) {
		g_warning ("%s()[%u] stream error, token %u: %s",
		           __func__, __LINE__, tok, tmp_err->message);
		goto exit;
	}
	mbox_name = g_strdup ((gchar *) token);

	/* read MYRIGHTS string */
	tok = camel_imapx_stream_astring (is, &token, cancellable, &tmp_err);
	if (tmp_err != NULL) {
		g_warning ("%s()[%u] stream error, token %u: %s",
		           __func__, __LINE__, tok, tmp_err->message);
		goto exit;
	}
	rights = g_strdup ((gchar *) token);

	/* rights string validation */
	if (! imapx_acl_entry_validate_rights (rights, &tmp_err))
		goto exit;

	g_hash_table_replace (acl->myrights, mbox_name, rights);
	ok = TRUE;

 exit:
	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		if (rights != NULL)
			g_free (rights);
		if (mbox_name != NULL)
			g_free (mbox_name);
	}

	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return ok;
}

GList*
camel_imapx_acl_get_as_list (CamelImapxAcl *acl,
                             const gchar *mbox_name)
{
	CamelImapxAclEntry *entry = NULL;
	GHashTable *entries_tbl = NULL;
	GList *access_ids = NULL;
	GList *access_ids_ptr = NULL;
	GList *entries_lst = NULL;
	gchar *rights = NULL;

	g_return_val_if_fail (acl != NULL, NULL);
	g_return_val_if_fail (acl->mboxes != NULL, NULL);
	g_return_val_if_fail (mbox_name != NULL, NULL);

	/* (acquire acl lock) */
	g_mutex_lock (&(acl->lock));

	entries_tbl = g_hash_table_lookup (acl->mboxes,
	                                   mbox_name);
	if (entries_tbl == NULL)
		goto exit;

	access_ids = g_hash_table_get_keys (entries_tbl);
	access_ids_ptr = access_ids;
	while (access_ids_ptr != NULL) {
		rights = g_hash_table_lookup (entries_tbl,
		                              access_ids_ptr->data);
		entry = camel_imapx_acl_entry_new ((gchar *) access_ids_ptr->data,
		                                   rights,
		                                   NULL);
		entries_lst = g_list_prepend (entries_lst, entry);
		access_ids_ptr = g_list_next (access_ids_ptr);
	}
	g_list_free (access_ids);

	if (entries_lst != NULL)
		entries_lst = g_list_reverse (entries_lst);
 exit:
	/* (release acl lock) */
	g_mutex_unlock (&(acl->lock));

	return entries_lst;
}

GList*
camel_imapx_acl_list_clone (GList *entries,
                            GError **err)
{
	CamelImapxAclEntry *entry = NULL;
	GList *new_lst = NULL;
	GList *entries_ptr = NULL;
	GError *tmp_err = NULL;

	entries_ptr = entries;
	while (entries_ptr != NULL) {
		entry = camel_imapx_acl_entry_clone ((CamelImapxAclEntry *) entries_ptr->data,
		                                     &tmp_err);
		if (tmp_err != NULL) {
			g_propagate_error (err, tmp_err);
			camel_imapx_acl_list_free (new_lst);
			return NULL;
		}

		new_lst = g_list_prepend (new_lst, entry);
		entries_ptr = g_list_next (entries_ptr);
	}

	new_lst = g_list_reverse (new_lst);
	return new_lst;
}

const gchar*
camel_imapx_acl_list_get_rights (const GList *entries,
                                 const gchar *access_id,
                                 GError **err)
{
	const CamelImapxAclEntry *entry = NULL;
	const GList *entries_ptr = NULL;
	const gchar *rights = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (entries != NULL, NULL);
	g_return_val_if_fail (access_id != NULL, NULL);
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	ok = imapx_acl_entry_validate_access_id (access_id,
	                                         &tmp_err);
	if (! ok)
		goto exit;

	entries_ptr = entries;
	while (entries_ptr != NULL) {
		entry = (const CamelImapxAclEntry *) entries_ptr->data;

		if (entry == NULL) {
			g_warning ("%s()[%u] NULL CamelImapxAclEntry in list, skipping!",
			           __func__, __LINE__);
			goto skip;
		}

		if (entry->access_id == NULL) {
			g_warning ("%s()[%u] NULL access_id in CamelImapxAclEntry, skipping!",
			           __func__, __LINE__);
			goto skip;
		}

		if (g_strcmp0 (access_id, entry->access_id) == 0) {
			ok = imapx_acl_entry_validate_rights (entry->rights,
			                                      &tmp_err);
			if (! ok)
				goto exit;

			rights = entry->rights;
			break;
		}

	skip:
		entries_ptr = g_list_next (entries_ptr);
	}

 exit:
	if (tmp_err != NULL)
		g_propagate_error (err, tmp_err);
	return rights;
}

gboolean
camel_imapx_acl_list_remove_entry (GList **entries,
                                   const gchar *access_id,
                                   GError **err)
{
	CamelImapxAclEntry *entry = NULL;
	GList *entries_ptr = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;
	gboolean found = FALSE;

	g_return_val_if_fail (entries != NULL, FALSE);
	g_return_val_if_fail (access_id != NULL, FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = imapx_acl_entry_validate_access_id (access_id, &tmp_err);
	if (! ok) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	entries_ptr = *entries;
	while (entries_ptr != NULL) {
		entry = (CamelImapxAclEntry *) (entries_ptr->data);
		if (entry == NULL || entry->access_id == NULL) {
			g_warning ("%s()[%u] Invalid CamelImapxAclEntry, skipping",
			           __func__, __LINE__);
			continue;
		}

		if (g_strcmp0 (entry->access_id, access_id) == 0) {
			found = TRUE;
			break;
		}

		entries_ptr = g_list_next (entries_ptr);
	}

	if (found) {
		*entries = g_list_delete_link (*entries,
		                               entries_ptr);
	}

	return found;
}

gboolean
camel_imapx_acl_list_update_from_entry (GList **entries,
                                        const CamelImapxAclEntry *entry,
                                        GError **err)
{
	CamelImapxAclEntry *tmp_entry = NULL;
	GList *entries_ptr = NULL;
	GError *tmp_err = NULL;
	gboolean found = FALSE;
	gboolean ok = FALSE;

	g_return_val_if_fail (entries != NULL, FALSE);
	/* entry may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	if (entry == NULL)
		return TRUE;

	ok = imapx_acl_entry_validate (entry, &tmp_err);
	if (! ok) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	/* find matching entry, if any */
	entries_ptr = *entries;
	while (entries_ptr != NULL) {
		tmp_entry = (CamelImapxAclEntry *) entries_ptr->data;
		found = (g_strcmp0 (tmp_entry->access_id, entry->access_id) == 0);
		if (found) {
			if (tmp_entry->rights)
				g_free (tmp_entry->rights);
			tmp_entry->rights = g_strdup (entry->rights);
			break;
		}
		entries_ptr = g_list_next (entries_ptr);
	}

	if (! found) {
		tmp_entry = camel_imapx_acl_entry_clone (entry,
		                                         NULL);
		*entries = g_list_append (*entries, tmp_entry);
	}

	return TRUE;
}

void
camel_imapx_acl_list_free (GList *entries)
{
	if (entries == NULL)
		return;

	g_list_free_full (entries,
	                  imapx_acl_entry_gdestroy);
}

gchar*
camel_imapx_acl_rights_merge (const gchar *oldrights,
                              const gchar *newrights,
                              GError **err)
{
	gchar *mergedrights = NULL;
	gchar *residue = NULL;
	gboolean ok = FALSE;

	/* oldrights may be NULL */
	/* newrights may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	/* validate both rights strings */
	ok = imapx_acl_entry_validate_rights (oldrights,
	                                      err);
	if (! ok)
		return NULL;
	ok = imapx_acl_entry_validate_rights (newrights,
	                                      err);
	if (! ok)
		return NULL;

	/* from the old rights string, we kill all
	 * known RFC4314 rights characters, including
	 * the virtual rights 'd' and 'c', while
	 * keeping unknown rights characters (as is
	 * mandated by the RFC)
	 */

	residue = imapx_acl_rights_get_unknown (oldrights);

	/* we now prepend our new rights string
	 * to the string of unknown rights chars
	 * from the oldrights, and let the server
	 * handle setting the virtual 'd' and
	 * 'c' rights if it is so inclined (i.e.,
	 * let the server append them as appropriate
	 * in the next GETACL response, since the
	 * server implementation decides how to group
	 * rights for the virtual 'c' and 'd' rights,
	 * and we have thereby ignored these virtual
	 * rights as we are supposed to)
	 */

	if ((newrights == NULL) && (residue == NULL))
		return NULL;

	if (newrights == NULL)
		return residue;

	mergedrights = g_strjoin (NULL, newrights, residue, NULL);

	if (residue != NULL)
		g_free (residue);

	return mergedrights;
}

GList*
camel_imapx_acl_commandlist_new (const GList *entries,
                                 const gchar *foldername,
                                 GError **err)
{
	const CamelImapxAclEntry *entry = NULL;
	CamelImapxAclCmd *cmd = NULL;
	GList *commands = NULL;
	const GList *entries_ptr = NULL;
	const gchar *token = NULL;
	gchar *command = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_return_val_if_fail (entries != NULL, NULL);
	g_return_val_if_fail (foldername != NULL, NULL);
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	entries_ptr = entries;
	while (entries_ptr != NULL) {
		entry = (const CamelImapxAclEntry *) (entries_ptr->data);
		if (entry == NULL)
			goto skip;
		ok = imapx_acl_entry_validate (entry,
		                               &tmp_err);
		if (! ok)
			goto exit;

		if (entry->rights == NULL) {
			/* delete from IMAP ACL */
			token = IMAPX_IMAP_TOKEN_DELETEACL;
			command = g_strdup_printf ("%s %s %s",
			                           token,
			                           foldername,
			                           entry->access_id);
		} else {
			/* add to IMAP ACL */
			token = IMAPX_IMAP_TOKEN_SETACL;
			command = g_strdup_printf ("%s %s %s %s",
			                           token,
			                           foldername,
			                           entry->access_id,
			                           entry->rights);
		}

		cmd = imapx_acl_cmd_new (token,
		                         command);
		g_free (command);
		commands = g_list_prepend (commands, cmd);

	skip:
		entries_ptr = g_list_next (entries_ptr);
	}

	commands = g_list_reverse (commands);

 exit:

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		if (commands != NULL) {
			imapx_acl_commandlist_free (commands);
			commands = NULL;
		}
	}

	return commands;
}

void
camel_imapx_acl_commandlist_free (GList *commands)
{
	imapx_acl_commandlist_free (commands);
}

/*----------------------------------------------------------------------------*/
