/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-server-metadata.h
 *
 *  2012-07-27, 11:02:38
 *  Copyright 2013, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_IMAPX_EXTD_SERVER_METADATA_H_
#define _CAMEL_IMAPX_EXTD_SERVER_METADATA_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolabutil/kolab-util-glib.h>

#include "camel-imapx-metadata.h"

/*----------------------------------------------------------------------------*/

#define IMAPX_IMAP_TOKEN_ANNOTATEMORE  "ANNOTATEMORE"
#define IMAPX_IMAP_TOKEN_ANNOTATION    "ANNOTATION"
#define IMAPX_IMAP_TOKEN_SETANNOTATION "SETANNOTATION"
#define IMAPX_IMAP_TOKEN_GETANNOTATION "GETANNOTATION"

/*----------------------------------------------------------------------------*/

KolabGConstList*
camel_imapx_extd_server_metadata_get_handler_descriptors (void);

gboolean
camel_imapx_extd_server_get_metadata (CamelIMAPXServer *is,
                                      CamelImapxMetadataSpec *spec,
                                      GCancellable *cancellable,
                                      GError **err);

gboolean
camel_imapx_extd_server_set_metadata (CamelIMAPXServer *is,
                                      CamelImapxMetadata *md,
                                      GCancellable *cancellable,
                                      GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* CAMEL_IMAPX_EXTD_SERVER_METADATA_H_ */

/*----------------------------------------------------------------------------*/
