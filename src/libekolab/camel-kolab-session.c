/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-session.c
 *
 *  Tue Aug 10 15:04:38 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/*
 * The CamelSession class for Kolab access. To be instantiated once for
 * each IMAPX Camel.Provider we have. Within EDS, this is one for address book
 * and one for calendar access. Within Evolution, there should be one more
 * for email.
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n-lib.h>

#include <pk11func.h>
#include <prtypes.h>

#include <libemail-engine/e-mail-authenticator.h>

/* Kolab error reporting */
#include <libekolabutil/kolab-util-error.h>

#include "camel-kolab-imapx-store.h"
#include "camel-kolab-session.h"

/*----------------------------------------------------------------------------*/

static gchar *nss_tok_pin = NULL; /* FIXME better solution for this */

/*----------------------------------------------------------------------------*/

struct _CamelKolabSessionPrivate {
	/* TODO get rid of this workaround */
	gchar *nss_tok_pwd;

	gboolean is_initialized;

	GWeakRef backend;
};

enum {
	PROP_0,
	PROP_BACKEND
};

#define CAMEL_KOLAB_SESSION_PRIVATE(obj)  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), CAMEL_TYPE_KOLAB_SESSION, CamelKolabSessionPrivate))

G_DEFINE_TYPE (CamelKolabSession, camel_kolab_session, CAMEL_TYPE_SESSION)

/*----------------------------------------------------------------------------*/

static void
camel_kolab_session_init (CamelKolabSession *self)
{
	self->priv = CAMEL_KOLAB_SESSION_PRIVATE (self);
}

static void
camel_kolab_session_set_backend (CamelKolabSession *session,
                                 EBackend *backend)
{
	g_return_if_fail (E_IS_BACKEND (backend));

	g_weak_ref_set (&session->priv->backend, backend);
}

static void
camel_kolab_session_set_property (GObject *object,
                                  guint property_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_BACKEND:
			camel_kolab_session_set_backend (
				CAMEL_KOLAB_SESSION (object),
				g_value_get_object (value));
			return;

		default:
			break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
camel_kolab_session_get_property (GObject *object,
                                  guint property_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_BACKEND:
			g_value_take_object (
				value,
				camel_kolab_session_ref_backend (
				CAMEL_KOLAB_SESSION (object)));
			return;

		default:
			break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
camel_kolab_session_dispose (GObject *object)
{
	g_assert (CAMEL_IS_KOLAB_SESSION (object));

	/* Chain up to parent's dispose() method. */
	G_OBJECT_CLASS (camel_kolab_session_parent_class)->dispose (object);
}

static void
camel_kolab_session_finalize (GObject *object)
{
	CamelKolabSession *self = NULL;
	CamelKolabSessionPrivate *priv = NULL;

	self = CAMEL_KOLAB_SESSION (object);
	priv = CAMEL_KOLAB_SESSION_PRIVATE (self);

	if (priv->nss_tok_pwd != NULL)
		g_free (priv->nss_tok_pwd);

	g_weak_ref_set (&priv->backend, NULL);

	/* Chain up to parent's finalize() method. */
	G_OBJECT_CLASS (camel_kolab_session_parent_class)->finalize (object);
}

/*----------------------------------------------------------------------------*/
/* local statics */

/* NSS token pin callback */
static gchar* PR_CALLBACK
pk11_password (PK11SlotInfo *slot,
               PRBool retry,
               gpointer arg)
{
	(void)slot;
	(void)arg;

	/* as recommended by PK11_SetPasswordFunc docs */
	if (retry)
		return NULL;

	return PL_strdup (nss_tok_pin);
}

/*----------------------------------------------------------------------------*/
/* class functions */

static gboolean
kolab_session_authenticate_sync (CamelSession *session,
                                 CamelService *service,
                                 const gchar *mechanism,
                                 GCancellable *cancellable,
                                 GError **error)
{
	CamelKolabSession *kolab_session = NULL;
	ESourceAuthenticator *auth = NULL;
	CamelServiceAuthType *authtype = NULL;
	CamelAuthenticationResult result = CAMEL_AUTHENTICATION_ERROR;
	EBackend *backend = NULL;
	gboolean authenticated = FALSE;
	GError *local_error = NULL;

	g_return_val_if_fail (CAMEL_IS_SESSION (session), FALSE);
	g_return_val_if_fail (CAMEL_IS_SERVICE (service), FALSE);
	/* mechanism may be NULL */
	/* cancellable may be NULL */
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	/* this is a dupe of mail_session_authenticate_sync()
	 * from e-mail-session.c in evolution's libemail-engine.
	 *
	 * Once libemail-engine becomes part of E-D-S, we
	 * might be able to just drop this dupe and use
	 * the original function from there directly
	 */

	/* Do not chain up.  Camel's default method is only an example for
	 * subclasses to follow.  Instead we mimic most of its logic here. */

	/* Treat a mechanism name of "none" as NULL. */
	if (g_strcmp0 (mechanism, "none") == 0)
		mechanism = NULL;

	/* APOP is one case where a non-SASL mechanism name is passed, so
	 * don't bail if the CamelServiceAuthType struct comes back NULL. */
	if (mechanism != NULL)
		authtype = camel_sasl_authtype (mechanism);

	/* If the SASL mechanism does not involve a user
	 * password, then it gets one shot to authenticate. */
	if ((authtype != NULL) && (!authtype->need_password)) {
		result = camel_service_authenticate_sync (service,
		                                          mechanism,
		                                          cancellable,
		                                          &local_error);
		if (local_error != NULL) {
			g_propagate_error (error, local_error);
			return FALSE;
		}
		if (result == CAMEL_AUTHENTICATION_REJECTED) {
			g_set_error (error,
			             CAMEL_SERVICE_ERROR,
			             CAMEL_SERVICE_ERROR_CANT_AUTHENTICATE,
			             _("%s authentication failed"),
			             mechanism);
			return FALSE;
		}
		return (result == CAMEL_AUTHENTICATION_ACCEPTED);
	}

	/* Some SASL mechanisms can attempt to authenticate without a
	 * user password being provided (e.g. single-sign-on credentials),
	 * but can fall back to a user password.  Handle that case next. */
	if (mechanism != NULL) {
		CamelProvider *provider = NULL;
		CamelSasl *sasl = NULL;
		const gchar *service_name = NULL;
		gboolean success = FALSE;

		provider = camel_service_get_provider (service);
		service_name = provider->protocol;

		/* XXX Would be nice if camel_sasl_try_empty_password_sync()
		 *     returned CamelAuthenticationResult so it's easier to
		 *     detect errors. */
		sasl = camel_sasl_new (service_name, mechanism, service);
		if (sasl != NULL) {
			success = camel_sasl_try_empty_password_sync (sasl,
			                                              cancellable,
			                                              &local_error);
			g_object_unref (sasl);
		}

		if (success) {
			if (local_error != NULL) {
				g_warning ("%s()[%u] Success but GError set: %s",
				           __func__, __LINE__, local_error->message);
				g_clear_error (&local_error);
			}
			return TRUE;
		}
	}

	/* Abort authentication if we got cancelled.
	 * Otherwise clear any errors and press on. */
	if (g_error_matches (local_error, G_IO_ERROR, G_IO_ERROR_CANCELLED)) {
		g_propagate_error (error, local_error);
		return FALSE;
	}

	if (local_error != NULL) {
		g_warning ("%s()[%u]: %s", __func__, __LINE__, local_error->message);
		g_clear_error (&local_error);
		local_error = NULL;
	}

	kolab_session = CAMEL_KOLAB_SESSION (session);
	backend = camel_kolab_session_ref_backend (kolab_session);

	auth = e_mail_authenticator_new (service, mechanism);

	authenticated = e_backend_authenticate_sync (backend,
	                                             auth,
	                                             cancellable,
	                                             &local_error);
	g_object_unref (auth);
	g_object_unref (backend);

	if (local_error != NULL) {
		g_propagate_error (error, local_error);
		authenticated = FALSE;
	}

	return authenticated;
}

static gint
kolab_session_alert_user (CamelSession *session,
                          CamelSessionAlertType type,
                          const gchar *prompt,
                          GSList *button_captions,
                          GCancellable *cancellable)
{
	gint rval = -1;

	g_return_val_if_fail (CAMEL_IS_SESSION (session), -1);
	/* prompt may be NULL */
	(void) button_captions;

	/* should we even pop up a dialog from within E-D-S? */
	if (prompt != NULL) {
		g_warning ("%s()[%u] (%u) \n%s",
		           __func__, __LINE__, type, prompt);
	}

	/* XXX SSL certificate workaround
	 *
	 * When seeing a new server certificate for the first time,
	 * which it cannot automatically validate by chaining-up the CA,
	 * CamelTCPStreamSSL::ssl_bad_cert() will invoke this function
	 * to query the user whether the certificate should or should not
	 * be accepted. In the evolution-kolab plugin, the collection
	 * backend will be the first one to arrive here, then the cal/book
	 * backends, each in separate processes. Last, Evo itself will
	 * arrive in its own CamelSession implementation's alert_user(),
	 * which displays the accept/decline dialog. Current implementation
	 * of CamelTCPStreamSSL::ssl_bad_cert() does the following, based
	 * on the return value of this function:
	 *
	 * 0: reject the cert in question
	 * 1: temporarily accept the cert in question
	 * 2: permanently accept the cert in question
	 *
	 * all else is regarded "fail and ask again"
	 *
	 * The best we can do here at the moment is to temporarily accept
	 * the server cert in backend (collection/calendar/contacts)
	 * context, since the certificate is *always* written into the
	 * Camel cert_db by CamelTCPStreamSSL::ssl_bad_cert(), only the
	 * trust level is set according to the user selection in alert_user().
	 * That means we need to act here and cannot just fail to let Evo
	 * handle everything. If we temporarily accept, Evolution will still
	 * ask for certificate accept/decline, and the result will be written
	 * to the cert_db, from where the trust level selection is picked
	 * up on next E-D-S restart
	 */

	if (g_slist_length (button_captions) == 3)
		/* this is some guessing about whether
		 * we're asked for server cert accept/decline
		 * (cannot parse button captions or prompt,
		 * since these will be translated, but bad_cert()
		 * is the only instance calling us - good luck!)
		 */
		rval = 1; /* temporarily accept, Evo will ask for confirmation */

	return rval;
}

/*----------------------------------------------------------------------------*/
/* class init */

static void
camel_kolab_session_class_init (CamelKolabSessionClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	CamelSessionClass *session_class = CAMEL_SESSION_CLASS (klass);
	CamelSessionClass *parent_class = CAMEL_SESSION_CLASS (camel_kolab_session_parent_class);

	g_type_class_add_private (klass, sizeof (CamelKolabSessionPrivate));

	object_class->set_property = camel_kolab_session_set_property;
	object_class->get_property = camel_kolab_session_get_property;
	object_class->dispose = camel_kolab_session_dispose;
	object_class->finalize = camel_kolab_session_finalize;

	session_class->add_service = parent_class->add_service;
	session_class->alert_user = kolab_session_alert_user;
	session_class->authenticate_sync = kolab_session_authenticate_sync;

	g_object_class_install_property (
		object_class,
		PROP_BACKEND,
		g_param_spec_object (
			"backend",
			"Backend",
			"Backend used for authentication",
			E_TYPE_BACKEND,
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT_ONLY |
			G_PARAM_STATIC_STRINGS));
}

/*----------------------------------------------------------------------------*/
/* public API */

CamelKolabSession *
camel_kolab_session_new (EBackend *backend,
                         const gchar *user_data_dir,
                         const gchar *user_cache_dir)
{
	g_return_val_if_fail (E_IS_BACKEND (backend), NULL);
	g_return_val_if_fail (user_data_dir != NULL, NULL);
	g_return_val_if_fail (user_cache_dir != NULL, NULL);

	return g_object_new (
		CAMEL_TYPE_KOLAB_SESSION,
		"backend", backend,
		"user-data-dir", user_data_dir,
		"user-cache-dir", user_cache_dir,
		NULL);
}

EBackend *
camel_kolab_session_ref_backend (CamelKolabSession *self)
{
	g_return_val_if_fail (CAMEL_IS_KOLAB_SESSION (self), NULL);

	return g_weak_ref_get (&self->priv->backend);
}

gboolean
camel_kolab_session_bringup (CamelKolabSession *self,
                             GCancellable *cancellable,
                             GError **err)
{
	/* TODO rework to use GInitable */

	CamelKolabSessionPrivate *priv = NULL;

	g_assert (CAMEL_IS_KOLAB_SESSION (self));
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = CAMEL_KOLAB_SESSION_PRIVATE (self);

	if (priv->is_initialized)
		return TRUE;

	/* TODO need to supply (parts of) storage paths here so the imap
	 *	objects for the backends will store their files in their
	 *	own respective directories
	 */

	/* TODO junk settings */

	/* setup further NSS bits here */
	PK11_SetPasswordFunc (pk11_password);

	priv->is_initialized = TRUE;
	g_debug ("%s: camel session initialized",
	         __func__);

	return TRUE;
}

gboolean
camel_kolab_session_shutdown (CamelKolabSession *self,
                              GCancellable *cancellable,
                              GError **err)
{
	CamelKolabSessionPrivate *priv = NULL;

	g_assert (CAMEL_IS_KOLAB_SESSION (self));
	(void)cancellable; /* FIXME */ /* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = CAMEL_KOLAB_SESSION_PRIVATE (self);

	if (! priv->is_initialized)
		return TRUE;

	g_debug ("%s: camel session shut down",
	         __func__);

	return TRUE;
}

void
camel_kolab_session_set_token_pin (CamelKolabSession *self,
                                   const gchar *pin)
{
	/* TODO rework
	 *
	 * The entire NSS token handling needs
	 * to be rewritten (it is a mere demonstrator
	 * presently, and should not be relied upon
	 * in critical environments
	 *
	 */

	g_assert (CAMEL_IS_KOLAB_SESSION (self));

	if (nss_tok_pin != NULL)
		g_free (nss_tok_pin);

	nss_tok_pin = g_strdup (pin);
}

/*----------------------------------------------------------------------------*/
