/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-summary.h
 *
 *  Fri Jan 28 10:05:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-mail-summary
 * @short_description: Summary information for a Kolab PIM email
 * @title: KolabMailSummary
 * @section_id:
 * @see_also: #KolabMailInfoDb, #KolabFolderSummary
 * @stability: unstable
 *
 * A #KolabMailSummary part of the data structure #KolabMailInfoDbRecord,
 * which is stored permanently in the #KolabMailInfoDb for each known Kolab
 * PIM email.
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_MAIL_SUMMARY_H_
#define _KOLAB_MAIL_SUMMARY_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include <libebook/libebook.h>
#include <libecal/libecal.h>

/*----------------------------------------------------------------------------*/

typedef enum {
	/* KolabMailHandle intern */
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_KOLAB_UID = 0,	/* Kolab object UID */
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_IMAP_UID,		/* IMAP Mail UID (always updated) */
	/* Evolution common */
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_SUMMARY,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_ORGANIZER,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_LOCATION,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_CATEGORY,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_DTSTART,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_DTSTART_TZID,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_DTEND,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_DTEND_TZID,
	/* EContact only */
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_FULLNAME,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_EMAIL_1,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_EMAIL_2,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_EMAIL_3,
	KOLAB_MAIL_SUMMARY_CHAR_FIELD_E_EMAIL_4,
	/* LAST */
	KOLAB_MAIL_SUMMARY_CHAR_LAST_FIELD
} KolabMailSummaryCharFieldID;

typedef enum {
	/* KolabMailHandle intern */
	KOLAB_MAIL_SUMMARY_UINT_FIELD_FOLDER_TYPE = 0,	/* KolabFolderMetaTypeID */
	KOLAB_MAIL_SUMMARY_UINT_FIELD_FOLDER_CONTEXT,	/* KolabFolderContextID  */
	KOLAB_MAIL_SUMMARY_UINT_FIELD_CACHE_LOCATION,	/* KolabObjectCacheLocation */
	KOLAB_MAIL_SUMMARY_UINT_FIELD_CACHE_STATUS,	/* KolabObjectCacheStatus */
	/* Evolution common */
	KOLAB_MAIL_SUMMARY_UINT_FIELD_E_CLASSIFICATION,
	KOLAB_MAIL_SUMMARY_UINT_FIELD_E_STATUS,
	/* LAST */
	KOLAB_MAIL_SUMMARY_UINT_LAST_FIELD
}  KolabMailSummaryUintFieldID;

typedef enum {
	/* Evolution common */
	KOLAB_MAIL_SUMMARY_INT_FIELD_E_PRIORITY = 0,
	KOLAB_MAIL_SUMMARY_INT_FIELD_E_PERCENT,
	/* LAST */
	KOLAB_MAIL_SUMMARY_INT_LAST_FIELD
} KolabMailSummaryIntFieldID;

typedef enum {
	/* KolabMailHandle intern */
	KOLAB_MAIL_SUMMARY_BOOL_FIELD_COMPLETE = 0,
	/* Evolution common */
	KOLAB_MAIL_SUMMARY_BOOL_FIELD_E_HAS_ATTENDEES,
	KOLAB_MAIL_SUMMARY_BOOL_FIELD_E_HAS_ATTACHMENTS,
	KOLAB_MAIL_SUMMARY_BOOL_FIELD_E_HAS_RECURRENCE,
	KOLAB_MAIL_SUMMARY_BOOL_FIELD_E_HAS_ALARMS,
	/* LAST */
	KOLAB_MAIL_SUMMARY_BOOL_LAST_FIELD
} KolabMailSummaryBoolFieldID;

/* SQLiteDB column numbers for KolabFolderSummary */
enum {
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_KOLAB_UID = 0,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_IMAP_UID,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_SUMMARY,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_ORGANIZER,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_LOCATION,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_CATEGORY,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_DTSTART,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_DTSTART_TZID,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_DTEND,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_DTEND_TZID,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_FULLNAME,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_EMAIL_1,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_EMAIL_2,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_EMAIL_3,
	KOLAB_MAIL_SUMMARY_SQLCOL_CHAR_FIELD_E_EMAIL_4,
	KOLAB_MAIL_SUMMARY_SQLCOL_UINT_FIELD_FOLDER_TYPE,
	KOLAB_MAIL_SUMMARY_SQLCOL_UINT_FIELD_FOLDER_CONTEXT,
	KOLAB_MAIL_SUMMARY_SQLCOL_UINT_FIELD_CACHE_LOCATION,
	KOLAB_MAIL_SUMMARY_SQLCOL_UINT_FIELD_CACHE_STATUS,
	KOLAB_MAIL_SUMMARY_SQLCOL_UINT_FIELD_E_CLASSIFICATION,
	KOLAB_MAIL_SUMMARY_SQLCOL_UINT_FIELD_E_STATUS,
	KOLAB_MAIL_SUMMARY_SQLCOL_INT_FIELD_E_PRIORITY,
	KOLAB_MAIL_SUMMARY_SQLCOL_INT_FIELD_E_PERCENT,
	KOLAB_MAIL_SUMMARY_SQLCOL_BOOL_FIELD_COMPLETE,
	KOLAB_MAIL_SUMMARY_SQLCOL_BOOL_FIELD_E_HAS_ATTENDEES,
	KOLAB_MAIL_SUMMARY_SQLCOL_BOOL_FIELD_E_HAS_ATTACHMENTS,
	KOLAB_MAIL_SUMMARY_SQLCOL_BOOL_FIELD_E_HAS_RECURRENCE,
	KOLAB_MAIL_SUMMARY_SQLCOL_BOOL_FIELD_E_HAS_ALARMS,
	/* LAST */
	KOLAB_MAIL_SUMMARY_LAST_SQLCOL
};

typedef struct _KolabMailSummary KolabMailSummary;
struct _KolabMailSummary
{
	/* ECalComponent/EContact fields */
	gchar   *sdata_char[KOLAB_MAIL_SUMMARY_CHAR_LAST_FIELD];
	guint    sdata_uint[KOLAB_MAIL_SUMMARY_UINT_LAST_FIELD];
	gint     sdata_int[KOLAB_MAIL_SUMMARY_INT_LAST_FIELD];
	gboolean sdata_bool[KOLAB_MAIL_SUMMARY_BOOL_LAST_FIELD];
};

/* TODO move to 'const' params as soon as ECalComponent/EContact APIs have been sanitized */
KolabMailSummary* kolab_mail_summary_new (void);
KolabMailSummary* kolab_mail_summary_new_from_ecalcomponent (ECalComponent *ecalcomp);
KolabMailSummary* kolab_mail_summary_new_from_econtact (EContact *econtact);
KolabMailSummary* kolab_mail_summary_clone (const KolabMailSummary *summary);
void kolab_mail_summary_update_eds_data (KolabMailSummary *summary, const KolabMailSummary *src_summary);
void kolab_mail_summary_free (KolabMailSummary *summary);
void kolab_mail_summary_gdestroy (gpointer data);

gboolean kolab_mail_summary_check (const KolabMailSummary *summary);
gboolean kolab_mail_summary_equal (const KolabMailSummary *summary1, const KolabMailSummary *summary2);

void kolab_mail_summary_set_char_field (KolabMailSummary *summary, KolabMailSummaryCharFieldID field_id, gchar *value);
const gchar* kolab_mail_summary_get_char_field (const KolabMailSummary *summary, KolabMailSummaryCharFieldID field_id);

void kolab_mail_summary_set_uint_field (KolabMailSummary *summary, KolabMailSummaryUintFieldID field_id, guint value);
guint kolab_mail_summary_get_uint_field (const KolabMailSummary *summary, KolabMailSummaryUintFieldID field_id);

void kolab_mail_summary_set_int_field (KolabMailSummary *summary, KolabMailSummaryIntFieldID field_id, gint value);
gint kolab_mail_summary_get_int_field (const KolabMailSummary *summary, KolabMailSummaryIntFieldID field_id);

void kolab_mail_summary_set_bool_field (KolabMailSummary *summary, KolabMailSummaryBoolFieldID field_id, gboolean value);
gboolean kolab_mail_summary_get_bool_field (const KolabMailSummary *summary, KolabMailSummaryBoolFieldID field_id);

void kolab_mail_summary_debug_print (const KolabMailSummary *summary);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_MAIL_SUMMARY_H_ */

/*----------------------------------------------------------------------------*/
