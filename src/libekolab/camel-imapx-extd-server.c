/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-server.c
 *
 *  2011-11-28, 20:16:38
 *  Copyright 2011, Christian Hilberg
 *  <hilberg@unix-ag.org>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include "camel-imapx-extd-store.h"
#include "camel-imapx-extd-store-friend.h"
#include "camel-imapx-extd-server.h"

/*----------------------------------------------------------------------------*/

static gpointer
imapx_extd_server_register_desc_list (CamelIMAPXServer *is,
                                      KolabGConstList *desc_lst)
{
	const CamelIMAPXUntaggedRespHandlerDesc *desc = NULL;
	const CamelIMAPXUntaggedRespHandlerDesc *prev = NULL;
	KolabGConstList *desc_lst_ptr = NULL;

	desc_lst_ptr = desc_lst;
	while (desc_lst_ptr != NULL) {
		desc = (const CamelIMAPXUntaggedRespHandlerDesc *) (desc_lst_ptr->const_data);
		prev = camel_imapx_server_register_untagged_handler (is,
		                                                     desc->untagged_response,
		                                                     desc);
		if (prev != NULL)
			return (gpointer) prev;

		desc_lst_ptr = desc_lst_ptr->next;
	}

	return NULL;
}

static gpointer
imapx_extd_server_register_untagged_fn (gpointer data)
{
	static GMutex reg_lock;
	CamelIMAPXServer *is = CAMEL_IMAPX_SERVER (data);
	const CamelIMAPXUntaggedRespHandlerDesc *prev = NULL;
	KolabGConstList *desc_lst = NULL;

	/* We need to make sure that the untagged handler
	 * functions do not have been registered before.
	 * Returning non-NULL here means that any handler
	 * has been registered before, which is an error.
	 * This needs to be checked for all handlers which
	 * are registered here.
	 */

	g_mutex_lock (&reg_lock);

	/* METADATA handlers */
	desc_lst = camel_imapx_extd_server_metadata_get_handler_descriptors ();
	prev = imapx_extd_server_register_desc_list (is, desc_lst);
	kolab_util_glib_gconstlist_free (desc_lst);
	if (prev != NULL)
		goto exit;

	/* ACL handlers */
	desc_lst = camel_imapx_extd_server_acl_get_handler_descriptors ();
	prev = imapx_extd_server_register_desc_list (is, desc_lst);
	kolab_util_glib_gconstlist_free (desc_lst);

 exit:
	g_mutex_unlock (&reg_lock);
	return (gpointer) prev;
}

/*----------------------------------------------------------------------------*/

gboolean
camel_imapx_extd_server_init (CamelIMAPXServer *is,
                              GCancellable *cancellable,
                              GError **err)
{
	gpointer prev = NULL;
	static GMutex init_lock;
	static GHashTable *inited_tbl = NULL;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	(void)cancellable; /* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_mutex_lock (&init_lock);

	if (inited_tbl == NULL) {
		inited_tbl = g_hash_table_new_full (g_direct_hash,
		                                    g_direct_equal,
		                                    NULL,
		                                    NULL);
	}

	if (g_hash_table_lookup (inited_tbl, (gpointer) is) == NULL) {
		prev = imapx_extd_server_register_untagged_fn (is);
		g_hash_table_insert (inited_tbl, (gpointer) is, (gpointer) is);
		if (prev != NULL) {
			g_warning ("%s()[%u] %s",
			           __func__,
			           __LINE__,
			           _("A CamelIMAPXServer extended untagged response handler has been registered twice"));
		}
	}

	g_mutex_unlock (&init_lock);

	return TRUE;
}

/*----------------------------------------------------------------------------*/
