/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-folder-summary.c
 *
 *  Mon Feb 14 11:43:25 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <libekolabutil/kolab-util-folder.h>
#include "kolab-folder-summary.h"

/*----------------------------------------------------------------------------*/
/* new/free/check */


KolabFolderSummary*
kolab_folder_summary_new (void)
{
	gint ii = 0;
	KolabFolderSummary *summary = g_new0 (KolabFolderSummary, 1);
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_CHAR_LAST_FIELD; ii++)
		summary->sdata_char[ii] = NULL;
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_UINT_LAST_FIELD; ii++)
		summary->sdata_uint[ii] = 0;
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_UINT64_LAST_FIELD; ii++)
		summary->sdata_uint64[ii] = 0;
#if 0
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_INT_LAST_FIELD; ii++)
		summary->sdata_int[ii] = 0;
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_BOOL_LAST_FIELD; ii++)
		summary->sdata_bool[ii] = FALSE;
#endif

	summary->sdata_uint[KOLAB_FOLDER_SUMMARY_UINT_FIELD_FOLDER_TYPE] = \
		KOLAB_FOLDER_TYPE_INVAL;
	summary->sdata_uint[KOLAB_FOLDER_SUMMARY_UINT_FIELD_FOLDER_CONTEXT] = \
		KOLAB_FOLDER_CONTEXT_INVAL;

	return summary;
}

KolabFolderSummary*
kolab_folder_summary_clone (const KolabFolderSummary *summary)
{
	KolabFolderSummary *new_summary = NULL;
	gint ii = 0;

	if (summary == NULL)
		return NULL;

	new_summary = kolab_folder_summary_new ();
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_CHAR_LAST_FIELD; ii++)
		new_summary->sdata_char[ii] = g_strdup (summary->sdata_char[ii]);
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_UINT_LAST_FIELD; ii++)
		new_summary->sdata_uint[ii] = summary->sdata_uint[ii];
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_UINT64_LAST_FIELD; ii++)
		new_summary->sdata_uint64[ii] = summary->sdata_uint64[ii];
#if 0
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_INT_LAST_FIELD; ii++)
		new_summary->sdata_int[ii] = summary->sdata_int[ii];
	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_BOOL_LAST_FIELD; ii++)
		new_summary->sdata_bool[ii] = summary->sdata_bool[ii];
#endif
	return new_summary;
}

void
kolab_folder_summary_free (KolabFolderSummary *summary)
{
	guint ii = 0;

	if (summary == NULL)
		return;

	for (ii = 0; ii < KOLAB_FOLDER_SUMMARY_CHAR_LAST_FIELD; ii++) {
		if (summary->sdata_char[ii] != NULL)
			g_free (summary->sdata_char[ii]);
	}
	g_free (summary);
}

void
kolab_folder_summary_gdestroy (gpointer data)
{
	KolabFolderSummary *summary = NULL;

	if (data == NULL)
		return;

	summary = (KolabFolderSummary *)data;
	kolab_folder_summary_free (summary);
}

gboolean
kolab_folder_summary_check (const KolabFolderSummary *summary)
{
	g_assert (summary != NULL);

	/* TODO implement me */

	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* getters/setters */

void
kolab_folder_summary_set_char_field (KolabFolderSummary *summary,
                                     KolabFolderSummaryCharFieldID field_id,
                                     gchar *value)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_CHAR_LAST_FIELD);

	if (summary->sdata_char[field_id] != NULL)
		g_free (summary->sdata_char[field_id]);

	summary->sdata_char[field_id] = value; /* no copy! */
}

const gchar*
kolab_folder_summary_get_char_field (const KolabFolderSummary *summary,
                                     KolabFolderSummaryCharFieldID field_id)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_CHAR_LAST_FIELD);

	return summary->sdata_char[field_id]; /* no copy! */
}

void
kolab_folder_summary_set_uint_field (KolabFolderSummary *summary,
                                     KolabFolderSummaryUintFieldID field_id,
                                     guint value)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_UINT_LAST_FIELD);

	summary->sdata_uint[field_id] = value;
}

guint
kolab_folder_summary_get_uint_field (const KolabFolderSummary *summary,
                                     KolabFolderSummaryUintFieldID field_id)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_UINT_LAST_FIELD);

	return summary->sdata_uint[field_id];
}

void
kolab_folder_summary_set_uint64_field (KolabFolderSummary *summary,
                                       KolabFolderSummaryUint64FieldID field_id,
                                       guint64 value)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_UINT64_LAST_FIELD);

	summary->sdata_uint64[field_id] = value;
}

guint64
kolab_folder_summary_get_uint64_field (const KolabFolderSummary *summary,
                                       KolabFolderSummaryUint64FieldID field_id)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_UINT64_LAST_FIELD);

	return summary->sdata_uint64[field_id];
}

#if 0
void
kolab_folder_summary_set_int_field (KolabFolderSummary *summary,
                                    KolabFolderSummaryIntFieldID field_id,
                                    gint value)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_INT_LAST_FIELD);

	summary->sdata_int[field_id] = value;
}

gint
kolab_folder_summary_get_int_field (const KolabFolderSummary *summary,
                                    KolabFolderSummaryIntFieldID field_id)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_INT_LAST_FIELD);

	return summary->sdata_int[field_id];
}

void
kolab_folder_summary_set_bool_field (KolabFolderSummary *summary,
                                     KolabFolderSummaryBoolFieldID field_id,
                                     gboolean value)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_BOOL_LAST_FIELD);

	summary->sdata_bool[field_id] = value;
}

gboolean
kolab_folder_summary_get_bool_field (const KolabFolderSummary *summary,
                                     KolabFolderSummaryBoolFieldID field_id)
{
	g_assert (summary != NULL);
	g_assert (field_id < KOLAB_FOLDER_SUMMARY_BOOL_LAST_FIELD);

	return summary->sdata_bool[field_id];
}
#endif


void
kolab_folder_summary_dump (const KolabFolderSummary *summary)
{
	g_debug ("\n%s: **** FolderSummary Begin ****", __func__);

	if (summary == NULL) {
		g_debug ("%s: FolderSummary is NULL", __func__);
		goto folder_summary_skip;
	}

	g_debug ("%s: Folder (%s)",
	         __func__,
	         kolab_folder_summary_get_char_field (summary,
	                                              KOLAB_FOLDER_SUMMARY_CHAR_FIELD_FOLDERNAME)
	         );
	g_debug ("%s: Folder Type: (%i) Context: (%i) Cache Location: (%i) Status: (%i)",
	         __func__,
	         kolab_folder_summary_get_uint_field (summary,
	                                              KOLAB_FOLDER_SUMMARY_UINT_FIELD_FOLDER_TYPE),
	         kolab_folder_summary_get_uint_field (summary,
	                                              KOLAB_FOLDER_SUMMARY_UINT_FIELD_FOLDER_CONTEXT),
	         kolab_folder_summary_get_uint_field (summary,
	                                              KOLAB_FOLDER_SUMMARY_UINT_FIELD_CACHE_LOCATION),
	         kolab_folder_summary_get_uint_field (summary,
	                                              KOLAB_FOLDER_SUMMARY_UINT_FIELD_CACHE_STATUS)
	         );
	g_debug ("%s: UIDVALIDITY (%ld)",
	         __func__,
	         (long int) kolab_folder_summary_get_uint64_field (summary,
	                                                           KOLAB_FOLDER_SUMMARY_UINT64_FIELD_UIDVALIDITY)
	         );

 folder_summary_skip:
	g_debug ("\n%s: **** FolderSummary End ****\n", __func__);
}

/*----------------------------------------------------------------------------*/
