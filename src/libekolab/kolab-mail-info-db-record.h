/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-record.h
 *
 *  Fri Mar 04 14:31:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-mail-info-db-record
 * @short_description: Info database record for Kolab PIM email
 * @title: KolabMailInfoDbRecord
 * @section_id:
 * @see_also: #KolabMailInfoDb, #KolabMailSummary
 * @stability: unstable
 *
 * A #KolabMailInfoDbRecord is stored permanently in the #KolabMailInfoDb for
 * each known Kolab PIM email.
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_MAIL_INFO_DB_RECORD_H_
#define _KOLAB_MAIL_INFO_DB_RECORD_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include <libekolabutil/kolab-util-folder.h>

#include "kolab-mail-summary.h"

/*----------------------------------------------------------------------------*/

typedef enum {
	KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_UID_SYNC = 0,	/* IMAP UID (updated on sync only) */
	KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_FOLDER_NAME,	/* Server side foldername (for renaming) */
	KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_CHECKSUM,	/* IMAP server-side (always updated) */
	KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_IMAP_CHECKSUM_SYNC, /* IMAP server-side (updated on sync only) */
	KOLAB_MAIL_INFO_DB_RECORD_CHAR_FIELD_SIDE_CHECKSUM,	/* SideCache object */
	KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD
} KolabMailInfoDbRecordCharFieldID;

typedef enum {
	KOLAB_MAIL_INFO_DB_RECORD_UINT_FIELD_IMAP_FOLDER_TYPE = 0,	/* needed ? */
	KOLAB_MAIL_INFO_DB_RECORD_UINT_FIELD_IMAP_FOLDER_CONTEXT,	/* needed ? */
	KOLAB_MAIL_INFO_DB_RECORD_UINT_LAST_FIELD
} KolabMailInfoDbRecordUintFieldID;

/* SQLiteDB column numbers for KolabMailInfoDbRecord */
enum {
	KOLAB_MAIL_INFO_DB_RECORD_SQLCOL_CHAR_FIELD_IMAP_UID_SYNC = KOLAB_MAIL_SUMMARY_LAST_SQLCOL,
	KOLAB_MAIL_INFO_DB_RECORD_SQLCOL_CHAR_FIELD_IMAP_FOLDER_NAME,
	KOLAB_MAIL_INFO_DB_RECORD_SQLCOL_CHAR_FIELD_IMAP_CHECKSUM,
	KOLAB_MAIL_INFO_DB_RECORD_SQLCOL_CHAR_FIELD_IMAP_CHECKSUM_SYNC,
	KOLAB_MAIL_INFO_DB_RECORD_SQLCOL_CHAR_FIELD_SIDE_CHECKSUM,
	KOLAB_MAIL_INFO_DB_RECORD_SQLCOL_UINT_FIELD_IMAP_FOLDER_TYPE,
	KOLAB_MAIL_INFO_DB_RECORD_SQLCOL_UINT_FIELD_IMAP_FOLDER_CONTEXT,
	/* LAST */
	KOLAB_MAIL_INFO_DB_RECORD_LAST_SQLCOL
};

typedef struct _KolabMailInfoDbRecord KolabMailInfoDbRecord;
struct _KolabMailInfoDbRecord {
	/* standard mail summary */
	KolabMailSummary *summary;

	/* extra data */
	gchar *rdata_char[KOLAB_MAIL_INFO_DB_RECORD_CHAR_LAST_FIELD];
	guint  rdata_uint[KOLAB_MAIL_INFO_DB_RECORD_UINT_LAST_FIELD];
};

/*----------------------------------------------------------------------------*/

KolabMailInfoDbRecord*
kolab_mail_info_db_record_new (void);

KolabMailInfoDbRecord*
kolab_mail_info_db_record_clone (const KolabMailInfoDbRecord *record);

void
kolab_mail_info_db_record_free (KolabMailInfoDbRecord *record);

void
kolab_mail_info_db_record_gdestroy (gpointer data);

gboolean
kolab_mail_info_db_record_equal (const KolabMailInfoDbRecord *record1,
                                 const KolabMailInfoDbRecord *record2);

/* accessors */

void
kolab_mail_info_db_record_set_char_field (KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordCharFieldID field_id,
                                          gchar *value);

const gchar*
kolab_mail_info_db_record_get_char_field (const KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordCharFieldID field_id);

void
kolab_mail_info_db_record_set_uint_field (KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordUintFieldID field_id,
                                          guint value);

guint
kolab_mail_info_db_record_get_uint_field (const KolabMailInfoDbRecord *record,
                                          KolabMailInfoDbRecordUintFieldID field_id);

/* debugging */

void
kolab_mail_info_db_record_dump (const KolabMailInfoDbRecord *record);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_MAIL_INFO_DB_RECORD_H_ */

/*----------------------------------------------------------------------------*/
