/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-data-imap-account.c
 *
 *  Fri Feb 17 17:23:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include "kolab-data-imap-account.h"

/*----------------------------------------------------------------------------*/

KolabDataImapAccount*
kolab_data_imap_account_new (void)
{
	KolabDataImapAccount *data = NULL;
	data = g_new0 (KolabDataImapAccount, 1);
	/* FIXME set field defaults */
	return data;
}

KolabDataImapAccount*
kolab_data_imap_account_clone (const KolabDataImapAccount *srcdata)
{
	KolabDataImapAccount *data = NULL;

	if (srcdata == NULL)
		return NULL;

	data = g_new0 (KolabDataImapAccount, 1);

	/* FIXME set fields from srcdata */
	data->foo = srcdata->foo;

	return data;
}

void
kolab_data_imap_account_free (KolabDataImapAccount *data)
{
	if (data == NULL)
		return;
	/* FIXME free fields */
	g_free (data);
}

/*----------------------------------------------------------------------------*/
