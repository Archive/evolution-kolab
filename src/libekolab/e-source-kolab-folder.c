/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include "e-source-kolab-folder.h"

#include "kolab-enumtypes.h"

#define E_SOURCE_KOLAB_FOLDER_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE \
	((obj), E_TYPE_SOURCE_KOLAB_FOLDER, ESourceKolabFolderPrivate))

struct _ESourceKolabFolderPrivate {
	KolabSyncStrategyID sync_strategy;
};

enum {
	PROP_0,
	PROP_SYNC_STRATEGY
};

G_DEFINE_TYPE (
	ESourceKolabFolder,
	e_source_kolab_folder,
	E_TYPE_SOURCE_RESOURCE)

static void
source_kolab_folder_set_property (GObject *object,
                                  guint property_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_SYNC_STRATEGY:
			e_source_kolab_folder_set_sync_strategy (
				E_SOURCE_KOLAB_FOLDER (object),
				g_value_get_enum (value));
			return;
		default:
			break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
source_kolab_folder_get_property (GObject *object,
                                  guint property_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
	switch (property_id) {
		case PROP_SYNC_STRATEGY:
			g_value_set_enum (
				value,
				e_source_kolab_folder_get_sync_strategy (
				E_SOURCE_KOLAB_FOLDER (object)));
			return;
		default:
			break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
e_source_kolab_folder_class_init (ESourceKolabFolderClass *class)
{
	GObjectClass *object_class;
	ESourceExtensionClass *extension_class;

	g_type_class_add_private (class, sizeof (ESourceKolabFolderPrivate));

	object_class = G_OBJECT_CLASS (class);
	object_class->set_property = source_kolab_folder_set_property;
	object_class->get_property = source_kolab_folder_get_property;

	extension_class = E_SOURCE_EXTENSION_CLASS (class);
	extension_class->name = E_SOURCE_EXTENSION_KOLAB_FOLDER;

	g_object_class_install_property (
		object_class,
		PROP_SYNC_STRATEGY,
		g_param_spec_enum (
			"sync-strategy",
			"Sync Strategy",
			"Conflict resolution strategy",
			KOLAB_TYPE_SYNC_STRATEGY_ID,
			KOLAB_SYNC_STRATEGY_DEFAULT,
			G_PARAM_READWRITE |
			G_PARAM_CONSTRUCT |
			G_PARAM_STATIC_STRINGS |
			E_SOURCE_PARAM_SETTING));
}

static void
e_source_kolab_folder_init (ESourceKolabFolder *extension)
{
	extension->priv = E_SOURCE_KOLAB_FOLDER_GET_PRIVATE (extension);
}

KolabSyncStrategyID
e_source_kolab_folder_get_sync_strategy (ESourceKolabFolder *extension)
{
	g_return_val_if_fail (
		E_IS_SOURCE_KOLAB_FOLDER (extension),
		KOLAB_SYNC_STRATEGY_DEFAULT);

	return extension->priv->sync_strategy;
}

void
e_source_kolab_folder_set_sync_strategy (ESourceKolabFolder *extension,
                                         KolabSyncStrategyID sync_strategy)
{
	g_return_if_fail (E_IS_SOURCE_KOLAB_FOLDER (extension));

	extension->priv->sync_strategy = sync_strategy;

	g_object_notify (G_OBJECT (extension), "sync-strategy");
}

