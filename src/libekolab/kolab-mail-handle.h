/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-handle.h
 *
 *  Fri Jan 14 15:15:54 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-mail-handle
 * @short_description: the abstract Kolab2 PIM email representation
 * @title: KolabMailHandle
 * @section_id:
 * @see_also: #KolabMailAccess
 * @stability: unstable
 *
 * This class is an abstract representation of a Kolab PIM email. As a
 * minimum, a #KolabMailHandle object carries a Kolab UID and can thus be
 * used to reference a certain PIM object.
 *
 * #KolabMailHandle objects have a notion of being "complete" or "incomplete".
 * In the latter case, they are merely shallow objects with very little information
 * attached to them. Before operating on a #KolabMailHandle object, make sure it is
 * complete. An incomplete object can be completed by a call to kolab_mail_access_retrieve_handle()
 * which will fetch the actual data from any of the caches within #KolabMailAccess.
 * To delete a PIM object, it's #KolabMailHandle representation does not
 * need to be completed.
 *
 * #KolabMailHandle objects carry with them a folder context, which is
 * %KOLAB_FOLDER_CONTEXT_CALENDAR or %KOLAB_FOLDER_CONTEXT_CONTACT
 * (see #KolabFolderContextID), respectively. The folder context is
 * determined by how the handle was created, this is, whether it was
 * created by kolab_mail_handle_new_from_ecalcomponent() (context is
 * %KOLAB_FOLDER_CONTEXT_CALENDAR) or kolab_mail_handle_new_from_econtact()
 * (context is %KOLAB_FOLDER_CONTEXT_CONTACT). This context must match
 * the context #KolabMailAccess was configured for, otherwise all
 * #KolabMailAccess operations with the handle will return an error.
 *
 * In order to retrieve a new Evolution PIM object from a #KolabMailHandle,
 * the contexts of the respective creation and retrieval functions must also
 * match.
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_MAIL_HANDLE_H_
#define _KOLAB_MAIL_HANDLE_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>

#include <libebook/libebook.h>
#include <libecal/libecal.h>

#include  <libekolabconv/main/src/kolab-conv.h>

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

#define KOLAB_TYPE_MAIL_HANDLE             (kolab_mail_handle_get_type ())
#define KOLAB_MAIL_HANDLE(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOLAB_TYPE_MAIL_HANDLE, KolabMailHandle))
#define KOLAB_MAIL_HANDLE_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), KOLAB_TYPE_MAIL_HANDLE, KolabMailHandleClass))
#define KOLAB_IS_MAIL_HANDLE(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOLAB_TYPE_MAIL_HANDLE))
#define KOLAB_IS_MAIL_HANDLE_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), KOLAB_TYPE_MAIL_HANDLE))
#define KOLAB_MAIL_HANDLE_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), KOLAB_TYPE_MAIL_HANDLE, KolabMailHandleClass))

typedef struct _KolabMailHandleClass KolabMailHandleClass;
typedef struct _KolabMailHandle KolabMailHandle;

struct _KolabMailHandleClass
{
	GObjectClass parent_class;
};

struct _KolabMailHandle
{
	GObject parent_instance;
};

GType kolab_mail_handle_get_type (void) G_GNUC_CONST;

/* TODO move to 'const' params as soon as ECalComponent/EContact APIs have been sanitized */
KolabMailHandle* kolab_mail_handle_new_from_ecalcomponent (ECalComponent *ecalcomp, ECalComponent *timezone);
KolabMailHandle* kolab_mail_handle_new_from_econtact (EContact *econtact);

ECalComponent* kolab_mail_handle_get_ecalcomponent (const KolabMailHandle *self);
ECalComponent* kolab_mail_handle_get_timezone (const KolabMailHandle *self);
EContact* kolab_mail_handle_get_econtact (const KolabMailHandle *self);

gboolean kolab_mail_handle_is_complete (const KolabMailHandle *self);
const gchar* kolab_mail_handle_get_uid (const KolabMailHandle *self);
const gchar* kolab_mail_handle_get_foldername (const KolabMailHandle *self);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_MAIL_HANDLE_H_ */

/*----------------------------------------------------------------------------*/
