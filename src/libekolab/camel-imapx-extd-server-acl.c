/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-server-acl.c
 *
 *  2012-07-30, 17:13:28
 *  Copyright 2012, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gi18n.h>

#include "camel-imapx-extd-store.h"
#include "camel-imapx-extd-store-friend.h"
#include "camel-imapx-extd-server.h"
#include "camel-imapx-extd-utils.h"
#include "camel-imapx-acl.h"

#include "camel-imapx-extd-server-acl.h"

/*----------------------------------------------------------------------------*/

static gboolean
imapx_extd_server_untagged_acl (CamelIMAPXServer *is,
                                CamelIMAPXStream *stream,
                                GCancellable *cancellable,
                                GError **err);

static gboolean
imapx_extd_server_untagged_myrights (CamelIMAPXServer *is,
                                     CamelIMAPXStream *stream,
                                     GCancellable *cancellable,
                                     GError **err);

static const CamelIMAPXUntaggedRespHandlerDesc desc_acl = {
	IMAPX_IMAP_TOKEN_ACL,                  /* untagged_response     */
	imapx_extd_server_untagged_acl,        /* handler               */
	NULL,                                  /* next_response         */
	TRUE                                   /* skip_stream_when_done */
};

static const CamelIMAPXUntaggedRespHandlerDesc desc_myrights = {
	IMAPX_IMAP_TOKEN_MYRIGHTS,             /* untagged_response     */
	imapx_extd_server_untagged_myrights,   /* handler               */
	NULL,                                  /* next_response         */
	TRUE                                   /* skip_stream_when_done */
};

/*----------------------------------------------------------------------------*/

static gboolean
imapx_extd_server_untagged_acl (CamelIMAPXServer *is,
                                CamelIMAPXStream *stream,
                                GCancellable *cancellable,
                                GError **err)
{
	static GMutex handler_lock;
	CamelIMAPXStore *store = NULL;
	CamelIMAPXExtdStore *estore = NULL;
	CamelImapxAcl *acl = NULL;
	guint32 capa = 0;
	guint32 capa_flag_id = 0;
	GError *tmp_err = NULL;
	gboolean parse_and_add_ok = FALSE;
	gboolean success = FALSE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_return_val_if_fail (CAMEL_IS_IMAPX_STREAM (stream), FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_mutex_lock (&handler_lock);

	store = camel_imapx_server_ref_store (is);
	estore = CAMEL_IMAPX_EXTD_STORE (store);

	/* capability check */
	capa_flag_id =
		camel_imapx_extd_store_get_capa_flag_id (estore,
		                                         CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ACL);
	capa = is->cinfo->capa & capa_flag_id;
	if (! capa) {
		g_set_error (err,
		             CAMEL_IMAPX_ERROR,
		             1, /* FIXME define and add a sensible code here */
		             _("Got ACL response but server did not advertise ACL capability"));
		goto exit;
	}

	acl = camel_imapx_extd_store_get_acl_table (estore);
	parse_and_add_ok =
		camel_imapx_acl_update_acl_from_server_response (acl,
		                                                 stream,
		                                                 cancellable,
		                                                 &tmp_err);

	if (! parse_and_add_ok) {
		g_propagate_error (err, tmp_err);
		goto exit;
	}

	success = TRUE;

 exit:
	g_object_unref (store);

	g_mutex_unlock (&handler_lock);

	return success;
}

static gboolean
imapx_extd_server_untagged_myrights (CamelIMAPXServer *is,
                                     CamelIMAPXStream *stream,
                                     GCancellable *cancellable,
                                     GError **err)
{
	static GMutex handler_lock;
	CamelIMAPXStore *store = NULL;
	CamelIMAPXExtdStore *estore = NULL;
	CamelImapxAcl *acl = NULL;
	guint32 capa = 0;
	guint32 capa_flag_id = 0;
	GError *tmp_err = NULL;
	gboolean parse_and_add_ok = FALSE;
	gboolean success = FALSE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_return_val_if_fail (CAMEL_IS_IMAPX_STREAM (stream), FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_mutex_lock (&handler_lock);

	store = camel_imapx_server_ref_store (is);
	estore = CAMEL_IMAPX_EXTD_STORE (store);

	/* capability check */
	capa_flag_id =
		camel_imapx_extd_store_get_capa_flag_id (estore,
		                                         CAMEL_IMAPX_EXTD_STORE_CAPA_FLAG_ACL);
	capa = is->cinfo->capa & capa_flag_id;
	if (! capa) {
		g_set_error (err,
		             CAMEL_IMAPX_ERROR,
		             1, /* FIXME define and add a sensible code here */
		             _("Got MYRIGHTS response but server did not advertise ACL capability"));
		goto exit;
	}

	acl = camel_imapx_extd_store_get_acl_table (estore);
	parse_and_add_ok =
		camel_imapx_acl_update_myrights_from_server_response (acl,
		                                                      stream,
		                                                      cancellable,
		                                                      &tmp_err);

	if (! parse_and_add_ok) {
		g_propagate_error (err, tmp_err);
		goto exit;
	}

	success = TRUE;

 exit:
	g_object_unref (store);

	g_mutex_unlock (&handler_lock);

	return success;
}

/*----------------------------------------------------------------------------*/

KolabGConstList*
camel_imapx_extd_server_acl_get_handler_descriptors (void)
{
	KolabGConstList *list = NULL;
	list = kolab_util_glib_gconstlist_prepend (list,
	                                           (gconstpointer)(&desc_myrights));
	list = kolab_util_glib_gconstlist_prepend (list,
	                                           (gconstpointer)(&desc_acl));
	return list;
}

gboolean
camel_imapx_extd_server_get_myrights (CamelIMAPXServer *is,
                                      const gchar *foldername,
                                      GCancellable *cancellable,
                                      GError **err)
{
	gchar *cmd = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_return_val_if_fail (foldername != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* TODO move MYRIGHTS string to -acl.[hc] */
	cmd = g_strdup_printf ("%s \"%s\"",
	                       IMAPX_IMAP_TOKEN_MYRIGHTS,
	                       foldername);

	/* run MYRIGHTS command */
	ok = camel_imapx_extd_utils_command_run (is,
	                                         IMAPX_IMAP_TOKEN_MYRIGHTS,
	                                         cmd,
	                                         cancellable,
	                                         &tmp_err);
	g_free (cmd);

	if (! ok)
		g_propagate_error (err, tmp_err);

	return ok;
}

gboolean
camel_imapx_extd_server_get_acl (CamelIMAPXServer *is,
                                 const gchar *foldername,
                                 GCancellable *cancellable,
                                 GError **err)
{
	gchar *cmd = NULL;
	GError *tmp_err = NULL;
	gboolean ok = FALSE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_return_val_if_fail (foldername != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* TODO move GETACL string to -acl.[hc] */
	cmd = g_strdup_printf ("%s \"%s\"",
	                       IMAPX_IMAP_TOKEN_GETACL,
	                       foldername);

	/* run GETACL command */
	ok = camel_imapx_extd_utils_command_run (is,
	                                         IMAPX_IMAP_TOKEN_GETACL,
	                                         cmd,
	                                         cancellable,
	                                         &tmp_err);
	g_free (cmd);

	if (! ok)
		g_propagate_error (err, tmp_err);

	return ok;
}

gboolean
camel_imapx_extd_server_set_acl (CamelIMAPXServer *is,
                                 const gchar *foldername,
                                 const GList *entries,
                                 GCancellable *cancellable,
                                 GError **err)
{
	static GMutex setter_lock;
	GList *commands = NULL;
	GList *cmd_ptr = NULL;
	CamelImapxAclCmd *cmd = NULL;
	GError *tmp_err = NULL;
	gboolean ok = TRUE;

	g_assert (CAMEL_IS_IMAPX_SERVER (is));
	g_return_val_if_fail (foldername != NULL, FALSE);
	g_return_val_if_fail (entries != NULL, FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_mutex_lock (&setter_lock);

	commands = camel_imapx_acl_commandlist_new (entries,
	                                            foldername,
	                                            &tmp_err);

	if (commands == NULL)
		goto exit;

	cmd_ptr = commands;
	while (cmd_ptr != NULL) {
		cmd = (CamelImapxAclCmd *) (cmd_ptr->data);
		ok = camel_imapx_extd_utils_command_run (is,
		                                         cmd->token,
		                                         cmd->command,
		                                         cancellable,
		                                         &tmp_err);
		if (! ok)
			goto exit;

		cmd_ptr = g_list_next (cmd_ptr);
	}

 exit:

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		ok = FALSE;
	}

	if (commands != NULL)
		camel_imapx_acl_commandlist_free (commands);

	g_mutex_unlock (&setter_lock);

	return ok;
}

/*----------------------------------------------------------------------------*/
