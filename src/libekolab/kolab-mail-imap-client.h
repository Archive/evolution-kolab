/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-imap-client.h
 *
 *  Fri Feb 04 11:19:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

/**
 * SECTION: kolab-mail-imap-client
 * @short_description: the Kolab2 IMAP client
 * @title: KolabMailImapClient
 * @section_id:
 * @see_also: #CamelKolabIMAPXStore, #CamelKolabSession
 * @stability: unstable
 *
 * This class is the Kolab2 mail access wrapper. It is
 * comprised of at least the following
 * <itemizedlist>
 *   <listitem>a #CamelKolabIMAPXStore object</listitem>
 *   <listitem>a #CamelKolabSession object</listitem>
 *   <listitem>some glue code / convenience code</listitem>
 * </itemizedlist>
 *
 * This wrapper uses #CamelKolabIMAPXStore and #CamelKolabSession the way
 * Evolution uses a camel provider, if possible in a simplified fashion (no other
 * "provider" than #CamelKolabIMAPXStore needs to be supported). The glue code
 * aggregates as many fixed IMAPX-sequences as possible to provide a
 * friendly API for use in #KolabMailAccess.
 *
 * Searching for emails based Kolab UID as well as based on search expressions
 * is available.
 *
 * Additional functionality like the assembly and disassembly of (Kolab) mail
 * parts and handling of MIME message parts is done mainly via the
 * #KolabMailMimeBuilder class.
 *
 * This class does not take care of offline PIM email management (transactions,
 * syncing, ...). This is taken care of in another class,
 * namely #KolabMailSynchronizer.
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_MAIL_IMAP_CLIENT_H_
#define _KOLAB_MAIL_IMAP_CLIENT_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libebackend/libebackend.h>

#include "kolab-mail-handle.h"
#include "kolab-mail-mime-builder.h"
#include "kolab-settings-handler.h"
#include "kolab-folder-summary.h"

/*----------------------------------------------------------------------------*/

G_BEGIN_DECLS

#define KOLAB_TYPE_MAIL_IMAP_CLIENT             (kolab_mail_imap_client_get_type ())
#define KOLAB_MAIL_IMAP_CLIENT(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOLAB_TYPE_MAIL_IMAP_CLIENT, KolabMailImapClient))
#define KOLAB_MAIL_IMAP_CLIENT_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), KOLAB_TYPE_MAIL_IMAP_CLIENT, KolabMailImapClientClass))
#define KOLAB_IS_MAIL_IMAP_CLIENT(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOLAB_TYPE_MAIL_IMAP_CLIENT))
#define KOLAB_IS_MAIL_IMAP_CLIENT_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), KOLAB_TYPE_MAIL_IMAP_CLIENT))
#define KOLAB_MAIL_IMAP_CLIENT_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), KOLAB_TYPE_MAIL_IMAP_CLIENT, KolabMailImapClientClass))

typedef struct _KolabMailImapClientClass KolabMailImapClientClass;
typedef struct _KolabMailImapClient KolabMailImapClient;

struct _KolabMailImapClientClass
{
	GObjectClass parent_class;
};

struct _KolabMailImapClient
{
	GObject parent_instance;
};

GType
kolab_mail_imap_client_get_type (void) G_GNUC_CONST;

gboolean
kolab_mail_imap_client_configure (KolabMailImapClient *self,
                                  KolabSettingsHandler *ksettings,
                                  KolabMailMimeBuilder *mimebuilder,
                                  GError **err);

gboolean
kolab_mail_imap_client_bringup (KolabMailImapClient *self,
                                GCancellable *cancellable,
                                GError **err);

gboolean
kolab_mail_imap_client_shutdown (KolabMailImapClient *self,
                                 GCancellable *cancellable,
                                 GError **err);

gboolean
kolab_mail_imap_client_go_online (KolabMailImapClient *self,
                                  GCancellable *cancellable,
                                  GError **err);

gboolean
kolab_mail_imap_client_go_offline (KolabMailImapClient *self,
                                   GCancellable *cancellable,
                                   GError **err);

const gchar*
kolab_mail_imap_client_get_password (KolabMailImapClient *self);

GList*
kolab_mail_imap_client_query_foldernames (KolabMailImapClient *self,
                                          GCancellable *cancellable,
                                          GError **err);

GList*
kolab_mail_imap_client_query_foldernames_anon (gpointer self,
                                               GCancellable *cancellable,
                                               GError **err);

GList*
kolab_mail_imap_client_query_folder_info_online (KolabMailImapClient *self,
                                                 GCancellable *cancellable,
                                                 GError **err);

KolabFolderTypeID
kolab_mail_imap_client_get_folder_type (KolabMailImapClient *self,
                                        const gchar *foldername,
                                        gboolean do_updatedb,
                                        GCancellable *cancellable,
                                        GError **err);

KolabFolderSummary*
kolab_mail_imap_client_query_folder_summary (KolabMailImapClient *self,
                                             const gchar *foldername,
                                             GCancellable *cancellable,
                                             GError **err);

gboolean
kolab_mail_imap_client_create_folder (KolabMailImapClient *self,
                                      const gchar *foldername,
                                      KolabFolderTypeID foldertype,
                                      GCancellable *cancellable,
                                      GError **err);

gboolean
kolab_mail_imap_client_delete_folder (KolabMailImapClient *self,
                                      const gchar *foldername,
                                      GCancellable *cancellable,
                                      GError **err);

gboolean kolab_mail_imap_client_exists_folder (KolabMailImapClient *self,
                                               const gchar *foldername,
                                               gboolean do_updatedb,
                                               GCancellable *cancellable,
                                               GError **err);

GHashTable*
kolab_mail_imap_client_query_summaries (KolabMailImapClient *self,
                                        const gchar* foldername,
                                        const gchar *sexp,
                                        gboolean update,
                                        GCancellable *cancellable,
                                        GError **err);

gboolean
kolab_mail_imap_client_store (KolabMailImapClient *self,
                              KolabMailHandle *kmailhandle,
                              const gchar *foldername,
                              gboolean update,
                              GCancellable *cancellable,
                              GError **err);

gboolean
kolab_mail_imap_client_retrieve (KolabMailImapClient *self,
                                 KolabMailHandle *kmailhandle,
                                 gboolean update,
                                 GCancellable *cancellable,
                                 GError **err);

gboolean
kolab_mail_imap_client_delete (KolabMailImapClient *self,
                               KolabMailHandle *kmailhandle,
                               gboolean imapuid_only,
                               gboolean update,
                               GCancellable *cancellable,
                               GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_MAIL_IMAP_CLIENT_H_ */

/*----------------------------------------------------------------------------*/
