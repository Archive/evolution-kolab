/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-mail-handle-friend.h
 *
 *  Fri Jan 14 15:15:54 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */
 
/*
 * This file declares KolabMailHandle "friend" functions which should not
 * be used by the uninitiated. They are intended for use in KolabMailAccess
 * et al, since we need to manipulate KolabMailHandle-internal data there.
 *
 * The following headers must already be included before including this file:
 *
 * kolab-mail-handle.h
 *
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_MAIL_HANDLE_FRIEND_H_
#define _KOLAB_MAIL_HANDLE_FRIEND_H_

/*----------------------------------------------------------------------------*/

#include "kolab-backend-types.h"
#include "kolab-mail-summary.h"

/*----------------------------------------------------------------------------*/

KolabMailHandle* kolab_mail_handle_new_shallow (const gchar *uid, const gchar *foldername);
KolabMailHandle* kolab_mail_handle_new_from_kconvmail (Kolab_conv_mail *kconvmail, const gchar *uid, const gchar *foldername, KolabFolderTypeID folder_type);
KolabMailHandle* kolab_mail_handle_new_from_handle (const KolabMailHandle *kmailhandle);
KolabMailHandle* kolab_mail_handle_new_from_handle_with_kconvmail (const KolabMailHandle *kmailhandle);
void kolab_mail_handle_set_kconvmail (KolabMailHandle *self, Kolab_conv_mail *kconvmail);
gboolean kolab_mail_handle_convert_kconvmail_to_eds (KolabMailHandle *self, GError **err);
gboolean kolab_mail_handle_convert_eds_to_kconvmail (KolabMailHandle *self, GError **err);
Kolab_conv_mail* kolab_mail_handle_resect_kconvmail (KolabMailHandle *self);
const Kolab_conv_mail* kolab_mail_handle_get_kconvmail (const KolabMailHandle *self);
void kolab_mail_handle_drop_kconvmail (KolabMailHandle *self);
gboolean kolab_mail_handle_has_kconvmail (KolabMailHandle *self);
gboolean kolab_mail_handle_kconvmail_is_equal (KolabMailHandle *self, KolabMailHandle *other);

void kolab_mail_handle_gdestroy (gpointer object);

KolabObjectCacheLocation kolab_mail_handle_get_cache_location (const KolabMailHandle *self);
gboolean kolab_mail_handle_set_cache_location (KolabMailHandle *self, KolabObjectCacheLocation location);
gboolean kolab_mail_handle_unset_cache_location (KolabMailHandle *self, KolabObjectCacheLocation location);

void kolab_mail_handle_set_uid (KolabMailHandle *self, const gchar *uid);
gboolean kolab_mail_handle_set_uid_full (KolabMailHandle *self, const gchar *uid, GError **err);
void kolab_mail_handle_set_foldername (KolabMailHandle *self, const gchar *foldername);

const KolabMailSummary* kolab_mail_handle_get_summary (const KolabMailHandle *self);
KolabMailSummary* kolab_mail_handle_get_summary_nonconst (const KolabMailHandle *self);
gboolean kolab_mail_handle_set_summary (KolabMailHandle *self, KolabMailSummary *summary, GError **err);
KolabMailSummary* kolab_mail_handle_resect_summary (KolabMailHandle *self, GError **err);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_MAIL_HANDLE_FRIEND_H_ */

/*----------------------------------------------------------------------------*/
