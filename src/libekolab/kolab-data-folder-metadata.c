/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-data-folder-metadata.c
 *
 *  Fri Feb 17 17:19:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include "kolab-data-folder-metadata.h"

/*----------------------------------------------------------------------------*/

KolabDataFolderMetadata*
kolab_data_folder_metadata_new (void)
{
	KolabDataFolderMetadata *data = NULL;
	data = g_new0 (KolabDataFolderMetadata, 1);

	/* set field defaults */
	data->foldername = NULL;
	data->foldertype = KOLAB_FOLDER_TYPE_INVAL;
	data->show_all = FALSE;
	data->strategy = KOLAB_SYNC_STRATEGY_DEFAULT;

	return data;
}

KolabDataFolderMetadata*
kolab_data_folder_metadata_clone (const KolabDataFolderMetadata *srcdata)
{
	KolabDataFolderMetadata *data = NULL;

	if (srcdata == NULL)
		return NULL;

	data = g_new0 (KolabDataFolderMetadata, 1);

	data->foldername = g_strdup (srcdata->foldername);
	data->foldertype = srcdata->foldertype;
	data->show_all = srcdata->show_all;

	return data;
}

void
kolab_data_folder_metadata_free (KolabDataFolderMetadata *data)
{
	if (data == NULL)
		return;

	if (data->foldername != NULL)
		g_free (data->foldername);

	g_free (data);
}

/*----------------------------------------------------------------------------*/
