/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-settings-handler.c
 *
 *  Wed Dec 22 13:46:54 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <config.h>

#include <errno.h>

#include <glib/gi18n-lib.h>

#include <libedataserver/libedataserver.h>
#include <libekolabutil/kolab-util-camel.h>
#include <libekolabutil/kolab-util-glib.h>

#include "e-source-kolab-folder.h"
#include "kolab-util-backend.h"
#include "kolab-settings-handler.h"

/*----------------------------------------------------------------------------*/

struct _KolabSettingsHandlerPrivate
{
	CamelKolabIMAPXSettings *camel_settings;
	EBackend *e_backend;

	gboolean is_configured;
	gboolean is_up;

	gchar   *sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_LAST_FIELD];
	guint    sdata_uint[KOLAB_SETTINGS_HANDLER_UINT_LAST_FIELD];
	gint     sdata_int[KOLAB_SETTINGS_HANDLER_INT_LAST_FIELD];
	gboolean sdata_bool[KOLAB_SETTINGS_HANDLER_BOOL_LAST_FIELD];
};

enum {
	PROP_0,
	PROP_CAMEL_SETTINGS,
	PROP_E_BACKEND
};

#define KOLAB_SETTINGS_HANDLER_PRIVATE(obj)  (G_TYPE_INSTANCE_GET_PRIVATE ((obj), KOLAB_TYPE_SETTINGS_HANDLER, KolabSettingsHandlerPrivate))

G_DEFINE_TYPE (KolabSettingsHandler, kolab_settings_handler, G_TYPE_OBJECT)

/*----------------------------------------------------------------------------*/

typedef gboolean (*KolabSettingsHandlerCharSetFunc) (KolabSettingsHandler*, gchar*, GError**);
typedef gboolean (*KolabSettingsHandlerUintSetFunc) (KolabSettingsHandler*, guint, GError**);
typedef gboolean (*KolabSettingsHandlerIntSetFunc) (KolabSettingsHandler*, gint, GError**);
typedef gboolean (*KolabSettingsHandlerBoolSetFunc) (KolabSettingsHandler*, gboolean, GError**);

typedef gboolean (*KolabSettingsHandlerGetFunc) (KolabSettingsHandler*, GError**);

static gboolean settings_handler_char_get_func_camel_data_dir (KolabSettingsHandler*, GError**);
static gboolean settings_handler_char_set_func_camel_data_dir (KolabSettingsHandler*, gchar*, GError**);
static gboolean settings_handler_char_get_func_camel_cache_dir (KolabSettingsHandler*, GError**);
static gboolean settings_handler_char_set_func_camel_cache_dir (KolabSettingsHandler*, gchar*, GError**);
static gboolean settings_handler_char_get_func_camel_config_dir (KolabSettingsHandler*, GError**);
static gboolean settings_handler_char_set_func_camel_config_dir (KolabSettingsHandler*, gchar*, GError**);
static gboolean settings_handler_char_get_func_user_home_dir (KolabSettingsHandler*, GError**);
static gboolean settings_handler_char_set_func_user_home_dir (KolabSettingsHandler*, gchar*, GError**);
static gboolean settings_handler_char_get_func_esource_uid (KolabSettingsHandler*, GError**);
static gboolean settings_handler_char_set_func_esource_uid (KolabSettingsHandler*, gchar*, GError**);
static gboolean settings_handler_uint_set_func_folder_context (KolabSettingsHandler*, guint, GError**);
static gboolean settings_handler_uint_get_func_folder_syncstrategy (KolabSettingsHandler*, GError**);
static gboolean settings_handler_uint_set_func_folder_syncstrategy (KolabSettingsHandler*, guint, GError**);

static KolabSettingsHandlerGetFunc _kolab_settings_handler_char_get_funcs[] = {
	settings_handler_char_get_func_camel_data_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_DATA_DIR */
	settings_handler_char_get_func_camel_cache_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CACHE_DIR */
	settings_handler_char_get_func_camel_config_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CONFIG_DIR */
	NULL, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_ACCOUNT_DIR */
	settings_handler_char_get_func_user_home_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_USER_HOME_DIR */
	settings_handler_char_get_func_esource_uid, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_ESOURCE_UID */
	NULL, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_KOLAB_USER_PASSWORD */
};

static KolabSettingsHandlerCharSetFunc _kolab_settings_handler_char_set_funcs[] = {
	settings_handler_char_set_func_camel_data_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_DATA_DIR */
	settings_handler_char_set_func_camel_cache_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CACHE_DIR */
	settings_handler_char_set_func_camel_config_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CONFIG_DIR */
	NULL, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_ACCOUNT_DIR */
	settings_handler_char_set_func_user_home_dir, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_USER_HOME_DIR */
	settings_handler_char_set_func_esource_uid, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_ESOURCE_UID */
	NULL, /* KOLAB_SETTINGS_HANDLER_CHAR_FIELD_KOLAB_USER_PASSWORD */
};

static KolabSettingsHandlerGetFunc _kolab_settings_handler_uint_get_funcs[] = {
	NULL, /* KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_CONTEXT */
	settings_handler_uint_get_func_folder_syncstrategy /* KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_SYNCSTRATEGY */
};

static KolabSettingsHandlerUintSetFunc _kolab_settings_handler_uint_set_funcs[] = {
	settings_handler_uint_set_func_folder_context, /* KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_CONTEXT */
	settings_handler_uint_set_func_folder_syncstrategy /* KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_SYNCSTRATEGY */
};

/*----------------------------------------------------------------------------*/

static gboolean
settings_handler_char_get_func_camel_data_dir (KolabSettingsHandler *self,
                                               GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	KolabFolderContextID context = KOLAB_FOLDER_CONTEXT_INVAL;
	gchar *data_dir = NULL;
	gchar *priv_data_dir = NULL;
	gchar *backend_dir = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);
	context = priv->sdata_uint[KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_CONTEXT];

	priv_data_dir = priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_DATA_DIR];
	if (priv_data_dir != NULL)
		return TRUE;

	/* TODO elaborate on directory creation */

	switch (context) {
	case KOLAB_FOLDER_CONTEXT_EMAIL:
		backend_dir = g_strdup ("mail");
		break;
	case KOLAB_FOLDER_CONTEXT_CONTACT:
		backend_dir = g_strdup ("addressbook");
		break;
	case KOLAB_FOLDER_CONTEXT_CALENDAR:
		backend_dir = g_strdup ("calendar");
		break;
	default:
		g_assert_not_reached ();
	}

	data_dir = g_build_filename (e_get_user_data_dir (),
	                             backend_dir,
	                             NULL);

	g_free (backend_dir);

	if (g_mkdir_with_parents (data_dir, 0700) != 0) {
		g_set_error (err,
		             G_FILE_ERROR,
		             g_file_error_from_errno (errno),
		             _("Could not create directory '%s' ('%s')"),
		             data_dir, g_strerror (errno));
		g_free (data_dir);
		return FALSE;
	}

	priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_DATA_DIR] = data_dir;
	return TRUE;
}

static gboolean
settings_handler_char_get_func_camel_cache_dir (KolabSettingsHandler *self,
                                                GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	KolabFolderContextID context = KOLAB_FOLDER_CONTEXT_INVAL;
	gchar *cache_dir = NULL;
	gchar *priv_cache_dir = NULL;
	gchar *backend_dir = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);
	context = priv->sdata_uint[KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_CONTEXT];

	priv_cache_dir = priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CACHE_DIR];
	if (priv_cache_dir != NULL)
		return TRUE;

	/* TODO elaborate on directory creation */

	switch (context) {
	case KOLAB_FOLDER_CONTEXT_EMAIL:
		backend_dir = g_strdup ("mail");
		break;
	case KOLAB_FOLDER_CONTEXT_CONTACT:
		backend_dir = g_strdup ("addressbook");
		break;
	case KOLAB_FOLDER_CONTEXT_CALENDAR:
		backend_dir = g_strdup ("calendar");
		break;
	default:
		g_assert_not_reached ();
	}

	cache_dir = g_build_filename (e_get_user_cache_dir (),
	                              backend_dir,
	                              NULL);

	g_free (backend_dir);

	if (g_mkdir_with_parents (cache_dir, 0700) != 0) {
		g_set_error (err,
		             G_FILE_ERROR,
		             g_file_error_from_errno (errno),
		             _("Could not create directory '%s' ('%s')"),
		             cache_dir, g_strerror (errno));
		g_free (cache_dir);
		return FALSE;
	}

	priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CACHE_DIR] = cache_dir;
	return TRUE;
}

static gboolean
settings_handler_char_get_func_user_home_dir (KolabSettingsHandler *self,
                                              GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	gchar *home_dir = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	home_dir = priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_USER_HOME_DIR];
	if (home_dir != NULL)
		return TRUE;

	home_dir = g_strdup (g_get_home_dir ());

	priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_USER_HOME_DIR] = home_dir;
	return TRUE;
}

static gboolean
settings_handler_char_set_func_camel_data_dir (KolabSettingsHandler *self,
                                               gchar *value,
                                               GError **err)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	(void)value;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_set_error (err,
	             KOLAB_BACKEND_ERROR,
	             KOLAB_BACKEND_ERROR_GENERIC,
	             _("Cannot set Camel data directory, it is a read-only resource"));
	return FALSE;
}

static gboolean
settings_handler_char_set_func_camel_cache_dir (KolabSettingsHandler *self,
                                                gchar *value,
                                                GError **err)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	(void)value;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_set_error (err,
	             KOLAB_BACKEND_ERROR,
	             KOLAB_BACKEND_ERROR_GENERIC,
	             _("Cannot set Camel cache directory, it is a read-only resource"));
	return FALSE;
}

static gboolean
settings_handler_char_set_func_user_home_dir (KolabSettingsHandler *self,
                                              gchar *value,
                                              GError **err)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	(void)value;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_set_error (err,
	             KOLAB_BACKEND_ERROR,
	             KOLAB_BACKEND_ERROR_GENERIC,
	             _("Cannot set user home directory, it is a read-only resource"));
	return FALSE;
}

static gboolean
settings_handler_char_get_func_esource_uid (KolabSettingsHandler *self,
                                            GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	ESource *esource = NULL;
	gchar *uid = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);
	g_return_val_if_fail (E_IS_BACKEND (priv->e_backend), FALSE);

	esource = e_backend_get_source (priv->e_backend);
	if (! E_IS_SOURCE (esource)) {
		g_set_error (err,
		             KOLAB_BACKEND_ERROR,
		             KOLAB_BACKEND_ERROR_GENERIC,
		             _("Could not get ESource for backend"));
		return FALSE;
	}

	uid = g_strdup (e_source_get_uid (esource));
	if (priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_ESOURCE_UID] != NULL)
		g_free (priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_ESOURCE_UID]);
	priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_ESOURCE_UID] = uid;

	return TRUE;
}

static gboolean
settings_handler_char_set_func_esource_uid (KolabSettingsHandler *self,
                                            gchar *value,
                                            GError **err)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	(void)value;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_set_error (err,
	             KOLAB_BACKEND_ERROR,
	             KOLAB_BACKEND_ERROR_GENERIC,
	             _("Cannot set ESource UID, it is a read-only resource"));
	return FALSE;
}

static gboolean
settings_handler_char_get_func_camel_config_dir (KolabSettingsHandler *self,
                                                 GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	gboolean ok = FALSE;
	const gchar *data_dir = NULL;
	gchar *config_dir = NULL;
	gchar *priv_config_dir = NULL;
	GError *tmp_err = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	priv_config_dir = priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CONFIG_DIR];
	if (priv_config_dir != NULL)
		return TRUE;

	ok = settings_handler_char_get_func_camel_data_dir (self, &tmp_err);
	if (! ok) {
		g_propagate_error (err, tmp_err);
		return FALSE;
	}

	data_dir = priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_DATA_DIR];

	config_dir = g_build_filename (data_dir, "config", NULL);

	if (g_mkdir_with_parents (config_dir, 0700) != 0) {
		g_set_error (err,
		             G_FILE_ERROR,
		             g_file_error_from_errno (errno),
		             _("Could not create directory '%s' ('%s')"),
		             config_dir, g_strerror (errno));
		g_free (config_dir);
		return FALSE;
	}

	priv->sdata_char[KOLAB_SETTINGS_HANDLER_CHAR_FIELD_CAMEL_CONFIG_DIR] = config_dir;
	return TRUE;
}

static gboolean
settings_handler_char_set_func_camel_config_dir (KolabSettingsHandler *self,
                                                 gchar *value,
                                                 GError **err)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	(void)value;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_set_error (err,
	             KOLAB_BACKEND_ERROR,
	             KOLAB_BACKEND_ERROR_GENERIC,
	             _("Cannot set Camel configuration directory, it is a read-only resource"));
	return FALSE;
}

static gboolean
settings_handler_uint_set_func_folder_context (KolabSettingsHandler *self,
                                               guint value,
                                               GError **err)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	(void)value;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_set_error (err,
	             KOLAB_BACKEND_ERROR,
	             KOLAB_BACKEND_ERROR_GENERIC,
	             _("Cannot change folder context after it has initially been set"));
	return FALSE;
}

static gboolean
settings_handler_uint_get_func_folder_syncstrategy (KolabSettingsHandler *self,
                                                    GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	ESource *esource = NULL;
	ESourceKolabFolder *extension = NULL;
	KolabSyncStrategyID strategy = KOLAB_SYNC_STRATEGY_DEFAULT;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);
	g_return_val_if_fail (E_IS_BACKEND (priv->e_backend), FALSE);

	esource = e_backend_get_source (priv->e_backend);
	if (! E_IS_SOURCE (esource)) {
		g_set_error (err,
		             KOLAB_BACKEND_ERROR,
		             KOLAB_BACKEND_ERROR_GENERIC,
		             _("Could not get ESource for backend"));
		return FALSE;
	}

	if (! e_source_has_extension (esource, E_SOURCE_EXTENSION_KOLAB_FOLDER)) {
		g_set_error (err,
		             KOLAB_BACKEND_ERROR,
		             KOLAB_BACKEND_ERROR_GENERIC,
		             _("ESource for backend has no 'Kolab Folder' extension"));
		return FALSE;
	}

	extension = e_source_get_extension (esource, E_SOURCE_EXTENSION_KOLAB_FOLDER);
	strategy = e_source_kolab_folder_get_sync_strategy (extension);
	priv->sdata_uint[KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_SYNCSTRATEGY] = strategy;

	return TRUE;
}

static gboolean
settings_handler_uint_set_func_folder_syncstrategy (KolabSettingsHandler *self,
                                                    guint value,
                                                    GError **err)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	(void)value;
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_set_error (err,
	             KOLAB_BACKEND_ERROR,
	             KOLAB_BACKEND_ERROR_GENERIC,
	             _("Cannot set Kolab Folder Sync Strategy, it is a read-only resource"));
	return FALSE;
}

/*----------------------------------------------------------------------------*/

static void
kolab_settings_handler_init (KolabSettingsHandler *self)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	gint ii = 0;

	g_return_if_fail (KOLAB_IS_SETTINGS_HANDLER (self));

	priv = self->priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	for (ii = 0; ii < KOLAB_SETTINGS_HANDLER_CHAR_LAST_FIELD; ii++)
		priv->sdata_char[ii] = NULL;
	for (ii = 0; ii < KOLAB_SETTINGS_HANDLER_UINT_LAST_FIELD; ii++)
		priv->sdata_uint[ii] = 0;
	for (ii = 0; ii < KOLAB_SETTINGS_HANDLER_INT_LAST_FIELD; ii++)
		priv->sdata_int[ii] = 0;
	for (ii = 0; ii < KOLAB_SETTINGS_HANDLER_BOOL_LAST_FIELD; ii++)
		priv->sdata_bool[ii] = FALSE;

	/* documenting initial settings (dumb fallback values) */
	priv->sdata_uint[KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_CONTEXT] = KOLAB_FOLDER_CONTEXT_INVAL;
	priv->sdata_uint[KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_SYNCSTRATEGY] = KOLAB_SYNC_STRATEGY_DEFAULT;
	priv->sdata_int[KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_IMAP_PORT]  = KOLAB_SERVER_IMAP_PORT;
	priv->sdata_int[KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_IMAPS_PORT] = KOLAB_SERVER_IMAPS_PORT;
	priv->sdata_int[KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_HTTP_PORT]  = KOLAB_SERVER_HTTP_PORT;
	priv->sdata_int[KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_HTTPS_PORT] = KOLAB_SERVER_HTTPS_PORT;
	priv->sdata_int[KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_LDAP_PORT]  = KOLAB_SERVER_LDAP_PORT;
	priv->sdata_int[KOLAB_SETTINGS_HANDLER_INT_FIELD_KOLAB_SERVER_LDAPS_PORT] = KOLAB_SERVER_LDAPS_PORT;

	priv->is_configured = FALSE;
	priv->is_up = FALSE;
}

static void
kolab_settings_handler_set_camel_settings (KolabSettingsHandler *ksettings,
                                           CamelKolabIMAPXSettings *camel_settings)
{
	g_return_if_fail (CAMEL_IS_KOLAB_IMAPX_SETTINGS (camel_settings));
	g_return_if_fail (ksettings->priv->camel_settings == NULL);

	ksettings->priv->camel_settings = g_object_ref (camel_settings);
}

static void
kolab_settings_handler_set_e_backend (KolabSettingsHandler *ksettings,
                                      EBackend *e_backend)
{
	g_return_if_fail (E_IS_BACKEND (e_backend));
	g_return_if_fail (ksettings->priv->e_backend == NULL);

	ksettings->priv->e_backend = g_object_ref (e_backend);
}

static void
kolab_settings_handler_set_property (GObject *object,
                                     guint property_id,
                                     const GValue *value,
                                     GParamSpec *pspec)
{
	switch (property_id) {
	case PROP_CAMEL_SETTINGS:
		kolab_settings_handler_set_camel_settings (
		                                           KOLAB_SETTINGS_HANDLER (object),
		                                           g_value_get_object (value));
		return;
	case PROP_E_BACKEND:
		kolab_settings_handler_set_e_backend (
		                                      KOLAB_SETTINGS_HANDLER (object),
		                                      g_value_get_object (value));
		return;
	default:
		break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
kolab_settings_handler_get_property (GObject *object,
                                     guint property_id,
                                     GValue *value,
                                     GParamSpec *pspec)
{
	switch (property_id) {
	case PROP_CAMEL_SETTINGS:
		g_value_set_object (
		                    value,
		                    kolab_settings_handler_get_camel_settings (
		                                                               KOLAB_SETTINGS_HANDLER (object)));
		return;
	case PROP_E_BACKEND:
		g_value_set_object (
		                    value,
		                    kolab_settings_handler_get_e_backend (
		                                                          KOLAB_SETTINGS_HANDLER (object)));
		return;
	default:
		break;
	}

	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
}

static void
kolab_settings_handler_dispose (GObject *object)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (object);

	if (priv->camel_settings != NULL) {
		g_object_unref (priv->camel_settings);
		priv->camel_settings = NULL;
	}

	if (priv->e_backend != NULL) {
		g_object_unref (priv->e_backend);
		priv->e_backend = NULL;
	}

	G_OBJECT_CLASS (kolab_settings_handler_parent_class)->dispose (object);
}

static void
kolab_settings_handler_finalize (GObject *object)
{
	KolabSettingsHandler *self = NULL;
	KolabSettingsHandlerPrivate *priv = NULL;
	gint ii = 0;

	self = KOLAB_SETTINGS_HANDLER (object);
	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	for (ii = 0; ii < KOLAB_SETTINGS_HANDLER_CHAR_LAST_FIELD; ii++) {
		if (priv->sdata_char[ii] != NULL)
			g_free (priv->sdata_char[ii]);
	}

	G_OBJECT_CLASS (kolab_settings_handler_parent_class)->finalize (object);
}

static void
kolab_settings_handler_class_init (KolabSettingsHandlerClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	/* GObjectClass* parent_class = G_OBJECT_CLASS (klass); */

	g_type_class_add_private (klass, sizeof (KolabSettingsHandlerPrivate));

	object_class->set_property = kolab_settings_handler_set_property;
	object_class->get_property = kolab_settings_handler_get_property;
	object_class->dispose = kolab_settings_handler_dispose;
	object_class->finalize = kolab_settings_handler_finalize;

	g_object_class_install_property (object_class,
	                                 PROP_CAMEL_SETTINGS,
	                                 g_param_spec_object ("camel-settings",
	                                                      "Camel Settings",
	                                                      "An authoritative CamelKolabIMAPXSettings",
	                                                      CAMEL_TYPE_KOLAB_IMAPX_SETTINGS,
	                                                      G_PARAM_READWRITE |
	                                                      G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class,
	                                 PROP_E_BACKEND,
	                                 g_param_spec_object ("e-backend",
	                                                      "EBackend",
	                                                      "An EBackend instance",
	                                                      E_TYPE_BACKEND,
	                                                      G_PARAM_READWRITE|
	                                                      G_PARAM_CONSTRUCT_ONLY));
}

KolabSettingsHandler *
kolab_settings_handler_new (CamelKolabIMAPXSettings *camel_settings,
                            EBackend *e_backend)
{
	KolabSettingsHandler *ksettings = NULL;

	g_return_val_if_fail (CAMEL_IS_KOLAB_IMAPX_SETTINGS (camel_settings), NULL);
	/* backend may be NULL */

	if (e_backend != NULL)
		ksettings = g_object_new (KOLAB_TYPE_SETTINGS_HANDLER,
		                          "camel-settings", camel_settings,
		                          "e-backend", e_backend,
		                          NULL);
	else
		ksettings = g_object_new (KOLAB_TYPE_SETTINGS_HANDLER,
		                          "camel-settings", camel_settings,
		                          NULL);

	return ksettings;
}

/*----------------------------------------------------------------------------*/
/* object config/status */

/**
 * kolab_settings_handler_configure:
 * @self: a #KolabSettingsHandler instance
 * @context: the folder context (i.e. backend type) the settings handler will
 *	     be used in (for now, either KOLAB_FOLDER_CONTEXT_CALENDAR or
 *	     KOLAB_FOLDER_CONTEXT_CONTACT)
 * @err: a #GError object (or NULL)
 *
 * Configures the object for operation. Must be called once after object
 * instantiation and before any other operation.
 *
 * Returns: TRUE on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_configure  (KolabSettingsHandler *self,
                                   KolabFolderContextID context,
                                   GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail ((context > KOLAB_FOLDER_CONTEXT_INVAL) &&
	                      (context < KOLAB_FOLDER_LAST_CONTEXT), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	/* configure once... */
	if (priv->is_configured == TRUE)
		return TRUE;

	priv->sdata_uint[KOLAB_SETTINGS_HANDLER_UINT_FIELD_FOLDER_CONTEXT] = context;

	priv->is_configured = TRUE;
	return TRUE;
}

/**
 * kolab_settings_handler_bringup:
 * @self: a #KolabSettingsHandler instance
 * @err: a #GError object (or NULL)
 *
 * Gets the #KolabSettingsHandler object into operational mode. This may involve
 * file I/O.
 * Must be called once after kolab_settings_handler_configure() and before
 * any other operation.
 *
 * Returns: TRUE on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_bringup (KolabSettingsHandler *self,
                                GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, FALSE);
	g_return_val_if_fail (priv->is_up == FALSE, FALSE);

	/* TODO implement me */

	priv->is_up = TRUE;
	return TRUE;
}

/**
 * kolab_settings_handler_shutdown:
 * @self: a #KolabSettingsHandler instance
 * @err: a #GError object (or NULL)
 *
 * Shuts down the #KolabSettingsHandler object.
 * Must be called before object destruction. No further operation on the
 * object is valid unless kolab_settings_handler_bringup() is called again.
 *
 * Returns: TRUE on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_shutdown (KolabSettingsHandler *self,
                                 GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_assert (priv->is_configured == TRUE);
	g_assert (priv->is_up == TRUE);

	/* TODO implement me */

	priv->is_up = FALSE;
	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* config item getters/setters */

CamelKolabIMAPXSettings *
kolab_settings_handler_get_camel_settings (KolabSettingsHandler *self)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), NULL);

	return self->priv->camel_settings;
}

EBackend *
kolab_settings_handler_get_e_backend (KolabSettingsHandler *self)
{
	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), NULL);

	return self->priv->e_backend;
}

/**
 * kolab_settings_handler_set_char_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the char field id to set a value for
 * @value: the string to set as a value for the given field
 * @err: a #GError object (or NULL)
 *
 * Sets @value as the value for the char field selected by @field_id.
 * If an error occurs and the internal value of the char field selected by
 * @field_id is not changed, no ownership is taken of @value. If the function
 * returns successfully, the #KolabSettingsHandler instance will have taken
 * ownership of @value, so make sure it is not free'd later on.
 *
 * Returns: TRUE on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_set_char_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerCharFieldID field_id,
                                       gchar *value,
                                       GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	KolabSettingsHandlerCharSetFunc setfunc = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (field_id < KOLAB_SETTINGS_HANDLER_CHAR_LAST_FIELD, FALSE);
	/* value may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, FALSE);
	g_return_val_if_fail (priv->is_up == TRUE, FALSE);

	setfunc = _kolab_settings_handler_char_set_funcs[field_id];
	if (setfunc != NULL) {
		ok = setfunc (self, value, &tmp_err);
		if (! ok) {
			g_propagate_error (err, tmp_err);
			return FALSE;
		}
	}

	if (priv->sdata_char[field_id] != NULL)
		g_free (priv->sdata_char[field_id]);

	priv->sdata_char[field_id] = value;
	return TRUE;
}

/**
 * kolab_settings_handler_get_char_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the char field id to get a value from
 * @err: a #GError object (or NULL)
 *
 * Gets the value of the char field selected by @field_id.
 *
 * Returns: the referenced string on success,
 *          NULL otherwise (with @err set)
 */
const gchar*
kolab_settings_handler_get_char_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerCharFieldID field_id,
                                       GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	KolabSettingsHandlerGetFunc getfunc = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), NULL);
	g_return_val_if_fail (field_id < KOLAB_SETTINGS_HANDLER_CHAR_LAST_FIELD, NULL);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, NULL);
	g_return_val_if_fail (priv->is_up == TRUE, NULL);

	getfunc = _kolab_settings_handler_char_get_funcs[field_id];
	if (getfunc != NULL) {
		ok = getfunc (self, &tmp_err);
		if (! ok) {
			g_propagate_error (err, tmp_err);
			return NULL;
		}
	}

	return priv->sdata_char[field_id];
}

/**
 * kolab_settings_handler_set_uint_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the uint field id to set a value for
 * @value: value to set for the given field
 * @err: a #GError object (or NULL)
 *
 * Sets @value as the value for the uint field selected by @field_id.
 * If an error occurs, the internal value of the uint field selected by
 * @field_id is not changed.
 *
 * Returns: TRUE on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_set_uint_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerUintFieldID field_id,
                                       guint value,
                                       GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	KolabSettingsHandlerUintSetFunc setfunc = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	g_return_val_if_fail (field_id < KOLAB_SETTINGS_HANDLER_UINT_LAST_FIELD, FALSE);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, FALSE);
	g_return_val_if_fail (priv->is_up == TRUE, FALSE);

	setfunc = _kolab_settings_handler_uint_set_funcs[field_id];
	if (setfunc != NULL) {
		ok = setfunc (self, value, &tmp_err);
		if (! ok) {
			g_propagate_error (err, tmp_err);
			return FALSE;
		}
	}

	priv->sdata_uint[field_id] = value;
	return TRUE;
}

/**
 * kolab_settings_handler_get_uint_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the uint field id to get a value from
 * @err: a #GError object (or NULL)
 *
 * Gets the value of the uint field selected by @field_id. Check @err for errors
 * since 0 may be a valid return value for the uint field.
 *
 * Returns: the referenced unsigned integer on success,
 *          0 otherwise (with @err set)
 */
guint
kolab_settings_handler_get_uint_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerUintFieldID field_id,
                                       GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;
	KolabSettingsHandlerGetFunc getfunc = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), 0);
	g_return_val_if_fail (field_id < KOLAB_SETTINGS_HANDLER_UINT_LAST_FIELD, 0);
	g_return_val_if_fail (err == NULL || *err == NULL, 0);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, 0);
	g_return_val_if_fail (priv->is_up == TRUE, 0);

	getfunc = _kolab_settings_handler_uint_get_funcs[field_id];
	if (getfunc != NULL) {
		ok = getfunc (self, &tmp_err);
		if (! ok) {
			g_propagate_error (err, tmp_err);
			return 0; /* no error indicator, check **err */
		}
	}

	return priv->sdata_uint[field_id];
}

/**
 * kolab_settings_handler_set_int_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the int field id to set a value for
 * @value: value to set for the given field
 * @err: a #GError object (or NULL)
 *
 * Sets @value as the value for the int field selected by @field_id.
 * If an error occurs, the internal value of the int field selected by
 * @field_id is not changed.
 *
 * Returns: TRUE on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_set_int_field (KolabSettingsHandler *self,
                                      KolabSettingsHandlerIntFieldID field_id,
                                      gint value,
                                      GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	/* g_assert (field_id < KOLAB_SETTINGS_HANDLER_INT_LAST_FIELD); */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, FALSE);
	g_return_val_if_fail (priv->is_up == TRUE, FALSE);

	priv->sdata_int[field_id] = value;
	return TRUE;
}

/**
 * kolab_settings_handler_get_int_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the int field id to get a value from
 * @err: a #GError object (or NULL)
 *
 * Gets the value of the int field selected by @field_id. Check @err for errors
 * since 0 may be a valid return value for the int field.
 *
 * Returns: the referenced integer on success,
 *          0 otherwise (with @err set)
 */
gint
kolab_settings_handler_get_int_field (KolabSettingsHandler *self,
                                      KolabSettingsHandlerIntFieldID field_id,
                                      GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), 0);
	/* g_assert (field_id < KOLAB_SETTINGS_HANDLER_INT_LAST_FIELD); */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, 0);
	g_return_val_if_fail (priv->is_up == TRUE, 0);

	return priv->sdata_int[field_id];
}

/**
 * kolab_settings_handler_set_bool_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the bool field id to set a value for
 * @value: value to set for the given field
 * @err: a #GError object (or NULL)
 *
 * Sets @value as the value for the bool field selected by @field_id.
 * If an error occurs, the internal value of the bool field selected by
 * @field_id is not changed.
 *
 * Returns: TRUE on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_set_bool_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerBoolFieldID field_id,
                                       gboolean value,
                                       GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	/* g_assert (field_id < KOLAB_SETTINGS_HANDLER_BOOL_LAST_FIELD); */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, FALSE);
	g_return_val_if_fail (priv->is_up == TRUE, FALSE);

	priv->sdata_bool[field_id] = value;
	return TRUE;
}

/**
 * kolab_settings_handler_get_bool_field:
 * @self: a #KolabSettingsHandler instance
 * @field_id: the bool field id to get a value from
 * @err: a #GError object (or NULL)
 *
 * Gets the value of the bool field selected by @field_id. Check @err for errors
 * since FALSE may be a valid return value for the bool field.
 *
 * Returns: the referenced boolean value on success,
 *          FALSE otherwise (with @err set)
 */
gboolean
kolab_settings_handler_get_bool_field (KolabSettingsHandler *self,
                                       KolabSettingsHandlerBoolFieldID field_id,
                                       GError **err)
{
	KolabSettingsHandlerPrivate *priv = NULL;

	g_return_val_if_fail (KOLAB_IS_SETTINGS_HANDLER (self), FALSE);
	/* g_assert (field_id < KOLAB_SETTINGS_HANDLER_BOOL_LAST_FIELD); */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	priv = KOLAB_SETTINGS_HANDLER_PRIVATE (self);

	g_return_val_if_fail (priv->is_configured == TRUE, FALSE);
	g_return_val_if_fail (priv->is_up == TRUE, FALSE);

	return priv->sdata_bool[field_id];
}

/*----------------------------------------------------------------------------*/
