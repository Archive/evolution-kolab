/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-data-folder-permissions.h
 *
 *  Fri Feb 17 17:07:05 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 *
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_DATA_FOLDER_PERMISSIONS_H_
#define _KOLAB_DATA_FOLDER_PERMISSIONS_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

/*----------------------------------------------------------------------------*/

typedef struct _KolabDataFolderPermissions KolabDataFolderPermissions;
struct _KolabDataFolderPermissions {
	GList *acl;
	GList *myrights;
};

/*----------------------------------------------------------------------------*/

KolabDataFolderPermissions*
kolab_data_folder_permissions_new (void);

KolabDataFolderPermissions*
kolab_data_folder_permissions_clone (const KolabDataFolderPermissions *srcdata,
                                     GError **err);

void
kolab_data_folder_permissions_free (KolabDataFolderPermissions *data);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_DATA_FOLDER_PERMISSIONS_H_ */

/*----------------------------------------------------------------------------*/
