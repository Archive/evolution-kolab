/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-store.h
 *
 *  2011-12-05, 19:05:30
 *  Copyright 2011, Christian Hilberg
 *  <hilberg@unix-ag.org>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_IMAPX_EXTD_STORE_H_
#define _CAMEL_IMAPX_EXTD_STORE_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolabutil/camel-system-headers.h>

#include "camel-imapx-acl.h"
#include "camel-imapx-extd-server.h"

/*----------------------------------------------------------------------------*/
/* Standard GObject macros */

#define CAMEL_TYPE_IMAPX_EXTD_STORE	  \
	(camel_imapx_extd_store_get_type ())
#define CAMEL_IMAPX_EXTD_STORE(obj)	  \
	(G_TYPE_CHECK_INSTANCE_CAST \
	 ((obj), CAMEL_TYPE_IMAPX_EXTD_STORE, CamelIMAPXExtdStore))
#define CAMEL_IMAPX_EXTD_STORE_CLASS(klass)	  \
	(G_TYPE_CHECK_CLASS_CAST \
	 ((klass), CAMEL_TYPE_IMAPX_EXTD_STORE, CamelIMAPXExtdStoreClass))
#define CAMEL_IS_IMAPX_EXTD_STORE(obj)	  \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	 ((obj), CAMEL_TYPE_IMAPX_EXTD_STORE))
#define CAMEL_IS_IMAPX_EXTD_STORE_CLASS(klass)	  \
	(G_TYPE_CHECK_CLASS_TYPE \
	 ((klass), CAMEL_TYPE_IMAPX_EXTD_STORE))
#define CAMEL_IMAPX_EXTD_STORE_GET_CLASS(obj)	  \
	(G_TYPE_INSTANCE_GET_CLASS \
	 ((obj), CAMEL_TYPE_IMAPX_EXTD_STORE, CamelIMAPXExtdStoreClass))

G_BEGIN_DECLS

typedef struct _CamelIMAPXExtdStore CamelIMAPXExtdStore;
typedef struct _CamelIMAPXExtdStoreClass CamelIMAPXExtdStoreClass;

struct _CamelIMAPXExtdStore {
	CamelIMAPXStore parent;
};

struct _CamelIMAPXExtdStoreClass {
	CamelIMAPXStoreClass parent_class;

	CamelIMAPXServer* (*get_server) (CamelIMAPXStore *self,
	                                 const gchar *foldername,
	                                 GCancellable *cancellable,
	                                 GError **err);

	camel_imapx_metadata_proto_t (*metadata_get_proto) (CamelIMAPXExtdStore *self);

	CamelImapxMetadata* (*get_metadata) (CamelIMAPXExtdStore *self,
	                                     CamelImapxMetadataSpec *spec,
	                                     gboolean do_resect,
	                                     GCancellable *cancellable,
	                                     GError **err);

	gboolean (*set_metadata) (CamelIMAPXExtdStore *self,
	                          CamelImapxMetadata *md,
	                          GCancellable *cancellable,
	                          GError **err);

	CamelImapxAcl* (*get_acl) (CamelIMAPXExtdStore *self,
	                           CamelImapxAclSpec *spec,
	                           gboolean do_resect,
	                           GCancellable *cancellable,
	                           GError **err);

	gboolean (*set_acl) (CamelIMAPXExtdStore *self,
	                     const gchar *foldername,
	                     const GList *entries,
	                     GCancellable *cancellable,
	                     GError **err);
};

GType
camel_imapx_extd_store_get_type (void);

CamelIMAPXServer*
camel_imapx_extd_store_get_server (CamelIMAPXStore *self,
                                   const gchar *foldername,
                                   GCancellable *cancellable,
                                   GError **err);

camel_imapx_metadata_proto_t
camel_imapx_extd_store_metadata_get_proto (CamelIMAPXExtdStore *self);

CamelImapxMetadata*
camel_imapx_extd_store_get_metadata (CamelIMAPXExtdStore *self,
                                     CamelImapxMetadataSpec *spec,
                                     gboolean do_resect,
                                     GCancellable *cancellable,
                                     GError **err);

gboolean
camel_imapx_extd_store_set_metadata (CamelIMAPXExtdStore *self,
                                     CamelImapxMetadata *md,
                                     GCancellable *cancellable,
                                     GError **err);

CamelImapxAcl*
camel_imapx_extd_store_get_acl (CamelIMAPXExtdStore *self,
                                CamelImapxAclSpec *spec,
                                gboolean do_resect,
                                GCancellable *cancellable,
                                GError **err);

gboolean
camel_imapx_extd_store_set_acl (CamelIMAPXExtdStore *self,
                                const gchar *foldername,
                                const GList *entries,
                                GCancellable *cancellable,
                                GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_IMAPX_EXTD_STORE_H_ */

/*----------------------------------------------------------------------------*/
