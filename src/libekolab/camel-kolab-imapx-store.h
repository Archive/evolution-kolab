/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-store.h
 *
 *  Fri Sep  3 12:48:31 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_KOLAB_IMAPX_STORE_H_
#define _CAMEL_KOLAB_IMAPX_STORE_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolabutil/kolab-util-folder.h>

#include "camel-imapx-extd-store.h"

/*----------------------------------------------------------------------------*/
/* GObject macros */

#define CAMEL_TYPE_KOLAB_IMAPX_STORE	  \
	(camel_kolab_imapx_store_get_type ())
#define CAMEL_KOLAB_IMAPX_STORE(obj)	  \
	(G_TYPE_CHECK_INSTANCE_CAST \
	 ((obj), CAMEL_TYPE_KOLAB_IMAPX_STORE, CamelKolabIMAPXStore))
#define CAMEL_KOLAB_IMAPX_STORE_CLASS(cls)	  \
	(G_TYPE_CHECK_CLASS_CAST \
	 ((cls), CAMEL_TYPE_KOLAB_IMAPX_STORE, CamelKolabIMAPXStoreClass))
#define CAMEL_IS_KOLAB_IMAPX_STORE(obj)	  \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	 ((obj), CAMEL_TYPE_KOLAB_IMAPX_STORE))
#define CAMEL_IS_KOLAB_IMAPX_STORE_CLASS(cls)	  \
	(G_TYPE_CHECK_CLASS_TYPE \
	 ((cls), CAMEL_TYPE_KOLAB_IMAPX_STORE))
#define CAMEL_KOLAB_IMAPX_STORE_GET_CLASS(obj)	  \
	(G_TYPE_INSTANCE_GET_CLASS \
	 ((obj), CAMEL_TYPE_KOLAB_IMAPX_STORE, CamelKolabIMAPXStoreClass))


G_BEGIN_DECLS

/* We're a descendant of CamelIMAPXExtdStore */

typedef struct _CamelKolabIMAPXStore CamelKolabIMAPXStore;
typedef struct _CamelKolabIMAPXStoreClass CamelKolabIMAPXStoreClass;

struct _CamelKolabIMAPXStore {
	CamelIMAPXExtdStore parent;
};

struct _CamelKolabIMAPXStoreClass {
	CamelIMAPXExtdStoreClass parent_class;

	gboolean (*set_folder_creation_type) (CamelKolabIMAPXStore *self,
	                                      KolabFolderTypeID type_id,
	                                      gboolean check_context);

	KolabFolderTypeID (*get_folder_creation_type) (CamelKolabIMAPXStore *self);

	gboolean (*set_folder_context) (CamelKolabIMAPXStore *self,
	                                KolabFolderContextID context);

	KolabFolderTypeID (*get_folder_type) (CamelKolabIMAPXStore *self,
	                                      const gchar *foldername,
	                                      gboolean do_updatedb,
	                                      GCancellable *cancellable,
	                                      GError **err);

	gboolean (*set_folder_type) (CamelKolabIMAPXStore *self,
	                             const gchar *foldername,
	                             KolabFolderTypeID foldertype,
	                             GCancellable *cancellable,
	                             GError **err);

	CamelFolderInfo* (*get_folder_info_online) (CamelKolabIMAPXStore *self,
	                                            const gchar *top,
	                                            CamelStoreGetFolderInfoFlags flags,
	                                            GCancellable *cancellable,
	                                            GError **err);

	GList* (*resect_folder_list) (CamelKolabIMAPXStore *self);

	gboolean (*get_show_all_folders) (CamelKolabIMAPXStore *self);

	gboolean (*set_show_all_folders) (CamelKolabIMAPXStore *self,
	                                  gboolean show_all,
	                                  GCancellable *cancellable,
	                                  GError **err);

	GList* (*get_folder_permissions) (CamelKolabIMAPXStore *self,
	                                  const gchar *foldername,
	                                  gboolean myrights,
	                                  GCancellable *cancellable,
	                                  GError **err);

	gboolean (*set_folder_permissions) (CamelKolabIMAPXStore *self,
	                                    const gchar *foldername,
	                                    const GList *permissions,
	                                    GCancellable *cancellable,
	                                    GError **err);
};

GType camel_kolab_imapx_store_get_type (void);

/* Kolab extensions */

/* metadata: set/get type for newly created folders */
gboolean
camel_kolab_imapx_store_set_folder_creation_type (CamelKolabIMAPXStore *self,
                                                  KolabFolderTypeID type_id,
                                                  gboolean check_context);

KolabFolderTypeID
camel_kolab_imapx_store_get_folder_creation_type (CamelKolabIMAPXStore *self);

/* metadata: set the folder context (email, calendar, contacts (defaults to email)) */
gboolean
camel_kolab_imapx_store_set_folder_context (CamelKolabIMAPXStore *self,
                                            KolabFolderContextID context);

/* metadata: get the folder type id */
KolabFolderTypeID
camel_kolab_imapx_store_get_folder_type (CamelKolabIMAPXStore *self,
                                         const gchar *foldername,
                                         gboolean do_updatedb,
                                         GCancellable *cancellable,
                                         GError **err);

/* metadata: set the folder type id */
gboolean
camel_kolab_imapx_store_set_folder_type (CamelKolabIMAPXStore *self,
                                         const gchar *foldername,
                                         KolabFolderTypeID foldertype,
                                         GCancellable *cancellable,
                                         GError **err);

/* online-query the folder info (no update of persistent DBs) */
CamelFolderInfo*
camel_kolab_imapx_store_get_folder_info_online (CamelKolabIMAPXStore *self,
                                                const gchar *top,
                                                CamelStoreGetFolderInfoFlags flags,
                                                GCancellable *cancellable,
                                                GError **err);
/* rip out list of foldernames */
GList*
camel_kolab_imapx_store_resect_folder_list (CamelKolabIMAPXStore *self);

/* metadata: whether all folders are shown or non-PIM-folders are hidden */
gboolean
camel_kolab_imapx_store_get_show_all_folders (CamelKolabIMAPXStore *self);

/* metadata: whether or not to unhide PIM-folders in mail context */
gboolean
camel_kolab_imapx_store_set_show_all_folders (CamelKolabIMAPXStore *self,
                                              gboolean show_all,
                                              GCancellable *cancellable,
                                              GError **err);

/* acl: get the permissions for a folder */
GList*
camel_kolab_imapx_store_get_folder_permissions (CamelKolabIMAPXStore *self,
                                                const gchar *foldername,
                                                gboolean myrights,
                                                GCancellable *cancellable,
                                                GError **err);

/* acl: set (update) the permissions for a folder */
gboolean
camel_kolab_imapx_store_set_folder_permissions (CamelKolabIMAPXStore *self,
                                                const gchar *foldername,
                                                const GList *permissions,
                                                GCancellable *cancellable,
                                                GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_KOLAB_IMAPX_STORE_H_ */

/*----------------------------------------------------------------------------*/
