/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-imapx-extd-server-acl.h
 *
 *  2012-07-30, 17:13:28
 *  Copyright 2012, Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_IMAPX_EXTD_SERVER_ACL_H_
#define _CAMEL_IMAPX_EXTD_SERVER_ACL_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>

#include <libekolabutil/kolab-util-glib.h>

/*----------------------------------------------------------------------------*/

KolabGConstList*
camel_imapx_extd_server_acl_get_handler_descriptors (void);

gboolean
camel_imapx_extd_server_get_myrights (CamelIMAPXServer *self,
                                      const gchar *foldername,
                                      GCancellable *cancellable,
                                      GError **err);

gboolean
camel_imapx_extd_server_get_acl (CamelIMAPXServer *self,
                                 const gchar *foldername,
                                 GCancellable *cancellable,
                                 GError **err);

gboolean
camel_imapx_extd_server_set_acl (CamelIMAPXServer *self,
                                 const gchar *foldername,
                                 const GList *entries,
                                 GCancellable *cancellable,
                                 GError **err);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* CAMEL_IMAPX_EXTD_SERVER_ACL_H_ */

/*----------------------------------------------------------------------------*/
