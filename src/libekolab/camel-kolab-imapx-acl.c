/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-acl.c
 *
 *  Tue Oct 02 17:17:03 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include "camel-kolab-imapx-acl.h"

/*----------------------------------------------------------------------------*/

CamelKolabImapxAcl*
camel_kolab_imapx_acl_new (gboolean locked)
{
	CamelKolabImapxAcl *kacl = g_new0 (CamelKolabImapxAcl, 1);
	kacl->acl = camel_imapx_acl_new (locked);
	return kacl;
}

void
camel_kolab_imapx_acl_free (CamelKolabImapxAcl *kacl)
{
	if (kacl == NULL)
		return;

	camel_imapx_acl_free (kacl->acl);

	g_free (kacl);
}

gchar*
camel_kolab_imapx_acl_get_myrights (CamelKolabImapxAcl *kacl,
                                    const gchar *foldername)
{
	gchar *rights = NULL;

	g_return_val_if_fail (kacl != NULL, NULL);
	g_return_val_if_fail (foldername != NULL, NULL);

	rights = camel_imapx_acl_get_myrights (kacl->acl,
	                                       foldername);
	return rights;
}

gboolean
camel_kolab_imapx_acl_update_myrights (CamelKolabImapxAcl *kacl,
                                       const gchar *foldername,
                                       const gchar *rights,
                                       GError **err)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (kacl != NULL, FALSE);
	g_return_val_if_fail (foldername != NULL, FALSE);
	/* rights may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	ok = camel_imapx_acl_update_myrights (kacl->acl,
	                                      foldername,
	                                      rights,
	                                      err);
	return ok;
}

gboolean
camel_kolab_imapx_acl_update_from_acl (CamelKolabImapxAcl *kacl,
                                       CamelImapxAcl *src_acl,
                                       GError **err)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (kacl != NULL, FALSE);
	/* src_kacl may be NULL */
	/* entries may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	if (src_acl == NULL)
		return TRUE;

	ok = camel_imapx_acl_update_from_acl (kacl->acl,
	                                      src_acl,
	                                      err);
	return ok;
}

gboolean
camel_kolab_imapx_acl_update_from_list (CamelKolabImapxAcl *kacl,
                                        const gchar *foldername,
                                        const GList *entries,
                                        GError **err)
{
	gboolean ok = FALSE;

	g_return_val_if_fail (kacl != NULL, FALSE);
	g_return_val_if_fail (foldername != NULL, FALSE);
	/* entries may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	if (entries == NULL)
		return TRUE;

	ok = camel_imapx_acl_update_from_list (kacl->acl,
	                                       foldername,
	                                       entries,
	                                       err);
	return ok;
}

GList*
camel_kolab_imapx_acl_get_as_list (CamelKolabImapxAcl *kacl,
                                   const gchar *foldername)
{
	GList *entries = NULL;

	g_return_val_if_fail (kacl != NULL, NULL);
	g_return_val_if_fail (foldername != NULL, NULL);

	entries = camel_imapx_acl_get_as_list (kacl->acl,
	                                       foldername);

	return entries;
}

GList*
camel_kolab_imapx_acl_list_clone (GList *entries,
                                  GError **err)
{
	GList *cloned = NULL;

	g_return_val_if_fail (entries != NULL, NULL);
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	cloned = camel_imapx_acl_list_clone (entries,
	                                     err);
	return cloned;
}

void
camel_kolab_imapx_acl_list_free (GList *entries)
{
	if (entries == NULL)
		return;
	camel_imapx_acl_list_free (entries);
}

/*----------------------------------------------------------------------------*/
