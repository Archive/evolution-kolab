/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-acl.h
 *
 *  Tue Oct 02 17:17:03 2012
 *  Copyright  2012  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/
/* IMAP ACL (RFC 4314) (Kolab specifics) */

#ifndef _CAMEL_KOLAB_IMAPX_ACL_H_
#define _CAMEL_KOLAB_IMAPX_ACL_H_

/*----------------------------------------------------------------------------*/
/* This is just a thin layer on top of camel-imapx-acl.[hc]. Since we're not
 * yet persistent with ACL (just online for now), this layer is not strictly
 * needed. We still implement it for the following reasons:
 *
 * - keep symmetry with the metadata implementation
 * - provide the proper place to add persistence
 *   (i.e., storing of ACL in an SQLiteDB, as
 *   metadata does), once we need that
 *
 * We do not implement persistent storing of ACL in camel-imapx-acl.[hc]
 * for the very same reason we do not persistently store metadata in
 * camel-imapx-metadata.[hc] : these files keep the implementation which
 * is not Kolab-specific, and these implementations may be merged into
 * upstream IMAPX one day. Since we do not know whether or not persistence
 * will be needed in upstream (that is, by any other code using IMAPX),
 * there is no reason to clutter the protocol implementation with SQLite
 * stuff. Instead, we do it here, if needed.
 *
 * Once Evolution-Data-Server becomes more offline-aware (again), we will need
 * ACL to be stored persistently so we can prevent a user from storing PIM
 * data into folders which we know she has no folder permissions for (otherwise
 * we run into the situation of having new/changed PIM objects in our offline
 * cache which we cannot write to the server due to missing permissions,
 * and we do not have a good facility to clear our cache (that is, deleting
 * a PIM folder only locally without deleting it from the server as well)).
 */
/*----------------------------------------------------------------------------*/

#include <glib.h>

#include "camel-imapx-acl.h"

/*----------------------------------------------------------------------------*/

typedef struct _CamelKolabImapxAcl CamelKolabImapxAcl;
struct _CamelKolabImapxAcl {
	/* SQliteDB here, once we need it */
	CamelImapxAcl *acl;
};

/*----------------------------------------------------------------------------*/

CamelKolabImapxAcl*
camel_kolab_imapx_acl_new (gboolean locked);

void
camel_kolab_imapx_acl_free (CamelKolabImapxAcl *kacl);

gchar*
camel_kolab_imapx_acl_get_myrights (CamelKolabImapxAcl *kacl,
                                    const gchar *foldername);

gboolean
camel_kolab_imapx_acl_update_myrights (CamelKolabImapxAcl *kacl,
                                       const gchar *foldername,
                                       const gchar *rights,
                                       GError **err);
gboolean
camel_kolab_imapx_acl_update_from_acl (CamelKolabImapxAcl *kacl,
                                       CamelImapxAcl *src_acl,
                                       GError **err);

gboolean
camel_kolab_imapx_acl_update_from_list (CamelKolabImapxAcl *kacl,
                                        const gchar *foldername,
                                        const GList *entries,
                                        GError **err);

GList*
camel_kolab_imapx_acl_get_as_list (CamelKolabImapxAcl *kacl,
                                   const gchar *foldername);

GList*
camel_kolab_imapx_acl_list_clone (GList *entries,
                                  GError **err);

void
camel_kolab_imapx_acl_list_free (GList *entries);

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_KOLAB_IMAPX_ACL_H_ */

/*----------------------------------------------------------------------------*/
