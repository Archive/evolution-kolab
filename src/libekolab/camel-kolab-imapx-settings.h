/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-settings.h
 *
 *  2011-12-02, 19:11:11
 *  Copyright 2011, Christian Hilberg
 *  <hilberg@unix-ag.org>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_KOLAB_IMAPX_SETTINGS_H_
#define _CAMEL_KOLAB_IMAPX_SETTINGS_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <glib-object.h>

#include <libekolab/kolab-types.h>
#include <libekolabutil/camel-system-headers.h>

/*----------------------------------------------------------------------------*/
/* Standard GObject macros */

#define CAMEL_TYPE_KOLAB_IMAPX_SETTINGS	  \
	(camel_kolab_imapx_settings_get_type ())
#define CAMEL_KOLAB_IMAPX_SETTINGS(obj)	  \
	(G_TYPE_CHECK_INSTANCE_CAST \
	 ((obj), CAMEL_TYPE_KOLAB_IMAPX_SETTINGS, CamelKolabIMAPXSettings))
#define CAMEL_KOLAB_IMAPX_SETTINGS_CLASS(klass)	  \
	(G_TYPE_CHECK_CLASS_CAST \
	 ((klass), CAMEL_TYPE_KOLAB_IMAPX_SETTINGS, CamelKolabIMAPXSettingsClass))
#define CAMEL_IS_KOLAB_IMAPX_SETTINGS(obj)	  \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	 ((obj), CAMEL_TYPE_KOLAB_IMAPX_SETTINGS))
#define CAMEL_IS_KOLAB_IMAPX_SETTINGS_CLASS(klass)	  \
	(G_TYPE_CHECK_CLASS_TYPE \
	 ((klass), CAMEL_TYPE_KOLAB_IMAPX_SETTINGS))
#define CAMEL_KOLAB_IMAPX_SETTINGS_GET_CLASS(obj)	  \
	(G_TYPE_INSTANCE_GET_CLASS \
	 ((obj), CAMEL_TYPE_KOLAB_IMAPX_SETTINGS, CamelKolabIMAPXSettingsClass))

G_BEGIN_DECLS

typedef struct _CamelKolabIMAPXSettings CamelKolabIMAPXSettings;
typedef struct _CamelKolabIMAPXSettingsClass CamelKolabIMAPXSettingsClass;
typedef struct _CamelKolabIMAPXSettingsPrivate CamelKolabIMAPXSettingsPrivate;

struct _CamelKolabIMAPXSettings {
	CamelIMAPXSettings parent;
	CamelKolabIMAPXSettingsPrivate *priv;
};

struct _CamelKolabIMAPXSettingsClass {
	CamelIMAPXSettingsClass parent_class;
};

GType
camel_kolab_imapx_settings_get_type (void);

CamelURL *
camel_kolab_imapx_settings_build_url (CamelKolabIMAPXSettings *settings);

const gchar *
camel_kolab_imapx_settings_get_pkcs11_pin (CamelKolabIMAPXSettings *settings);
gchar *
camel_kolab_imapx_settings_dup_pkcs11_pin (CamelKolabIMAPXSettings *settings);
void
camel_kolab_imapx_settings_set_pkcs11_pin (CamelKolabIMAPXSettings *settings,
                                           const gchar *pkcs11_pin);

G_END_DECLS

/*----------------------------------------------------------------------------*/

#endif /* CAMEL_KOLAB_IMAPX_SETTINGS_H_ */

/*----------------------------------------------------------------------------*/
