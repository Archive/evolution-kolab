/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include "e-kolab-backend.h"

#include <config.h>
#include <glib/gi18n-lib.h>

#include <libekolab/e-source-kolab-folder.h>
#include <libekolab/camel-kolab-imapx-settings.h>

#include <libekolabutil/kolab-util-camel.h>
#include <libekolabutil/kolab-util.h>

#define E_KOLAB_BACKEND_GET_PRIVATE(obj) \
	(G_TYPE_INSTANCE_GET_PRIVATE \
	((obj), E_TYPE_KOLAB_BACKEND, EKolabBackendPrivate))

/* This forces the GType to be registered in a way that
 * avoids a "statement with no effect" compiler warning.
 * FIXME Use g_type_ensure() once we require GLib 2.34. */
#define REGISTER_TYPE(type) \
	(g_type_class_unref (g_type_class_ref (type)))

struct _EKolabBackendPrivate {
	KolabMailAccess *koma;
	GMutex koma_lock;
};

G_DEFINE_DYNAMIC_TYPE (
	EKolabBackend,
	e_kolab_backend,
	E_TYPE_COLLECTION_BACKEND)

static CamelKolabIMAPXSettings *
kolab_backend_get_settings (EKolabBackend *backend)
{
	ESource *source;
	ESourceCamel *extension;
	CamelSettings *settings;
	const gchar *extension_name;
	const gchar *protocol;

	protocol = KOLAB_CAMEL_PROVIDER_PROTOCOL;
	source = e_backend_get_source (E_BACKEND (backend));
	extension_name = e_source_camel_get_extension_name (protocol);
	extension = e_source_get_extension (source, extension_name);
	settings = e_source_camel_get_settings (extension);

	return CAMEL_KOLAB_IMAPX_SETTINGS (settings);
}

static void
kolab_backend_sync_folders_done_cb (GObject *source_object,
                                    GAsyncResult *result,
                                    gpointer user_data)
{
	EKolabBackend *backend;
	GError *error = NULL;

	backend = E_KOLAB_BACKEND (source_object);

	e_kolab_backend_sync_folders_finish (backend, result, &error);

	if (error != NULL) {
		g_critical ("%s: %s", G_STRFUNC, error->message);
		g_error_free (error);
	}
}

static void
kolab_backend_dispose (GObject *object)
{
	EKolabBackendPrivate *priv;

	priv = E_KOLAB_BACKEND_GET_PRIVATE (object);

	if (priv->koma != NULL) {
		g_object_unref (priv->koma);
		priv->koma = NULL;
	}

	/* Chain up to parent's dispose() method. */
	G_OBJECT_CLASS (e_kolab_backend_parent_class)->dispose (object);
}

static void
kolab_backend_finalize (GObject *object)
{
	EKolabBackendPrivate *priv;

	priv = E_KOLAB_BACKEND_GET_PRIVATE (object);

	g_mutex_clear (&priv->koma_lock);

	/* Chain up to parent's finalize() method. */
	G_OBJECT_CLASS (e_kolab_backend_parent_class)->finalize (object);
}

static void
kolab_backend_constructed (GObject *object)
{
	ESource *source;

	/* Chain up to parent's constructed() method. */
	G_OBJECT_CLASS (e_kolab_backend_parent_class)->constructed (object);

	source = e_backend_get_source (E_BACKEND (object));

	/* XXX Wondering if we ought to delay this until after folders
	 *     are initially populated, just to remove the possibility
	 *     of weird races with clients trying to create folders. */
	e_server_side_source_set_remote_creatable (
		E_SERVER_SIDE_SOURCE (source), TRUE);
}

static void
kolab_backend_populate (ECollectionBackend *backend)
{
	e_kolab_backend_sync_folders (
		E_KOLAB_BACKEND (backend), NULL,
		kolab_backend_sync_folders_done_cb, NULL);
}

static gchar *
kolab_backend_dup_resource_id (ECollectionBackend *backend,
                               ESource *child_source)
{
	ESourceResource *extension;
	const gchar *extension_name;

	extension_name = E_SOURCE_EXTENSION_KOLAB_FOLDER;
	extension = e_source_get_extension (child_source, extension_name);

	return e_source_resource_dup_identity (extension);
}

static void
kolab_backend_child_added (ECollectionBackend *backend,
                           ESource *child_source)
{
	/* Chain up to parent's child_added() method. */
	E_COLLECTION_BACKEND_CLASS (e_kolab_backend_parent_class)->
		child_added (backend, child_source);
}

static void
kolab_backend_child_removed (ECollectionBackend *backend,
                             ESource *child_source)
{
	/* Chain up to parent's child_removed() method. */
	E_COLLECTION_BACKEND_CLASS (e_kolab_backend_parent_class)->
		child_removed (backend, child_source);
}

static gboolean
kolab_backend_create_resource_sync (ECollectionBackend *backend,
                                    ESource *source,
                                    GCancellable *cancellable,
                                    GError **error)
{
	KolabMailAccess *koma;
	KolabFolderTypeID folder_type = KOLAB_FOLDER_TYPE_INVAL;
	const gchar *extension_name;
	gchar *display_name;
	gchar *folder_path;
	gboolean success = FALSE;

	koma = e_kolab_backend_ref_mail_access_sync (
		E_KOLAB_BACKEND (backend), cancellable, error);
	if (koma == NULL)
		goto exit;

	extension_name = E_SOURCE_EXTENSION_ADDRESS_BOOK;
	if (e_source_has_extension (source, extension_name))
		folder_type = KOLAB_FOLDER_TYPE_CONTACT;

	extension_name = E_SOURCE_EXTENSION_CALENDAR;
	if (e_source_has_extension (source, extension_name))
		folder_type = KOLAB_FOLDER_TYPE_EVENT;

	extension_name = E_SOURCE_EXTENSION_MEMO_LIST;
	if (e_source_has_extension (source, extension_name))
		folder_type = KOLAB_FOLDER_TYPE_NOTE;

	extension_name = E_SOURCE_EXTENSION_TASK_LIST;
	if (e_source_has_extension (source, extension_name))
		folder_type = KOLAB_FOLDER_TYPE_TASK;

	if (folder_type == KOLAB_FOLDER_TYPE_INVAL) {
		g_set_error (
			error, G_IO_ERROR,
			G_IO_ERROR_INVALID_ARGUMENT,
			_("Could not determine a suitable folder "
			"type for a new folder named '%s'"),
			e_source_get_display_name (source));
		goto exit;
	}

	/* XXX Do we need to worry about path separator
	 *     characters in the display name here? */
	display_name = e_source_dup_display_name (source);
	folder_path = g_strjoin (
		KOLAB_PATH_SEPARATOR_S,
		"INBOX", display_name, NULL);
	g_free (display_name);

	success = kolab_mail_access_create_source (
		koma, folder_path, folder_type, cancellable, error);

	if (success) {
		ESourceRegistryServer *server;
		ESourceResource *extension;
		ESource *parent_source;
		const gchar *cache_dir;
		const gchar *parent_uid;

		/* Configure the source as a collection member. */
		parent_source = e_backend_get_source (E_BACKEND (backend));
		parent_uid = e_source_get_uid (parent_source);
		e_source_set_parent (source, parent_uid);

		/* The resource ID is the folder path.  It's not a stable
		 * identifier (renaming the folder will invalidate the ID)
		 * but it's currently the only way to refer to the folder. */
		extension_name = E_SOURCE_EXTENSION_KOLAB_FOLDER;
		extension = e_source_get_extension (source, extension_name);
		e_source_resource_set_identity (extension, folder_path);

		/* Changes should be written back to the cache directory. */
		cache_dir = e_collection_backend_get_cache_dir (backend);
		e_server_side_source_set_write_directory (
			E_SERVER_SIDE_SOURCE (source), cache_dir);

		/* Set permissions for clients. */
		e_server_side_source_set_writable (
			E_SERVER_SIDE_SOURCE (source), TRUE);
		e_server_side_source_set_remote_deletable (
			E_SERVER_SIDE_SOURCE (source), TRUE);

		server = e_collection_backend_ref_server (backend);
		e_source_registry_server_add_source (server, source);
		g_object_unref (server);
	}

	g_free (folder_path);

exit:
	if (koma != NULL)
		g_object_unref (koma);

	return success;
}

static gboolean
kolab_backend_delete_resource_sync (ECollectionBackend *backend,
                                    ESource *source,
                                    GCancellable *cancellable,
                                    GError **error)
{
	KolabMailAccess *koma = NULL;
	ESourceRegistryServer *server = NULL;
	ESourceResource *extension = NULL;
	const gchar *extension_name = NULL;
	gchar *folder_path = NULL;
	gboolean success = FALSE;

	koma = e_kolab_backend_ref_mail_access_sync (
		E_KOLAB_BACKEND (backend), cancellable, error);
	if (koma == NULL)
		goto exit;

	extension_name = E_SOURCE_EXTENSION_KOLAB_FOLDER;
	if (!e_source_has_extension (source, extension_name)) {
		g_set_error (
			error, G_IO_ERROR,
			G_IO_ERROR_INVALID_ARGUMENT,
			_("Data source '%s' does not "
			"represent a Kolab folder"),
			e_source_get_display_name (source));
		goto exit;
	}
	extension = e_source_get_extension (source, extension_name);

	/* The folder path is the resource ID.  It's not a stable
	 * identifier (renaming the folder will invalidate the ID)
	 * but it's currently the only way to refer to the folder. */
	folder_path = e_source_resource_dup_identity (extension);

	success = kolab_mail_access_delete_source (
		koma, folder_path, cancellable, error);

	g_free (folder_path);

	if (success)
		success = e_source_remove_sync (source, cancellable, error);

 exit:
	if (koma != NULL)
		g_object_unref (koma);

	return success;
}

static void
e_kolab_backend_class_init (EKolabBackendClass *class)
{
	GObjectClass *object_class;
	ECollectionBackendClass *backend_class;

	g_type_class_add_private (class, sizeof (EKolabBackendPrivate));

	object_class = G_OBJECT_CLASS (class);
	object_class->dispose = kolab_backend_dispose;
	object_class->finalize = kolab_backend_finalize;
	object_class->constructed = kolab_backend_constructed;

	backend_class = E_COLLECTION_BACKEND_CLASS (class);
	backend_class->populate = kolab_backend_populate;
	backend_class->dup_resource_id = kolab_backend_dup_resource_id;
	backend_class->child_added = kolab_backend_child_added;
	backend_class->child_removed = kolab_backend_child_removed;
	backend_class->create_resource_sync = kolab_backend_create_resource_sync;
	backend_class->delete_resource_sync = kolab_backend_delete_resource_sync;

	/* Register relevant ESourceExtension types. */
	REGISTER_TYPE (E_TYPE_SOURCE_KOLAB_FOLDER);

	/* This generates an ESourceCamel subtype for CamelKolabIMAPXSettings. */
	e_source_camel_generate_subtype ("kolab", CAMEL_TYPE_KOLAB_IMAPX_SETTINGS);
}

static void
e_kolab_backend_class_finalize (EKolabBackendClass *class)
{
}

static void
e_kolab_backend_init (EKolabBackend *backend)
{
	GError *error = NULL;

	backend->priv = E_KOLAB_BACKEND_GET_PRIVATE (backend);

	g_mutex_init (&backend->priv->koma_lock);

	/* init subsystems (these are no-ops if already called before) */
	kolab_util_glib_init ();
	/* Initialize Camel and NSS.  If we fail here, there's not
	 * much else to do but abort the whole service immediately. */
	if (!kolab_util_camel_init (&error)) {
		g_error ("%s: %s", G_STRFUNC, error->message);
		g_assert_not_reached ();
	}
}

void
e_kolab_backend_type_register (GTypeModule *type_module)
{
	/* XXX G_DEFINE_DYNAMIC_TYPE declares a static type registration
	 *     function, so we have to wrap it with a public function in
	 *     order to register types from a separate compilation unit. */
	e_kolab_backend_register_type (type_module);
}

ESource *
e_kolab_backend_new_child (EKolabBackend *backend,
                           KolabFolderDescriptor *desc)
{
	ESource *source;
	ESourceBackend *backend_extension;
	ESourceResource *resource_extension;
	const gchar *display_name;
	const gchar *extension_name;

	g_return_val_if_fail (E_IS_KOLAB_BACKEND (backend), NULL);
	g_return_val_if_fail (desc != NULL, NULL);

	/* Return NULL for folder types we don't support, and map
	 * the folder type to an equivalent ESourceExtension name
	 * while we're at it. */
	switch (desc->type_id) {
		case KOLAB_FOLDER_TYPE_EVENT:
		case KOLAB_FOLDER_TYPE_EVENT_DEFAULT:
			extension_name = E_SOURCE_EXTENSION_CALENDAR;
			break;
		case KOLAB_FOLDER_TYPE_TASK:
		case KOLAB_FOLDER_TYPE_TASK_DEFAULT:
			extension_name = E_SOURCE_EXTENSION_TASK_LIST;
			break;
		case KOLAB_FOLDER_TYPE_NOTE:
		case KOLAB_FOLDER_TYPE_NOTE_DEFAULT:
			extension_name = E_SOURCE_EXTENSION_MEMO_LIST;
			break;
		case KOLAB_FOLDER_TYPE_CONTACT:
		case KOLAB_FOLDER_TYPE_CONTACT_DEFAULT:
			extension_name = E_SOURCE_EXTENSION_ADDRESS_BOOK;
			break;
		default:
			return NULL;
	}

	source = e_collection_backend_new_child (
		E_COLLECTION_BACKEND (backend), desc->name);

	/* Use the extension name set in the switch statement above. */
	backend_extension = e_source_get_extension (source, extension_name);
	e_source_backend_set_backend_name (backend_extension, "kolab");

	/* XXX The resource identity is the folder path.  This is not a
	 *     stable identifier -- a folder rename will invalidate the
	 *     cache on the next start -- but it's all we're given. */
	extension_name = E_SOURCE_EXTENSION_KOLAB_FOLDER;
	resource_extension = e_source_get_extension (source, extension_name);
	e_source_resource_set_identity (resource_extension, desc->name);

	/* The hierarchy is flattened for non-mail folders,
	 * so the last path segment is the display name. */
	if (desc->name != NULL) {
		gchar **segments;
		guint n_segments;

		segments = g_strsplit (desc->name, KOLAB_PATH_SEPARATOR_S, -1);
		n_segments = g_strv_length (segments);

		if (n_segments > 0) {
			const gchar *display_name;
			display_name = segments[n_segments - 1];
			e_source_set_display_name (source, display_name);
		}

		g_strfreev (segments);
	}

	return source;
}

KolabMailAccess *
e_kolab_backend_ref_mail_access_sync (EKolabBackend *backend,
                                      GCancellable *cancellable,
                                      GError **error)
{
	KolabMailAccess *koma = NULL;
	CamelKolabIMAPXSettings *settings;
	KolabSettingsHandler *settings_handler;
	ESource *source;
	gboolean success;

	g_return_val_if_fail (E_IS_KOLAB_BACKEND (backend), NULL);

	g_mutex_lock (&backend->priv->koma_lock);
	if (backend->priv->koma != NULL)
		koma = g_object_ref (backend->priv->koma);
	g_mutex_unlock (&backend->priv->koma_lock);

	/* Return a stashed KolabMailAccess if we have one. */
	if (koma != NULL)
		return koma;

	/* FIXME Creating a KolabMailAccess instance is a somewhat
	 *       involved process which is repeated in the calendar
	 *       and address book backends.  Should be centralized
	 *       in libekolab and provided as a one-step function.
	 *       The configure steps at least should be done during
	 *       initialization using constructor properties. */

	settings = kolab_backend_get_settings (backend);
	source = e_backend_get_source (E_BACKEND (backend));

	settings_handler = kolab_settings_handler_new (settings,
	                                               E_BACKEND (backend));

	/* XXX Christian recommends KOLAB_FOLDER_CONTEXT_EMAIL here
	 *     although the operations we need are supposed to work
	 *     for any folder context. */
	success = kolab_settings_handler_configure (
		settings_handler, KOLAB_FOLDER_CONTEXT_EMAIL, error);
	if (!success) {
		g_object_unref (settings_handler);
		return NULL;
	}

	if (!kolab_settings_handler_bringup (settings_handler, error)) {
		g_object_unref (settings_handler);
		return NULL;
	}

	koma = g_object_new (KOLAB_TYPE_MAIL_ACCESS, NULL);

	success = kolab_mail_access_configure (
		koma, settings_handler, error);
	if (!success) {
		g_object_unref (settings_handler);
		g_object_unref (koma);
		return NULL;
	}

	if (!kolab_mail_access_bringup (koma, cancellable, error)) {
		g_object_unref (settings_handler);
		g_object_unref (koma);
		return NULL;
	}

	success = kolab_mail_access_set_opmode (
		koma, KOLAB_MAIL_ACCESS_OPMODE_ONLINE, cancellable, error);
	if (!success) {
		g_object_unref (settings_handler);
		g_object_unref (koma);
		return NULL;
	}

	/* Another thread may have created a KolabMailAccess since
	 * our previous check above.  If so, replace it with ours. */
	g_mutex_lock (&backend->priv->koma_lock);
	if (backend->priv->koma != NULL)
		g_object_unref (backend->priv->koma);
	backend->priv->koma = g_object_ref (koma);
	g_mutex_unlock (&backend->priv->koma_lock);

	g_object_unref (settings_handler);

	return koma;
}

/* Helper for e_kolab_backend_ref_mail_access() */
static void
kolab_backend_ref_mail_access_thread (GSimpleAsyncResult *simple,
                                      GObject *object,
                                      GCancellable *cancellable)
{
	KolabMailAccess *koma;
	GError *error = NULL;

	koma = e_kolab_backend_ref_mail_access_sync (
		E_KOLAB_BACKEND (object), cancellable, &error);

	if (koma != NULL)
		g_simple_async_result_set_op_res_gpointer (
			simple, koma, (GDestroyNotify) g_object_unref);

	if (error != NULL)
		g_simple_async_result_take_error (simple, error);
}

void
e_kolab_backend_ref_mail_access (EKolabBackend *backend,
                                 GCancellable *cancellable,
                                 GAsyncReadyCallback callback,
                                 gpointer user_data)
{
	GSimpleAsyncResult *simple;

	g_return_if_fail (E_IS_KOLAB_BACKEND (backend));

	simple = g_simple_async_result_new (
		G_OBJECT (backend), callback, user_data,
		e_kolab_backend_ref_mail_access);

	g_simple_async_result_set_check_cancellable (simple, cancellable);

	g_simple_async_result_run_in_thread (
		simple, kolab_backend_ref_mail_access_thread,
		G_PRIORITY_DEFAULT, cancellable);

	g_object_unref (simple);
}

KolabMailAccess *
e_kolab_backend_ref_mail_access_finish (EKolabBackend *backend,
                                        GAsyncResult *result,
                                        GError **error)
{
	GSimpleAsyncResult *simple;
	KolabMailAccess *koma;

	g_return_val_if_fail (
		g_simple_async_result_is_valid (
		result, G_OBJECT (backend),
		e_kolab_backend_ref_mail_access), NULL);

	simple = G_SIMPLE_ASYNC_RESULT (result);

	if (g_simple_async_result_propagate_error (simple, error))
		return NULL;

	koma = g_simple_async_result_get_op_res_gpointer (simple);
	g_return_val_if_fail (KOLAB_IS_MAIL_ACCESS (koma), NULL);

	return g_object_ref (koma);
}

gboolean
e_kolab_backend_sync_folders_sync (EKolabBackend *backend,
                                   GCancellable *cancellable,
                                   GError **error)
{
	ECollectionBackend *col_backend;
	ESourceRegistryServer *server;
	KolabMailAccess *koma;
	GList *list, *link;
	GQueue trash = G_QUEUE_INIT;
	GError *local_error = NULL;

	g_return_val_if_fail (E_IS_KOLAB_BACKEND (backend), FALSE);

	col_backend = E_COLLECTION_BACKEND (backend);

	/* If we're offline, all we can do is add the last known set
	 * of folders from our cache of previously used ESources. */
	if (!e_backend_get_online (E_BACKEND (backend))) {
		server = e_collection_backend_ref_server (col_backend);
		list = e_collection_backend_claim_all_resources (col_backend);

		for (link = list; link != NULL; link = g_list_next (link))
			/* FIXME should we set the source deletable here
			 * itself, though trying to remove a source while
			 * in offline mode will fail?
			 */
			e_source_registry_server_add_source (
				server, E_SOURCE (link->data));

		g_list_free_full (list, (GDestroyNotify) g_object_unref);
		g_object_unref (server);

		return TRUE;
	}

	koma = e_kolab_backend_ref_mail_access_sync (
		backend, cancellable, error);

	if (koma == NULL)
		return FALSE;

	list = kolab_mail_access_query_folder_info_online (
		koma, cancellable, &local_error);

	g_object_unref (koma);

	if (local_error != NULL) {
		g_propagate_error (error, local_error);
		return FALSE;
	}

	/* Create or reuse ESources to represent Kolab folders. */

	server = e_collection_backend_ref_server (col_backend);

	for (link = list; link != NULL; link = g_list_next (link)) {
		KolabFolderDescriptor *desc = link->data;
		ESource *source;

		source = e_kolab_backend_new_child (backend, desc);

		if (source != NULL) {
			e_server_side_source_set_remote_deletable (E_SERVER_SIDE_SOURCE (source), TRUE);
			e_source_registry_server_add_source (server, source);
			g_object_unref (source);
		}
	}

	g_object_unref (server);

	g_list_free_full (
		list, (GDestroyNotify) kolab_util_folder_descriptor_free);

	/* Discard any previously cached folders no longer
	 * present in the folder list from the Kolab server.
	 *
	 * Note: This is just an attempt to cleanup cached
	 *       data, so we don't really care about errors.
	 */

	list = e_collection_backend_claim_all_resources (col_backend);

	for (link = list; link != NULL; link = g_list_next (link)) {
		ESource *source = E_SOURCE (link->data);
		e_source_remove_sync (source, NULL, NULL);
	}

	g_list_free_full (list, (GDestroyNotify) g_object_unref);

	return TRUE;
}

/* Helper for e_kolab_backend_sync_folders() */
static void
kolab_backend_sync_folders_thread (GSimpleAsyncResult *simple,
                                   GObject *object,
                                   GCancellable *cancellable)
{
	GError *error = NULL;

	e_kolab_backend_sync_folders_sync (
		E_KOLAB_BACKEND (object), cancellable, &error);

	if (error != NULL)
		g_simple_async_result_take_error (simple, error);
}

void
e_kolab_backend_sync_folders (EKolabBackend *backend,
                              GCancellable *cancellable,
                              GAsyncReadyCallback callback,
                              gpointer user_data)
{
	GSimpleAsyncResult *simple;

	g_return_if_fail (E_IS_KOLAB_BACKEND (backend));

	simple = g_simple_async_result_new (
		G_OBJECT (backend), callback, user_data,
		e_kolab_backend_sync_folders);

	g_simple_async_result_set_check_cancellable (simple, cancellable);

	g_simple_async_result_run_in_thread (
		simple, kolab_backend_sync_folders_thread,
		G_PRIORITY_DEFAULT, cancellable);

	g_object_unref (simple);
}

gboolean
e_kolab_backend_sync_folders_finish (EKolabBackend *backend,
                                     GAsyncResult *result,
                                     GError **error)
{
	GSimpleAsyncResult *simple;

	g_return_val_if_fail (
		g_simple_async_result_is_valid (
		result, G_OBJECT (backend),
		e_kolab_backend_sync_folders), FALSE);

	simple = G_SIMPLE_ASYNC_RESULT (result);

	/* Assume success unless a GError was set. */
	return !g_simple_async_result_propagate_error (simple, error);
}

