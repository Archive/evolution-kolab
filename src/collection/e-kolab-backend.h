/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef E_KOLAB_BACKEND_H
#define E_KOLAB_BACKEND_H

#include <libebackend/libebackend.h>

#include "libekolab/kolab-mail-access.h"

/* Standard GObject macros */
#define E_TYPE_KOLAB_BACKEND \
	(e_kolab_backend_get_type ())
#define E_KOLAB_BACKEND(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), E_TYPE_KOLAB_BACKEND, EKolabBackend))
#define E_KOLAB_BACKEND_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), E_TYPE_KOLAB_BACKEND, EKolabBackendClass))
#define E_IS_KOLAB_BACKEND(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), E_TYPE_KOLAB_BACKEND))
#define E_IS_KOLAB_BACKEND_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), E_TYPE_KOLAB_BACKEND))
#define E_KOLAB_BACKEND_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), E_TYPE_KOLAB_BACKEND, EKolabBackendClass))

G_BEGIN_DECLS

typedef struct _EKolabBackend EKolabBackend;
typedef struct _EKolabBackendClass EKolabBackendClass;
typedef struct _EKolabBackendPrivate EKolabBackendPrivate;

struct _EKolabBackend {
	ECollectionBackend parent;
	EKolabBackendPrivate *priv;
};

struct _EKolabBackendClass {
	ECollectionBackendClass parent_class;
};

GType		e_kolab_backend_get_type	(void) G_GNUC_CONST;
void		e_kolab_backend_type_register	(GTypeModule *type_module);
ESource *	e_kolab_backend_new_child	(EKolabBackend *backend,
						 KolabFolderDescriptor *desc);
KolabMailAccess *
		e_kolab_backend_ref_mail_access_sync
						(EKolabBackend *backend,
						 GCancellable *cancellable,
						 GError **error);
void		e_kolab_backend_ref_mail_access	(EKolabBackend *backend,
						 GCancellable *cancellable,
						 GAsyncReadyCallback callback,
						 gpointer user_data);
KolabMailAccess *
		e_kolab_backend_ref_mail_access_finish
						(EKolabBackend *backend,
						 GAsyncResult *result,
						 GError **error);
gboolean	e_kolab_backend_sync_folders_sync
						(EKolabBackend *backend,
						 GCancellable *cancellable,
						 GError **error);
void		e_kolab_backend_sync_folders	(EKolabBackend *backend,
						 GCancellable *cancellable,
						 GAsyncReadyCallback callback,
						 gpointer user_data);
gboolean	e_kolab_backend_sync_folders_finish
						(EKolabBackend *backend,
						 GAsyncResult *result,
						 GError **error);

G_END_DECLS

#endif /* E_KOLAB_BACKEND_H */

