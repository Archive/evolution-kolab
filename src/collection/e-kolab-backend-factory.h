/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef E_KOLAB_BACKEND_FACTORY_H
#define E_KOLAB_BACKEND_FACTORY_H

#include <libebackend/libebackend.h>

/* Standard GObject macros */
#define E_TYPE_KOLAB_BACKEND_FACTORY \
	(e_kolab_backend_get_type ())
#define E_KOLAB_BACKEND_FACTORY(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), E_TYPE_KOLAB_BACKEND_FACTORY, EKolabBackendFactory))
#define E_KOLAB_BACKEND_FACTORY_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), E_TYPE_KOLAB_BACKEND_FACTORY, EKolabBackendFactoryClass))
#define E_IS_KOLAB_BACKEND_FACTORY(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), E_TYPE_KOLAB_BACKEND_FACTORY))
#define E_IS_KOLAB_BACKEND_FACTORY_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), E_TYPE_KOLAB_BACKEND_FACTORY))
#define E_KOLAB_BACKEND_FACTORY_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), E_TYPE_KOLAB_BACKEND_FACTORY, EKolabBackendFactoryClass))

G_BEGIN_DECLS

typedef struct _EKolabBackendFactory EKolabBackendFactory;
typedef struct _EKolabBackendFactoryClass EKolabBackendFactoryClass;
typedef struct _EKolabBackendFactoryPrivate EKolabBackendFactoryPrivate;

struct _EKolabBackendFactory {
	ECollectionBackendFactory parent;
	EKolabBackendFactoryPrivate *priv;
};

struct _EKolabBackendFactoryClass {
	ECollectionBackendFactoryClass parent_class;
};

GType		e_kolab_backend_factory_get_type
						(void) G_GNUC_CONST;
void		e_kolab_backend_factory_type_register
						(GTypeModule *type_module);

G_END_DECLS

#endif /* E_KOLAB_BACKEND_FACTORY_H */

