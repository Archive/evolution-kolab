/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include "e-cal-config-kolab.h"

G_DEFINE_DYNAMIC_TYPE (
	ECalConfigKolab,
	e_cal_config_kolab,
	E_TYPE_SOURCE_CONFIG_BACKEND)

static gboolean
cal_config_kolab_allow_creation (ESourceConfigBackend *backend)
{
	return TRUE;
}

static void
e_cal_config_kolab_class_init (ECalConfigKolabClass *class)
{
	EExtensionClass *extension_class;
	ESourceConfigBackendClass *backend_class;

	extension_class = E_EXTENSION_CLASS (class);
	extension_class->extensible_type = E_TYPE_CAL_SOURCE_CONFIG;

	backend_class = E_SOURCE_CONFIG_BACKEND_CLASS (class);
	backend_class->backend_name = "kolab";
	backend_class->allow_creation = cal_config_kolab_allow_creation;
}

static void
e_cal_config_kolab_class_finalize (ECalConfigKolabClass *class)
{
}

static void
e_cal_config_kolab_init (ECalConfigKolab *backend)
{
}

void
e_cal_config_kolab_type_register (GTypeModule *type_module)
{
	/* XXX G_DEFINE_DYNAMIC_TYPE declares a static type registration
	 *     function, so we have to wrap it with a public function in
	 *     order to register types from a separate compilation unit. */
	e_cal_config_kolab_register_type (type_module);
}

