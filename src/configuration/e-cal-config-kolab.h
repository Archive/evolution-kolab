/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef E_CAL_CONFIG_KOLAB_H
#define E_CAL_CONFIG_KOLAB_H

#include <e-util/e-util.h>

/* Standard GObject macros */
#define E_TYPE_CAL_CONFIG_KOLAB \
	(e_cal_config_kolab_get_type ())
#define E_CAL_CONFIG_KOLAB(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), E_TYPE_CAL_CONFIG_KOLAB, ECalConfigKolab))
#define E_CAL_CONFIG_KOLAB_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), E_TYPE_CAL_CONFIG_KOLAB, ECalConfigKolabClass))
#define E_IS_CAL_CONFIG_KOLAB(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), E_TYPE_CAL_CONFIG_KOLAB))
#define E_IS_CAL_CONFIG_KOLAB_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), E_TYPE_CAL_CONFIG_KOLAB))
#define E_CAL_CONFIG_KOLAB_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), E_TYPE_CAL_CONFIG_KOLAB, ECalConfigKolabClass))

G_BEGIN_DECLS

typedef struct _ECalConfigKolab ECalConfigKolab;
typedef struct _ECalConfigKolabClass ECalConfigKolabClass;
typedef struct _ECalConfigKolabPrivate ECalConfigKolabPrivate;

struct _ECalConfigKolab {
	ESourceConfigBackend parent;
	ECalConfigKolabPrivate *priv;
};

struct _ECalConfigKolabClass {
	ESourceConfigBackendClass parent_class;
};

GType		e_cal_config_kolab_get_type	(void) G_GNUC_CONST;
void		e_cal_config_kolab_type_register
						(GTypeModule *type_module);

G_END_DECLS

#endif /* E_CAL_CONFIG_KOLAB_H */

