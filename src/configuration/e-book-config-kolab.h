/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef E_BOOK_CONFIG_KOLAB_H
#define E_BOOK_CONFIG_KOLAB_H

#include <e-util/e-util.h>

/* Standard GObject macros */
#define E_TYPE_BOOK_CONFIG_KOLAB \
	(e_book_config_kolab_get_type ())
#define E_BOOK_CONFIG_KOLAB(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST \
	((obj), E_TYPE_BOOK_CONFIG_KOLAB, EBookConfigKolab))
#define E_BOOK_CONFIG_KOLAB_CLASS(cls) \
	(G_TYPE_CHECK_CLASS_CAST \
	((cls), E_TYPE_BOOK_CONFIG_KOLAB, EBookConfigKolabClass))
#define E_IS_BOOK_CONFIG_KOLAB(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE \
	((obj), E_TYPE_BOOK_CONFIG_KOLAB))
#define E_IS_BOOK_CONFIG_KOLAB_CLASS(clas) \
	(G_TYPE_CHECK_CLASS_TYPE \
	((cls), E_TYPE_BOOK_CONFIG_KOLAB))
#define E_BOOK_CONFIG_KOLAB_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS \
	((obj), E_TYPE_BOOK_CONFIG_KOLAB, EBookConfigKolabClass))

G_BEGIN_DECLS

typedef struct _EBookConfigKolab EBookConfigKolab;
typedef struct _EBookConfigKolabClass EBookConfigKolabClass;
typedef struct _EBookConfigKolabPrivate EBookConfigKolabPrivate;

struct _EBookConfigKolab {
	ESourceConfigBackend parent;
	EBookConfigKolabPrivate *priv;
};

struct _EBookConfigKolabClass {
	ESourceConfigBackendClass parent_class;
};

GType		e_book_config_kolab_get_type	(void) G_GNUC_CONST;
void		e_book_config_kolab_type_register
						(GTypeModule *type_module);

G_END_DECLS

#endif /* E_BOOK_CONFIG_KOLAB_H */

