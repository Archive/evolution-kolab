/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-error.c
 *
 *  Mon Feb 28 15:46:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include "kolab-util-error.h"

/*----------------------------------------------------------------------------*/
/* CamelIMAPX error GQuark						      */

GQuark
kolab_camel_error_quark (void)
{
	static GQuark quark = 0;

	if (G_UNLIKELY (quark == 0)) {
		const gchar *string = "camel-imapx-error-quark";
		quark = g_quark_from_static_string (string);
	}

	return quark;
}

/*----------------------------------------------------------------------------*/
/* CamelKolab error GQuark                                                    */

GQuark
kolab_camel_kolab_error_quark (void)
{
	static GQuark quark = 0;

	if (G_UNLIKELY (quark == 0)) {
		const gchar *string = "camel-kolab-error-quark";
		quark = g_quark_from_static_string (string);
	}

	return quark;
}

/*----------------------------------------------------------------------------*/
/* KolabUtil error GQuark                                                     */

GQuark
kolab_util_error_quark (void)
{
	static GQuark quark = 0;

	if (G_UNLIKELY (quark == 0)) {
		const gchar *string = "kolab-util--error-quark";
		quark = g_quark_from_static_string (string);
	}

	return quark;
}

/*----------------------------------------------------------------------------*/
