/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-folder.h
 *
 *  Mon Feb 28 17:57:21 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <glib.h>

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_FOLDER_H_
#define _KOLAB_UTIL_FOLDER_H_

/* IMAP folder annotation: /vendor/kolab/folder-type
 *
 * Keep this enum in order (camel_kolab_utils_folder_type_*()
 * relies on it!)
 */
typedef enum {
	KOLAB_FOLDER_TYPE_INVAL = 0,		/* 00 */
	/* Mail-IMAPX (Evo) */
	KOLAB_FOLDER_TYPE_UNKNOWN,		/* 01 */
	KOLAB_FOLDER_TYPE_EMAIL,		/* 02 */
	KOLAB_FOLDER_TYPE_EMAIL_INBOX,		/* 03 */
	KOLAB_FOLDER_TYPE_EMAIL_DRAFTS,		/* 04 */
	KOLAB_FOLDER_TYPE_EMAIL_SENTITEMS,	/* 05 */
	KOLAB_FOLDER_TYPE_EMAIL_JUNKEMAIL,	/* 06 */
	/* Cal-IMAPX (ECal-Backend) */
	KOLAB_FOLDER_TYPE_EVENT,		/* 07 */
	KOLAB_FOLDER_TYPE_EVENT_DEFAULT,	/* 08 */
	KOLAB_FOLDER_TYPE_JOURNAL,		/* 09 */
	KOLAB_FOLDER_TYPE_JOURNAL_DEFAULT,	/* 10 */
	KOLAB_FOLDER_TYPE_TASK,			/* 11 */
	KOLAB_FOLDER_TYPE_TASK_DEFAULT,		/* 12 */
	KOLAB_FOLDER_TYPE_NOTE,			/* 13 */
	KOLAB_FOLDER_TYPE_NOTE_DEFAULT,		/* 14 */
	/* Book-IMAPX (EBook-Backend) */
	KOLAB_FOLDER_TYPE_CONTACT,		/* 15 */
	KOLAB_FOLDER_TYPE_CONTACT_DEFAULT,	/* 16 */
	KOLAB_FOLDER_LAST_TYPE,
	KOLAB_FOLDER_TYPE_AUTO = KOLAB_FOLDER_TYPE_INVAL
} KolabFolderTypeID;

/* IMAP folder annotation: /vendor/kolab/incidences-for
 * TODO check whether this annotation info is needed
 *      (used for events, tasks)
 *
 typedef enum {
 KOLAB_INCIDENCES_FOR_INVAL	= 0,
 KOLAB_INCIDENCES_FOR_NOBODY	= 1 << 0,
 KOLAB_INCIDENCES_FOR_ADMINS	= 1 << 1,
 KOLAB_INCIDENCES_FOR_READERS	= 1 << 2
 } KolabFolderMetaIncidencesForID;
*/

/* IMAP folder annotation: /vendor/kolab/pxfb-readable-for
 * TODO do we need to care for the (p)xfb annotation locally?
 *	(holds blank-separated list of email addresses of who
 *	may read xfb data)
 *	If the client does not have read permission, it will
 *	just not get the data.
 *	Maybe needed when GUI facility is there to manage
 *	these permissions in Evo
 */

/* IMAP folder type context */
typedef enum {
	KOLAB_FOLDER_CONTEXT_INVAL = 0,
	KOLAB_FOLDER_CONTEXT_EMAIL,
	KOLAB_FOLDER_CONTEXT_CALENDAR,
	KOLAB_FOLDER_CONTEXT_CONTACT,
	KOLAB_FOLDER_LAST_CONTEXT
} KolabFolderContextID;

/* IMAP ACL folder permissions types */
typedef enum {
	KOLAB_FOLDER_PERM_NOCHANGE = 0,
	KOLAB_FOLDER_PERM_READ,
	KOLAB_FOLDER_PERM_APPEND,
	KOLAB_FOLDER_PERM_WRITE,
	KOLAB_FOLDER_PERM_ALL,
	KOLAB_FOLDER_LAST_PERM
} KolabFolderPermID;

typedef struct _KolabFolderDescriptor KolabFolderDescriptor;
struct _KolabFolderDescriptor {
	gchar *name;
	KolabFolderTypeID type_id;
};

#define KOLAB_FOLDER_UPDATE_DELAY_MICROSECS ((gint64) 13000000)

/*----------------------------------------------------------------------------*/

KolabFolderTypeID
kolab_util_folder_type_get_id (const gchar *typestring);

const gchar*
kolab_util_folder_type_get_string (KolabFolderTypeID foldertype);

KolabFolderContextID
kolab_util_folder_type_map_to_context_id (KolabFolderTypeID type_id);

gboolean
kolab_util_folder_type_match_with_context_id (KolabFolderTypeID type_id,
                                              KolabFolderContextID context_id);

const gchar*
kolab_util_folder_perm_get_string (KolabFolderPermID perm_id);

KolabFolderDescriptor*
kolab_util_folder_descriptor_new (const gchar *foldername,
                                  KolabFolderTypeID type_id);

void
kolab_util_folder_descriptor_free (KolabFolderDescriptor *desc);

void
kolab_util_folder_descriptor_glist_free (GList *list);

GHashTable*
kolab_util_folder_timestamp_table_new (void);

void
kolab_util_folder_timestamp_table_free (GHashTable *table);

void
kolab_util_folder_timestamp_table_update (GHashTable *table,
                                          const gchar *foldername);

gint64
kolab_util_folder_timestamp_table_lookup (GHashTable *table,
                                          const gchar *foldername);

gint64
kolab_util_folder_timestamp_table_msec_since_update (GHashTable *table,
                                                     const gchar *foldername);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_FOLDER_ */

/*----------------------------------------------------------------------------*/
