/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-glib.h
 *
 *  Fri Aug 13 12:11:23 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_GLIB_H_
#define _KOLAB_UTIL_GLIB_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

/*----------------------------------------------------------------------------*/

typedef struct _KolabGConstList KolabGConstList;
struct _KolabGConstList {
	gconstpointer const_data;
	KolabGConstList *next;
	KolabGConstList *prev;
};

/*----------------------------------------------------------------------------*/
/* GLib init/shutdown */

void kolab_util_glib_init (void);
void kolab_util_glib_shutdown (void);

/*----------------------------------------------------------------------------*/
/* GLib data structures convenience functions */

void
kolab_util_glib_ghashtable_gdestroy (gpointer data);

GHashTable*
kolab_util_glib_ghashtable_new_from_str_glist (GList *list);

void
kolab_util_glib_glist_free_segment (GList *list);
void
kolab_util_glib_glist_free (GList *list);

void
kolab_util_glib_glist_gdestroy (gpointer data);

gint
kolab_util_glib_string_gcompare (gconstpointer a,
                                 gconstpointer b);

void
kolab_util_glib_gconstlist_free (KolabGConstList *list);

KolabGConstList*
kolab_util_glib_gconstlist_prepend (KolabGConstList *list,
                                    gconstpointer data);

KolabGConstList*
kolab_util_glib_gconstlist_append (KolabGConstList *list,
                                   gconstpointer data);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_GLIB_H_ */

/*----------------------------------------------------------------------------*/
