/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * evolution-kolab
 * Copyright (C) Silvan Marco Fin 2010 <silvan@kernelconcepts.de>
 * 
 * evolution-kolab is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * evolution-kolab is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _KOLAB_UTIL_LDAP_H_
#define _KOLAB_UTIL_LDAP_H_

#include <glib.h>
#include <glib-object.h>
#include <curl/curl.h>

G_BEGIN_DECLS

#define KOLAB_TYPE_UTIL_LDAP             (kolab_util_ldap_get_type ())
#define KOLAB_UTIL_LDAP(obj)             (G_TYPE_CHECK_INSTANCE_CAST ((obj), KOLAB_TYPE_UTIL_LDAP, KolabUtilLdap))
#define KOLAB_UTIL_LDAP_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST ((klass), KOLAB_TYPE_UTIL_LDAP, KolabUtilLdapClass))
#define KOLAB_IS_UTIL_LDAP(obj)          (G_TYPE_CHECK_INSTANCE_TYPE ((obj), KOLAB_TYPE_UTIL_LDAP))
#define KOLAB_IS_UTIL_LDAP_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE ((klass), KOLAB_TYPE_UTIL_LDAP))
#define KOLAB_UTIL_LDAP_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), KOLAB_TYPE_UTIL_LDAP, KolabUtilLdapClass))

typedef enum 
{
	LDAP_NOENC, /* no encryption */
	LDAP_TLS, /* starttls handshake */
	LDAP_SSL /* use SSL encryption */
} KolabUtilLdapEncryptionMode;

typedef struct _KolabUtilLdapClass KolabUtilLdapClass;
typedef struct _KolabUtilLdap KolabUtilLdap;

struct _KolabUtilLdapClass
{
	GObjectClass parent_class;
};

struct _KolabUtilLdap
{
	GObject parent_instance;

	CURL *kc_handle;
	gboolean use_pkcs11;
};

/* Public methods */
GType kolab_util_ldap_get_type (void) G_GNUC_CONST;
gssize kolab_util_ldap_get (KolabUtilLdap *self);
CURLcode kolab_util_ldap_set_searchurl (KolabUtilLdap *self, const gchar *url);
KolabUtilLdapEncryptionMode parse_ldap_encryption_mode (gchar *token);
CURLcode kolab_util_ldap_set_encryption_mode (KolabUtilLdap *self, 
                                              const KolabUtilLdapEncryptionMode mode);
CURLcode kolab_util_ldap_set_credentials (KolabUtilLdap *self, 
                                          const gchar *username,
                                          const gchar *password);
CURLcode kolab_util_ldap_set_pkcs11pin (KolabUtilLdap *self, 
                                        const gchar *pin);

G_END_DECLS

#endif /* _KOLAB_UTIL_LDAP_H_ */
