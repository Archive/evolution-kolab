/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-stream.h
 *
 *  Thu Aug 12 11:09:45 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_KOLAB_STREAM_H_
#define _CAMEL_KOLAB_STREAM_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <sys/types.h>

#include "camel-system-headers.h"

/*----------------------------------------------------------------------------*/

CamelStream* camel_kolab_stream_new_filestream (const gchar *filename, const gint flags, const mode_t mode, GError **err);
CamelStream* camel_kolab_stream_new_memstream (GByteArray *buffer);

void camel_kolab_stream_free (CamelStream *stream);

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_KOLAB_STREAM_H_ */

/*----------------------------------------------------------------------------*/
