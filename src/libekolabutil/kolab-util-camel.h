/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-camel.h
 *
 *  Thu Feb 24 15:42:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_CAMEL_H_
#define _KOLAB_UTIL_CAMEL_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <gio/gio.h>

#include "camel-system-headers.h"

/*----------------------------------------------------------------------------*/

#define KOLAB_CAMEL_PROVIDER_PROTOCOL   "kolab"
#define KOLAB_CAMEL_PROVIDER_NAME       "Kolab2"
#define KOLAB_CAMEL_URI_PREFIX          "kolab://"

#define KOLAB_CAMELURL_DUMMY_URL           "protocol://user@host.domain.tld/"
#define KOLAB_CAMELURL_PARAM_SSL           "use_ssl"
#define KOLAB_CAMELURL_TLSVARIANT_NONE     "never"
#define KOLAB_CAMELURL_TLSVARIANT_SSL      "always"
#define KOLAB_CAMELURL_TLSVARIANT_STARTTLS "when-possible"

#define KOLAB_IMAP_CLIENT_X_EVO_UID_HEADER "X-Evolution-MimeMessage-IMAP-UID"
#define KOLAB_IMAP_CLIENT_DUMMY_FROM_NAME  "Nobody"
#define KOLAB_IMAP_CLIENT_DUMMY_FROM_ADDR  "nobody@localhost.localdomain"

/* default fallback TCP ports */
#define KOLAB_SERVER_IMAP_PORT  143
#define KOLAB_SERVER_IMAPS_PORT 993
#define KOLAB_SERVER_HTTP_PORT  80
#define KOLAB_SERVER_HTTPS_PORT 443
#define KOLAB_SERVER_LDAP_PORT  389
#define KOLAB_SERVER_LDAPS_PORT 636

/*----------------------------------------------------------------------------*/

typedef struct _KolabUtilCamelIMAPPath KolabUtilCamelIMAPPath;
struct _KolabUtilCamelIMAPPath {
	gchar *parent_name;
	gchar *folder_name;
};

/*----------------------------------------------------------------------------*/
/* Camel init/shutdown */

gboolean kolab_util_camel_init (GError **err);
gboolean kolab_util_camel_shutdown (GError **err);

/*----------------------------------------------------------------------------*/
/* Camel helper functions */

gchar*
kolab_util_camel_get_storage_path (CamelService *service,
                                   CamelSession *session,
                                   GError **err);

guint64
kolab_util_camel_imapx_folder_get_uidvalidity (CamelIMAPXFolder *folder);

GList*
kolab_util_camel_folderlist_from_folderinfo (CamelFolderInfo *fi);

KolabUtilCamelIMAPPath*
kolab_util_camel_imap_path_new (void);

void
kolab_util_camel_imap_path_free (KolabUtilCamelIMAPPath *path);

KolabUtilCamelIMAPPath*
kolab_util_camel_imap_path_split (const gchar *full_path,
                                  const gchar *delim);

gboolean
kolab_util_camel_imapx_stream_eos (CamelIMAPXStream *is,
                                   GCancellable *cancellable,
                                   GError **err);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_CAMEL_H_ */

/*----------------------------------------------------------------------------*/
