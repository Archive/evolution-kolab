/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-folder.c
 *
 *  Mon Feb 28 17:57:21 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include "kolab-util-folder.h"

/*----------------------------------------------------------------------------*/

static gchar *kolab_folder_type_inv_map[] = {
	"---INVALID---",	/* KOLAB_FOLDER_TYPE_INVAL */
	"---UNKNOWN---",	/* KOLAB_FOLDER_TYPE_UNKNOWN */

	"mail",			/* KOLAB_FOLDER_TYPE_EMAIL */
	"mail.inbox",		/* KOLAB_FOLDER_TYPE_EMAIL_INBOX */
	"mail.drafts",		/* KOLAB_FOLDER_TYPE_EMAIL_DRAFTS */
	"mail.sentitems",	/* KOLAB_FOLDER_TYPE_EMAIL_SENTITEMS */
	"mail.junkemail",	/* KOLAB_FOLDER_TYPE_EMAIL_JUNKEMAIL */

	"event",		/* KOLAB_FOLDER_TYPE_EVENT */
	"event.default",	/* KOLAB_FOLDER_TYPE_EVENT_DEFAULT */
	"journal",		/* KOLAB_FOLDER_TYPE_JOURNAL */
	"journal.default",	/* KOLAB_FOLDER_TYPE_JOURNAL_DEFAULT */
	"task",			/* KOLAB_FOLDER_TYPE_TASK */
	"task.default",		/* KOLAB_FOLDER_TYPE_TASK_DEFAULT */
	"note",			/* KOLAB_FOLDER_TYPE_NOTE */
	"note.default",		/* KOLAB_FOLDER_TYPE_NOTE_DEFAULT */

	"contact",		/* KOLAB_FOLDER_TYPE_CONTACT */
	"contact.default"	/* KOLAB_FOLDER_TYPE_CONTACT_DEFAULT */
};

static gchar *kolab_folder_perm_inv_map[] = {
	"---NOCHANGE---",       /* KOLAB_FOLDER_PERM_NOCHANGE */
	"lrs",                  /* KOLAB_FOLDER_PERM_READ */
	"lrsip",                /* KOLAB_FOLDER_PERM_APPEND */
	"lrswipte",             /* KOLAB_FOLDER_PERM_WRITE */
	"lrswipakxte"           /* KOLAB_FOLDER_PERM_ALL */
};

static KolabFolderTypeID kolab_folder_type_nums[KOLAB_FOLDER_LAST_TYPE];
static GHashTable *kolab_folder_type_map = NULL;
static GMutex init_lock;

/*----------------------------------------------------------------------------*/

static void
util_folder_init (void)
{
	gint ii = 0;

	g_mutex_lock (&init_lock);

	if (kolab_folder_type_map == NULL) {
		kolab_folder_type_map = g_hash_table_new (g_str_hash, g_str_equal);
		for (ii = 0; ii < KOLAB_FOLDER_LAST_TYPE; ii++) {
			kolab_folder_type_nums[ii] = ii;
			g_hash_table_insert (kolab_folder_type_map,
			                     kolab_folder_type_inv_map[ii],
			                     &(kolab_folder_type_nums[ii]));
		}
	}

	g_mutex_unlock (&init_lock);
}

/*----------------------------------------------------------------------------*/

KolabFolderTypeID
kolab_util_folder_type_get_id (const gchar *typestring)
{
	/* when looking up a value from kolab_folder_type_map, store
	 * it in gpointer, check for NULL, then dereference and cast
	 * to KolabFolderTypeID
	 */
	gpointer map_entry = NULL;
	KolabFolderTypeID id = KOLAB_FOLDER_TYPE_INVAL;

	g_assert (typestring != NULL);

	util_folder_init ();

	map_entry = g_hash_table_lookup (kolab_folder_type_map, typestring);

	if (map_entry == NULL)
		return KOLAB_FOLDER_TYPE_INVAL;

	id = *((KolabFolderTypeID*)map_entry);
	return id;
}

const gchar*
kolab_util_folder_type_get_string (KolabFolderTypeID foldertype)
{
	g_return_val_if_fail (foldertype < KOLAB_FOLDER_LAST_TYPE, NULL);

	util_folder_init ();

	return kolab_folder_type_inv_map[foldertype];
}

KolabFolderContextID
kolab_util_folder_type_map_to_context_id (KolabFolderTypeID type_id)
{
	/* TODO better handling here */
	g_assert ((type_id > KOLAB_FOLDER_TYPE_INVAL) &&
	          (type_id < KOLAB_FOLDER_LAST_TYPE));

	if ((type_id >= KOLAB_FOLDER_TYPE_UNKNOWN) &&
	    (type_id <= KOLAB_FOLDER_TYPE_EMAIL_JUNKEMAIL))
		return KOLAB_FOLDER_CONTEXT_EMAIL;

	if ((type_id >= KOLAB_FOLDER_TYPE_EVENT) &&
	    (type_id <= KOLAB_FOLDER_TYPE_NOTE_DEFAULT))
		return KOLAB_FOLDER_CONTEXT_CALENDAR;

	return KOLAB_FOLDER_CONTEXT_CONTACT;
}

gboolean
kolab_util_folder_type_match_with_context_id (KolabFolderTypeID type_id,
                                              KolabFolderContextID context_id)
{
	/* TODO better handling here */
	g_assert ((type_id > KOLAB_FOLDER_TYPE_INVAL) &&
	          (type_id < KOLAB_FOLDER_LAST_TYPE));
	g_assert ((context_id > KOLAB_FOLDER_CONTEXT_INVAL) &&
	          (context_id < KOLAB_FOLDER_LAST_CONTEXT));

	switch (context_id) {
	case KOLAB_FOLDER_CONTEXT_EMAIL:
		if ((type_id < KOLAB_FOLDER_TYPE_UNKNOWN) ||
		    (type_id > KOLAB_FOLDER_TYPE_EMAIL_JUNKEMAIL))
			return FALSE;
		break;
	case KOLAB_FOLDER_CONTEXT_CALENDAR:
		if ((type_id < KOLAB_FOLDER_TYPE_EVENT) ||
		    (type_id > KOLAB_FOLDER_TYPE_NOTE_DEFAULT))
			return FALSE;
		break;
	case KOLAB_FOLDER_CONTEXT_CONTACT:
		if ((type_id < KOLAB_FOLDER_TYPE_CONTACT) ||
		    (type_id > KOLAB_FOLDER_TYPE_CONTACT_DEFAULT))
			return FALSE;
		break;
	default:
		/* can't happen */
		g_assert_not_reached ();

	}
	return TRUE;
}

/*----------------------------------------------------------------------------*/

const gchar*
kolab_util_folder_perm_get_string (KolabFolderPermID perm_id)
{
	g_return_val_if_fail (perm_id < KOLAB_FOLDER_LAST_PERM, NULL);

	return kolab_folder_perm_inv_map[perm_id];
}

/*----------------------------------------------------------------------------*/

KolabFolderDescriptor*
kolab_util_folder_descriptor_new (const gchar *foldername,
                                  KolabFolderTypeID type_id)
{
	KolabFolderDescriptor *desc = NULL;

	g_return_val_if_fail (foldername != NULL, NULL);
	g_return_val_if_fail ((type_id > KOLAB_FOLDER_TYPE_INVAL) &&
	                      (type_id < KOLAB_FOLDER_LAST_TYPE), NULL);

	desc = g_new0 (KolabFolderDescriptor, 1);
	desc->name = g_strdup (foldername);
	desc->type_id = type_id;

	return desc;
}

void
kolab_util_folder_descriptor_free (KolabFolderDescriptor *desc)
{
	if (desc == NULL)
		return;

	if (desc->name != NULL)
		g_free (desc->name);

	g_free (desc);
}

void
kolab_util_folder_descriptor_glist_free (GList *list)
{
	GList *list_ptr = list;

	while (list_ptr != NULL) {
		kolab_util_folder_descriptor_free (list_ptr->data);
		list_ptr = g_list_next (list_ptr);
	}
	if (list != NULL)
		g_list_free (list);
}

/*----------------------------------------------------------------------------*/

GHashTable*
kolab_util_folder_timestamp_table_new (void)
{
	GHashTable *table =
		g_hash_table_new_full (g_str_hash,
		                       g_str_equal,
		                       g_free,
		                       g_free);
	return table;
}

void
kolab_util_folder_timestamp_table_free (GHashTable *table)
{
	g_return_if_fail (table != NULL);
	g_hash_table_destroy (table);
}

void
kolab_util_folder_timestamp_table_update (GHashTable *table,
                                          const gchar *foldername)
{
	gint64 timestamp = 0;
	gint64 *stamp_ptr = NULL;

	g_return_if_fail (table != NULL);
	g_return_if_fail (foldername != NULL);

	timestamp = g_get_monotonic_time ();
	g_return_if_fail (timestamp >= 0);

	stamp_ptr = (gint64*) g_malloc0 (sizeof (gint64));
	g_return_if_fail (stamp_ptr != NULL);

	*stamp_ptr = timestamp;

	g_hash_table_replace (table,
	                      g_strdup (foldername),
	                      stamp_ptr);
}

gint64
kolab_util_folder_timestamp_table_lookup (GHashTable *table,
                                          const gchar *foldername)
{
	gint64 timestamp = 0;
	gpointer stamp_ptr = NULL;

	g_return_val_if_fail (table != NULL, -1);
	g_return_val_if_fail (foldername != NULL, -1);

	stamp_ptr = g_hash_table_lookup (table,
	                                 foldername);
	if (stamp_ptr == NULL)
		return -1;

	timestamp = *((gint64*) stamp_ptr);

	return timestamp;
}

gint64
kolab_util_folder_timestamp_table_msec_since_update (GHashTable *table,
                                                     const gchar *foldername)
{
	gint64 timestamp_current = 0;
	gint64 timestamp_latest = 0;
	gint64 timestamp_diff = 0;

	g_return_val_if_fail (table != NULL, -1);
	g_return_val_if_fail (foldername != NULL, -1);

	timestamp_current = g_get_monotonic_time ();
	g_return_val_if_fail (timestamp_current >= 0, -1);

	timestamp_latest =
		kolab_util_folder_timestamp_table_lookup (table,
		                                          foldername);
	if (timestamp_latest < 0)
		return -1;

	timestamp_diff = timestamp_current - timestamp_latest;

	if (timestamp_diff < 0)
		timestamp_diff = -1;

	return timestamp_diff;
}

/*----------------------------------------------------------------------------*/

