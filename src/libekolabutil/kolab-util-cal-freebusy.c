/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-cal-freebusy.c
 *
 *  Tue Aug  3 15:10:03 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/
/** @file */

/*
 * Utility functions for the calendar backend --
 * free/busy access
 *
 * TODO where to store account information - get this from
 *      the eplugin code?
 * - We need to store account information
 *   in a central place for backend access (like here).
 * - Check for a common account info struct
 * - will this info be stored in the CamelSession object?
 * TODO we need a libekolabconv function to generate
 *      the f/b urls
 */

/*----------------------------------------------------------------------------*/

#include <libical/icalvcal.h>

#include "camel-system-headers.h"
#include "kolab-util-cal-freebusy.h"

/*----------------------------------------------------------------------------*/

ECalComponent*
kolab_cal_util_fb_new_ecalcomp_from_request (KolabUtilHttpJob *job,
                                             Kolab_conv_freebusy_type listtype,
                                             GError **err)
{
	CamelURL *camel_url = NULL;
	gchar *url_string = NULL;
	gchar *servername = NULL;
	gboolean use_ssl = FALSE;
	gssize nbytes = 0;
	ECalComponent *ecalcomp = NULL;
	GError *tmp_err = NULL;

	/* preconditions */
	g_assert (job != NULL);
	g_assert (job->buffer == NULL);
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	url_string = camel_url_to_string (job->url, 0);
	g_assert (url_string != NULL);

	servername = g_strdup (job->url->host);
	use_ssl = kolab_util_http_protocol_is_ssl (url_string);

	g_free (url_string);

	url_string = kolabconv_cal_util_freebusy_new_fb_url (servername,
	                                                     job->url->user,
	                                                     use_ssl,
	                                                     listtype);
	g_assert (url_string != NULL);
	g_debug ("%s: \n\t\t\t\t%s", __func__, url_string);

	/* FIXME create a new extended CamelURL from url_string
	 *
	 * - merge the old and the new CamelURL
	 * - pay attention to passwd,authmech,... set
	 *   on the original CamelURL
	 *
	 */
	camel_url = camel_url_new (url_string, NULL);
	g_assert (camel_url != NULL);
	g_free (url_string);
	camel_url_set_user (camel_url, job->url->user);
	/* TODO authmech ? */

	camel_url_set_port (camel_url, job->url->port);
	if (job->url->query) camel_url_set_query (camel_url, job->url->query);
	if (job->url->fragment) camel_url_set_fragment (camel_url, job->url->fragment);
	camel_url_free (job->url);
	job->url = camel_url;

	job->buffer = g_byte_array_new ();
	g_assert (job->buffer != NULL);

	/* issue HTTP GET request */
	nbytes = kolab_util_http_get (job, &tmp_err);
	if (tmp_err != NULL)
		goto skip;

	job->nbytes = nbytes; /* save number of bytes read since buffer will be destroyed */
	g_debug ("%s: read %d bytes", __func__, nbytes);

	/* create new ECalComponent */
	ecalcomp = kolabconv_cal_util_freebusy_ecalcomp_new_from_ics ((gchar*)(job->buffer->data),
	                                                              nbytes,
	                                                              &tmp_err);
 skip:
	if ((ecalcomp == NULL) && (tmp_err != NULL))
		g_propagate_error (err, tmp_err);

	g_byte_array_unref (job->buffer);
	job->buffer = NULL;
	g_free (servername);

	/* postconditions */
	g_assert (job->buffer == NULL);

	return ecalcomp;
}

void
kolab_cal_util_fb_ecalcomp_free (ECalComponent *ecalcomp)
{
	/* this function could be removed */
	if (ecalcomp == NULL)
		return;
	g_object_unref (ecalcomp);
}

/*----------------------------------------------------------------------------*/
