/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-kconv.h
 *
 *  Mon Apr 11 11:02:25 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_KCONV_H_
#define _KOLAB_UTIL_KCONV_H_

/*----------------------------------------------------------------------------*/

#include <libekolabconv/main/src/kolab-conv.h>

/*----------------------------------------------------------------------------*/

void kolab_util_kconv_kconvmail_gdestroy (gpointer data);
Kolab_conv_mail* kolab_util_kconv_kconvmail_clone (const Kolab_conv_mail *kconvmail);

GList* kolab_util_kconv_kconvmail_data_base64_encode (const Kolab_conv_mail *kconvmail);
Kolab_conv_mail* kolab_util_kconv_kconvmail_data_base64_decode (GList *kconvbase64);
gchar* kolab_util_kconv_kconvmail_checksum (const Kolab_conv_mail *kconvmail);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_KCONV_H_ */

/*----------------------------------------------------------------------------*/
