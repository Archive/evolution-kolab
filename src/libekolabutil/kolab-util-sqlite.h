/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-sqlite.h
 *
 *  Fri May 20 10:13:25 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_SQLITE_H_
#define _KOLAB_UTIL_SQLITE_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include <sqlite3.h>

/*----------------------------------------------------------------------------*/

typedef struct _KolabUtilSqliteDb {
	sqlite3  *db;	/* sqlite3 DB handle */
	gchar    *path; /* path to the sqlite3 DB file */
	gint	 ctr;	/* aux counter, usable with callback functions */
} KolabUtilSqliteDb;

/*----------------------------------------------------------------------------*/

KolabUtilSqliteDb* kolab_util_sqlite_db_new (void);
gboolean kolab_util_sqlite_db_free (KolabUtilSqliteDb *kdb, GError **err);
gboolean kolab_util_sqlite_db_open (KolabUtilSqliteDb *kdb, const gchar *dbpath, const gchar *filename, GError **err);
gboolean kolab_util_sqlite_db_close (KolabUtilSqliteDb *kdb, GError **err);

gboolean kolab_util_sqlite_table_exists (KolabUtilSqliteDb *kdb, const gchar *name, GError **err);
gboolean kolab_util_sqlite_table_drop (KolabUtilSqliteDb *kdb, const gchar *name, GError **err);
gint kolab_util_sqlite_table_get_rowcount (KolabUtilSqliteDb *kdb, const gchar *name, GError **err);

gboolean kolab_util_sqlite_exec_str (KolabUtilSqliteDb *kdb, const gchar *sql_str, GError **err);

gboolean kolab_util_sqlite_prep_stmt (KolabUtilSqliteDb *kdb, sqlite3_stmt **sql_stmt, const gchar *sql_str, GError **err);
gboolean kolab_util_sqlite_fnlz_stmt (KolabUtilSqliteDb *kdb, sqlite3_stmt *sql_stmt, GError **err);

gboolean kolab_util_sqlite_transaction_start (KolabUtilSqliteDb *kdb, GError **err);
gboolean kolab_util_sqlite_transaction_commit (KolabUtilSqliteDb *kdb, GError **err);
gboolean kolab_util_sqlite_transaction_abort (KolabUtilSqliteDb *kdb, GError **err);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_SQLITE_H_ */

/*----------------------------------------------------------------------------*/
