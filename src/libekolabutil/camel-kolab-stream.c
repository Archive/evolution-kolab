/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-stream.h
 *
 *  Thu Aug 12 11:09:45 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#ifndef VERSION
#define VERSION "0.1"
#endif

#include "camel-kolab-stream.h"

/*----------------------------------------------------------------------------*/

CamelStream*
camel_kolab_stream_new_filestream (const gchar *filename,
                                   const gint flags,
                                   const mode_t mode,
                                   GError **err)
{
	CamelStream *stream = NULL;
	GError *tmp_err = NULL;

	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	stream = camel_stream_fs_new_with_name (filename,
	                                        flags,
	                                        mode,
	                                        &tmp_err);

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return NULL;
	}

	if (stream == NULL)
		g_debug ("%s: CamelStreamFs is NULL", __func__);

	return stream;
}

CamelStream*
camel_kolab_stream_new_memstream (GByteArray *buffer)
{
	/* caution: CamelStreamMem behaves differently
	 * compared to the other CamelStream classes,
	 * type-wise ...
	 */
	CamelStreamMem *stream = (CamelStreamMem*)camel_stream_mem_new ();

	if (buffer != NULL)
		camel_stream_mem_set_byte_array (stream, buffer);

	return (CamelStream*)stream;
}

void camel_kolab_stream_free (CamelStream *stream)
{
	if (stream == NULL)
		return;
	g_object_unref (stream);
}

/*----------------------------------------------------------------------------*/
