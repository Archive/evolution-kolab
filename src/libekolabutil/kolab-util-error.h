/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-error.h
 *
 *  Mon Feb 28 15:46:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_ERROR_H_
#define _KOLAB_UTIL_ERROR_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>
#include "camel-system-headers.h"

/*----------------------------------------------------------------------------*/
/* CamelIMAPX GError                                                          */

#define KOLAB_CAMEL_ERROR	  \
	(kolab_camel_error_quark ())

typedef enum {
	KOLAB_CAMEL_ERROR_GENERIC /* lazy fallback error */
} KolabCamelError;

GQuark kolab_camel_error_quark (void) G_GNUC_CONST;


/*----------------------------------------------------------------------------*/
/* CamelKolab GError                                                          */

#define KOLAB_CAMEL_KOLAB_ERROR	  \
	(kolab_camel_kolab_error_quark ())

typedef enum {
	KOLAB_CAMEL_KOLAB_ERROR_GENERIC,
	KOLAB_CAMEL_KOLAB_ERROR_DB,     /* metadata SQLite DB     */
	KOLAB_CAMEL_KOLAB_ERROR_FORMAT,	/* Kolab format violation */
	KOLAB_CAMEL_KOLAB_ERROR_TYPE,   /* Kolab type error       */
	KOLAB_CAMEL_KOLAB_ERROR_SERVER	/* server connect problem */
} KolabCamelKolabError;

GQuark kolab_camel_kolab_error_quark (void) G_GNUC_CONST;

/*----------------------------------------------------------------------------*/
/* KolabUtil GError                                                           */

#define KOLAB_UTIL_ERROR	  \
	(kolab_util_error_quark ())

typedef enum {
	KOLAB_UTIL_ERROR_SQLITE_DB     /* SQLite DB */
} KolabUtilError;

GQuark kolab_util_error_quark (void) G_GNUC_CONST;

/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_ERROR_H_ */

/*----------------------------------------------------------------------------*/
