/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-http.h
 *
 *  Fri Aug  6 10:30:54 2010
 *  Copyright  2010 Christian Hilberg <hilberg@kernelconcepts.de>
 *         and 2011 Silvan Marco Fin <silvan@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _KOLAB_UTIL_HTTP_H_
#define _KOLAB_UTIL_HTTP_H_

/*----------------------------------------------------------------------------*/

#include <glib.h>

#include "camel-system-headers.h"

/*----------------------------------------------------------------------------*/

/* Error handling for kolab-util-http methods */

#define KOLAB_UTIL_HTTP_ERROR kolab_util_http_error_quark ()

GQuark kolab_util_http_error_quark (void);

typedef enum {
	KOLAB_UTIL_HTTP_ERROR_UNKNOWN_ERROR,
	KOLAB_UTIL_HTTP_ERROR_CLIENT_ERROR,
	KOLAB_UTIL_HTTP_ERROR_SERVER_ERROR,
	KOLAB_UTIL_HTTP_ERROR_CONFIGURATION_ERROR
} KolabUtilHttpError;

/*
 * Wrapper functions for HTTP access.
 *
 * The functions herein must provide an interface to the rest of the
 * plugin which will hide all HTTP- and TLS-Library specifics so the
 * libs used now can be replaced when the GIO TLS works have landed
 * and LibSoup will be suitable for our needs.
 */

/**
 * KolabUtilHttpJob:
 * @url: the CamelURL containing all server information
 *          (needs to be maintained manually)
 * @buffer: the byte buffer to hold the HTTP server response
 *          (needs to be maintained manually)
 * @nbytes: can be used to store a bytes count (aside from the buffer length)
 * @pkcs11pin: the pin needed to access client cert on pkcs #11 tokens
 *
 * This structure holds all information needed by the kolab_util_http_get()
 * function to send a query to a webserver.
 */
typedef struct _KolabUtilHttpJob {
	CamelURL *url;
	GByteArray *buffer;
	gssize nbytes;
	gchar *passwd;
	gchar *pkcs11pin;
} KolabUtilHttpJob;

void kolab_util_http_init (const gchar *user_home);
void kolab_util_http_shutdown (void);

KolabUtilHttpJob* kolab_util_http_job_new (void);
void kolab_util_http_job_free (KolabUtilHttpJob *job);

gssize kolab_util_http_get (KolabUtilHttpJob *job, GError **error);

gchar* kolab_util_http_new_hostname_from_url (const gchar *url_string);
gboolean kolab_util_http_protocol_is_ssl (const gchar *url_string);

/*----------------------------------------------------------------------------*/

#endif /* _KOLAB_UTIL_HTTP_H_ */

/*----------------------------------------------------------------------------*/
