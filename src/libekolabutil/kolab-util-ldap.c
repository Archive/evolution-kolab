/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * evolution-kolab
 * Copyright (C) Silvan Marco Fin 2010 <silvan@kernelconcepts.de>
 *
 * evolution-kolab is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * evolution-kolab is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "kolab-util-ldap.h"
#include "camel-system-headers.h"

#include <curl/curl.h>

#include <string.h>


G_DEFINE_TYPE (KolabUtilLdap, kolab_util_ldap, G_TYPE_OBJECT)

static void
kolab_util_ldap_init (KolabUtilLdap *self)
{
	self->kc_handle = NULL;
	self->kc_handle = curl_easy_init ();
	self->use_pkcs11 = FALSE;

	/* TODO: no error handling in the initialisation currently */
	curl_easy_setopt (self->kc_handle, CURLOPT_VERBOSE, 1);
	curl_easy_setopt (self->kc_handle, CURLOPT_NETRC, CURL_NETRC_IGNORED);
	curl_easy_setopt (self->kc_handle, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt (self->kc_handle, CURLOPT_SSL_VERIFYPEER, 1);
	curl_easy_setopt (self->kc_handle, CURLOPT_SSL_VERIFYHOST, 2);

	curl_easy_setopt (self->kc_handle, CURLOPT_SSLVERSION, CURL_SSLVERSION_SSLv3);

	g_assert (self->kc_handle != NULL);
}

static void
kolab_util_ldap_finalize (GObject *object)
{
	/* TODO: Add deinitalization code here */

	G_OBJECT_CLASS (kolab_util_ldap_parent_class)->finalize (object);
}

static void
kolab_util_ldap_class_init (KolabUtilLdapClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);
	/* not used, so far
	 * GObjectClass* parent_class = G_OBJECT_CLASS (klass);
	 */

	curl_global_init (CURL_GLOBAL_ALL);

	object_class->finalize = kolab_util_ldap_finalize;
}

/* TODO: check wether curl_global_init() and curl_global_cleanup() have to be
 * called in a class init/finalization method (class destructor). Perhaps this is
 * not desireable, since libcurl might already be initialized or yet be in use
 * by some other utility class.
 */

CURLcode
kolab_util_ldap_set_searchurl (KolabUtilLdap *self, const gchar *url)
{
	return curl_easy_setopt (self->kc_handle, CURLOPT_URL, url);
}

gssize
kolab_util_ldap_get (KolabUtilLdap *self)
{
	gssize nbytes = 0;
	CURLcode curlcode;

	curlcode = curl_easy_perform(self->kc_handle);

	g_assert(curlcode == 0); /* FIXME: error handling */

	return nbytes;
}

KolabUtilLdapEncryptionMode
parse_ldap_encryption_mode(gchar *token)
{
	if (g_str_equal ("tls", token))
		return LDAP_TLS;
	else if (g_str_equal ("ssl", token))
		return LDAP_SSL;
	else /* if (g_str_equal("no", token)) */
		return LDAP_NOENC;

}

CURLcode
kolab_util_ldap_set_encryption_mode(KolabUtilLdap *self,
                                    KolabUtilLdapEncryptionMode mode)
{
	CURLcode curl_code;

	g_return_val_if_fail (KOLAB_IS_UTIL_LDAP(self), -1);

	g_printerr ("Mode: %d\n", mode);

	switch (mode) {
	case LDAP_TLS:
		curl_code = curl_easy_setopt (self->kc_handle, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1);
		break;
	case LDAP_SSL:
		curl_code = curl_easy_setopt (self->kc_handle, CURLOPT_SSLVERSION, CURL_SSLVERSION_SSLv3);
		break;
	default:
		/* how do you tell curl, not to use ssl? */
		return CURLE_OK;
	}
	return curl_code;
}

CURLcode
kolab_util_ldap_set_credentials (KolabUtilLdap *self,
                                 const gchar *username,
                                 const gchar *password)
{
	CURLcode curl_code;
	gchar *tmp;

	g_return_val_if_fail (KOLAB_IS_UTIL_LDAP (self), -1);

	tmp = g_strconcat (username, ":", password, NULL);
	curl_code = curl_easy_setopt (self->kc_handle, CURLOPT_USERPWD, tmp);

	return curl_code;
}

CURLcode
kolab_util_ldap_set_pkcs11pin (KolabUtilLdap *self,
                               const gchar *pin)
{
	CURLcode curl_code;

	g_return_val_if_fail(KOLAB_IS_UTIL_LDAP (self), -1);

	curl_code = curl_easy_setopt (self->kc_handle, CURLOPT_KEYPASSWD, pin);

	return curl_code;
}
