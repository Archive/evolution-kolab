/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-glib.c
 *
 *  Fri Aug 13 12:11:23 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <glib-object.h>

#include "kolab-util-glib.h"

/*----------------------------------------------------------------------------*/
/* GLib init/shutdown */

void
kolab_util_glib_init (void)
{
	static gboolean is_initialized = FALSE;

	if (is_initialized == TRUE)
		return;

	g_type_init ();

	is_initialized = TRUE;
	g_debug ("%s: GLib initialized", __func__);
}

void
kolab_util_glib_shutdown (void)
{
}

/*----------------------------------------------------------------------------*/
/* GLib data structures convenience functions */

/* hash tables */

void
kolab_util_glib_ghashtable_gdestroy (gpointer data)
{
	GHashTable *tbl = NULL;

	if (data == NULL)
		return;

	tbl = (GHashTable *) data;
	g_hash_table_destroy (tbl);
}

/**
 * kolab_util_glib_ghashtable_new_from_glist:
 * @list: a list containing gchar* data _only_ (or NULL)
 *
 * Allocates a new GHashTable and inserts all @list data
 * as hash table keys.
 * The list data is interpreted as gchar* data and copied
 * from the @list. The value for each key is a pointer back
 * to it's key data. Keys are destroyed when the hash table
 * is, or when g_hash_table_remove() or g_hash_table_replace()
 * are called on existing keys. There is no destruction function
 * set for values. If you replace the value pointers by some
 * kind of other data, you need to take care to free it
 * manually.
 *
 * Returns: A newly allocated hash table containing the
 *          @list elements as string keys for quick lookup,
 *	    or NULL if the @list supplied is NULL
 */
GHashTable*
kolab_util_glib_ghashtable_new_from_str_glist (GList *list)
{
	GHashTable *tbl = NULL;
	GList *lst_ptr = NULL;
	gchar *tmp_str = NULL;

	if (list == NULL)
		return NULL;

	tbl = g_hash_table_new_full (g_str_hash,
	                             g_str_equal,
	                             g_free,
	                             NULL);
	lst_ptr = list;
	while (lst_ptr != NULL) {
		tmp_str = g_strdup ((const gchar *)(lst_ptr->data));
		g_hash_table_insert (tbl, tmp_str, tmp_str);
		lst_ptr = g_list_next (lst_ptr);
	}

	return tbl;
}

/* GList */

void
kolab_util_glib_glist_free_segment (GList *list)
{
	GList *list_ptr = NULL;

	if (list == NULL)
		return;

	list_ptr = list;
	while (list_ptr != NULL) {
		if (list_ptr->data != NULL) {
			g_free (list_ptr->data);
			list_ptr->data = NULL;
		}
		list_ptr = g_list_next (list_ptr);
	}
}

void
kolab_util_glib_glist_free (GList *list)
{
	kolab_util_glib_glist_free_segment (list);
	if (list != NULL)
		g_list_free (list);
}

void
kolab_util_glib_glist_gdestroy (gpointer data)
{
	GList *list = NULL;

	if (data == NULL)
		return;

	list = (GList *)data;
	kolab_util_glib_glist_free (list);
}

gint
kolab_util_glib_string_gcompare (gconstpointer a,
                                 gconstpointer b)
{
	const gchar *str_a = (const gchar *)a;
	const gchar *str_b = (const gchar *)b;

	return g_strcmp0 (str_a, str_b);
}

void
kolab_util_glib_gconstlist_free (KolabGConstList *list)
{
	KolabGConstList *anchor = NULL;
	KolabGConstList *ptr = NULL;

	if (list == NULL)
		return;

	anchor = list;
	ptr = list->next;

	while (ptr != NULL) {
		g_free (anchor);
		anchor = ptr;
		ptr = anchor->next;
	}
	g_free (anchor);
}

KolabGConstList*
kolab_util_glib_gconstlist_prepend (KolabGConstList *list, gconstpointer data)
{
	KolabGConstList *elem = g_new0 (KolabGConstList, 1);

	elem->const_data = data;
	elem->next = list;
	if (list != NULL)
		list->prev = elem;

	return elem;
}

KolabGConstList*
kolab_util_glib_gconstlist_append (KolabGConstList *list, gconstpointer data)
{
	KolabGConstList *elem = g_new0 (KolabGConstList, 1);
	KolabGConstList *ptr = NULL;

	elem->const_data = data;

	if (list == NULL)
		return elem;

	ptr = list;
	while (ptr->next != NULL)
		ptr = ptr->next;

	ptr->next = elem;
	elem->prev = ptr;

	return list;
}

/*----------------------------------------------------------------------------*/
