/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            kolab-util-camel.c
 *
 *  Thu Feb 24 15:42:23 2011
 *  Copyright  2011  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib/gprintf.h>
#include <glib/gi18n-lib.h>

#include <libedataserver/libedataserver.h>

#include "kolab-util-error.h"
#include "kolab-util-glib.h"
#include "kolab-util-camel.h"

/*----------------------------------------------------------------------------*/
/* local statics */

static gpointer
util_camel_init_fn (gpointer data)
{
	GError **err = (GError **) (&data);
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	/* init Camel subsystem */
	if (camel_init (e_get_user_data_dir (), TRUE) != 0) {
		g_set_error (err,
		             KOLAB_CAMEL_ERROR,
		             KOLAB_CAMEL_ERROR_GENERIC,
		             _("Failed to initialize Camel subsystem"));
		return (gpointer) 1;
	}

	/* init the CamelProvider system */
	camel_provider_init ();

	g_debug ("%s: camel system initialized", __func__);

	return NULL; /* ok */
}

/*----------------------------------------------------------------------------*/
/* Camel init/shutdown */

gboolean
kolab_util_camel_init (GError **err)
{
	static GOnce my_once = G_ONCE_INIT;

	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	g_once (&my_once,
	        util_camel_init_fn,
	        (gpointer) *err);

	if (my_once.retval != NULL)
		return FALSE;

	return TRUE;
}

gboolean
kolab_util_camel_shutdown (GError **err)
{
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);
	return TRUE;
}

/*----------------------------------------------------------------------------*/
/* Camel helper functions */

gchar*
kolab_util_camel_get_storage_path (CamelService *service,
                                   CamelSession *session,
                                   GError **err)
{
	gchar *store_path = NULL;

	g_assert (CAMEL_IS_SERVICE (service));
	g_assert (CAMEL_IS_SESSION (session));
	g_return_val_if_fail (err == NULL || *err == NULL, NULL);

	store_path = g_strdup (camel_service_get_user_cache_dir (service));

	if (store_path == NULL) {
		/* FIXME mark this as a translatable string */
		g_set_error (err,
		             KOLAB_CAMEL_KOLAB_ERROR,
		             KOLAB_CAMEL_KOLAB_ERROR_DB,
		             _("Could not get Camel storage path"));
	}

	return store_path;
}

guint64
kolab_util_camel_imapx_folder_get_uidvalidity (CamelIMAPXFolder *folder)
{
	CamelIMAPXFolder *ifolder = NULL;
	guint64 uidvalidity = 0;

	ifolder = CAMEL_IMAPX_FOLDER (folder);

	/* TODO check whether we need to hold folder lock here */
	uidvalidity = ifolder->uidvalidity_on_server;

	return uidvalidity;
}

GList*
kolab_util_camel_folderlist_from_folderinfo (CamelFolderInfo *fi)
{
	GList *folders_self = NULL;
	GList *folders_chld = NULL;
	GList *folders_next = NULL;

	if (fi == NULL)
		return NULL;

	folders_chld = kolab_util_camel_folderlist_from_folderinfo (fi->child);
	folders_next = kolab_util_camel_folderlist_from_folderinfo (fi->next);

	folders_self = g_list_append (folders_self, g_strdup (fi->full_name));
	if (folders_chld != NULL)
		folders_self = g_list_concat (folders_self, folders_chld);
	if (folders_next != NULL)
		folders_self = g_list_concat (folders_self, folders_next);

	return folders_self;
}

KolabUtilCamelIMAPPath*
kolab_util_camel_imap_path_new (void)
{
	KolabUtilCamelIMAPPath *path = NULL;

	path = g_new0 (KolabUtilCamelIMAPPath, 1);
	path->parent_name = NULL;
	path->folder_name = NULL;

	return path;
}

void
kolab_util_camel_imap_path_free (KolabUtilCamelIMAPPath *path)
{
	if (path == NULL)
		return;

	if (path->parent_name != NULL)
		g_free (path->parent_name);
	if (path->folder_name != NULL)
		g_free (path->folder_name);

	g_free (path);
}

KolabUtilCamelIMAPPath*
kolab_util_camel_imap_path_split (const gchar *full_path,
                                  const gchar *delim)
{
	KolabUtilCamelIMAPPath *path = NULL;
	guint strv_nelem = 0;
	gchar **strv = NULL;
	gchar *strv_tmp = NULL;

	if (full_path == NULL)
		return NULL;

	path = kolab_util_camel_imap_path_new ();

	if (delim == NULL) {
		path->folder_name = g_strdup (full_path);
		goto done;
	}

	strv = g_strsplit (full_path, delim, -1);
	strv_nelem = g_strv_length (strv);

	strv_tmp = strv[strv_nelem - 1];
	strv[strv_nelem - 1] = '\0';
	path->parent_name = g_strjoinv (delim, strv);
	strv[strv_nelem - 1] = strv_tmp;

	path->folder_name = g_strdup (strv[strv_nelem - 1]);

	g_strfreev (strv);

 done:

	return path;
}

gboolean
kolab_util_camel_imapx_stream_eos (CamelIMAPXStream *is,
                                   GCancellable *cancellable,
                                   GError **err)
{
	gint tok = 0;
	guchar *token = NULL;
	guint len = 0;
	GError *tmp_err = NULL;
	gboolean at_end = FALSE;

	g_return_val_if_fail (CAMEL_IS_IMAPX_STREAM (is), FALSE);
	/* cancellable may be NULL */
	g_return_val_if_fail (err == NULL || *err == NULL, FALSE);

	tok = camel_imapx_stream_token (is, &token, &len, cancellable, &tmp_err);

	if (tmp_err != NULL) {
		g_propagate_error (err, tmp_err);
		return TRUE;
	}

	at_end =  (tok == '\n' || tok < 0);
	camel_imapx_stream_ungettoken (is, tok, token, len);

	return at_end;
}

/*----------------------------------------------------------------------------*/
