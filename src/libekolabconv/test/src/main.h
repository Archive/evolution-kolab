/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * main.h
 *
 *  Created on: 06.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */
#ifndef MAIN_H_
#define MAIN_H_

#define TEST_CONFIG_FILE_NAME "src/libekolabconv/test/config/main.conf"

gchar *tempfile;
gchar *test_resource_folder;
gchar *kolab_folder_I_contact;
gchar *kolab_folder_I_event;
gchar *kolab_folder_I_task;
gchar *kolab_folder_I_note;
gchar *evolution_folder_I_contact;
gchar *evolution_folder_I_event;
gchar *evolution_folder_I_task;
gchar *evolution_folder_I_note;
FILE *log_fp;

GList *free_mem_after_test;

#endif /* MAIN_H_ */
