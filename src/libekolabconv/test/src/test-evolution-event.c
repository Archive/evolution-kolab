/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_evolution_event.c
 *
 *  Created on: 21.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Fabian Köster <f.koester@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include "test-convert-setup.h"
#include "test-util.h"


void
validate_evolution_event_1(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, "IEvent",
	                 "20100825T143806Z-1270-1000-1-38@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC, "2010-08-25 14:40:07",
	                 "2010-08-25 14:40:07", stage);

	validate_iincidence(ievent->incidence,
	                    "event_e1", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-08-27 14:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->show_time_as == SHOW_TIME_AS_BUSY);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-08-27 15:00:00", ievent->end_date->date_time);
}

void
validate_evolution_event_2(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common,
	                 NULL,
	                 "20100825T144011Z-1270-1000-1-39@hhelwi-virt.bonn.tarent.de",
	                 "Beschreibung\nText\n123",
	                 "Urlaubskarten,Strategien,Urlaub,Konkurrenz,Jahrestag,"
	                 "Anrufe,Allgemeines,Heiße Kontakte,Lieferanten,Zeit "
	                 "und Ausgaben,Geschenke,International,Favoriten,Ideen,"
	                 "VIP,Geschäftlich,Persönlich,Status,Ziele/Zielsetzungen,"
	                 "Geburtstag,Schlüssel-Kunde,Wartend",
	                 ICOMMON_SENSITIVITY_PUBLIC, "2010-08-25 14:42:20", "2010-08-25 14:42:20", stage);

	validate_iincidence(ievent->incidence,
	                    "event_e2", /* summary */
	                    "Ort", /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-08-27 14:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-08-28 17:00:00", ievent->end_date->date_time);

	g_assert(ievent->incidence->common->evolution_store == NULL);
}

void
validate_evolution_event_3(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	validate_icommon(ievent->incidence->common,
	                 NULL,
	                 "20100825T144419Z-1270-1000-1-41@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC, "2010-08-25 14:45:51", "2010-08-25 14:45:51", stage);

	validate_iincidence(ievent->incidence,
	                    "event_e3", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-08-27 14:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-08-27 15:00:00", ievent->end_date->date_time);
}

void
validate_evolution_event_4(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	validate_icommon(ievent->incidence->common,
	                 NULL,
	                 "20100825T144649Z-1270-1000-1-43@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC, "2010-08-25 14:48:07", "2010-08-25 14:48:07", stage);

	validate_iincidence(ievent->incidence,
	                    "event_e4", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-08-27 14:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-08-27 15:00:00", ievent->end_date->date_time);
}

void
validate_evolution_event_5(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,	/* type */
	                     -15,			/* start_offset		*/
	                     0,			/* end_offset		*/
	                     2,			/* repeat_count		*/
	                     5,			/* repeat_interval	*/
	                     NULL,			/* display_text		*/
	                     NULL,			/* audio_file		*/
	                     NULL,			/* procParam_program	*/
	                     NULL,			/* procParam_arguments	*/
	                     NULL,			/* emailParam_subject	*/
	                     NULL);			/* emailParam_mailText	*/
}

void
validate_evolution_event_6(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_AUDIO,	/* type			*/
	                     1020,			/* start_offset		*/
	                     0,			/* end_offset		*/
	                     3,			/* repeat_count		*/
	                     360,			/* repeat_interval	*/
	                     NULL,			/* display_text		*/
	                     NULL,			/* audio_file		*/
	                     NULL,			/* procParam_program	*/
	                     NULL,			/* procParam_arguments	*/
	                     NULL,			/* emailParam_subject	*/
	                     NULL);			/* emailParam_mailText	*/
}

void
validate_evolution_event_7(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_AUDIO,	/* type			*/
	                     0,			/* start_offset		*/
	                     18720,			/* end_offset		*/
	                     0,			/* repeat_count		*/
	                     0,			/* repeat_interval	*/
	                     NULL,			/* display_text		*/
	                     "file:///usr/share/sounds/gnome/default/alerts/bark.ogg", /* audio_file */
	                     NULL,			/* procParam_program	*/
	                     NULL,			/* procParam_arguments	*/
	                     NULL,			/* emailParam_subject	*/
	                     NULL);			/* emailParam_mailText	*/
}

void
validate_evolution_event_8(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_PROCEDURE,	/* type			*/
	                     0,			/* start_offset		*/
	                     -15840,			/* end_offset		*/
	                     3,			/* repeat_count		*/
	                     120,			/* repeat_interval	*/
	                     NULL,			/* display_text		*/
	                     NULL,			/* audio_file		*/
	                     "/usr/bin/xmessage",	/* procParam_program	*/
	                     "Alert!",		/* procParam_arguments	*/
	                     NULL,			/* emailParam_subject	*/
	                     NULL);			/* emailParam_mailText	*/
}

void
validate_evolution_event_9(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(SHOW_TIME_AS_FREE == ievent->show_time_as);

	validate_iincidence(ievent->incidence,
	                    "event_e9", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-08-27", /* startdate */
	                    stage);

	assert_equal_gdate("2010-08-27", ievent->end_date->date);
}

void
validate_evolution_event_10(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_DAILY, 2, 4, NULL, 0, 0, 0);
}

void
validate_evolution_event_11(const I_event *ievent, gint stage)
{
	gint rdate[3] = {10,9,2010};

	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T150952Z-1270-1000-1-52@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:11:22", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_DAILY, 2, 0, rdate, 0, 0, 0);
}

void
validate_evolution_event_12(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T151125Z-1270-1000-1-53@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:12:13", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_DAILY, 3, 0, NULL, 0, 0, 0);
}

void
validate_evolution_event_13(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T151648Z-1270-1000-1-55@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:18:08", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY, 2, 4, NULL, I_COMMON_TUESDAY | I_COMMON_THURSDAY | I_COMMON_SUNDAY, 0, 0);
}

void
validate_evolution_event_14(const I_event *ievent, gint stage)
{
	gint rdate[3] = {27,9,2010};

	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T151811Z-1270-1000-1-56@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:19:22", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY, 2, 0, rdate, I_COMMON_MONDAY | I_COMMON_TUESDAY, 0, 0);
}

void
validate_evolution_event_15(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T151925Z-1270-1000-1-57@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:20:05", stage);
	;
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY, 1, 0, NULL, I_COMMON_SATURDAY | I_COMMON_SUNDAY, 0, 0);
}

void
validate_evolution_event_16(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T152033Z-1270-1000-1-58@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:24:45", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER, 2, 2, NULL, 0, 1, 0);
}

void
validate_evolution_event_17(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * RRULE;X-EVOLUTION-end_date=20101007T203000Z:FREQ=MONTHLY;COUNT=2;BYDAY=TH;BYSETPOS=1
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="monthly" type="weekday">
	 * 	<interval>1</interval>
	 * 	<day>thursday</day>
	 * 	<daynumber>1</daynumber>
	 * 	<range type="number">2</range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T152448Z-1270-1000-1-59@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:26:43", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY, 1, 2, NULL, I_COMMON_THURSDAY, 1, 0);
}

void
validate_evolution_event_18(const I_event *ievent, gint stage)
{
	gint rdate[3] = {23,11,2010};

	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * RRULE:FREQ=MONTHLY;UNTIL=20101123;BYDAY=WE;BYSETPOS=-1
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="monthly" type="weekday">
	 *     <interval>1</interval>
	 *     <day>wednesday</day>
	 *     <daynumber>-1</daynumber>
	 *     <range type="date" >2010-11-23</range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T152646Z-1270-1000-1-60@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:28:25", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY, 1, 0, rdate, I_COMMON_WEDNESDAY, -1, 0);
}

void
validate_evolution_event_19(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * RRULE:FREQ=MONTHLY;BYMONTHDAY=13
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="monthly" type="daynumber">
	 *     <interval>1</interval>
	 *     <daynumber>13</daynumber>
	 *     <range type="none" ></range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T152828Z-1270-1000-1-61@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:29:19", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER, 1, 0, NULL, 0, 13, 0);
}

void
validate_evolution_event_20(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * RRULE;X-EVOLUTION-end_date=20101031T213000Z:FREQ=MONTHLY;COUNT=3;BYMONTHDAY=-1
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="monthly" type="daynumber">
	 *     <interval>1</interval>
	 *     <daynumber>-1</daynumber>
	 *     <range type="number">3</range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T152921Z-1270-1000-1-62@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:30:55", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER, 1, 3, NULL, 0, -1, 0);
}

void
validate_evolution_event_21(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * RRULE;X-EVOLUTION-end_date=20101229T213000Z:FREQ=MONTHLY;COUNT=2;BYDAY=WE;BYSETPOS=5
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="monthly" type="weekday" >
	 *     <interval>1</interval>
	 *     <day>wednesday</day>
	 *     <daynumber>5</daynumber>
	 *     <range type="number" >2</range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T153056Z-1270-1000-1-63@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:33:59", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY, 1, 2, NULL, I_COMMON_WEDNESDAY, 5, 0);
}

void
validate_evolution_event_22(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * DTSTART;TZID=/freeassociation.sourceforge.net/Tzfile/Europe/Berlin:20100831T223000
	 * RRULE;X-EVOLUTION-end_date=20110831T203000Z:FREQ=YEARLY;COUNT=2
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="yearly" type="monthday">
	 *     <interval>1</interval>
	 *     <daynumber>31</daynumber>
	 *     <month>august</month>
	 *     <range type="number">2</range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T153415Z-1270-1000-1-64@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:35:20", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY, 1, 2, NULL, 0, 31, I_COMMON_AUG);
}



void
validate_evolution_event_23(const I_event *ievent, gint stage)
{
	gint rdate[3] = {31,8,2016};

	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * RRULE:FREQ=YEARLY;UNTIL=20160831;INTERVAL=2
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="yearly" type="monthday">
	 *     <interval>2</interval>
	 *     <daynumber>31</daynumber>
	 *     <month>august</month>
	 *     <range type="date" >2016-08-31</range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T153522Z-1270-1000-1-65@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:36:55", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY, 2, 0, rdate, 0, 31, I_COMMON_AUG);
}

void
validate_evolution_event_24(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/*
	 * iCalendar:
	 *
	 * RRULE:FREQ=YEARLY
	 *
	 *
	 * Kolab:
	 *
	 * <recurrence cycle="yearly" type="monthday">
	 *     <interval>1</interval>
	 *     <daynumber>31</daynumber>
	 *     <month>august</month>
	 *     <range type="none" ></range>
	 * </recurrence>
	 */
	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T153656Z-1270-1000-1-66@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-25 15:37:29", stage);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY, 1, 0, NULL, 0, 31, I_COMMON_AUG);
}

void
validate_evolution_event_25(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);
	g_assert(ievent->incidence->recurrence != NULL);

	/*
	 * iCalendar:
	 *
	 * RRULE;X-EVOLUTION-end_date=20100924T203000Z:FREQ=I_REC_CYCLE_DAILY;COUNT=13;INTERVAL=2
	 * EXDATE;VALUE=DATE:20100918
	 * EXDATE;VALUE=DATE:20100917
	 * EXDATE;VALUE=DATE:20100908
	 *
	 *
	 * Kolab:
	 *
	 *
	 * <recurrence cycle="daily" >
	 *     <interval>2</interval>
	 *     <range type="number" >13</range>
	 *     <exclusion>2010-09-08</exclusion>
	 *     <exclusion>2010-09-17</exclusion>
	 *     <exclusion>2010-09-18</exclusion>
	 * </recurrence>
	 */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_DAILY,	/* Recurrence_cycle	*/
	                    2,			/* interval		*/
	                    13,			/* range_number		*/
	                    NULL,			/* range_date		*/
	                    0,			/* weekdays		*/
	                    0,			/* day_number		*/
	                    I_COMMON_MONTH_NULL);	/* month		*/

	/* exclusions */
	g_assert(ievent->incidence->recurrence->exclusion != NULL);
	assert_GDate_in_list(ievent->incidence->recurrence->exclusion, "2010-09-08");
	assert_GDate_in_list(ievent->incidence->recurrence->exclusion, "2010-09-17");
	assert_GDate_in_list(ievent->incidence->recurrence->exclusion, "2010-09-18");
	assert_list_length(ievent->incidence->recurrence->exclusion, 3);
}

void
validate_evolution_event_26(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T154548Z-1270-1000-1-71@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PRIVATE,
	                 NULL, "2010-08-25 15:46:07", stage);

	g_assert(ievent->incidence->recurrence == NULL);
}

void
validate_evolution_event_27(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100825T154636Z-1270-1000-1-72@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_CONFIDENTIAL,
	                 NULL, "2010-08-25 15:47:12", stage);

	g_assert(ievent->incidence->recurrence == NULL);
}

void
validate_evolution_event_28(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20100826T073231Z-1270-1000-1-74@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-08-26 07:40:05", stage);

	g_assert_cmpint(g_list_length(ievent->incidence->attendee), ==, 6);
	assert_attendee_in_list(ievent->incidence->attendee, "Donald Duck",
	                        "donald@duck.de", I_INC_STATUS_NONE, TRUE, TRUE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_INDIVIDUAL);
	assert_attendee_in_list(ievent->incidence->attendee, "Micky Maus",
	                        "micky@maus.nl", I_INC_STATUS_ACCEPTED, FALSE, TRUE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_GROUP);
	assert_attendee_in_list(ievent->incidence->attendee, "Daisy Duck",
	                        "daisy@duck.de", I_INC_STATUS_DECLINED, TRUE, TRUE, I_INC_ROLE_OPTIONAL, I_INC_CUTYPE_RESOURCE);
	assert_attendee_in_list(ievent->incidence->attendee, "Bill Gates",
	                        "bill@gates.com", I_INC_STATUS_TENTATIVE, TRUE, TRUE, I_INC_ROLE_RESOURCE, I_INC_CUTYPE_ROOM);
	assert_attendee_in_list(ievent->incidence->attendee, "Sir Mix-a-Lot",
	                        "sir@mix-a-lot.com", I_INC_STATUS_DELEGATED, FALSE, TRUE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);
	assert_attendee_in_list(ievent->incidence->attendee, "MC Hammer",
	                        "mc@hammer.org", I_INC_STATUS_NONE, TRUE, TRUE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_INDIVIDUAL);
	/*
	  ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=CHAIR;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;
	  CN=Donald Duck;LANGUAGE=en:MAILTO:donald@duck.de
	  ATTENDEE;CUTYPE=GROUP;ROLE=REQ-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=FALSE;
	  CN=Micky Maus;LANGUAGE=en:MAILTO:micky@maus.nl
	  ATTENDEE;CUTYPE=RESOURCE;ROLE=OPT-PARTICIPANT;PARTSTAT=DECLINED;RSVP=TRUE;
	  CN=Daisy Duck;LANGUAGE=en:MAILTO:daisy@duck.de
	  ATTENDEE;CUTYPE=ROOM;ROLE=NON-PARTICIPANT;PARTSTAT=TENTATIVE;RSVP=TRUE;
	  CN=Bill Gates;LANGUAGE=en:MAILTO:bill@gates.com
	  ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=DELEGATED;RSVP=FALSE;CN=Sir
	  Mix-a-Lot;LANGUAGE=en:MAILTO:sir@mix-a-lot.com
	  ATTENDEE;CUTYPE=INDIVIDUAL;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=MC Hammer;
	  LANGUAGE=en:MAILTO:mc@hammer.org
	*/

	g_assert(ievent->incidence->recurrence == NULL);
}

void
validate_evolution_event_29(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon(ievent->incidence->common, NULL,
	                 "20101004T083558Z-1277-1000-1-3@hhelwi-virt.bonn.tarent.de",
	                 "این مقاله پیرامون تکامل مادی "
	                 "(زیست‌شناختی) است. برای دیگر کاربردهای "
	                 "«تکامل»، تکامل (ابهام‌زدایی) را "
	                 "ببینید.\n\nبرای بحث ساده‌تر، مقدمه‌ای بر "
	                 "نظریه تکامل را ببینید.",
	                 "نظرية التطور",
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 NULL, "2010-10-04 08:38:57", stage);

	g_assert(ievent->incidence->recurrence == NULL);
}

void
validate_evolution_event_30(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);
	g_assert(ievent->incidence->recurrence != NULL);

	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,	/* Recurrence_cycle	*/
	                    1,				/* interval		*/
	                    10,				/* range_number		*/
	                    NULL,				/* range_date		*/
	                    2,				/* weekdays		*/
	                    4,				/* day_number		*/
	                    I_COMMON_FEB);			/* month		*/
}

void
validate_evolution_event_tz1(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_equal_gchar("BEGIN:VTIMEZONE\n"
	                   "TZID:/freeassociation.sourceforge.net/Tzfile/America/La_Paz\n"
	                   "X-LIC-LOCATION:America/La_Paz\n"
	                   "BEGIN:STANDARD\n"
	                   "TZNAME:BOT\n"
	                   "DTSTART:19700320T230000\n"
	                   "TZOFFSETFROM:-0400\n"
	                   "TZOFFSETTO:-0400\n"
	                   "END:STANDARD\n"
	                   "END:VTIMEZONE\n",
	                   strip_cr_cpy(ievent->incidence->common->vtimezone));

	validate_iincidence(ievent->incidence,
	                    "event_tz1", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-11-03 14:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-11-03 15:00:00", ievent->end_date->date_time);
}

void
validate_evolution_event_tz2(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_equal_gchar("BEGIN:VTIMEZONE\n"
	                   "TZID:/freeassociation.sourceforge.net/Tzfile/Africa/Windhoek\n"
	                   "X-LIC-LOCATION:Africa/Windhoek\n"
	                   "BEGIN:STANDARD\n"
	                   "TZNAME:WAT\n"
	                   "DTSTART:19700404T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=4\n"
	                   "TZOFFSETFROM:+0200\n"
	                   "TZOFFSETTO:+0100\n"
	                   "END:STANDARD\n"
	                   "BEGIN:DAYLIGHT\n"
	                   "TZNAME:WAST\n"
	                   "DTSTART:19700905T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=9\n"
	                   "TZOFFSETFROM:+0100\n"
	                   "TZOFFSETTO:+0200\n"
	                   "END:DAYLIGHT\n"
	                   "END:VTIMEZONE\n",
	                   strip_cr_cpy(ievent->incidence->common->vtimezone));

	validate_iincidence(ievent->incidence,
	                    "event_tz2", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-11-03 08:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-11-03 09:00:00", ievent->end_date->date_time);
}

void
validate_evolution_event_tz3(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_equal_gchar("BEGIN:VTIMEZONE\n"
	                   "TZID:/freeassociation.sourceforge.net/Tzfile/America/New_York\n"
	                   "X-LIC-LOCATION:America/New_York\n"
	                   "BEGIN:STANDARD\n"
	                   "TZNAME:EST\n"
	                   "DTSTART:19701107T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11\n"
	                   "TZOFFSETFROM:-0400\n"
	                   "TZOFFSETTO:-0500\n"
	                   "END:STANDARD\n"
	                   "BEGIN:DAYLIGHT\n"
	                   "TZNAME:EDT\n"
	                   "DTSTART:19700314T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3\n"
	                   "TZOFFSETFROM:-0500\n"
	                   "TZOFFSETTO:-0400\n"
	                   "END:DAYLIGHT\n"
	                   "END:VTIMEZONE\n",
	                   strip_cr_cpy(ievent->incidence->common->vtimezone));

	validate_iincidence(ievent->incidence,
	                    "event_tz3", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-11-03 14:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-11-03 15:00:00", ievent->end_date->date_time);
}

void
validate_evolution_event_tz4(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_equal_gchar("BEGIN:VTIMEZONE\n"
	                   "TZID:/freeassociation.sourceforge.net/Tzfile/Australia/Sydney\n"
	                   "X-LIC-LOCATION:Australia/Sydney\n"
	                   "BEGIN:STANDARD\n"
	                   "TZNAME:EST\n"
	                   "DTSTART:19700404T030000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=4\n"
	                   "TZOFFSETFROM:+1100\n"
	                   "TZOFFSETTO:+1000\n"
	                   "END:STANDARD\n"
	                   "BEGIN:DAYLIGHT\n"
	                   "TZNAME:EST\n"
	                   "DTSTART:19701003T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=10\n"
	                   "TZOFFSETFROM:+1000\n"
	                   "TZOFFSETTO:+1100\n"
	                   "END:DAYLIGHT\n"
	                   "END:VTIMEZONE\n",
	                   strip_cr_cpy(ievent->incidence->common->vtimezone));

	validate_iincidence(ievent->incidence,
	                    "event_tz4", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-11-02 23:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-11-03 00:00:00", ievent->end_date->date_time);
}

void
validate_evolution_event_tz5(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_equal_gchar("BEGIN:VTIMEZONE\n"
	                   "TZID:/freeassociation.sourceforge.net/Tzfile/Europe/Berlin\n"
	                   "X-LIC-LOCATION:Europe/Berlin\n"
	                   "BEGIN:STANDARD\n"
	                   "TZNAME:CET\n"
	                   "DTSTART:19701031T030000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10\n"
	                   "TZOFFSETFROM:+0200\n"
	                   "TZOFFSETTO:+0100\n"
	                   "END:STANDARD\n"
	                   "BEGIN:DAYLIGHT\n"
	                   "TZNAME:CEST\n"
	                   "DTSTART:19700328T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3\n"
	                   "TZOFFSETFROM:+0100\n"
	                   "TZOFFSETTO:+0200\n"
	                   "END:DAYLIGHT\n"
	                   "END:VTIMEZONE\n",
	                   strip_cr_cpy(ievent->incidence->common->vtimezone));

	validate_iincidence(ievent->incidence,
	                    "event_tz5", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-11-03 11:05:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-11-03 12:05:00", ievent->end_date->date_time);
}

void
validate_evolution_event_tz6(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent->incidence->common->vtimezone == NULL);

	validate_iincidence(ievent->incidence,
	                    "event_tz6", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2010-11-04 11:13:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2010-11-04 12:13:00", ievent->end_date->date_time);
}

void
validate_evolution_event_tz7(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_equal_gchar("BEGIN:VTIMEZONE\n"
	                   "TZID:/freeassociation.sourceforge.net/Tzfile/Europe/Berlin\n"
	                   "X-LIC-LOCATION:Europe/Berlin\n"
	                   "BEGIN:STANDARD\n"
	                   "TZNAME:CET\n"
	                   "DTSTART:19701030T030000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10\n"
	                   "TZOFFSETFROM:+0200\n"
	                   "TZOFFSETTO:+0100\n"
	                   "END:STANDARD\n"
	                   "BEGIN:DAYLIGHT\n"
	                   "TZNAME:CEST\n"
	                   "DTSTART:19700327T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3\n"
	                   "TZOFFSETFROM:+0100\n"
	                   "TZOFFSETTO:+0200\n"
	                   "END:DAYLIGHT\n"
	                   "END:VTIMEZONE\n",
	                   strip_cr_cpy(ievent->incidence->common->vtimezone));

	assert_equal("start date before DST, end date in DST", ievent->incidence->common->body);

	validate_iincidence(ievent->incidence,
	                    "event_tz7", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2011-03-27 00:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2011-03-27 01:30:00", ievent->end_date->date_time);
}

void
validate_evolution_event_tz8(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	assert_equal_gchar("BEGIN:VTIMEZONE\n"
	                   "TZID:/freeassociation.sourceforge.net/Tzfile/Europe/Berlin\n"
	                   "X-LIC-LOCATION:Europe/Berlin\n"
	                   "BEGIN:STANDARD\n"
	                   "TZNAME:CET\n"
	                   "DTSTART:19701030T030000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10\n"
	                   "TZOFFSETFROM:+0200\n"
	                   "TZOFFSETTO:+0100\n"
	                   "END:STANDARD\n"
	                   "BEGIN:DAYLIGHT\n"
	                   "TZNAME:CEST\n"
	                   "DTSTART:19700327T020000\n"
	                   "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3\n"
	                   "TZOFFSETFROM:+0100\n"
	                   "TZOFFSETTO:+0200\n"
	                   "END:DAYLIGHT\n"
	                   "END:VTIMEZONE\n",
	                   strip_cr_cpy(ievent->incidence->common->vtimezone));

	assert_equal("start date in DST, end date after DST", ievent->incidence->common->body);

	validate_iincidence(ievent->incidence,
	                    "event_tz8", /* summary */
	                    NULL, /* location */
	                    NULL, /* organizer displayname */
	                    NULL, /* organizer smtp-address */
	                    "2011-10-29 23:30:00", /* startdate */
	                    stage);

	g_assert(ievent->incidence->recurrence == NULL);

	g_assert(ievent->end_date != NULL);
	assert_equal_timestamp("2011-10-30 01:30:00", ievent->end_date->date_time);
}

void
validate_evolution_event_r1(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	g_assert(ievent != NULL);
	g_assert(ievent->incidence != NULL);

	validate_icommon (ievent->incidence->common, NULL,
	                  "20110120T132752Z-5806-1000-5804-0@hhelwi-virt.bonn.tarent.de",
	                  "d",
	                  "c",
	                  ICOMMON_SENSITIVITY_PUBLIC, NULL, NULL, stage);

	g_assert(ievent->incidence->recurrence == NULL);
}

void
validate_evolution_event_b1(const I_event *ievent, gint stage)
{
	Alarm *mail_alarm = NULL;

	(void)stage;

	/* resulting alarm should look like this in kolab:
	   <advanced-alarms>
	   <alarm type="email">
	   <enabled>1</enabled>
	   <start-offset>-15</start-offset>
	   <addresses>
	   <address>mail@gmx.de</address>
	   <address>mail2@server.de</address>
	   </addresses>
	   <mail-text>Hier der Text</mail-text>
	   <attachments/>
	   </alarm>
	   </advanced-alarms>
	*/


	/* advanced alarms */
	g_assert_cmpint(g_list_length(ievent->incidence->advanced_alarm), ==, 1);
	/* this is a mapped simple alarm from kolab */
	mail_alarm = assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                                  I_ALARM_TYPE_EMAIL,
	                                  -15,
	                                  0,
	                                  0,
	                                  0,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL);

	g_assert(mail_alarm->email_param);
	g_assert_cmpint(g_list_length(mail_alarm->email_param->addresses), ==, 3);
	g_assert_cmpstr(((GString*)g_list_nth(mail_alarm->email_param->addresses, 0)->data)->str, ==, "donald@duck.de");
	g_assert_cmpstr(((GString*)g_list_nth(mail_alarm->email_param->addresses, 1)->data)->str, ==, "bill@gates.com");
	g_assert_cmpstr(((GString*)g_list_nth(mail_alarm->email_param->addresses, 2)->data)->str, ==, "micky@maus.de");
}

void
validate_evolution_event_b2(const I_event *ievent, gint stage)
{
	(void)stage;

	g_assert_cmpint(g_list_length(ievent->incidence->attendee), ==, 5);
	assert_attendee_in_list(ievent->incidence->attendee, "Individual",
	                        "h.helwich@tarent.de", I_INC_STATUS_NONE, TRUE, FALSE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_INDIVIDUAL);
	assert_attendee_in_list(ievent->incidence->attendee, "Group",
	                        "mc@hammer.org", I_INC_STATUS_NONE, TRUE, FALSE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_GROUP);
	assert_attendee_in_list(ievent->incidence->attendee, "Resource",
	                        "donald@duck.de", I_INC_STATUS_NONE, TRUE, FALSE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_RESOURCE);
	assert_attendee_in_list(ievent->incidence->attendee, "Room",
	                        "daisy@duck.de", I_INC_STATUS_NONE, TRUE, FALSE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_ROOM);
	assert_attendee_in_list(ievent->incidence->attendee, "Unknown",
	                        "bill@gates.com", I_INC_STATUS_NONE, TRUE, FALSE, I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);
}
