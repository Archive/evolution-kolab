/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_evolution_contact.c
 *
 *  Created on: 13.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Fabian Köster <f.koester@tarent.de>,
 *     			Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include "../../main/src/kolab-conv.h"
#include "test-convert-setup.h"
#include "test-util.h"
#include "testbase.h"


void
validate_evolution_contact_1 (const I_contact *icontact, gint stage)
{
	log_bitmask(stage);

	/*
	 * Common Fields
	 */
	validate_icommon(icontact->common,
	                 NULL,
	                 "pas-id-4C1F87A700000000",
	                 "Notizen\n123",
	                 "Miscellaneous,Strategien,Strategies,Wartend,Time & Expenses,Competition,Key Customer,Geschenke,Gifts,Geburtstag,Ideen,Suppliers,Konkurrenz,Ideas,Jahrestag,VIP,Holiday,International,Favoriten,Heiße Kontakte,Favorites,Goals/Objectives,Geschäftlich,Urlaubskarten,Persönlich,Urlaub,Zeit und Ausgaben,Hot Contacts,Ziele/Zielsetzungen,Lieferanten,Phone Calls,Anrufe,Status,Waiting,Holiday Cards,Business,Allgemeines,Schlüssel-Kunde",
	                 ICOMMON_SENSITIVITY_NULL,
	                 NULL,
	                 "2010-08-24 15:37:58", stage);

	/*
	 * Name Fields
	 */
	assert_equal("Nachname", icontact->last_name);
	assert_equal("Vorname", icontact->given_name);
	assert_equal("Zwischenname", icontact->middle_names);
	assert_equal("Titel", icontact->prefix);
	assert_equal("Suffix", icontact->suffix);
	assert_equal("Titel Vorname Zwischenname Nachname Suffix", icontact->full_name);
	assert_equal("Spitzname", icontact->nick_name);

	/*
	 * Misc Fields
	 */
	assert_equal("Ehepartner", icontact->spouse_name);
	assert_equal_gdate("2010-08-03", icontact->birthday);
	assert_equal_gdate("2010-08-04", icontact->anniversary);

	/*
	 * Emails
	 */
	assert_email_in_list (icontact->emails, "gesch2@host.de", NULL);
	assert_email_in_list (icontact->emails, "weitere1@host.de", NULL);
	assert_email_in_list (icontact->emails, "private@host.de", NULL);
	assert_email_in_list (icontact->emails, "gesch@host.de", NULL);
	assert_list_length(icontact->emails, 4);

	/*
	 * Addresses
	 */
	assert_address_in_list(icontact->addresses, ICONTACT_ADDR_TYPE_HOME,
	                       "P Adresse", "P Postfach", "P Stadt", "P Bundesstaat", "P Postleitzahl", "P Land");
	assert_address_in_list(icontact->addresses, ICONTACT_ADDR_TYPE_BUSINESS,
	                       "G Adresse", "G Postfach", "G Stadt", "G Bundesstaat", "G Postleitzahl", "G Land");
	assert_address_in_list(icontact->addresses, ICONTACT_ADDR_TYPE_OTHER,
	                       "W Adresse", "W Postfach", "W Stadt", "W Bundesstaat", "W Postleitzahl", "W Land");
	assert_list_length(icontact->addresses, 3);

	/*
	 * Phone Numbers
	 */
	assert_phone_in_list (icontact->phone_numbers, "tel geschäftlich", ICONTACT_PHONE_BUSINESS_1);
	assert_phone_in_list (icontact->phone_numbers, "fax geschäftlich", ICONTACT_PHONE_BUSINESS_FAX);
	assert_phone_in_list (icontact->phone_numbers, "mobil", ICONTACT_PHONE_MOBILE);
	assert_phone_in_list (icontact->phone_numbers, "tel privat", ICONTACT_PHONE_HOME_1);
	assert_list_length(icontact->phone_numbers, 4);

	/*
	 * Corporation Fields
	 */
	assert_equal("Firma", icontact->organization);
	assert_equal("Abteilung", icontact->department);
	assert_equal("Büro", icontact->office_location);
	assert_equal("geschäftlich Titel", icontact->job_title);
	assert_equal("Beruf", icontact->profession);
	assert_equal("Vorgesetzter", icontact->manager_name);
	assert_equal("Assistent", icontact->assistant);

	/*
	 * Other Internet Communication Fields
	 */
	assert_equal("Homepage", icontact->web_page);
	assert_equal("Verfügbarkeit", icontact->free_busy_url);

	/*
	 * X-Custom (IM's + BlogFeed URL)
	 */
	assert_custom_field_in_list(icontact->custom_list, "messaging/aim", "All", "aim");
	assert_custom_field_in_list(icontact->custom_list, "messaging/yahoo", "All", "yahoo");
	assert_custom_field_in_list(icontact->custom_list, "messaging/msn", "All", "msn");
	assert_custom_field_in_list(icontact->custom_list, "messaging/icq", "All", "icq");
	assert_custom_field_in_list(icontact->custom_list, "", "BlogFeed", "Weblog");
	assert_list_length(icontact->custom_list, 5);

	/*
	 * Attached Multimedia Files
	 */
	assert_binary_attachment_equal("./src/libekolabconv/test/resources/evolution/contact/contact_e1_photo.png",
	                               "kolab-picture.png",
	                               "image/png",
	                               icontact->photo);

	/*
	 * Geolocation Fields
	 */
	assert_equal_double(DEGREE_NOT_SET, icontact->latitude);
	assert_equal_double(DEGREE_NOT_SET, icontact->longitude);

	/*
	 * Evolution Store
	 */

	assert_equal_gchar("BEGIN:VCARD\r\nVERSION:3.0\r\nX-EVOLUTION-VIDEO-URL:Video-Chat\r\nCALURI:Kalender\r\nX-MOZILLA-HTML:TRUE\r\nEND:VCARD",
	                   icontact->common->evolution_store);
}


void
validate_evolution_contact_2 (const I_contact *icontact, gint stage)
{
	log_bitmask(stage);

	/*
	 * Common Fields
	 */
	validate_icommon(icontact->common,
	                 NULL,
	                 "pas-id-4C73E9E900000000",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_NULL,
	                 NULL,
	                 "2010-08-24 15:48:57", stage);

	/*
	 * Name Fields
	 */
	assert_equal(NULL, icontact->last_name);
	assert_equal ("Name", icontact->given_name);
	assert_equal(NULL, icontact->middle_names);
	assert_equal(NULL, icontact->prefix);
	assert_equal(NULL, icontact->suffix);
	assert_equal ("Name", icontact->full_name);
	assert_equal(NULL, icontact->nick_name);

	/*
	 * Misc Fields
	 */
	assert_equal(NULL, icontact->spouse_name);
	assert_equal_gdate(NULL, icontact->birthday);
	assert_equal_gdate(NULL, icontact->anniversary);

	/*
	 * Emails
	 */
	assert_list_null(icontact->emails);

	/*
	 * Addresses
	 */
	assert_list_null(icontact->addresses);

	/*
	 * Phone Numbers
	 */
	assert_list_null(icontact->phone_numbers);

	/*
	 * Corporation Fields
	 */
	assert_equal(NULL, icontact->organization);
	assert_equal(NULL, icontact->department);
	assert_equal(NULL, icontact->office_location);
	assert_equal(NULL, icontact->job_title);
	assert_equal(NULL, icontact->profession);
	assert_equal(NULL, icontact->manager_name);
	assert_equal(NULL, icontact->assistant);

	/*
	 * Other Internet Communication Fields
	 */
	assert_equal(NULL, icontact->web_page);
	assert_equal(NULL, icontact->free_busy_url);

	/*
	 * X-Custom (IM's + BlogFeed URL)
	 */
	assert_custom_field_in_list(icontact->custom_list, "", "BlogFeed", "");
	assert_list_length(icontact->custom_list, 1);

	/*
	 * Attached Multimedia Files
	 */
	assert(icontact->photo == NULL);

	/*
	 * Geolocation Fields
	 */
	assert_equal_double(DEGREE_NOT_SET, icontact->latitude);
	assert_equal_double(DEGREE_NOT_SET, icontact->longitude);

	/*
	 * Evolution Store
	 */

	assert_equal_gchar("BEGIN:VCARD\r\nVERSION:3.0\r\nX-EVOLUTION-VIDEO-URL:\r\nCALURI:\r\nX-MOZILLA-HTML:FALSE\r\nEND:VCARD",
	                   icontact->common->evolution_store);
}

void
validate_evolution_contact_3 (const I_contact *icontact, gint stage)
{
	log_bitmask(stage);

	/*
	 * Common Fields
	 */
	validate_icommon(icontact->common,
	                 NULL,
	                 "pas-id-4C73EAF900000001",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_NULL,
	                 NULL,
	                 "2010-08-24 16:01:39", stage);

	/*
	 * Name Fields
	 */
	assert_equal(NULL, icontact->last_name);
	assert_equal("Name", icontact->given_name);
	assert_equal(NULL, icontact->middle_names);
	assert_equal(NULL, icontact->prefix);
	assert_equal(NULL, icontact->suffix);
	assert_equal("Name", icontact->full_name);
	assert_equal(NULL, icontact->nick_name);

	/*
	 * Misc Fields
	 */
	assert_equal(NULL, icontact->spouse_name);
	assert_equal_gdate(NULL, icontact->birthday);
	assert_equal_gdate(NULL, icontact->anniversary);

	/*
	 * Emails
	 */
	assert_list_null(icontact->emails);

	/*
	 * Addresses
	 */
	assert_list_null(icontact->addresses);

	/*
	 * Phone Numbers
	 */
	assert_phone_in_list (icontact->phone_numbers, "Weiteres Fax", ICONTACT_PHONE_BUSINESS_FAX); /* Kolab does not have a type 'Other Fax' so it is mapped to business fax */
	assert_phone_in_list (icontact->phone_numbers, "Weiteres Tel", ICONTACT_PHONE_OTHER);
	assert_phone_in_list (icontact->phone_numbers, "Fax privat", ICONTACT_PHONE_HOME_FAX);
	assert_phone_in_list (icontact->phone_numbers, "Autotelefon", ICONTACT_PHONE_CAR);
	assert_phone_in_list (icontact->phone_numbers, "ISDN", ICONTACT_PHONE_ISDN);
	assert_phone_in_list (icontact->phone_numbers, "Firmentel", ICONTACT_PHONE_COMPANY);
	assert_phone_in_list (icontact->phone_numbers, "Tel Assistent", ICONTACT_PHONE_ASSISTANT);
	assert_phone_in_list (icontact->phone_numbers, "Rückruf Tel", ICONTACT_PHONE_CALLBACK);
	assert_list_length(icontact->phone_numbers, 8);

	/*
	 * Corporation Fields
	 */
	assert_equal(NULL, icontact->organization);
	assert_equal(NULL, icontact->department);
	assert_equal(NULL, icontact->office_location);
	assert_equal(NULL, icontact->job_title);
	assert_equal(NULL, icontact->profession);
	assert_equal(NULL, icontact->manager_name);
	assert_equal(NULL, icontact->assistant);

	/*
	 * Other Internet Communication Fields
	 */
	assert_equal(NULL, icontact->web_page);
	assert_equal(NULL, icontact->free_busy_url);

	/*
	 * X-Custom (IM's + BlogFeed URL)
	 */
	assert_custom_field_in_list(icontact->custom_list, "messaging/groupwise", "All", "GroupWise");
	assert_custom_field_in_list(icontact->custom_list, "messaging/xmpp", "All", "Jabber");
	assert_custom_field_in_list(icontact->custom_list, "messaging/gadu", "All", "Gadu-Gadu");
	assert_custom_field_in_list(icontact->custom_list, "messaging/skype", "All", "Skype");
	assert_custom_field_in_list(icontact->custom_list, "", "BlogFeed", "");
	assert_list_length(icontact->custom_list, 5);

	/*
	 * Attached Multimedia Files
	 */
	assert(icontact->photo == NULL);

	/*
	 * Geolocation Fields
	 */
	assert_equal_double(DEGREE_NOT_SET, icontact->latitude);
	assert_equal_double(DEGREE_NOT_SET, icontact->longitude);

	/*
	 * Evolution Store
	 */

	assert_equal_gchar("BEGIN:VCARD\r\nVERSION:3.0\r\nX-EVOLUTION-VIDEO-URL:\r\nCALURI:\r\nX-MOZILLA-HTML:FALSE\r\nEND:VCARD",
	                   icontact->common->evolution_store);
}

void
validate_evolution_contact_4 (const I_contact *icontact, gint stage)
{
	log_bitmask(stage);

	/*
	 * Common Fields
	 */
	validate_icommon(icontact->common,
	                 NULL,
	                 "pas-id-4C73EB8400000002",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_NULL,
	                 NULL,
	                 "2010-08-24 15:56:53", stage);

	/*
	 * Name Fields
	 */
	assert_equal(NULL, icontact->last_name);
	assert_equal("Name", icontact->given_name);
	assert_equal(NULL, icontact->middle_names);
	assert_equal(NULL, icontact->prefix);
	assert_equal(NULL, icontact->suffix);
	assert_equal("Name", icontact->full_name);
	assert_equal(NULL, icontact->nick_name);

	/*
	 * Misc Fields
	 */
	assert_equal(NULL, icontact->spouse_name);
	assert_equal_gdate(NULL, icontact->birthday);
	assert_equal_gdate(NULL, icontact->anniversary);

	/*
	 * Emails
	 */
	assert_list_null(icontact->emails);

	/*
	 * Addresses
	 */
	assert_list_null(icontact->addresses);

	/*
	 * Phone Numbers
	 */
	assert_phone_in_list (icontact->phone_numbers, "Pager", ICONTACT_PHONE_PAGER);
	assert_phone_in_list (icontact->phone_numbers, "Tel primär", ICONTACT_PHONE_PRIMARY);
	assert_phone_in_list (icontact->phone_numbers, "Funk", ICONTACT_PHONE_RADIO);
	assert_phone_in_list (icontact->phone_numbers, "Telex", ICONTACT_PHONE_TELEX);
	assert_phone_in_list (icontact->phone_numbers, "TTY", ICONTACT_PHONE_TTYTDD);
	assert_phone_in_list (icontact->phone_numbers, "Weiteres Tel2", ICONTACT_PHONE_OTHER);
	assert_phone_in_list (icontact->phone_numbers, "Weiteres Tel1", ICONTACT_PHONE_OTHER);
	assert_phone_in_list (icontact->phone_numbers, "Weiteres Tel3", ICONTACT_PHONE_OTHER);

	assert_list_length(icontact->phone_numbers, 8);

	/*
	 * Corporation Fields
	 */
	assert_equal(NULL, icontact->organization);
	assert_equal(NULL, icontact->department);
	assert_equal(NULL, icontact->office_location);
	assert_equal(NULL, icontact->job_title);
	assert_equal(NULL, icontact->profession);
	assert_equal(NULL, icontact->manager_name);
	assert_equal(NULL, icontact->assistant);

	/*
	 * Other Internet Communication Fields
	 */
	assert_equal(NULL, icontact->web_page);
	assert_equal(NULL, icontact->free_busy_url);

	/*
	 * X-Custom (IM's + BlogFeed URL)
	 */
	assert_custom_field_in_list(icontact->custom_list, "", "BlogFeed", "");
	assert_list_length(icontact->custom_list, 1);

	/*
	 * Attached Multimedia Files
	 */
	assert(icontact->photo == NULL);

	/*
	 * Geolocation Fields
	 */
	assert_equal_double(DEGREE_NOT_SET, icontact->latitude);
	assert_equal_double(DEGREE_NOT_SET, icontact->longitude);

	/*
	 * Evolution Store
	 */

	assert_equal_gchar("BEGIN:VCARD\r\nVERSION:3.0\r\nX-EVOLUTION-VIDEO-URL:\r\nCALURI:\r\nX-MOZILLA-HTML:FALSE\r\nEND:VCARD",
	                   icontact->common->evolution_store);
}

void
validate_evolution_contact_5 (const I_contact *icontact, gint stage)
{
	log_bitmask(stage);

	/*
	 * Common Fields
	 */
	validate_icommon(icontact->common,
	                 NULL,
	                 "pas-id-4CA9939100000000",
	                 "Στην επιστήμη της βιολογίας, με τον όρο εξέλιξη εννοείται η αλλαγή στις"
	                 " ιδιότητες ενός πληθυσμού οργανισμών στο πέρασμα του χρόνου, μεταξύ δια"
	                 "φορετικών γενεών[1]. Αν και τέτοιου τύπου μεταβολές παρατηρούνται σε μι"
	                 "κρή κλίμακα σε κάθε γενιά, μακροπρόθεσμα και αθροιστικά μπορούν να οδηγ"
	                 "ήσουν σε σημαντικές διαφοροποιήσεις στις ιδιότητες ενός οργανισμού, ώστ"
	                 "ε να οδηγήσουν τελικά στη δημιουργία νέων διακριτών ειδών (βλ. ειδογένε"
	                 "ση). Εξελικτικές θεωρούνται ειδικά οι αλλαγές που κληροδοτούνται μέσω τ"
	                 "ου γενετικού υλικού από γενιά σε γενιά, συνεπώς συνιστούν μια πληθυσμικ"
	                 "ή διαδικασία και διακρίνονται από άλλες, όπως η οντογένεση, ή γενικά η "
	                 "ανάπτυξη ενός οργανισμού ατομικά. Η εξέλιξη αποτελεί φαινόμενο που υλοπ"
	                 "οιείται σταδιακά και σε μεγάλο βάθος χρόνου. Με λίγες εξαιρέσεις[2], απ"
	                 "αιτείται το πέρασμα αρκετών γενεών για εξελικτικές αλλαγές μεγάλης κλίμ"
	                 "ακας, όπως για παράδειγμα η εξέλιξη των πτηνών από τα ερπετά. Λαμβάνει "
	                 "επίσης χώρα με διαφορετικούς ρυθμούς ανάλογα με το είδος και το περιβάλ"
	                 "λον του[3].",
	                 "զարգացում",
	                 ICOMMON_SENSITIVITY_NULL,
	                 NULL,
	                 "2010-10-04 08:42:57", stage);

	/*
	 * Name Fields
	 */
	assert_equal("подразумева", icontact->last_name);
	assert_equal("Еволуција", icontact->given_name);
	assert_equal("еволуција", icontact->middle_names);
	assert_equal("акумулирању", icontact->prefix);
	assert_equal("између", icontact->suffix);
	assert_equal("акумулирању Еволуција еволуција подразумева између", icontact->full_name);
	assert_equal("Էվոլյուցիա", icontact->nick_name);

	/*
	 * Misc Fields
	 */
	assert_equal("रचना", icontact->spouse_name);
	assert_equal_gdate(NULL, icontact->birthday);
	assert_equal_gdate(NULL, icontact->anniversary);

	/*
	 * Emails
	 */
	assert_list_null(icontact->emails);

	/*
	 * Addresses
	 */
	assert_address_in_list(icontact->addresses, ICONTACT_ADDR_TYPE_HOME,
	                       "ജീവികൾ സ്വയം പരിണമിച്ച് ലളിതഘടനയിൽ നിന്ന് സങ്കീർണഘടനയിലേക്കെത്തിച്ചേരുന്നു എന്ന് പരിണാമവാദം "
	                       "സിധാന്തിക്കുന്നു. ചാൾസ് ഡാർവിൻ ആണ്‌ ഈ സിദ്ധാന്തത്തിന്റെ ഉപജ്ഞാതാവ്.",
	                       "ശരിയാവണമെങ്കിൽ", "ജൈവപരിവർത്തനം", "ജീവികൾ", "", "ഭൂമിയിൽ");
	assert_list_length(icontact->addresses, 1);

	/*
	 * Phone Numbers
	 */
	assert_list_null(icontact->phone_numbers);

	/*
	 * Corporation Fields
	 */
	assert_equal("वातावरण", icontact->organization);
	assert_equal("समय", icontact->department);
	assert_equal("प्रजातियों", icontact->office_location);
	assert_equal("उत्पत्ति", icontact->job_title);
	assert_equal("क्रम-विकास", icontact->profession);
	assert_equal("अनुकूल", icontact->manager_name);
	assert_equal("विभिन्न", icontact->assistant);

	/*
	 * Other Internet Communication Fields
	 */
	assert_equal(NULL, icontact->web_page);
	assert_equal(NULL, icontact->free_busy_url);

	/*
	 * X-Custom (IM's + BlogFeed URL)
	 */
	assert_custom_field_in_list(icontact->custom_list, "", "BlogFeed", "");
	assert_list_length(icontact->custom_list, 1);

	/*
	 * Attached Multimedia Files
	 */
	assert(icontact->photo == NULL);

	/*
	 * Geolocation Fields
	 */
	assert_equal_double(DEGREE_NOT_SET, icontact->latitude);
	assert_equal_double(DEGREE_NOT_SET, icontact->longitude);

	/*
	 * Evolution Store
	 */

	assert_equal_gchar("BEGIN:VCARD\r\nVERSION:3.0\r\nX-EVOLUTION-VIDEO-URL:\r\nCALURI:\r\nX-MOZILLA-HTML:FALSE\r\nEND:VCARD",
	                   icontact->common->evolution_store);
}


void
validate_evolution_contact_b1 (const I_contact *icontact, gint stage)
{
	log_bitmask(stage);

	/*
	 * Common Fields
	 */
	validate_icommon(icontact->common,
	                 NULL,
	                 "20110330T111653Z-5957-1000-19106-0@christian-t60",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_NULL,
	                 NULL,
	                 NULL, stage);

	/*
	 * Name Fields
	 */
	assert_equal("Bar", icontact->last_name);
	assert_equal("Foo", icontact->given_name);
	assert_equal(NULL, icontact->middle_names);
	assert_equal(NULL, icontact->prefix);
	assert_equal(NULL, icontact->suffix);
	assert_equal("Foo Bar", icontact->full_name);
	assert_equal("foobar", icontact->nick_name);

	/*
	 * Misc Fields
	 */
	assert_equal(NULL, icontact->spouse_name);
	assert_equal_gdate(NULL, icontact->birthday);
	assert_equal_gdate(NULL, icontact->anniversary);

	/*
	 * Emails
	 */
	assert_list_null(icontact->emails);

	/*
	 * Addresses
	 */
	assert_list_null(icontact->addresses);

	/*
	 * Phone Numbers
	 */
	assert_list_null(icontact->phone_numbers);

	/*
	 * Corporation Fields
	 */
	assert_equal(NULL, icontact->organization);
	assert_equal(NULL, icontact->department);
	assert_equal(NULL, icontact->office_location);
	assert_equal(NULL, icontact->job_title);
	assert_equal(NULL, icontact->profession);
	assert_equal(NULL, icontact->manager_name);
	assert_equal(NULL, icontact->assistant);

	/*
	 * Other Internet Communication Fields
	 */
	assert_equal(NULL, icontact->web_page);
	assert_equal(NULL, icontact->free_busy_url);

	/*
	 * X-Custom (IM's + BlogFeed URL)
	 */
	assert_custom_field_in_list(icontact->custom_list, "", "BlogFeed", "");
	assert_list_length(icontact->custom_list, 1);

	/*
	 * Attached Multimedia Files
	 */
	assert(icontact->photo == NULL);

	/*
	 * Geolocation Fields
	 */
	assert_equal_double(DEGREE_NOT_SET, icontact->latitude);
	assert_equal_double(DEGREE_NOT_SET, icontact->longitude);

	/*
	 * Evolution Store
	 */

	assert_equal_gchar("BEGIN:VCARD\r\nVERSION:3.0\r\nX-EVOLUTION-VIDEO-URL:\r\nCALURI:\r\nX-MOZILLA-HTML:FALSE\r\nEND:VCARD",
	                   icontact->common->evolution_store);

}
