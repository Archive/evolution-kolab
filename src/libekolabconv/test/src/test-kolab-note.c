/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_kolab_note.c
 *
 *  Created on: 27.12.2010
 *      Author: Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include "../../main/src/kolab-conv.h"
#include "test-convert-setup.h"
#include "test-util.h"
#include "testbase.h"



void
validate_kolab_note_100(const I_note *inote, gint stage)
{
	g_assert(inote != NULL);
	g_assert(inote->common != NULL);

	validate_icommon(inote->common, "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.4BF7A3B46373482BBF3633FDF5CE1DA4",
	                 "Text\r\n123\r\n", NULL, ICOMMON_SENSITIVITY_PUBLIC, "2010-08-24 09:30:50", "2010-08-24 09:30:50", stage);

	assert_equal("Text", inote->summary);

	/* kolab store */
	g_assert(inote->common->kolab_store == NULL);

	/* kolab attachment store */

	g_assert_cmpint(g_list_length(inote->common->kolab_attachment_store), ==, 1);

	assert_binary_attachment_store_equal("src/libekolabconv/test/resources/imap/Notes/100.tnef.attach",
	                                     NULL, "application/x-outlook-tnef",
	                                     inote->common, 0);
}

void
validate_kolab_note_200(const I_note *inote, gint stage)
{
	GList *list = NULL;

	g_assert(inote != NULL);
	g_assert(inote->common != NULL);

	validate_icommon(inote->common, "KNotes 3.6, Kolab resource",
	                 "libkcal-659505129.557",
	                 IS_TEST_STAGE_INTERN_TO_EVO(stage) ? "\nText\n123\n" : "<html><head><meta name=\"qrichtext\" content=\"1\" /></head><body style=\"font-size:10pt;font-family:Sans Serif\">\n<p>Text</p>\n<p><span style=\"font-weight:600\">1</span><span style=\"font-style:italic\">2</span><span style=\"text-decoration:underline\">3</span></p>\n</body></html>\n",
	                 NULL, ICOMMON_SENSITIVITY_PUBLIC, "2010-08-24 13:41:27", "2010-08-24 13:42:25", stage);

	assert_equal("Name", inote->summary);

	/* kolab store */
	g_assert(inote->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(inote->common, inote->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "foreground-color");
	validate_kolab_store_xml(list, 1, "background-color");
}
