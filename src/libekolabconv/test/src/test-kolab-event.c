/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_kolab_event.c
 *
 *  Created on: 16.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include "test-convert-setup.h"
#include "test-util.h"
#include "testbase.h"


void
validate_kolab_event_100(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.132E74C769684382891031D889490CC8",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:25:20",
	                 "2010-08-23 08:25:55",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "Betreff",
	                    "Ort",
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-24 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");

	/* kolab attachment store */

	g_assert_cmpint(g_list_length(ievent->incidence->common->kolab_attachment_store), ==, 1);

	assert_binary_attachment_store_equal("src/libekolabconv/test/resources/imap/Calendar/100.tnef.attach",
	                                     NULL, "application/x-outlook-tnef",
	                                     ievent->incidence->common, 0);
}

void
validate_kolab_event_101(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.087637891D644410BF210F577C90DA1A",
	                 "Text\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:29:37",
	                 "2010-08-23 08:29:37",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "Betreff",
	                    "Ort",
	                    NULL,
	                    NULL,
	                    "2010-08-25 07:00:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Hendrik Helwich (h.helwich@tarent.de)",
	                        "h.helwich@tarent.de",
	                        I_INC_STATUS_NONE,
	                        TRUE,
	                        TRUE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	/* event */
	assert_equal_timestamp("2010-08-26 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 6);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "last-action");
	validate_kolab_store_xml(list, 2, "last-action-date");
	validate_kolab_store_xml(list, 3, "update-count");
	validate_kolab_store_xml(list, 4, "owner-event-id");
	validate_kolab_store_xml(list, 5, "creator");
}

void
validate_kolab_event_102(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.B59E888AB0D84F5BA1BAD43BEEC15FC2",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:31:50",
	                 "2010-08-23 08:34:40",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "Betreff",
	                    "Ort",
	                    NULL,
	                    NULL,
	                    "2010-08-23",
	                    stage);

	/* event */
	assert_equal_gdate("2010-08-23", ievent->end_date->date);
	g_assert(SHOW_TIME_AS_FREE == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_103(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.3FF30815C6534CC1B04231E7E1B84DE0",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:34:15",
	                 "2010-08-23 08:34:39",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "Betreff",
	                    "Ort",
	                    NULL,
	                    NULL,
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-23 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_FREE == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_104(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.99E4CF1408D64CD9BE391B3B1BA433E1",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:34:43",
	                 "2010-08-23 08:36:59",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-23 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);	/* I_INC_STATUS_TENTATIVE not available in Kontact and Evolution */

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_105(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.3B4BE7A2FD344DF8876CA05F1A33DA44",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:34:57",
	                 "2010-08-23 08:36:59",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-23 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);	/* SHOW_TIME_AS_OUT_OF_OFFICE not available in Kontact and Evolution */

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_106(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.90A2920B92BF4FAF83DC9CEB3B7B84C8",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:35:58",
	                 "2010-08-23 08:36:57",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-23 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_107(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.094C43511FE84231AFF0DA64A0F3D572",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:37:30",
	                 "2010-08-23 08:38:42",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-23 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_108(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.CF7182FCA75645D3A819F2E18B9DE739",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:38:24",
	                 "2010-08-23 08:38:24",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-23 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_109(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.114120AC5BD141FDB8117DF70AB5FAED",
	                 "\r\n",
	                 "Blaue Kategorie",
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:43:04",
	                 "2010-08-23 08:43:04",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_110(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.8277A45E2B1D4A448773C0B2E326C0A2",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:38:35",
	                 "2010-08-23 08:44:02",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 07:00:00",
	                    stage);

	/* event */
	assert_equal_timestamp("2010-08-23 07:30:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_111(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.E9FEA7AA3F0544D6A8FEA3C7C8D70F6B",
	                 "\r\n",
	                 "Lila Kategorie,Gelbe Kategorie,Blaue Kategorie",
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:44:05",
	                 "2010-08-23 08:44:05",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_112(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.925E577D43C64BF0A203F73FEB4DD757",
	                 "Text\r\n123\r\nabc\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 08:44:39",
	                 "2010-08-23 08:44:39",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_113(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.A3BC0405B1A949D7849CCD093139805D",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PRIVATE,
	                 "2010-08-23 08:45:53",
	                 "2010-08-23 08:45:53",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_114(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.EC02464D4C9B42CE9C69B8580BDA76D9",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:25:41",
	                 "2010-08-23 09:25:41",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_115(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.61A1257E11404AAC84D7108B5648F978",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:26:29",
	                 "2010-08-23 09:26:29",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_DAILY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_116(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.48D03B223AEC4BBE83F5330B57A63912",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:27:16",
	                 "2010-08-23 09:27:16",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_DAILY,
	                    3,
	                    0,
	                    NULL,
	                    0,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_117(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.0F9627DA8AB14AC794730E1608BA4932",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:28:03",
	                 "2010-08-23 09:28:03",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_DAILY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY | I_COMMON_TUESDAY | I_COMMON_WEDNESDAY | I_COMMON_THURSDAY | I_COMMON_FRIDAY,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_118(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.50BCC1BD7F2942A49EB9A185C5DBDC4E",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:30:29",
	                 "2010-08-23 09:30:29",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-24 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    2,
	                    0,
	                    NULL,
	                    I_COMMON_TUESDAY | I_COMMON_THURSDAY | I_COMMON_SUNDAY,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-24 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_119(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.F84F22C2458F4D928F37677231CBB026",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:31:13",
	                 "2010-08-23 09:31:13",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    23,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_120(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.2A18DA5FB8844E388EA9C9D570C5B7E6",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:32:11",
	                 "2010-08-23 09:32:11",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-31 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER,
	                    3,
	                    0,
	                    NULL,
	                    0,
	                    31,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-31 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_121(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.73CB404DC1B3494083891CB6C909BB23",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:35:08",
	                 "2010-08-23 09:35:08",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-28 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER,
	                    2,
	                    0,
	                    NULL,
	                    0,
	                    28,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-28 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_122(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.3F7EF2D43F8C4E48B8E81510FAC83775",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:36:53",
	                 "2010-08-23 09:36:53",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-24 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY,
	                    2,
	                    0,
	                    NULL,
	                    I_COMMON_TUESDAY,
	                    4,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-24 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_123(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.01609FAA3B154E168ADB4C242781C110",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:37:34",
	                 "2010-08-23 09:37:34",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-28 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY,
	                    3,
	                    0,
	                    NULL,
	                    I_COMMON_SATURDAY,
	                    5,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-28 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_124(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.8CCB5B6F56934CA6863C7A17C820538D",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:39:19",
	                 "2010-08-23 09:39:19",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    23,
	                    I_COMMON_AUG);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_125(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.92A024C2D37E4F2097034A6C7E99B440",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:40:51",
	                 "2010-08-23 09:40:51",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2011-02-28 11:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    28,
	                    I_COMMON_FEB);

	/* event */
	assert_equal_timestamp("2011-02-28 12:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_126(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.1EBA0394588747AD92DA11E56C85FE44",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:42:27",
	                 "2010-08-23 09:42:27",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2011-08-04 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY | I_COMMON_TUESDAY | I_COMMON_WEDNESDAY | I_COMMON_THURSDAY | I_COMMON_FRIDAY | I_COMMON_SATURDAY | I_COMMON_SUNDAY,
	                    4,
	                    I_COMMON_AUG);

	/* event */
	assert_equal_timestamp("2011-08-04 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/*
	  <recurrence cycle="yearly" type="weekday">
	  <interval>1</interval>
	  <day>monday</day>
	  <day>tuesday</day>
	  <day>wednesday</day>
	  <day>thursday</day>
	  <day>friday</day>
	  <day>saturday</day>
	  <day>sunday</day>
	  <daynumber>4</daynumber>
	  <month>august</month>
	  <range type="none"/>
	  </recurrence>

	  RRULE:FREQ=YEARLY;BYDAY=MO,TU,WE,TH,FR,SA,SU;BYMONTHDAY=4;BYMONTH=8
	*/

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_127(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.9960E30147D0425A82514E061A8A94C8",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:43:33",
	                 "2010-08-23 09:43:33",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2011-08-04 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY | I_COMMON_TUESDAY | I_COMMON_WEDNESDAY | I_COMMON_THURSDAY | I_COMMON_FRIDAY,
	                    4,
	                    I_COMMON_AUG);

	/* event */
	assert_equal_timestamp("2011-08-04 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_128(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.68714769A05F40F08A491E5E72F5F633",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:44:07",
	                 "2010-08-23 09:44:07",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2011-08-14 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_SUNDAY | I_COMMON_SATURDAY,
	                    4,
	                    I_COMMON_AUG);

	/* event */
	assert_equal_timestamp("2011-08-14 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_129(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.0033AB320BDA4918A559E29A7DB61CF8",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:44:43",
	                 "2010-08-23 09:44:43",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-29 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_SUNDAY,
	                    5,
	                    I_COMMON_AUG);

	/* event */
	assert_equal_timestamp("2010-08-29 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_130(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.D3610D9851A341B7992AF1904607B520",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:45:26",
	                 "2010-08-23 09:45:26",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2011-02-28 11:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_SUNDAY | I_COMMON_MONDAY | I_COMMON_TUESDAY | I_COMMON_WEDNESDAY | I_COMMON_THURSDAY | I_COMMON_FRIDAY | I_COMMON_SATURDAY,
	                    5,
	                    I_COMMON_FEB);

	/* event */
	assert_equal_timestamp("2011-02-28 12:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_131(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.64D402213A924BF39811455D36BDE67A",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:47:45",
	                 "2010-08-23 09:47:45",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2011-08-01 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_SUNDAY | I_COMMON_MONDAY | I_COMMON_TUESDAY | I_COMMON_WEDNESDAY | I_COMMON_THURSDAY | I_COMMON_FRIDAY | I_COMMON_SATURDAY,
	                    1,
	                    I_COMMON_AUG);

	/* event */
	assert_equal_timestamp("2011-08-01 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_132(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.6B121CBF922C41F0AC5E91B56EAA4610",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:48:35",
	                 "2010-08-23 09:48:35",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2011-08-01 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY | I_COMMON_TUESDAY | I_COMMON_WEDNESDAY | I_COMMON_THURSDAY | I_COMMON_FRIDAY,
	                    1,
	                    I_COMMON_AUG);

	/* event */
	assert_equal_timestamp("2011-08-01 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_133(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.67BBC9B188794A27968E490FC381CD38",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:50:43",
	                 "2010-08-23 09:50:43",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-11-28 11:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_SUNDAY | I_COMMON_SATURDAY,
	                    5,
	                    I_COMMON_NOV);

	/* event */
	assert_equal_timestamp("2010-11-28 12:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_134(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.368CA0DA11DC4F759A1F7670AFC8BCD3",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:52:50",
	                 "2010-08-23 09:52:50",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-31 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY,
	                    2,
	                    0,
	                    NULL,
	                    I_COMMON_TUESDAY,
	                    5,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-31 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_135(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.90650AA582394CDFB0B3D090A1A7A30C",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:54:06",
	                 "2010-08-23 09:54:06",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-12-27 11:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    10,
	                    NULL,
	                    1,
	                    4,
	                    I_COMMON_DEC);

	/* event */
	assert_equal_timestamp("2010-12-27 12:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_136(const I_event *ievent, gint stage)
{
	gint rdate[3] = {25,10,2020};
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.A864570513B049FDB74C7295055FAAF4",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:54:49",
	                 "2010-08-23 09:54:49",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    0,
	                    rdate,
	                    I_COMMON_MONDAY,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_137(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.7B3E79BD41E047BE8EAF573F72A3C3BF",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:55:35",
	                 "2010-08-23 09:55:35",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* event */
	assert_equal_timestamp("2010-08-23 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_138(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.009A32D1BD574FC681F9E6FBA499BB35",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 09:57:48",
	                 "2010-08-23 09:57:48",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-30 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-30 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_139(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.BC1711428F1646098131D3943241B780",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 10:01:45",
	                 "2010-08-23 10:01:45",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-10-11 10:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-10-11 11:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}

void
validate_kolab_event_140(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.908D4D5690AD48148FB6ECDB41A0499D",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 10:03:29",
	                 "2010-08-23 10:03:29",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-23 12:30:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_MONDAY,
	                    0,
	                    0);

	/* event */
	assert_equal_timestamp("2010-08-23 16:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "color-label");
	validate_kolab_store_xml(list, 1, "creator");
}


void
validate_kolab_event_201(const I_event *ievent, gint stage)
{
	GList *list = NULL;
	gint llen;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-414546177.646",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 14:13:01",
	                 "2010-08-24 14:13:01",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "event_k1",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24 08:00:00",
	                    stage);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Hendrik Helwich",
	                        "h.helwich@tarent.de",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	/* event */
	assert_equal_timestamp("2010-08-24 10:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	llen = g_list_length(list);
	g_assert_cmpint(llen, ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}

void
validate_kolab_event_202(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1478387587.1025",
	                 "Text\n123",
	                 "Anwesenheit,Besonderer Anlass,Besprechung,Feiertag,Geburtstag,Geschäftlich,Geschäftliches,Persönlich,Reise,Schulung,Sprintprozess,Telefonanruf,Termin,Treffen,Urlaub,Verschiedenes",
	                 ICOMMON_SENSITIVITY_PRIVATE,
	                 "2010-08-24 14:14:33",
	                 "2010-08-24 14:14:33",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "event_k2",
	                    "Adresse",
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24 08:00:00",
	                    stage);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Hendrik Helwich",
	                        "h.helwich@tarent.de",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	/* event */
	assert_equal_timestamp("2010-08-26 10:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_FREE == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}

void
validate_kolab_event_203(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1971934702.871",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 14:15:44",
	                 "2010-08-24 14:15:44",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "event_k3",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-26",
	                    stage);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Hendrik Helwich",
	                        "h.helwich@tarent.de",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	/* event */
	assert_equal_gdate("2010-08-27", ievent->end_date->date);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}

void
validate_kolab_event_204(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);
	/* TODO: Incidence: not handled elements: <advanced-alarms><enabled> */

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-503971337.955",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 14:17:42",
	                 "2010-08-24 14:17:42",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "event_k4",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24 08:00:00",
	                    stage);

	/* advanced alarms */

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     900,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     -900,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* event */
	assert_equal_timestamp("2010-08-24 10:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");

	/* kolab attachment store */

	g_assert_cmpint(g_list_length(ievent->incidence->common->kolab_attachment_store), ==, 1);

	assert_binary_attachment_store_equal(
	                                     "src/libekolabconv/test/resources/imap/Calendar/204.png.attach",
	                                     "anhang1", "image/png",
	                                     ievent->incidence->common, 0);
}

void
validate_kolab_event_205(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1839033818.373",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 14:21:57",
	                 "2010-08-24 14:21:57",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "event_k5",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24 08:00:00",
	                    stage);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Vorname1 Nachname1",
	                        "Name1@example.net",
	                        I_INC_STATUS_NONE,
	                        TRUE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Vorname2 Nachname2",
	                        "Name2@example.net",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_OPTIONAL, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Vorname3 Nachname3",
	                        "Name3@example.net",
	                        I_INC_STATUS_DECLINED,
	                        TRUE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_RESOURCE, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Vorname4 Nachname4",
	                        "Name4@example.net",
	                        I_INC_STATUS_TENTATIVE,
	                        TRUE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Vorname5 Nachname5",
	                        "Name5@example.net",
	                        I_INC_STATUS_DELEGATED,
	                        TRUE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Vorname6 Nachname6",
	                        "Name6@example.net",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Vorname7 Nachname7",
	                        "Name7@example.net",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	/* event */
	assert_equal_timestamp("2010-08-24 10:00:00", ievent->end_date->date_time);
	g_assert(SHOW_TIME_AS_BUSY == ievent->show_time_as);

	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}

void
validate_kolab_event_206(const I_event *ievent, gint stage)
{
	GList *list;

	log_bitmask(stage);
	/* same as 205 with additional kolab store unknown elements
	 * only test kolab store here
	 */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* get top level unknown elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 4);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
	validate_kolab_store_xml(list, 1, "unknown_root1");
	validate_kolab_store_xml(list, 2, "unknown_root2");
	validate_kolab_store_xml(list, 3, "unknown_root3");

	list = kolab_store_get_element_list(ievent->incidence->common, (gpointer) KOLAB_STORE_PTR_INC_ORGANIZER);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "unknown_org");

	list = kolab_store_get_element_list(ievent->incidence->common, (gpointer) KOLAB_STORE_PTR_INC_RECURRENCE);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "unknown_recurr");
}

void
validate_kolab_event_207(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/* yearly recurrence is mapped to monthly recurrence every 12 Months */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,	/* Recurrence_cycle	*/
	                    1,				/* interval		*/
	                    -1,				/* range_number		*/
	                    NULL,				/* range_date		*/
	                    4,				/* weekdays		*/
	                    4,				/* day_number		*/
	                    I_COMMON_JAN);			/* month		*/
}

void
validate_kolab_event_208(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/* recurrence cannot be handled by evolution */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_YEARDAY,	/* Recurrence_cycle	*/
	                    1,				/* interval		*/
	                    0,				/* range_number		*/
	                    NULL,				/* range_date		*/
	                    0,				/* weekdays		*/
	                    29,				/* day_number		*/
	                    I_COMMON_MONTH_NULL);		/* month		*/
}

void
validate_kolab_event_500(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 4.4.2, Kolab resource",
	                 "libkcal-921351035.297",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-07-30 16:33:17",
	                 "2010-07-30 16:34:51",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "bumsfallera",
	                    "hier",
	                    "Christian Hilberg",
	                    "hilberg@kernelconcepts.de",
	                    "2010-07-30 09:30:00",
	                    stage);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        NULL,
	                        "silvan@kobalt.kc.local",
	                        I_INC_STATUS_NONE,
	                        TRUE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(ievent->incidence->attendee,
	                        "Christian Hilberg",
	                        "hilberg@kernelconcepts.de",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);
}



void
validate_kolab_event_501(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100115.1075236), Kolab resource",
	                 "libkcal-1477131994.487",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2011-03-16 08:57:00",
	                 "2011-03-16 08:57:00",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "erinnerunge audio",
	                    NULL,
	                    "James Kirk",
	                    "jkirk@tarent.de",
	                    "2011-03-16",
	                    stage);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        "James Kirk",
	                        "jkirk@tarent.de",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_AUDIO,
	                     -15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);
}

void
validate_kolab_event_502(const I_event *ievent, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100115.1075236), Kolab resource",
	                 "libkcal-1356975605.58",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2011-03-16 13:24:00",
	                 "2011-03-16 13:24:00",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "Erinnerung Datei ohne Anhang",
	                    NULL,
	                    "James Kirk",
	                    "jkirk@tarent.de",
	                    "2011-03-15",
	                    stage);

	/* attendees */
	assert_attendee_in_list(ievent->incidence->attendee,
	                        "James Kirk",
	                        "jkirk@tarent.de",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,	/* TODO: Incidence: <invitation-sent> must be false, runs anyway when its true */
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_PROCEDURE,
	                     0,
	                     15,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);
}

void
validate_kolab_event_503(const I_event *ievent, gint stage)
{
	Alarm *mail_alarm = NULL;

	(void)stage;

	/* advanced alarms */
	g_assert_cmpint(g_list_length(ievent->incidence->advanced_alarm), ==, 2);

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     15,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* this is a mapped simple alarm from kolab */
	mail_alarm = assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                                  I_ALARM_TYPE_EMAIL,
	                                  -15,
	                                  0,
	                                  0,
	                                  0,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL);

	g_assert(mail_alarm->email_param);
	g_assert_cmpint(g_list_length(mail_alarm->email_param->addresses), ==, 1);
	g_assert_cmpstr(((GString*)mail_alarm->email_param->addresses->data)->str, ==, "ialexa@tarent.de");

}


void
validate_kolab_event_504(const I_event *ievent, gint stage)
{
	Alarm *mail_alarm = NULL;

	(void)stage;

	/* advanced alarms */
	g_assert_cmpint(g_list_length(ievent->incidence->advanced_alarm), ==, 2);

	/* this is a mapped simple alarm from kolab */
	assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     99,
	                     0,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* this is a mapped simple alarm from kolab */
	mail_alarm = assert_Alarm_in_list(ievent->incidence->advanced_alarm,
	                                  I_ALARM_TYPE_DISPLAY,
	                                  -99,
	                                  0,
	                                  0,
	                                  0,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL,
	                                  NULL);
	(void)mail_alarm;
}

void
validate_kolab_event_505(const I_event *ievent, gint stage)
{
	(void)stage;

	/* TEST TEMPORARILY DEACTIVATED */
	g_debug ("%s()[%u]: TEST TEMPORARILY DEACTIVATED!",
	         __func__, __LINE__);
	return;	

	validate_link_attachments(ievent->incidence->common,
	                          "file:///home/hhelwi/attach_test/penguin.png",
	                          "file:///home/hhelwi/attach_test/bluefish.png", NULL);

	validate_inline_attachment_names(ievent->incidence->common,
	                                 "bluefish.png", "penguin.png", NULL);

	assert_binary_attachment_store_equal("src/libekolabconv/test/resources/imap/Calendar/505.bluefish.png",
	                                     "bluefish.png", "image/png",
	                                     ievent->incidence->common, 0);

	assert_binary_attachment_store_equal("src/libekolabconv/test/resources/imap/Calendar/505.clanbomber.png",
	                                     "bomb.png", "image/png",
	                                     ievent->incidence->common, 1);

	assert_binary_attachment_store_equal("src/libekolabconv/test/resources/imap/Calendar/505.package_toys.png",
	                                     "package_toys.png", "image/png",
	                                     ievent->incidence->common, 2);

	assert_binary_attachment_store_equal("src/libekolabconv/test/resources/imap/Calendar/505.penguin.png",
	                                     "penguin.png", "image/png",
	                                     ievent->incidence->common, 3);
}

void
validate_kolab_event_506(const I_event *ievent, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(ievent->incidence->common,
	                 "Evolution/libekolabconv",
	                 "999463355",
	                 NULL,
	                 "Feiertag",
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-07-30 14:15:10",
	                 "2010-07-30 14:15:10",
	                 stage);

	/* incidence */
	validate_iincidence(ievent->incidence,
	                    "Tag der Arbeit",
	                    NULL,
	                    "Silvan Marco Fin",
	                    "silvan@kobalt.kc.local",
	                    "2009-05-01",
	                    stage);

	/* recurrence */
	validate_recurrence(ievent->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY,	/* Recurrence_cycle	*/
	                    1,				/* interval		*/
	                    0,				/* range_number		*/
	                    NULL,				/* range_date		*/
	                    0,				/* weekdays		*/
	                    1,				/* day_number		*/
	                    I_COMMON_MAY);			/* month		*/

	/* event */
	assert_equal_gdate("2009-05-01", ievent->end_date->date);


	/* kolab store */
	g_assert(ievent->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(ievent->incidence->common, ievent->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 0);
}
