/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_kolab_task.c
 *
 *  Created on: 21.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include "test-convert-setup.h"
#include "test-util.h"
#include "../../main/src/kolab-conv.h"
#include "../../main/src/kolab/kolab-util.h"
#include "testbase.h"


void
validate_kolab_task_100(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.BD18A4EC0466435FB0B5C71E0D70677C",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 13:28:30",
	                 "2010-08-23 13:28:30",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task1",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);

	/* kolab attachment store */

	g_assert_cmpint(g_list_length(itask->incidence->common->kolab_attachment_store), ==, 1);

	assert_binary_attachment_store_equal("src/libekolabconv/test/resources/imap/Tasks/100.tnef.attach",
	                                     NULL, "application/x-outlook-tnef",
	                                     itask->incidence->common, 0);
}

void
validate_kolab_task_101(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.DC06829AFDA34005B808D375C0143DC4",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 13:29:46",
	                 "2010-08-23 13:29:46",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task2",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(1, priority_xkcal_to_k(itask->priority));
	assert_equal_int(1, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_IN_PROGRESS == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_102(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.CB6B1047C0644A7988056CE7E96830FD",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 14:33:30",
	                 "2010-08-23 14:51:25",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task3",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(5, priority_xkcal_to_k(itask->priority));
	assert_equal_int(9, itask->priority);
	assert_equal_int(100, itask->completed);
	g_assert(I_TASK_COMPLETED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_103(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.396476894362478781A6D65BBE8547E0",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 14:34:39",
	                 "2010-08-23 14:46:58",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task4",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(25, itask->completed);
	g_assert(I_TASK_WAITING_ON_SOMEONE_ELSE == itask->status || I_TASK_NOT_STARTED == itask->status); /* no corresponding value available, mapped to I_TASK_NOT_STARTED */

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_104(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.6B82E435283F4373BBA360C4AE608D82",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 14:35:14",
	                 "2010-08-23 14:46:51",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task5",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(75, itask->completed);
	g_assert(I_TASK_DEFERRED == itask->status || I_TASK_NOT_STARTED == itask->status); /* no corresponding value available, mapped to I_TASK_NOT_STARTED */

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_105(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.3954FA1FE6B44B8C916D3BFB44C5BA2E",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 14:49:09",
	                 "2010-08-23 14:49:09",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task6",
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-25",
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-27", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_106(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.E9A3CC6AB743483E908B81FCBE8FFA5A",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 14:53:17",
	                 "2010-08-23 15:03:50",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task7",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/*  unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "reminder-time");

	/* TODO: Consider if toltec's <reminder-time> should/can be mapped in some way, actually saved to kolab store
	 *       <reminder-time>2010-08-17T06:30:00Z</reminder-time>
	 */
}


void
validate_kolab_task_107(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.F9358F55D8FD475886974C50CA2C1CF1",
	                 "Text\r\n123\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 14:53:50",
	                 "2010-08-23 14:53:50",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task8",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_108(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.34F258EA2D9844BC9F51C3D4EC54F591",
	                 "\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PRIVATE,
	                 "2010-08-23 14:56:00",
	                 "2010-08-23 14:56:00",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task9",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_109(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO: Still problem with lost blank in body in test 4
	 * common
	 */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.4808AA62322C49B5BB781EA026E36DB0",
	                 IS_TEST_STAGE_EVO_TO_FILE(stage) ? "\r\n" : " \r\n", /* XXX Space lost during conversion. */
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 08:29:38",
	                 "2010-08-24 08:29:38",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task11",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_110(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO: Still problem with lost blank in body in test 4
	 * common
	 */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.F08D21AF4C8A4F4FB77968AF672F2A42",
	                 IS_TEST_STAGE_EVO_TO_FILE(stage) ? "\r\n" : " \r\n", /* XXX Space lost during conversion. */
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 08:35:08",
	                 "2010-08-24 08:35:08",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task12",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_111(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.C6FA75F707E74116B563FBCBB92ADC4C",
	                 "\r\n\r\nMaren und Max Mustermann\r\nMusterstr. 8, 12345 Musterstadt, Deutschland\r\nTel.: +49 333 88888, Fax: +49 333 88999\r\nInternet: www.example.com  E-Mail: mustermann@example.com\r\n\r\n\r\n\r\n",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 08:37:33",
	                 "2010-08-24 08:37:33",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task13",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_112(const I_task *itask, gint stage)
{
	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Toltec Connector Open Format (2.0)",
	                 "TOLTEC.EF3F44A545B04349AC252E027EF6B4FA",
	                 "\r\n",
	                 "Blaue Kategorie,Gelbe Kategorie,Grüne Kategorie",
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-23 14:59:20",
	                 "2010-08-23 14:59:20",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task10",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store == NULL);
}


void
validate_kolab_task_201(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-564685637.860",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 11:44:55",
	                 "2010-08-24 11:44:55",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k1",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_202(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-2048156149.312",
	                 "Text\n123",
	                 "Anwesenheit,Besonderer Anlass,Besprechung,Feiertag,Geburtstag,Gesch\303\244ftlich,Gesch\303\244ftliches,Pers\303\266nlich,Reise,Schulung,Sprintprozess,Telefonanruf,Termin,Treffen,Urlaub,Verschiedenes",
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 11:47:48",
	                 "2010-08-24 11:47:48",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k2",
	                    "Adresse",
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_203(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-57372580.449",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PRIVATE,
	                 "2010-08-24 11:52:26",
	                 "2010-08-24 11:52:26",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k3",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24",
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_204(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-475965571.406",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_CONFIDENTIAL,
	                 "2010-08-24 11:53:13",
	                 "2010-08-24 11:53:13",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k4",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24 11:52:00",
	                    stage);

	/* task */
	assert_equal_int(1, priority_xkcal_to_k(itask->priority));
	assert_equal_int(1, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_205(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-2073502845.847",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 11:54:03",
	                 "2010-08-24 11:54:03",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k5",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24 11:53:00",
	                    stage);

	/* task */
	assert_equal_int(5, priority_xkcal_to_k(itask->priority));
	assert_equal_int(9, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_timestamp("2010-08-31 11:53:00", itask->due_date->date_time);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_206(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1845750769.323",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 11:54:53",
	                 "2010-08-24 11:54:53",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k6",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(50, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_207(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-2105009129.534",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 11:59:26",
	                 "2010-08-24 11:59:26",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k7",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(100, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");

	assert_equal_timestamp("2010-08-24 11:55:01", itask->completed_datetime);
}


void
validate_kolab_task_208(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);
	/* TODO: Incidence: not handled elements: <advanced-alarms><enabled> */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-875220073.856",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:12:07",
	                 "2010-08-24 12:12:07",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k8",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* advanced alarms */
	assert_Alarm_in_list(itask->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     0,
	                     -15,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");

	/* example Alarm =========================================================== */
#if 0
	assert_Alarm_in_list(itask->incidence->advanced_alarm,
	                     I_ALARM_TYPE_EMAIL,
	                     0, 0, 0, 0,
	                     NULL, NULL, NULL, NULL,
	                     "dfgdfgdfg", "sdgdfgdfg");
#endif
	/* example Alarm with email_param =========================================== */
#if 0
	Alarm *alarm;
	alarm = assert_Alarm_in_list(itask->incidence->advanced_alarm,
	                             I_ALARM_TYPE_EMAIL,
	                             0, 0, 0, 0,
	                             NULL, NULL, NULL, NULL,
	                             "dfgdfgdfg", "sdgdfgdfg");
	if (alarm != NULL){
		assert_String_in_list(alarm->email_param->addresses, "sadfsdfsdf");
		assert_String_in_list(alarm->email_param->addresses, "sdf");
		assert_String_in_list(alarm->email_param->attachments, "/path/to/a/scary-picture.jpg");
		assert_String_in_list(alarm->email_param->attachments, "/path/to/another/scary-picture.png");
	}
#endif
}


void
validate_kolab_task_209(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);
	/* TODO: Incidence: not handled elements: <advanced-alarms><enabled> */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-65962915.813",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:12:58",
	                 "2010-08-24 12:12:58",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k9",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	assert_Alarm_in_list(itask->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     0,
	                     -900,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_210(const I_task *itask, gint stage)
{
	GList *list;

	log_bitmask(stage);
	/* TODO: Incidence: not handled elements: <advanced-alarms><enabled> */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-981829110.767",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:14:15",
	                 "2010-08-24 12:14:15",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k10",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	assert_Alarm_in_list(itask->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     0,
	                     -21600,
	                     0,
	                     0,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_211(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1488850974.420",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:17:11",
	                 "2010-08-24 12:17:11",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k11",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_212(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-959617369.362",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:17:48",
	                 "2010-08-24 12:17:48",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k12",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level element */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_213(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-31817.817",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:27:59",
	                 "2010-08-24 12:27:59",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k13",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* attendees */
	assert_attendee_in_list(itask->incidence->attendee,
	                        "Vorname Nachname",
	                        "em@il",
	                        I_INC_STATUS_NONE,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Vorname2 Nachname2",
	                        "em@il2",
	                        I_INC_STATUS_ACCEPTED,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_OPTIONAL, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Vorname3 Nachname3",
	                        "em@il3",
	                        I_INC_STATUS_DECLINED,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_RESOURCE, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Vorname4 Nachname4",
	                        "em@il4",
	                        I_INC_STATUS_TENTATIVE,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Vorname5 Nachname5",
	                        "em@il5",
	                        I_INC_STATUS_DELEGATED,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Vorname6 Nachname6",
	                        "em@il6",
	                        I_INC_STATUS_ACCEPTED,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Vorname7 Nachname7",
	                        "em@il7",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_UNDEFINED);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_214(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1992675640.644",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:31:12",
	                 "2010-08-24 12:31:12",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k14",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_DAILY,
	                    3,
	                    0,
	                    NULL,
	                    0,
	                    0,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_debug("kolabstore list length: %i", g_list_length(list));
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_215(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-557133945.485",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:32:18",
	                 "2010-08-24 12:32:18",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k15",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    2,
	                    0,
	                    NULL,
	                    I_COMMON_TUESDAY | I_COMMON_THURSDAY | I_COMMON_SUNDAY,
	                    0,
	                    0);

	g_assert(itask->incidence->common->kolab_store != NULL);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_216(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1813144776.207",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:34:06",
	                 "2010-08-24 12:34:06",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k16",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER,
	                    2,
	                    0,
	                    NULL,
	                    0,
	                    24,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_217(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1099303233.392",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:34:30",
	                 "2010-08-24 12:34:30",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k17",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    31,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_218(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1899014988.736",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:35:51",
	                 "2010-08-24 12:35:51",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k18",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    -1,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_219(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1604395120.121",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:36:49",
	                 "2010-08-24 12:36:49",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k19",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_DAYNUMBER,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    -5,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_220(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1644058209.390",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:37:51",
	                 "2010-08-24 12:37:51",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k20",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_WEDNESDAY,
	                    3,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_221(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1721553616.1041",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:38:27",
	                 "2010-08-24 12:38:27",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k21",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY,
	                    2,
	                    0,
	                    NULL,
	                    I_COMMON_TUESDAY,
	                    -1,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_222(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1644716785.216",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:39:24",
	                 "2010-08-24 12:39:24",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k22",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_MONTHLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_FRIDAY,
	                    -5,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_223(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1609928323.626",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:43:29",
	                 "2010-08-24 12:43:29",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k23",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24 10:30:00",
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY,
	                    2,
	                    0,
	                    NULL,
	                    0,
	                    24,
	                    I_COMMON_AUG);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_224(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-674787867.159",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:44:31",
	                 "2010-08-24 12:44:31",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k24",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-02-28 12:44:31",
	                    stage);


	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    28,
	                    I_COMMON_FEB);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_225(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-98450016.195",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:47:03",
	                 "2010-08-24 12:47:03",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k25",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
#if 0
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    Tuesday,
	                    4,
	                    I_COMMON_MAR);
#endif

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);

	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	assert_list_length(list, 2);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
	validate_kolab_store_xml(list, 1, "recurrence");
}


void
validate_kolab_task_226(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-415677569.335",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:48:04",
	                 "2010-08-24 12:48:04",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k26",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
#if 0
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    Friday,
	                    5,
	                    I_COMMON_APR);
#endif

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);

	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	assert_list_length(list, 2);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
	validate_kolab_store_xml(list, 1, "recurrence");
}


void
validate_kolab_task_227(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-764657317.114",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:48:46",
	                 "2010-08-24 12:48:46",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k27",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-24",
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    24,
	                    I_COMMON_AUG);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_228(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1997930013.160",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:49:26",
	                 "2010-08-24 12:49:26",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k28",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    "2010-08-29 11:53:00",
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_WEEKDAY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_SUNDAY,
	                    -5,
	                    I_COMMON_AUG);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);

	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	assert_list_length(list, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
	/* validate_kolab_store_xml(list, 1, "recurrence"); */
}


void
validate_kolab_task_229(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1382625299.123",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:50:02",
	                 "2010-08-24 12:50:02",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k29",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_YEARDAY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    236,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_230(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-284542898.784",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:51:12",
	                 "2010-08-24 12:51:12",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k30",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_YEARDAY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    366,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_231(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-661442993.263",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:51:56",
	                 "2010-08-24 12:51:56",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k31",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    3,
	                    NULL,
	                    I_COMMON_TUESDAY,
	                    0,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_232(const I_task *itask, gint stage)
{
	gint rdate[3] = {24,8,2011};
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-695549868.242",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:52:34",
	                 "2010-08-24 12:52:34",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k32",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    0,
	                    rdate,
	                    I_COMMON_TUESDAY,
	                    0,
	                    0);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}


void
validate_kolab_task_233(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-1794627645.771",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:53:25",
	                 "2010-08-24 12:53:25",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k33",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_WEEKLY,
	                    1,
	                    0,
	                    NULL,
	                    I_COMMON_TUESDAY,
	                    0,
	                    0);

	/*  recurrence exclusions */
	/* TODO Double free problem, free function for exclusions commented in struct. */
	assert_GDate_in_list(itask->incidence->recurrence->exclusion, "2010-08-24");
	assert_GDate_in_list(itask->incidence->recurrence->exclusion, "2010-11-23");
	assert_GDate_in_list(itask->incidence->recurrence->exclusion, "2010-11-26");

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}

void
validate_kolab_task_234(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	assert_list_length(list, 2);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
	validate_kolab_store_xml(list, 1, "parent");
}

void
validate_kolab_task_235(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	assert_list_length(list, 1);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
}

void
validate_kolab_task_236(const I_task *itask, gint stage)
{
	GList *list = NULL;

	g_assert(itask != NULL);
	g_assert(itask->incidence != NULL);

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "KOrganizer 3.5.9 (enterprise35 20100805.1161816), Kolab resource",
	                 "libkcal-674787867.159",
	                 NULL,
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2010-08-24 12:44:31",
	                 "2010-08-24 12:44:31",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_k24",
	                    NULL,
	                    "Hendrik Helwich",
	                    "h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* recurrence */
#if 0
	validate_recurrence(itask->incidence->recurrence,
	                    I_REC_CYCLE_YEARLY_MONTHDAY,
	                    1,
	                    0,
	                    NULL,
	                    0,
	                    28,
	                    I_COMMON_FEB);
#endif

	/* task */
	assert_equal_int(5, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);
	assert_equal_gdate("2010-08-31", itask->due_date->date);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	assert_list_length(list, 2);
	validate_kolab_store_xml(list, 0, "pilot-sync-status");
	validate_kolab_store_xml(list, 1, "recurrence");
}


void
validate_kolab_task_502(const I_task *itask, gint stage)
{
	(void)stage;

	/* advanced alarms */

	assert_Alarm_in_list(itask->incidence->advanced_alarm,
	                     I_ALARM_TYPE_DISPLAY,
	                     0,
	                     -42,
	                     0,
	                     0,
	                     "test\ntext\n123",
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL,
	                     NULL);
}


void
validate_kolab_task_503(const I_task *itask, gint stage)
{
	GList *list = NULL;

	log_bitmask(stage);

	/* common */
	validate_icommon(itask->incidence->common,
	                 "Horde::Kolab",
	                 "a5caed56de89c80cac676dc4284d81f7",
	                 "line1\r\n\\tline2",
	                 NULL,
	                 ICOMMON_SENSITIVITY_PUBLIC,
	                 "2011-08-23 11:29:29",
	                 "2011-08-23 11:29:29",
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "summary?",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority));
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status);

	/* kolab store */
	g_assert(itask->incidence->common->kolab_store != NULL);
	/* unknown top level elements */
	list = kolab_store_get_element_list(itask->incidence->common, itask->incidence->common);
	g_assert_cmpint(g_list_length(list), ==, 2);
	validate_kolab_store_xml(list, 0, "creator");
	validate_kolab_store_xml(list, 1, "parent");

	list = kolab_store_get_element_list(itask->incidence->common, KOLAB_STORE_PTR_INC_ORGANIZER);
	g_assert_cmpint(g_list_length(list), ==, 1);
	validate_kolab_store_xml(list, 0, "uid");

	/* kolab attachment store */
	g_assert_cmpint(g_list_length(itask->incidence->common->kolab_attachment_store), ==, 0);
}

void
validate_kolab_task_3286494(const I_task *itask, gint stage)
{
	(void)stage;

	/* advanced alarms */

	assert_Alarm_in_list(itask->incidence->advanced_alarm,
	                     I_ALARM_TYPE_PROCEDURE,
	                     0,
	                     -15,
	                     1,
	                     1,
	                     NULL,
	                     NULL,
	                     "/home/ialexa/reinschrift.odt",
	                     NULL,
	                     NULL,
	                     NULL);
}
