/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * email-parser.h
 *
 *  Created on: 04.08.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef EMAILPARSER_H_
#define EMAILPARSER_H_

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <string.h>
#include <gmime/gmime.h>

#include "kolab-conv.h"
#include "util.h"

#define CONTACT_CLOSING_TAG		"</contact>"
#define TASK_CLOSING_TAG		"</task>"
#define EVENT_CLOSING_TAG		"</event>"
#define NOTE_CLOSING_TAG		"</note>"

/**
 * This function reads an email file using Gmime library and then
 * generates an equivalent Kolab_conv_mail structure containing an array
 * of Kolab_conv_mail_part struct having Xml data and attachments if any.
 *
 * @param  filename
 * 	   The filename with path to the email file to be read by gmime.
 * @param  error
 * 	   For Error handling
 *
 * @return Pointer to generated Kolab_conv_mail structure.
 */
Kolab_conv_mail* read_kolab_email_file (const gchar *filename, GError** error);

/**
 * Writes the Kolab_conv_mail struct to disk in the form of an email file
 * using gmime library.
 *
 * @param  mail
 * 	   A pointer to Kolab_conv_mail struct containing the data to be written
 * 	   to disk.
 * @param  filename
 * 	   The filename with path to the email file to be read by gmime.
 * @param  error
 * 	   For Error handling
 *
 * @return Pointer to generated Kolab_conv_mail structure.
 */
void write_kolab_email_file (const Kolab_conv_mail *mail, const gchar *filename, GError** error);



#endif /* EMAILPARSER_H_ */
