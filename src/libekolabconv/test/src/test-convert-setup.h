/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_convert_setup.h
 *
 *  Created on: 02.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#ifndef TEST_CONVERT_SETUP_H_
#define TEST_CONVERT_SETUP_H_

#include <structs/contact.h>
#include <structs/event.h>
#include <structs/task.h>
#include <structs/note.h>

void validate_kolab_contact_100(const I_contact *icontact, gint stage);
void validate_kolab_contact_200(const I_contact *icontact, gint stage);
void validate_kolab_contact_201(const I_contact *icontact, gint stage);
void validate_kolab_contact_300(const I_contact *icontact, gint stage);
void validate_kolab_contact_302(const I_contact *icontact, gint stage);
void validate_kolab_contact_500(const I_contact *icontact, gint stage);


void validate_evolution_contact_1(const I_contact *icontact, gint stage);
void validate_evolution_contact_2(const I_contact *icontact, gint stage);
void validate_evolution_contact_3(const I_contact *icontact, gint stage);
void validate_evolution_contact_4(const I_contact *icontact, gint stage);
void validate_evolution_contact_5(const I_contact *icontact, gint stage);
void validate_evolution_contact_b1(const I_contact *icontact, gint stage);


void validate_kolab_event_100(const I_event *ievent, gint stage);
void validate_kolab_event_101(const I_event *ievent, gint stage);
void validate_kolab_event_102(const I_event *ievent, gint stage);
void validate_kolab_event_103(const I_event *ievent, gint stage);
void validate_kolab_event_104(const I_event *ievent, gint stage);
void validate_kolab_event_105(const I_event *ievent, gint stage);
void validate_kolab_event_106(const I_event *ievent, gint stage);
void validate_kolab_event_107(const I_event *ievent, gint stage);
void validate_kolab_event_108(const I_event *ievent, gint stage);
void validate_kolab_event_109(const I_event *ievent, gint stage);
void validate_kolab_event_110(const I_event *ievent, gint stage);
void validate_kolab_event_111(const I_event *ievent, gint stage);
void validate_kolab_event_112(const I_event *ievent, gint stage);
void validate_kolab_event_113(const I_event *ievent, gint stage);
void validate_kolab_event_114(const I_event *ievent, gint stage);
void validate_kolab_event_115(const I_event *ievent, gint stage);
void validate_kolab_event_116(const I_event *ievent, gint stage);
void validate_kolab_event_117(const I_event *ievent, gint stage);
void validate_kolab_event_118(const I_event *ievent, gint stage);
void validate_kolab_event_119(const I_event *ievent, gint stage);
void validate_kolab_event_120(const I_event *ievent, gint stage);
void validate_kolab_event_121(const I_event *ievent, gint stage);
void validate_kolab_event_122(const I_event *ievent, gint stage);
void validate_kolab_event_123(const I_event *ievent, gint stage);
void validate_kolab_event_124(const I_event *ievent, gint stage);
void validate_kolab_event_125(const I_event *ievent, gint stage);
void validate_kolab_event_126(const I_event *ievent, gint stage);
void validate_kolab_event_127(const I_event *ievent, gint stage);
void validate_kolab_event_128(const I_event *ievent, gint stage);
void validate_kolab_event_129(const I_event *ievent, gint stage);
void validate_kolab_event_130(const I_event *ievent, gint stage);
void validate_kolab_event_131(const I_event *ievent, gint stage);
void validate_kolab_event_132(const I_event *ievent, gint stage);
void validate_kolab_event_133(const I_event *ievent, gint stage);
void validate_kolab_event_134(const I_event *ievent, gint stage);
void validate_kolab_event_135(const I_event *ievent, gint stage);
void validate_kolab_event_136(const I_event *ievent, gint stage);
void validate_kolab_event_137(const I_event *ievent, gint stage);
void validate_kolab_event_138(const I_event *ievent, gint stage);
void validate_kolab_event_139(const I_event *ievent, gint stage);
void validate_kolab_event_140(const I_event *ievent, gint stage);

void validate_kolab_event_201(const I_event *ievent, gint stage);
void validate_kolab_event_202(const I_event *ievent, gint stage);
void validate_kolab_event_203(const I_event *ievent, gint stage);
void validate_kolab_event_204(const I_event *ievent, gint stage);
void validate_kolab_event_205(const I_event *ievent, gint stage);
void validate_kolab_event_206(const I_event *ievent, gint stage);
void validate_kolab_event_207(const I_event *ievent, gint stage);
void validate_kolab_event_208(const I_event *ievent, gint stage);
void validate_kolab_event_500(const I_event *ievent, gint stage);
void validate_kolab_event_501(const I_event *ievent, gint stage);
void validate_kolab_event_502(const I_event *ievent, gint stage);
void validate_kolab_event_503(const I_event *ievent, gint stage);
void validate_kolab_event_504(const I_event *ievent, gint stage);
void validate_kolab_event_505(const I_event *ievent, gint stage);
void validate_kolab_event_506(const I_event *ievent, gint stage);


void validate_evolution_event_1(const I_event *ievent, gint stage);
void validate_evolution_event_2(const I_event *ievent, gint stage);
void validate_evolution_event_3(const I_event *ievent, gint stage);
void validate_evolution_event_4(const I_event *ievent, gint stage);
void validate_evolution_event_5(const I_event *ievent, gint stage);
void validate_evolution_event_6(const I_event *ievent, gint stage);
void validate_evolution_event_7(const I_event *ievent, gint stage);
void validate_evolution_event_8(const I_event *ievent, gint stage);
void validate_evolution_event_9(const I_event *ievent, gint stage);
void validate_evolution_event_10(const I_event *ievent, gint stage);
void validate_evolution_event_11(const I_event *ievent, gint stage);
void validate_evolution_event_12(const I_event *ievent, gint stage);
void validate_evolution_event_13(const I_event *ievent, gint stage);
void validate_evolution_event_14(const I_event *ievent, gint stage);
void validate_evolution_event_15(const I_event *ievent, gint stage);
void validate_evolution_event_16(const I_event *ievent, gint stage);
void validate_evolution_event_17(const I_event *ievent, gint stage);
void validate_evolution_event_18(const I_event *ievent, gint stage);
void validate_evolution_event_19(const I_event *ievent, gint stage);
void validate_evolution_event_20(const I_event *ievent, gint stage);
void validate_evolution_event_21(const I_event *ievent, gint stage);
void validate_evolution_event_22(const I_event *ievent, gint stage);
void validate_evolution_event_23(const I_event *ievent, gint stage);
void validate_evolution_event_24(const I_event *ievent, gint stage);
void validate_evolution_event_25(const I_event *ievent, gint stage);
void validate_evolution_event_26(const I_event *ievent, gint stage);
void validate_evolution_event_27(const I_event *ievent, gint stage);
void validate_evolution_event_28(const I_event *ievent, gint stage);
void validate_evolution_event_29(const I_event *ievent, gint stage);
void validate_evolution_event_30(const I_event *ievent, gint stage);
void validate_evolution_event_tz1(const I_event *ievent, gint stage);
void validate_evolution_event_tz2(const I_event *ievent, gint stage);
void validate_evolution_event_tz3(const I_event *ievent, gint stage);
void validate_evolution_event_tz4(const I_event *ievent, gint stage);
void validate_evolution_event_tz5(const I_event *ievent, gint stage);
void validate_evolution_event_tz6(const I_event *ievent, gint stage);
void validate_evolution_event_tz7(const I_event *ievent, gint stage);
void validate_evolution_event_tz8(const I_event *ievent, gint stage);
void validate_evolution_event_r1(const I_event *ievent, gint stage);
void validate_evolution_event_b1(const I_event *ievent, gint stage);
void validate_evolution_event_b2(const I_event *ievent, gint stage);


void validate_kolab_task_100(const I_task *itask, gint stage);
void validate_kolab_task_101(const I_task *itask, gint stage);
void validate_kolab_task_102(const I_task *itask, gint stage);
void validate_kolab_task_103(const I_task *itask, gint stage);
void validate_kolab_task_104(const I_task *itask, gint stage);
void validate_kolab_task_105(const I_task *itask, gint stage);
void validate_kolab_task_106(const I_task *itask, gint stage);
void validate_kolab_task_107(const I_task *itask, gint stage);
void validate_kolab_task_108(const I_task *itask, gint stage);
void validate_kolab_task_109(const I_task *itask, gint stage);
void validate_kolab_task_110(const I_task *itask, gint stage);
void validate_kolab_task_111(const I_task *itask, gint stage);
void validate_kolab_task_112(const I_task *itask, gint stage);
void validate_kolab_task_201(const I_task *itask, gint stage);
void validate_kolab_task_202(const I_task *itask, gint stage);
void validate_kolab_task_203(const I_task *itask, gint stage);
void validate_kolab_task_204(const I_task *itask, gint stage);
void validate_kolab_task_205(const I_task *itask, gint stage);
void validate_kolab_task_206(const I_task *itask, gint stage);
void validate_kolab_task_207(const I_task *itask, gint stage);
void validate_kolab_task_208(const I_task *itask, gint stage);
void validate_kolab_task_209(const I_task *itask, gint stage);
void validate_kolab_task_210(const I_task *itask, gint stage);
void validate_kolab_task_211(const I_task *itask, gint stage);
void validate_kolab_task_212(const I_task *itask, gint stage);
void validate_kolab_task_213(const I_task *itask, gint stage);
void validate_kolab_task_214(const I_task *itask, gint stage);
void validate_kolab_task_215(const I_task *itask, gint stage);
void validate_kolab_task_216(const I_task *itask, gint stage);
void validate_kolab_task_217(const I_task *itask, gint stage);
void validate_kolab_task_218(const I_task *itask, gint stage);
void validate_kolab_task_219(const I_task *itask, gint stage);
void validate_kolab_task_220(const I_task *itask, gint stage);
void validate_kolab_task_221(const I_task *itask, gint stage);
void validate_kolab_task_222(const I_task *itask, gint stage);
void validate_kolab_task_223(const I_task *itask, gint stage);
void validate_kolab_task_224(const I_task *itask, gint stage);
void validate_kolab_task_225(const I_task *itask, gint stage);
void validate_kolab_task_226(const I_task *itask, gint stage);
void validate_kolab_task_227(const I_task *itask, gint stage);
void validate_kolab_task_228(const I_task *itask, gint stage);
void validate_kolab_task_229(const I_task *itask, gint stage);
void validate_kolab_task_230(const I_task *itask, gint stage);
void validate_kolab_task_231(const I_task *itask, gint stage);
void validate_kolab_task_232(const I_task *itask, gint stage);
void validate_kolab_task_233(const I_task *itask, gint stage);
void validate_kolab_task_234(const I_task *itask, gint stage);
void validate_kolab_task_235(const I_task *itask, gint stage);
void validate_kolab_task_236(const I_task *itask, gint stage);
void validate_kolab_task_502(const I_task *itask, gint stage);
void validate_kolab_task_503(const I_task *itask, gint stage);
void validate_kolab_task_3286494(const I_task *itask, gint stage);


void validate_evolution_task_1(const I_task *itask, gint stage);
void validate_evolution_task_2(const I_task *itask, gint stage);
void validate_evolution_task_3(const I_task *itask, gint stage);
void validate_evolution_task_4(const I_task *itask, gint stage);
void validate_evolution_task_5(const I_task *itask, gint stage);
void validate_evolution_task_6(const I_task *itask, gint stage);
void validate_evolution_task_7(const I_task *itask, gint stage);
void validate_evolution_task_8(const I_task *itask, gint stage);
void validate_evolution_task_9(const I_task *itask, gint stage);
void validate_evolution_task_10(const I_task *itask, gint stage);
void validate_evolution_task_11(const I_task *itask, gint stage);
void validate_evolution_task_12(const I_task *itask, gint stage);
void validate_evolution_task_13(const I_task *itask, gint stage);
void validate_evolution_task_14(const I_task *itask, gint stage);
void validate_evolution_task_bug_3286514(const I_task *itask, gint stage);


void validate_kolab_note_100(const I_note *inote, gint stage);
void validate_kolab_note_200(const I_note *inote, gint stage);


void validate_evolution_note_1(const I_note *inote, gint stage);
void validate_evolution_note_2(const I_note *inote, gint stage);
void validate_evolution_note_bug_3297386(const I_note *inote, gint stage);


#define process_pim_type(PIM_TYPE)	  \
	struct { \
		gchar *filename; \
		void (*validate_op)(const PIM_TYPE*, int stage); \
	}


static const process_pim_type(I_contact) test_k_I_contact[] = {
	{"100.", validate_kolab_contact_100},
	{"200.", validate_kolab_contact_200},
	{"201.", validate_kolab_contact_201},
	{"300.", validate_kolab_contact_300},
	{"302.", validate_kolab_contact_302},
	{"500.", validate_kolab_contact_500}
};

static const process_pim_type(I_contact) test_e_I_contact[] = {
	{"contact_e1.vcf", validate_evolution_contact_1},
	{"contact_e2.vcf", validate_evolution_contact_2},
	{"contact_e3.vcf", validate_evolution_contact_3},
	{"contact_e4.vcf", validate_evolution_contact_4},
	{"contact_e5.vcf", validate_evolution_contact_5},
	{"contact_b1.vcf", validate_evolution_contact_b1}
};


static const process_pim_type(I_event) test_k_I_event[] = {
	{"100.", validate_kolab_event_100},
	{"101.", validate_kolab_event_101},
	{"102.", validate_kolab_event_102},
	{"103.", validate_kolab_event_103},
	{"104.", validate_kolab_event_104},
	{"105.", validate_kolab_event_105},
	{"106.", validate_kolab_event_106},
	{"107.", validate_kolab_event_107},
	{"108.", validate_kolab_event_108},
	{"109.", validate_kolab_event_109},
	{"110.", validate_kolab_event_110},
	{"111.", validate_kolab_event_111},
	{"112.", validate_kolab_event_112},
	{"113.", validate_kolab_event_113},
	{"114.", validate_kolab_event_114},
	{"115.", validate_kolab_event_115},
	{"116.", validate_kolab_event_116},
	{"117.", validate_kolab_event_117},
	{"118.", validate_kolab_event_118},
	{"119.", validate_kolab_event_119},
	{"120.", validate_kolab_event_120},
	{"121.", validate_kolab_event_121},
	{"122.", validate_kolab_event_122},
	{"123.", validate_kolab_event_123},
	{"124.", validate_kolab_event_124},
	{"125.", validate_kolab_event_125},
	{"126.", validate_kolab_event_126},
	{"127.", validate_kolab_event_127},
	{"128.", validate_kolab_event_128},
	{"129.", validate_kolab_event_129},
	{"130.", validate_kolab_event_130},
	{"131.", validate_kolab_event_131},
	{"132.", validate_kolab_event_132},
	{"133.", validate_kolab_event_133},
	{"134.", validate_kolab_event_134},
	{"135.", validate_kolab_event_135},
	{"136.", validate_kolab_event_136},
	{"137.", validate_kolab_event_137},
	{"138.", validate_kolab_event_138},
	{"139.", validate_kolab_event_139},
	{"140.", validate_kolab_event_140},
	{"201.", validate_kolab_event_201},
	{"202.", validate_kolab_event_202},
	{"203.", validate_kolab_event_203},
	{"204.", validate_kolab_event_204},
	{"205.", validate_kolab_event_205},
	{"206.", validate_kolab_event_206},
	{"207.", validate_kolab_event_207},
	{"208.", validate_kolab_event_208},
	{"500.", validate_kolab_event_500},
	{"501.", validate_kolab_event_501},
	{"502.", validate_kolab_event_502},
	{"503.", validate_kolab_event_503},
	{"504.", validate_kolab_event_504},
	{"505.", validate_kolab_event_505},
	{"506.", validate_kolab_event_506}
};

static const process_pim_type(I_event) test_e_I_event[] = {
	{"event_e1.ics", validate_evolution_event_1},
	{"event_e2.ics", validate_evolution_event_2},
	{"event_e3.ics", validate_evolution_event_3},
	{"event_e4.ics", validate_evolution_event_4},
	{"event_e5.ics", validate_evolution_event_5},
	{"event_e6.ics", validate_evolution_event_6},
	{"event_e7.ics", validate_evolution_event_7},
	{"event_e8.ics", validate_evolution_event_8},
	{"event_e9.ics", validate_evolution_event_9},
	{"event_e10.ics", validate_evolution_event_10},
	{"event_e11.ics", validate_evolution_event_11},
	{"event_e12.ics", validate_evolution_event_12},
	{"event_e13.ics", validate_evolution_event_13},
	{"event_e14.ics", validate_evolution_event_14},
	{"event_e15.ics", validate_evolution_event_15},
	{"event_e16.ics", validate_evolution_event_16},
	{"event_e17.ics", validate_evolution_event_17},
	{"event_e18.ics", validate_evolution_event_18},
	{"event_e19.ics", validate_evolution_event_19},
	{"event_e20.ics", validate_evolution_event_20},
	{"event_e21.ics", validate_evolution_event_21},
	{"event_e22.ics", validate_evolution_event_22},
	{"event_e23.ics", validate_evolution_event_23},
	{"event_e24.ics", validate_evolution_event_24},
	{"event_e25.ics", validate_evolution_event_25},
	{"event_e26.ics", validate_evolution_event_26},
	{"event_e27.ics", validate_evolution_event_27},
	{"event_e28.ics", validate_evolution_event_28},
	{"event_e29.ics", validate_evolution_event_29},
	{"event_e30.ics", validate_evolution_event_30},
	{"event_tz1.ics", validate_evolution_event_tz1},
	{"event_tz2.ics", validate_evolution_event_tz2},
	{"event_tz3.ics", validate_evolution_event_tz3},
	{"event_tz4.ics", validate_evolution_event_tz4},
	{"event_tz5.ics", validate_evolution_event_tz5},
	{"event_tz6.ics", validate_evolution_event_tz6},
	{"event_tz7.ics", validate_evolution_event_tz7},
	{"event_tz8.ics", validate_evolution_event_tz8},
	{"event_r1.ics", validate_evolution_event_r1},
	{"event_b1.ics", validate_evolution_event_b1},
	{"event_b2.ics", validate_evolution_event_b2}
};


static const process_pim_type(I_task) test_k_I_task[] = {
	{"100.", validate_kolab_task_100},
	{"101.", validate_kolab_task_101},
	{"102.", validate_kolab_task_102},
	{"103.", validate_kolab_task_103},
	{"104.", validate_kolab_task_104},
	{"105.", validate_kolab_task_105},
	{"106.", validate_kolab_task_106},
	{"107.", validate_kolab_task_107},
	{"108.", validate_kolab_task_108},
	{"109.", validate_kolab_task_109},
	{"110.", validate_kolab_task_110},
	{"111.", validate_kolab_task_111},
	{"112.", validate_kolab_task_112},
	{"201.", validate_kolab_task_201},
	{"202.", validate_kolab_task_202},
	{"203.", validate_kolab_task_203},
	{"204.", validate_kolab_task_204},
	{"205.", validate_kolab_task_205},
	{"206.", validate_kolab_task_206},
	{"207.", validate_kolab_task_207},
	{"208.", validate_kolab_task_208},
	{"209.", validate_kolab_task_209},
	{"210.", validate_kolab_task_210},
	{"211.", validate_kolab_task_211},
	{"212.", validate_kolab_task_212},
	{"213.", validate_kolab_task_213},
	{"214.", validate_kolab_task_214},
	{"215.", validate_kolab_task_215},
	{"216.", validate_kolab_task_216},
	{"217.", validate_kolab_task_217},
	{"218.", validate_kolab_task_218},
	{"219.", validate_kolab_task_219},
	{"220.", validate_kolab_task_220},
	{"221.", validate_kolab_task_221},
	{"222.", validate_kolab_task_222},
	{"223.", validate_kolab_task_223},
	{"224.", validate_kolab_task_224},
	{"225.", validate_kolab_task_225},
	{"226.", validate_kolab_task_226},
	{"227.", validate_kolab_task_227},
	{"228.", validate_kolab_task_228},
	{"229.", validate_kolab_task_229},
	{"230.", validate_kolab_task_230},
	{"231.", validate_kolab_task_231},
	{"232.", validate_kolab_task_232},
	{"233.", validate_kolab_task_233},
	{"234.", validate_kolab_task_234},
	{"235.", validate_kolab_task_235},
	{"236.", validate_kolab_task_236},
	{"502.", validate_kolab_task_502},
	{"503.", validate_kolab_task_503},
	{"3286494.", validate_kolab_task_3286494}
};

static const process_pim_type(I_task) test_e_I_task[] = {
	{"task_e1.ics", validate_evolution_task_1},
	{"task_e2.ics", validate_evolution_task_2},
	{"task_e3.ics", validate_evolution_task_3},
	{"task_e4.ics", validate_evolution_task_4},
	{"task_e5.ics", validate_evolution_task_5},
	{"task_e6.ics", validate_evolution_task_6},
	{"task_e7.ics", validate_evolution_task_7},
	{"task_e8.ics", validate_evolution_task_8},
	{"task_e9.ics", validate_evolution_task_9},
	{"task_e10.ics", validate_evolution_task_10},
	{"task_e11.ics", validate_evolution_task_11},
	{"task_e12.ics", validate_evolution_task_12},
	{"task_e13.ics", validate_evolution_task_13},
	{"task_e14.ics", validate_evolution_task_14},
	{"bug_3286514.ics", validate_evolution_task_bug_3286514}
};


static const process_pim_type(I_note) test_k_I_note[] = {
	{"100.", validate_kolab_note_100},
	{"200.", validate_kolab_note_200}
};

static const process_pim_type(I_note) test_e_I_note[] = {
	{"note_e1.ics", validate_evolution_note_1},
	{"note_e2.ics", validate_evolution_note_2},
	{"bug_3297386.ics", validate_evolution_note_bug_3297386}
};



#endif /* TEST_CONVERT_SETUP_H_ */
