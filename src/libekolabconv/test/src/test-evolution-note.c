/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_evolution_note.c
 *
 *  Created on: 27.12.2010
 *      Author: Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include "test-convert-setup.h"
#include "test-util.h"

void
validate_evolution_note_1(const I_note *inote, gint stage)
{
	g_assert(inote != NULL);
	g_assert(inote->common != NULL);

	validate_icommon(inote->common, "-//Ximian//NONSGML Evolution Calendar//EN",
	                 "20100824T152249Z-1270-1000-1-2@hhelwi-virt.bonn.tarent.de",
	                 NULL, NULL, ICOMMON_SENSITIVITY_PUBLIC, "2010-08-24 15:22:58", "2010-08-24 15:22:58", stage);

	assert_equal("note_e1", inote->summary);
}

void
validate_evolution_note_2(const I_note *inote, gint stage)
{
	g_assert(inote != NULL);
	g_assert(inote->common != NULL);

	validate_icommon(inote->common, "-//Ximian//NONSGML Evolution Calendar//EN",
	                 "20100824T152122Z-1270-1000-1-1@hhelwi-virt.bonn.tarent.de",
	                 "Text\n123", NULL, ICOMMON_SENSITIVITY_PUBLIC, "2010-08-24 15:22:46", "2010-08-24 15:22:46", stage);

	assert_equal("note_e2", inote->summary);
}

void
validate_evolution_note_bug_3297386(const I_note *inote, gint stage)
{
	(void)stage;

	assert_equal_gchar("BEGIN:VJOURNAL\r\nORGANIZER;CN=Hendrik Helwich:MAILTO:h.helwich@tarent.de\r\nX-EVOLUTION-RECIPIENTS:bill@gates.com\\;donald@duck.de\r\nDTSTART;VALUE=DATE:20110505\r\nEND:VJOURNAL\r\n",
	                   inote->common->evolution_store);
}
