/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * testbase.h
 *
 *  Created on: 02.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      	Peter Neuhaus <p.neuhaus@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef TESTBASE_H_
#define TESTBASE_H_

/*
 * convenience macros for testing TestStageEnum bits
 */
#define		IS_TEST_STAGE_FILE_TO_EVO(x)		((x) & TEST_STAGE_FILE_TO_EVO)
#define		IS_TEST_STAGE_EVO_TO_INTERN(x)		((x) & TEST_STAGE_EVO_TO_INTERN)
#define		IS_TEST_STAGE_INTERN_TO_KMAIL(x)	((x) & TEST_STAGE_INTERN_TO_KMAIL)
#define		IS_TEST_STAGE_KMAIL_TO_FILE(x)		((x) & TEST_STAGE_KMAIL_TO_FILE)
#define		IS_TEST_STAGE_FILE_TO_KMAIL(x)		((x) & TEST_STAGE_FILE_TO_KMAIL)
#define		IS_TEST_STAGE_KMAIL_TO_INTERN(x)	((x) & TEST_STAGE_KMAIL_TO_INTERN)
#define		IS_TEST_STAGE_INTERN_TO_EVO(x)		((x) & TEST_STAGE_INTERN_TO_EVO)
#define		IS_TEST_STAGE_EVO_TO_FILE(x)		((x) & TEST_STAGE_EVO_TO_FILE)


/**
 * All conversion mappings which are tested. They can be combined to a bitfield
 * to define a set of executed conversions. This can be useful in the test
 * validation functions to consider lossy conversions.
 */
typedef enum {
	TEST_STAGE_FILE_TO_EVO = 1,
	TEST_STAGE_EVO_TO_INTERN = 2,
	TEST_STAGE_INTERN_TO_KMAIL= 4,
	TEST_STAGE_KMAIL_TO_FILE = 8,
	TEST_STAGE_FILE_TO_KMAIL = 16,
	TEST_STAGE_KMAIL_TO_INTERN = 32,
	TEST_STAGE_INTERN_TO_EVO = 64,
	TEST_STAGE_EVO_TO_FILE = 128
} TestStageEnum;

void add_all_tests_I_contact(void);
void add_all_tests_I_event(void);
void add_all_tests_I_task(void);
void add_all_tests_I_note(void);

#endif /* TESTBASE_H_ */
