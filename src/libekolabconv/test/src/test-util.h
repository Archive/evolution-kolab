/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_util.h
 *
 *  Created on: Sep 30, 2010
 *      Author: Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */
/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef TEST_UTIL_H_
#define TEST_UTIL_H_

#include <glib.h>
#include "../../main/src/structs/contact.h"
#include "../../main/src/structs/incidence.h"

/* if expression "E" is false, bitmap "M" is set at position "P" */
#define CHECK_FAILED(E, M, P) if (!(E)) M |= (1 << (P-1))

void assert_equal(gchar *expected, GString *value);
void assert_equal_gchar(gchar *expected, gchar *value);

void assert_equal_int (gint expected, gint value);

void assert_equal_double (gdouble expected, gdouble value);

void assert_equal_gdate(gchar *expected, GDate *gdate);

void assert_equal_timestamp(gchar *expected, time_t *timestamp);

void assert_timestamp_not_older_than_10_seconds(time_t *timestamp);

Phone_number *assert_phone_in_list (GList *phone_nrs, gchar *nr_expected, Icontact_phone_type type_expected);

Email *assert_email_in_list (GList *emails, gchar *smtp_address_exp, gchar *display_name_exp);

Address *assert_address_in_list (GList *addresses, Icontact_address_type type, gchar *street, gchar *pobox,
                                 gchar *locality, gchar *region, gchar *postal_code, gchar *country);

void assert_String_in_list(GList *string_list, gchar *value);

void assert_custom_field_in_list (GList *custom_list, gchar *custom_field_type, gchar *custom_field_name, gchar *field_value_exp);

Alarm* assert_Alarm_in_list (GList *advancedAlarm, Alarm_type type, gint startOffset, gint endOffset,
                             gint repeatCount, gint repeatInterval, gchar *displayText, gchar *audioFile,
                             gchar *procParam_program, gchar *procParam_arguments,
                             gchar *emailParam_subject, gchar *emailParam_mailText);

void assert_attendee_in_list(GList *attendees, gchar *display_name, gchar *smtp_address, Incidence_status status,
                             gboolean requestResponse, gboolean invitationSent, Incidence_role role, Incidence_cutype type);

void assert_GDate_in_list(GList *date_list, gchar *value);

void validate_iincidence(const I_incidence *iincidence, gchar *summary, gchar *location,
                         gchar *organizerDisplayName, gchar *organizerSmtpAddress,
                         gchar* startDate, gint stage);

void validate_icommon(const I_common *icommon, gchar* product_id, gchar* uid,
                      gchar *body, gchar *categories, Sensitivity sensitivity, gchar *creation_date, gchar *last_modification_date, gint stage);

void validate_recurrence(const Recurrence *recurrence, Recurrence_cycle cycle,
                         gint interval, gint rangeNumber, gint *rangeDate, gint weekdays,
                         gint dayNumber, Month month);

void assert_binary_attachment_equal(gchar *exp_data_file_name, gchar *name_exp, gchar *mime_type_exp, Kolab_conv_mail_part *value);

char *int_to_bitmask (int i);
void log_bitmask(int i);

void assert_list_null(GList *value);

void assert_list_length(GList *value, int lenght_exp);

void validate_kolab_store_xml(GList *elements, gint index, gchar *name);

void validate_link_attachments(const I_common *icommon, ...);
void validate_inline_attachment_names(const I_common *icommon, ...);

void strip_cr(char *s);
char *strip_cr_cpy(const char *s);

void assert_binary_attachment_store_equal(gchar *exp_data_file_name, gchar *name_exp, gchar *mime_type_exp,
                                          I_common *icommon, gint idx);

#endif /* TEST_UTIL_H_ */
