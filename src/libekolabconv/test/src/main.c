/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * main.c
 *
 *  Created on: 02.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include <gio/gio.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <evolution/evolution.h>
#include <kolab/kolab.h>
#include <structs/contact.h>
#include "main.h"
#include "testbase.h"



static void
read_config_file()
{
	GError *error = NULL;
	GKeyFile *key_file = g_key_file_new();
	gchar *log_file_name = NULL;

	if (!g_key_file_load_from_file (key_file, TEST_CONFIG_FILE_NAME, G_KEY_FILE_NONE, &error)) {
		g_error ( "error reading config file %s:\n%s", TEST_CONFIG_FILE_NAME, error->message);
		exit(-1);
	}

	tempfile = g_key_file_get_string(key_file, "main", "Tempfile", NULL);
	test_resource_folder = g_key_file_get_string(key_file, "main", "TestResourceFolder", NULL);
	kolab_folder_I_contact = g_key_file_get_string(key_file, "main", "KolabContactFolder", NULL);
	kolab_folder_I_event   = g_key_file_get_string(key_file, "main", "KolabEventFolder", NULL);
	kolab_folder_I_task    = g_key_file_get_string(key_file, "main", "KolabTaskFolder", NULL);
	kolab_folder_I_note    = g_key_file_get_string(key_file, "main", "KolabNoteFolder", NULL);
	evolution_folder_I_contact = g_key_file_get_string(key_file, "main", "EvolutionContactFolder", NULL);
	evolution_folder_I_event   = g_key_file_get_string(key_file, "main", "EvolutionEventFolder", NULL);
	evolution_folder_I_task    = g_key_file_get_string(key_file, "main", "EvolutionTaskFolder", NULL);
	evolution_folder_I_note    = g_key_file_get_string(key_file, "main", "EvolutionNoteFolder", NULL);
	log_file_name = g_key_file_get_string(key_file, "main", "TestLogFile", NULL);

	log_fp = fopen(log_file_name, "w");
	g_free(log_file_name);
	g_key_file_free(key_file);
}

static void
log_handler (const gchar *log_domain, GLogLevelFlags log_level,
             const gchar *message, gpointer user_data)
{
	gchar *level;

	(void)log_domain;
	(void)user_data;

	switch (log_level) {
	case G_LOG_LEVEL_ERROR:
		level = "ERROR";
		break;
	case G_LOG_LEVEL_CRITICAL:
		level = "CRITICAL";
		break;
	case G_LOG_LEVEL_WARNING:
		level = "WARNING";
		break;
	case G_LOG_LEVEL_MESSAGE:
		level = "MESSAGE";
		break;
	case G_LOG_LEVEL_INFO:
		level = "INFO";
		break;
	case G_LOG_LEVEL_DEBUG:
		level = "DEBUG";
		break;
	default:
		level = "UNDEF";
		break;
	}
	fprintf(log_fp, "%s: %s\n", level, message);
	fflush(log_fp);
	/* g_log_default_handler   (log_domain, log_level,message, user_data); */
}

static void
free_mem(gpointer mem_ptr, gpointer unused)
{
	(void)unused;
	g_free(mem_ptr);
}


int
main (int argc, char *argv[])
{
	int success;

	/* read values from test configuration file */
	read_config_file();

	/* add log handler for all levels */
	g_log_set_handler (G_LOG_DOMAIN, G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL
	                   | G_LOG_FLAG_RECURSION, log_handler, NULL);

	/* initialize glib testing framework */
	g_test_init (&argc, &argv, NULL);

	free_mem_after_test = NULL;

	add_all_tests_I_contact();
	add_all_tests_I_event();
	add_all_tests_I_task();
	add_all_tests_I_note();

	/* run tests */
	success = g_test_run ();

	g_list_foreach(free_mem_after_test, free_mem, NULL);
	g_list_free(free_mem_after_test);

	g_free(tempfile);
	g_free(test_resource_folder);
	g_free(kolab_folder_I_contact);
	g_free(kolab_folder_I_event);
	g_free(kolab_folder_I_task);
	g_free(kolab_folder_I_note);
	g_free(evolution_folder_I_contact);
	g_free(evolution_folder_I_event);
	g_free(evolution_folder_I_task);
	g_free(evolution_folder_I_note);

	fclose(log_fp);

	if (success == 0)
		printf("All Tests OK\n");

	return success;
}
