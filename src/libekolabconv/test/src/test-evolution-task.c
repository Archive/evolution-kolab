/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * test_evolution_task.c
 *
 *  Created on: 21.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include <glib.h>
#include "test-convert-setup.h"
#include "test-util.h"
#include "../../main/src/kolab-conv.h"
#include "../../main/src/structs/task.h"
#include "../../main/src/kolab/kolab-util.h"
#include "testbase.h"

void
validate_evolution_task_1(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T112850Z-1270-1000-1-12@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:29:02",						/* creation_date		*/
	                 "2010-08-25 11:29:02",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e1",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
}

void
validate_evolution_task_2(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* todo not handled elements: DTSTAMP, SEQUENCE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T113232Z-1270-1000-1-14@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 "Beschreibung",							/* body				*/
	                 "Urlaubskarten,Strategien,Urlaub,Konkurrenz,Jahrestag,Anrufe,Allgemeines,Heiße Kontakte,Lieferanten,Zeit und Ausgaben,Geschenke,International,Favoriten,Ideen,VIP,Geschäftlich,Persönlich,Status,Ziele/Zielsetzungen,Geburtstag,Schlüssel-Kunde,Wartend", /* categories */
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:34:47",						/* creation_date		*/
	                 "2010-08-25 11:34:47",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e2",
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-18",
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
}


void
validate_evolution_task_3(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE, VTIMEZONE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T113451Z-1270-1000-1-15@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:36:15",						/* creation_date		*/
	                 "2010-08-25 11:36:15",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e3",
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-11 13:30:00",
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
}


void
validate_evolution_task_4(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T113625Z-1270-1000-1-16@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:36:55",						/* creation_date		*/
	                 "2010-08-25 11:36:55",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e4",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
	assert_equal_gdate("2010-08-25", itask->due_date->date);
}


void
validate_evolution_task_5(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T113703Z-1270-1000-1-17@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:37:59",						/* creation_date		*/
	                 "2010-08-25 11:37:59",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e5",
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-20 01:30:00",
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
	assert_equal_timestamp("2010-08-21 00:00:00", itask->due_date->date_time);
}


void
validate_evolution_task_6(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE, ATTACH:file: */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T113802Z-1270-1000-1-18@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:39:28",						/* creation_date		*/
	                 "2010-08-25 11:39:28",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e6",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);

	/* evolution store */

	g_assert(itask->incidence->common->evolution_store == NULL);
}


void
validate_evolution_task_7(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE, URL */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T114223Z-1270-1000-1-20@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:44:18",						/* creation_date		*/
	                 "2010-08-25 11:44:18",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e7",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(4, priority_xkcal_to_k(itask->priority)); /* 7 not possible in kolab, mapped to 4 */
	assert_equal_int(7, itask->priority);
	assert_equal_int(42, itask->completed);
	g_assert(I_TASK_IN_PROGRESS == itask->status);

	/*
	 * Evolution Store
	 */
	assert_equal_gchar("BEGIN:VTODO\r\nURL:www.example.de\r\nEND:VTODO\r\n",
	                   itask->incidence->common->evolution_store);
}


void
validate_evolution_task_8(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE, COMPLETED */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T114436Z-1270-1000-1-21@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:45:56",						/* creation_date		*/
	                 "2010-08-25 11:45:56",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e8",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 5 not possible in kolab, mapped to 3 */
	assert_equal_int(5, itask->priority);
	assert_equal_int(100, itask->completed);
	g_assert(I_TASK_COMPLETED == itask->status);
}


void
validate_evolution_task_9(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T114618Z-1270-1000-1-22@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:48:20",						/* creation_date		*/
	                 "2010-08-25 11:48:20",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e9",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(2, priority_xkcal_to_k(itask->priority));  /* evolution 3 mapped to kolab 2 */
	assert_equal_int(3, itask->priority);
	assert_equal_int(97, itask->completed);
	g_assert(I_TASK_NOT_STARTED == itask->status); /* Cancelled is mapped to I_TASK_NOT_STARTED due to no equivalent value */
}


void
validate_evolution_task_10(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE, COMPLETED */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T114822Z-1270-1000-1-23@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:49:29",						/* creation_date		*/
	                 "2010-08-25 11:49:29",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e10",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(100, itask->completed);
	g_assert(I_TASK_COMPLETED == itask->status);
}


void
validate_evolution_task_11(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE, COMPLETED */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T115001Z-1270-1000-1-25@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 11:50:33",						/* creation_date		*/
	                 "2010-08-25 11:55:03",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e11",
	                    NULL,
	                    NULL,
	                    NULL,
	                    "2010-08-19 08:00:00",
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(100, itask->completed);
	g_assert(I_TASK_COMPLETED == itask->status);
	assert_equal_timestamp("2010-08-28 01:30:00", itask->due_date->date_time);
}


void
validate_evolution_task_12(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE, X-LIC-ERROR;X-LIC-ERRORTYPE, ATTENDEE;CUTYPE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T120335Z-1270-1000-1-28@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PUBLIC,					/* sensitivity			*/
	                 "2010-08-25 12:04:46",						/* creation_date		*/
	                 "2010-08-25 12:15:25",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e12",
	                    NULL,
	                    "Hendrik Helwich",
	                    "MAILTO:h.helwich@tarent.de",
	                    NULL,
	                    stage);

	/* attendees */
	assert_attendee_in_list(itask->incidence->attendee,
	                        "Donald Duck",
	                        "donald@duck.de",
	                        I_INC_STATUS_NONE, /* Needs_Action is mapped to none due to noch equivalent value */
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_INDIVIDUAL); /* Chait is mapped to I_INC_ROLE_REQUIRED due to non equivalent value */

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Micky Maus",
	                        "micky@maus.nl",
	                        I_INC_STATUS_ACCEPTED,
	                        FALSE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_GROUP);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Daisy Duck",
	                        "daisy@duck.de",
	                        I_INC_STATUS_DECLINED,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_OPTIONAL, I_INC_CUTYPE_RESOURCE);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Bill Gates",
	                        "bill@gates.com",
	                        I_INC_STATUS_TENTATIVE,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_RESOURCE, I_INC_CUTYPE_ROOM);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "Sir Mix-a-Lot",
	                        "sir@mix-a-lot.com",
	                        I_INC_STATUS_DELEGATED,
	                        FALSE,
	                        FALSE,
	                        0, I_INC_CUTYPE_UNDEFINED);

	assert_attendee_in_list(itask->incidence->attendee,
	                        "MC Hammer",
	                        "mc@hammer.org",
	                        I_INC_STATUS_NONE,
	                        TRUE,
	                        FALSE,
	                        I_INC_ROLE_REQUIRED, I_INC_CUTYPE_INDIVIDUAL);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
}


void
validate_evolution_task_13(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T142609Z-1270-1000-1-35@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_PRIVATE,					/* sensitivity			*/
	                 "2010-08-25 14:30:21",						/* creation_date		*/
	                 "2010-08-25 14:30:21",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e13",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
}


void
validate_evolution_task_14(const I_task *itask, gint stage)
{
	log_bitmask(stage);
	/* TODO not handled elements: DTSTAMP, SEQUENCE */

	/* common */
	validate_icommon(itask->incidence->common,
	                 "-//Ximian//NONSGML Evolution Calendar//EN",			/* product_id			*/
	                 "20100825T143053Z-1270-1000-1-36@hhelwi-virt.bonn.tarent.de",	/* uid				*/
	                 NULL,								/* body				*/
	                 NULL,								/* categories			*/
	                 ICOMMON_SENSITIVITY_CONFIDENTIAL,				/* sensitivity			*/
	                 "2010-08-25 14:31:48",						/* creation_date		*/
	                 "2010-08-25 14:31:48",						/* last_modification_date	*/
	                 stage);

	/* incidence */
	validate_iincidence(itask->incidence,
	                    "task_e14",
	                    NULL,
	                    NULL,
	                    NULL,
	                    NULL,
	                    stage);

	/* task */
	assert_equal_int(3, priority_xkcal_to_k(itask->priority)); /* 0 not possible in kolab, set to default 3 */
	assert_equal_int(0, itask->priority);
	assert_equal_int(0, itask->completed);
}



void
validate_evolution_task_bug_3286514(const I_task *itask, gint stage)
{
	(void)stage;

	assert_equal_timestamp("2011-05-09 23:30:00", itask->completed_datetime);
}
