/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * kolab-conv.h
 *
 *  Created on: 23.06.2010
 *      Author: Hendrik Helwich   <h.helwich@tarent.de>,
 *              Christian Hilberg <hilberg@kernelconcepts.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef KOLABCONV_H_
#define KOLABCONV_H_

#include <stdlib.h>
#include <string.h>
#include <glib.h>
#include <libebook/libebook.h>
#include <libecal/libecal.h>

/*
 * some kolab constants
 * (see http://www.kolab.org/doc/kolabformat-2.0rc7-html/c159.html)
 */

#define KOLAB_TYPE_MAIL    "mail"
#define KOLAB_TYPE_EVENT   "event"
#define KOLAB_TYPE_JOURNAL "journal"
#define KOLAB_TYPE_TASK    "task"
#define KOLAB_TYPE_NOTE    "note"
#define KOLAB_TYPE_CONTACT "contact"

#define KOLAB_SUBTYPE_DEFAULT "default"

#define KOLAB_SUBTYPE_MAIL_INBOX     "inbox"
#define KOLAB_SUBTYPE_MAIL_DRAFTS    "drafts"
#define KOLAB_SUBTYPE_MAIL_SENTITEMS "sentitems"
#define KOLAB_SUBTYPE_MAIL_JUNKEMAIL "junkemail"

#define KOLAB_ANNOTATION_FOLDER_TYPE "/vendor/kolab/folder-type"
#define KOLAB_ANNOTATION_FOLDER_TYPE_ATTRIBUTE "value.shared"

#define KOLAB_MESSAGE_MIMETYPE_PREFIX "application/x-vnd.kolab."

#define KOLABCONV_PRODUCT_ID "Evolution/libekolabconv"

/* define error classes.
 * The prefix of all error classes is: KOLABCONV_ERROR_ */

#define KOLABCONV_ERROR_GENERIC (kolabconv_error_generic_quark())
#define KOLABCONV_ERROR_READ_KOLAB (kolabconv_error_read_kolab_quark())
#define KOLABCONV_ERROR_READ_EVOLUTION (kolabconv_error_read_evolution_quark())

GQuark kolabconv_error_generic_quark(void);
GQuark kolabconv_error_read_kolab_quark(void);
GQuark kolabconv_error_read_evolution_quark(void);

/*
 * define error codes.
 * The prefix of all error codes is the name of the corresponding error class.
 */

typedef enum {
	KOLABCONV_ERROR_GENERIC_NOT_INITIALIZED
} Kolabconv_error_generic_codes;

typedef enum {
	KOLABCONV_ERROR_READ_KOLAB_INPUT_IS_NULL,
	KOLABCONV_ERROR_READ_KOLAB_MISSING_XML_PART,
	KOLABCONV_ERROR_READ_KOLAB_MISSING_DATA_PART,
	KOLABCONV_ERROR_READ_KOLAB_MALFORMED_XML,
	KOLABCONV_ERROR_READ_KOLAB_INVALID_KOLAB_XML,
	KOLABCONV_ERROR_READ_KOLAB_INVALID_KOLAB_DATA
} Kolabconv_error_read_kolab_codes;

typedef enum {
	KOLABCONV_ERROR_READ_EVOLUTION_INPUT_IS_NULL,
	KOLABCONV_ERROR_READ_EVOLUTION_INVALID_FIELD_DATA
} Kolabconv_error_read_evolution_codes;

/*
 * define enumerations and data structures
 */

typedef struct {
	gchar  *name;		/* name of the mail part */
	gchar  *mime_type;	/* mimetype of the mail part */
	guint   length;		/* size of the data of the mail part */
	gchar *data;		/* the data of the mail part */
} Kolab_conv_mail_part;

typedef struct {
	Kolab_conv_mail_part *mail_parts;
	guint length;		/* number of mail parts	*/
} Kolab_conv_mail;

typedef struct {
	ECalComponent *maincomp;
	ECalComponent *timezone; /* can be null for UTC */
} ECalComponentWithTZ;

/**
 * Kolab_conv_freebusy_type:
 * @KOLABCONV_FB_TYPE_TRIGGER: F/B trigger request
 * @KOLABCONV_FB_TYPE_IFB: request an IFB data set
 * @KOLABCONV_FB_TYPE_VFB: request an VFB data set
 * @KOLABCONV_FB_TYPE_XFB: request an XFB data set
 *
 * Used to tell kolabconv_cal_util_freebusy_new_fb_url() which kind
 * of Kolab F/B request URL to create
 */
typedef enum {
	KOLABCONV_FB_TYPE_TRIGGER = 0,
	KOLABCONV_FB_TYPE_IFB,
	KOLABCONV_FB_TYPE_VFB,
	KOLABCONV_FB_TYPE_XFB,
	KOLABCONV_FB_LASTTYPE
} Kolab_conv_freebusy_type;

/*
 * Function Prototypes
 */

/* these functions must be called to initialize or shutdown this library */
void kolabconv_initialize (void);
void kolabconv_shutdown (void);

/* functions to convert a kolab contact mail to an evolution contact and back */
EContact* kolabconv_kcontact_to_econtact (const Kolab_conv_mail*, GError**);
Kolab_conv_mail* kolabconv_econtact_to_kcontact (const EContact*, GError**);

/* functions to convert a kolab event mail to an evolution event and back */
ECalComponentWithTZ* kolabconv_kevent_to_eevent (const Kolab_conv_mail*, GError**);
Kolab_conv_mail* kolabconv_eevent_to_kevent (const ECalComponentWithTZ*, GError**);

/* functions to convert a kolab task mail to an evolution task and back */
ECalComponentWithTZ* kolabconv_ktask_to_etask (const Kolab_conv_mail*, GError**);
Kolab_conv_mail* kolabconv_etask_to_ktask (const ECalComponentWithTZ*, GError**);

/* functions to convert a kolab note mail to an evolution note and back */
ECalComponentWithTZ* kolabconv_knote_to_enote (const Kolab_conv_mail*, GError**);
Kolab_conv_mail* kolabconv_enote_to_knote (const ECalComponentWithTZ*, GError**);

/* functions for the handling of free/busy URLs */
gchar*
kolabconv_cal_util_freebusy_new_fb_url (const gchar *servername,
                                        const gchar *username,
                                        gboolean use_ssl,
                                        Kolab_conv_freebusy_type listtype);

ECalComponent*
kolabconv_cal_util_freebusy_ecalcomp_new_from_ics (const gchar *ics_string,
                                                   gssize nbytes,
                                                   GError **err);

/* functions to free data structures */
void kolabconv_free_econtact(EContact *econtact);
void kolabconv_free_ecalendar(ECalComponentWithTZ *ecalcomp);
void kolabconv_free_kmail(Kolab_conv_mail *kmail);
/* void kolabconv_free_kmail_part(Kolab_conv_mail_part **mailpart); */
void kolabconv_free_kmail_part(void **mailpart);

#endif /* KOLABCONV_H_ */
