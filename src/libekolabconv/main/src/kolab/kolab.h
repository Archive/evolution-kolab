/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * kolab.h
 *
 *  Created on: 29.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */



#ifndef KOLAB_H_
#define KOLAB_H_

#include <glib.h>		/* GError */
#include "../kolab-conv.h"	/* Kolab mail struct */
/* internal structures */
#include "../structs/event.h"
#include "../structs/task.h"
#include "../structs/contact.h"
#include "../structs/note.h"


/**
 * Parse a kolab contact mail structure and create a private contact interchange data structure.
 *
 * @param  kcontact
 *         a kolab contact mail structure
 * @param  error
 *         a glib error structure
 * @return a private contact interchange data structure
 */
I_contact* conv_kolab_conv_mail_to_I_contact (const Kolab_conv_mail* kcontact, GError** error);

/**
 * Create a kolab contact mail structure from a private contact interchange data structure.
 *
 * @param  icontact
 *         an contact interchange data structure. The structure will be freed
 *         and set to NULL.
 * @param  error
 *         a glib error structure
 * @return a kolab contact mail structure
 */
Kolab_conv_mail *conv_I_contact_to_kolab_conv_mail (I_contact **icontact, GError** error);
Kolab_conv_mail *conv_I_event_to_kolab_conv_mail (I_event **i_event_ptr, GError **error);

I_event *conv_kolab_conv_mail_to_I_event (const Kolab_conv_mail *kevent, GError **error);
Kolab_conv_mail *conv_I_event_to_KolabConvMail (I_event **ievent, GError **error);

I_task *conv_kolab_conv_mail_to_I_task (const Kolab_conv_mail *ktask, GError **error);
Kolab_conv_mail *conv_I_task_to_kolab_conv_mail (I_task **itask, GError **error);

I_note *conv_kolab_conv_mail_to_I_note (const Kolab_conv_mail *knote, GError **error);
Kolab_conv_mail *conv_I_note_to_kolab_conv_mail (I_note **inote, GError **error);

#endif /* KOLAB_H_ */
