/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * contact-k-to-i.c
 *
 *  Created on: 12.10.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


/*
 * This file holds operations to read the kolab contact fields from an XML tree
 * to an internal structure.
 *
 * The related header file is: kolab.h
 */

#include <config.h>

#include <glib/gi18n-lib.h>

#include "priv-kolab.h"
#include "kolab.h" /* public part of this file */
#include "../util.h"

/*
 * prototypes for static functions
 */
static void process_contact_k_to_i (I_contact *i_contact, const xmlNodePtr node, GError** error);
static void process_attachments_k_to_i (I_contact* i_contact, GError** gerror);

static gboolean read_contact_element(I_contact *i_contact, xmlNodePtr node, GError **error);

static gboolean handle_sensitivity(I_contact *i_contact, xmlNodePtr node);
static gboolean handle_picture_name (I_contact *i_contact, xmlNodePtr node);
static gboolean handle_element_custom (I_contact *i_contact, const xmlNodePtr node);
static gboolean handle_element_phone (I_contact *i_contact, const xmlNodePtr node);
static gboolean handle_element_email (I_contact *i_contact, const xmlNodePtr node);
static gboolean handle_element_address (I_contact *i_contact, const xmlNodePtr node);
static gboolean handle_contact_name (I_contact *i_contact, xmlNodePtr node);

static Icontact_address_type address_type_k_to_icontact(gchar *k_addr_type);
static Icontact_phone_type phone_type_k_to_icontact(gchar *k_phone_type);


/**
 * Converts a kolab contact mail structure to an internal contact structure.
 *
 * @param  k_contact
 *         A kolab contact mail structure
 * @param  error
 *         A pointer to a NULL error object pointer which will be filled if an
 *         error occurs. It must be checked after calling this operation.
 * @return An internal contact structure
 */
I_contact*
conv_kolab_conv_mail_to_I_contact (const Kolab_conv_mail* k_contact, GError** error)
{
	I_contact *i_contact = NULL;
	xmlNodePtr node = NULL, n_ptr;
	xmlDocPtr doc = NULL;

	log_debug ("\nconv_kolab_conv_mail_to_I_contact(): convert Kolab_conv_mail to I_contact.");

	if (*error != NULL)
		return NULL;
	/* get parsed kolab XML */
	i_contact = new_i_contact ();
	doc = util_get_kolab_xml_part(k_contact, &i_contact->common->kolab_attachment_store, error);

	if (*error != NULL)
		return NULL;

	/* find root node */
	for (n_ptr = doc->children; n_ptr != NULL; n_ptr = n_ptr->next)
		if (n_ptr->type == XML_ELEMENT_NODE &&
		    g_strcmp0((gchar*)n_ptr->name, KLB_CNT) == 0)
			node = n_ptr;
	if (node == NULL) {
		g_set_error (error, KOLABCONV_ERROR_READ_KOLAB,
		             KOLABCONV_ERROR_READ_KOLAB_INVALID_KOLAB_XML,
		             _("Root tag is missing"));
		free_i_contact (&i_contact);
		xmlFreeDoc(doc);
		return NULL;
	}

	process_contact_k_to_i (i_contact, node, error);
	xmlFreeDoc(doc);
	if (*error != NULL) {
		free_i_contact(&i_contact);
		return NULL;
	}

	process_attachments_k_to_i (i_contact, error);
	if (*error != NULL){
		free_i_contact(&i_contact);
		return NULL;
	}

	return i_contact;
}

/**
 * This is the main function to populated the icontact object. It expects a
 * top level node of xml tree containing the kolab contact tag and then parses
 * through its children separating simple (having only one child) and complex nodes
 * (having more than one children) and populating the icontact for all of these nodes.
 *
 * @param icontact
 * 	  A pointer to a I_contact object pre-initialized, which is to be populated.
 * @param node
 * 	  The xml node containing the top level xml tag.
 * @param  error
 *         A pointer to a NULL error object pointer which will be filled if an
 *         error occurs. It must be checked after calling this operation.
 */
static void
process_contact_k_to_i (I_contact *i_contact, const xmlNodePtr node, GError** error)
{
	/* iterate over all child elements of event_node an process them */
	xmlNodePtr n_ptr; /* used in for loop */
	for (n_ptr = node->children; n_ptr != NULL; n_ptr = n_ptr->next) {
		if (n_ptr->type == XML_ELEMENT_NODE) {
			gboolean node_processed = read_contact_element(i_contact, n_ptr, error);
			if (*error != NULL)
				return;
			if (!node_processed)
				i_kolab_store_add_xml_element(i_contact->common, i_contact->common, n_ptr);
		} else
			log_debug("ignored XML node with name %s", n_ptr->name);
	}
}


/**
 * Move special kolab mail attachments which a referenced from inside the kolab
 * XML from the attachment store to the internal contact structure (right now
 * only a photo attachment).
 *
 * @param  i_contact
 *         internal contact structure which also holds an attachment store
 * @param  error
 *         A pointer to a NULL error object pointer which will be filled if an
 *         error occurs. It must be checked after calling this operation
 */
static void
process_attachments_k_to_i (I_contact* i_contact, GError** error)
{
	GList *attach = NULL;

	g_assert(error != NULL && *error == NULL);
	/* iteration of kolabXmlMailArray for adding attachments */

	attach = i_contact->common->kolab_attachment_store;
	while (attach) {
		Kolab_conv_mail_part *mail_part = attach->data;

		/* if (i_contact->picture_name && strcmp (i_contact->picture_name->str, mail_part->name) == 0) { */
		if (i_contact->photo && i_contact->photo->name && strcmp (i_contact->photo->name, mail_part->name) == 0) {
			g_free(i_contact->photo->name);
			g_free(i_contact->photo);
			i_contact->photo = mail_part;

			i_contact->common->kolab_attachment_store =
				g_list_remove(i_contact->common->kolab_attachment_store, (gconstpointer) mail_part);
		} else
			i_contact->photo = NULL;

		attach = attach->next;
	}
}


/**
 * This function handles all contact specific elements, that are availabe in kolab.
 *
 * Called by a function iterating over all nodes, this function returns TRUE if node
 * was found, after writing value in intern structure.
 *
 * If no fitting node found, this function returns FALSE. This is important to write those
 * elements in kolab store.
 */
static gboolean
read_contact_element(I_contact *i_contact, xmlNodePtr node, GError **error)
{
	if (common_k_to_i(i_contact->common, node, error)) {
		return handle_sensitivity(i_contact, node);	/* node processed if it is not sensitivity */
	} else if (*error != NULL)
		return FALSE;

	return
		handle_contact_name(i_contact, node) ||
		handle_element_string(KLB_CNT_FREE_BUSY_URL, &i_contact->free_busy_url, node) ||
		handle_element_string(KLB_CNT_ORGANIZATION, &i_contact->organization, node) ||
		handle_element_string(KLB_CNT_WEB_PAGE, &i_contact->web_page, node) ||
		/* im-address: not available in Evolution, written in kolab store */
		handle_element_string(KLB_CNT_DEPARTMENT, &i_contact->department, node) ||
		handle_element_string(KLB_CNT_OFFICE_LOCATION, &i_contact->office_location, node) ||
		handle_element_string(KLB_CNT_PROFESSION, &i_contact->profession, node) ||
		handle_element_string(KLB_CNT_JOB_TITLE, &i_contact->job_title, node) ||
		handle_element_string(KLB_CNT_MANAGER_NAME, &i_contact->manager_name, node) ||
		handle_element_string(KLB_CNT_ASSISTANT, &i_contact->assistant, node) ||
		handle_element_string(KLB_CNT_NICK_NAME, &i_contact->nick_name, node) ||
		handle_element_string(KLB_CNT_SPOUSE_NAME, &i_contact->spouse_name, node) ||
		handle_element_date(KLB_CNT_BIRTHDAY, &i_contact->birthday, node) ||
		handle_element_date(KLB_CNT_ANNIVERSARY, &i_contact->anniversary, node) ||
		handle_picture_name(i_contact, node) ||
		/* handle_element_string(KLB_CNT_PICTURE, &i_contact->picture_name, node) || */
		/* children: not available in Evolution, written in kolab store
		 * gender: not available in Evolution, written in kolab store
		 * language: not available in Evolution, written in kolab store
		 */
		handle_element_phone (i_contact, node) ||
		handle_element_email (i_contact, node) ||
		handle_element_address (i_contact, node) ||
		/* preferred address: not available in Evolution, written in kolab store */
		handle_element_double (KLB_CNT_LATITUDE, &i_contact->latitude, node) ||
		handle_element_double (KLB_CNT_LONGITUDE, &i_contact->longitude, node) ||
		handle_element_string(KLB_CNT_NAME_SUFFIX, &i_contact->suffix, node) ||
		handle_element_custom (i_contact, node);
}

static gboolean
handle_sensitivity(I_contact *i_contact, xmlNodePtr node)
{
	if (strcmp ((gchar*) node->name, KLB_CMN_SENSITIVITY) != 0)
		return TRUE;	/* Not sensitivity node, so node is processed. */

	/* Otherwise it is the sensitivity node and will be reset to null,
	 * and returned false for not processed, that it will be put in kolab store.
	 */
	i_contact->common->sensitivity = ICOMMON_SENSITIVITY_NULL;
	return FALSE;
}

static gboolean
handle_picture_name (I_contact *i_contact, xmlNodePtr node)
{
	gchar *picture_name = NULL;

	if (strcmp ((gchar*) node->name, KLB_CNT_PICTURE) != 0)
		return FALSE;	/* Not a photo_name node */

	picture_name = xml_get_node_text(node);

	/* create mail part and set name if not empty */
	if (*picture_name) {
		i_contact->photo = g_new0(Kolab_conv_mail_part, 1);
		i_contact->photo->name = g_strdup_printf("%s", picture_name);
	}
#if 0
	else
		i_contact->photo = NULL;
#endif
	return TRUE;
}

/**
 * Appends x-custom element to list in intern structure.
 * Also appends special elements, that represent more than one dataset with separator in value field.
 */
static gboolean
handle_element_custom (I_contact *i_contact, const xmlNodePtr node)
{
	xmlChar* node_value = NULL;
	xmlChar* node_app = NULL;
	xmlChar* node_name = NULL;
	gchar *str = NULL;
	gchar *delim = "\356\200\200";	/*  separator written by Kontact (=EE=84=A0) */
	gchar **tokens = NULL;
	gint i;

	if (strcmp ((gchar*) node->name, KLBX_CNT_X_CUSTOM_LIST) != 0)
		return FALSE;

	if (xmlHasProp (node, (xmlChar*) KLBX_CNT_X_CUSTOM_VALUE))
		node_value = xmlGetProp (node, (xmlChar*) KLBX_CNT_X_CUSTOM_VALUE);
	if (xmlHasProp (node, (xmlChar*) KLBX_CNT_X_CUSTOM_APP))
		node_app = xmlGetProp (node, (xmlChar*) KLBX_CNT_X_CUSTOM_APP);
	if (xmlHasProp (node, (xmlChar*) KLBX_CNT_X_CUSTOM_NAME))
		node_name = xmlGetProp (node, (xmlChar*) KLBX_CNT_X_CUSTOM_NAME);

	/* split value string if delimiter is available
	 * Kontact saves values of datasets from the same type in same xml tag value with delimiter
	 */
	str = (gchar*)node_value;
	tokens = str_split(str, delim);

	for (i = 0; tokens[i]; ++i) {
		/* Add a new custom field to the contact object member custom_list */
		Custom *custom_field = g_new (Custom, 1);
		custom_field->value = g_string_new(tokens[i]);
		custom_field->app = g_string_new ((gchar *) node_app);
		custom_field->name = g_string_new ((gchar *) node_name);

		i_contact->custom_list = g_list_append (i_contact->custom_list, (Custom *) custom_field);

		/* printf("%i: '%s'\n", i, tokens[i]); */
	}

	free_str_tokens(tokens);

	if (node_value != NULL)
		xmlFree (node_value);
	if (node_app != NULL)
		xmlFree (node_app);
	if (node_name != NULL)
		xmlFree (node_name);

	return TRUE;
}

/**
 * This function expects the xml node containing the phone tag and
 * then parse its children to populate an already initialized icontact.
 *
 * @param icontact
 * 	  A pointer to a I_contact object in which phone is to be filled.
 * @param node
 * 	  The xml node containing the phone tag.
 */
static gboolean
handle_element_phone (I_contact *i_contact, const xmlNodePtr node)
{
	const guint max_numbers = 8; /* maximum values that can be shown by evolution, remaining numbers in kolab-store */
	xmlNodePtr text = NULL;
	xmlNodePtr child = NULL;
	xmlNodePtr child_contents = NULL;
	Phone_number *phone_nr = NULL;

	if (strcmp ((gchar*) node->name, KLB_CNT_PHONE_LIST) != 0)
		return FALSE;

	text = node->children;
	phone_nr = g_new0 (Phone_number, 1);

	do {
		child = text->next;
		child_contents = child->children;

		if (strcmp ((gchar *) child->name, KLB_CNT_PHONE_TYPE) == 0)
			phone_nr->type = phone_type_k_to_icontact ((gchar *) child_contents->content);
		else if (strcmp ((gchar *) child->name, KLB_CNT_PHONE_NUMBER) == 0)
			phone_nr->number = g_strdup((gchar*) child_contents->content);
		else
			i_kolab_store_add_xml_element(i_contact->common, phone_nr, child);
		text = child->next;

	} while (text->next != NULL);

	/* special case for combined types with primary */
	if (phone_nr->type == ICONTACT_PHONE_BUSINESS_2 || phone_nr->type == ICONTACT_PHONE_HOME_2) {
		/* if type that has to be saved a two nodes, there must be 2 free places in array */
		if (g_list_length(i_contact->phone_numbers) < max_numbers-1) {
			Phone_number *primary_phone = NULL;

			if (phone_nr->type == ICONTACT_PHONE_BUSINESS_2)	/* business 2 = business 1 & primary */
				phone_nr->type = ICONTACT_PHONE_BUSINESS_1;
			else if (phone_nr->type == ICONTACT_PHONE_HOME_2) 	/* home 2 = home 1 & primary */
				phone_nr->type = ICONTACT_PHONE_HOME_1;

			i_contact->phone_numbers = g_list_append (i_contact->phone_numbers, (Phone_number *) phone_nr);

			/* additional primary node */							/* + primary */
			primary_phone = g_new0 (Phone_number, 1);
			primary_phone->type = ICONTACT_PHONE_PRIMARY;
			primary_phone->number = g_strdup(phone_nr->number);
			i_contact->phone_numbers = g_list_append (i_contact->phone_numbers, (Phone_number *) primary_phone);
		} else  {
			/* array full, put in kolab store */
			free_phone_number((void **) &phone_nr);
			i_kolab_store_add_xml_element(i_contact->common, i_contact->common, node);
		}
	} else if (g_list_length(i_contact->phone_numbers) < max_numbers) {
		/* if normal type, saved as one node, there must be 1 free place in array */
		i_contact->phone_numbers = g_list_append (i_contact->phone_numbers, (Phone_number *) phone_nr);
	} else {
		/* array full, put in kolab store */
		free_phone_number((void **) &phone_nr);
		i_kolab_store_add_xml_element(i_contact->common, i_contact->common, node);
	}

	return TRUE;
}


/**
 * This function expects the xml node containing the email tag and
 * then parse its children to populate an already initialized icontact.
 *
 * @param icontact
 * 	  A pointer to a I_contact object in which email is to be filled.
 * @param node
 * 	  The xml node containing the email tag.
 */
static gboolean
handle_element_email (I_contact *i_contact, const xmlNodePtr node)
{
	xmlNodePtr text = NULL;
	xmlNodePtr child = NULL;
	xmlNodePtr child_contents = NULL;
	Email *email = NULL;

	if (strcmp ((gchar*) node->name, KLB_CNT_EMAIL_LIST) != 0)
		return FALSE;

	if (g_list_length(i_contact->emails) >= 4)
		return FALSE;

	text = node->children;
	email = g_new0 (Email, 1);

	while (text != NULL && text->next != NULL) {

		child = text->next;
		child_contents = child->children;

		if (strcmp ((gchar *) child->name, KLB_CNT_EMAIL_SMTP_ADDRESS) == 0)
			email->smtp_address = g_string_new ((gchar *) child_contents->content);
		else
			i_kolab_store_add_xml_element(i_contact->common, email, child);

		text = child->next;
	}

	i_contact->emails = g_list_append (i_contact->emails, (Email *) email);

	return TRUE;
}


/**
 * This function expects the xml node containing the address tag and
 * then parse its children to populate an already initialized icontact.
 *
 * @param icontact
 * 	  A pointer to a I_contact object in which address is to be filled.
 * @param node
 * 	  The xml node containing the address tag.
 */
static gboolean
handle_element_address (I_contact *i_contact, const xmlNodePtr node)
{
	Address *address = NULL;
	xmlNodePtr text = NULL;
	xmlNodePtr child = NULL;
	xmlNodePtr child_contents = NULL;

	if (strcmp ((gchar*) node->name, KLB_CNT_ADDRESS_LIST) != 0)
		return FALSE;

	address = g_new0 (Address, 1);
	text = node->children;

	do {
		child = text->next;
		child_contents = child->children;

		if (strcmp ((gchar *) child->name, KLB_CNT_ADDRESS_TYPE) == 0)
			address->type = address_type_k_to_icontact ((gchar *) child_contents->content);
		else if (strcmp ((gchar *) child->name, KLB_CNT_ADDRESS_STREET) == 0)
			address->street = g_string_new (xml_get_node_text(child));
		else if (strcmp ((gchar *) child->name, KLBX_CNT_ADDRESS_POBOX) == 0)
			address->pobox = g_string_new (xml_get_node_text(child));
		else if (strcmp ((gchar *) child->name, KLB_CNT_ADDRESS_LOCALITY) == 0)
			address->locality = g_string_new (xml_get_node_text(child));
		else if (strcmp ((gchar *) child->name, KLB_CNT_ADDRESS_REGION) == 0)
			address->region = g_string_new (xml_get_node_text(child));
		else if (strcmp ((gchar *) child->name, KLB_CNT_ADDRESS_POSTAL_CODE) == 0)
			address->postal_code = g_string_new (xml_get_node_text(child));
		else if (strcmp ((gchar *) child->name, KLB_CNT_ADDRESS_COUNTRY) == 0)
			address->country = g_string_new (xml_get_node_text(child));
		else
			i_kolab_store_add_xml_element(i_contact->common, address, child);

		text = child->next;
		/* log_debug("\nBBB: %s-%s-%s-%s",text->name, text->content, child->name, child->content); */
		/* log_debug("\nAAA%s", (char *)text->next); */

	} while (text->next != NULL);

	/* test if element of same type still in address list (evolution can just handle one of each type) */
	if (g_list_find_custom(i_contact->addresses, &address->type, (GCompareFunc)strcmp)) {
		/* TODO Remove, if needed, child-element from store, because in this case full address will be put in kolab store. */
		/* g_hash_table_remove(i_contact->common->kolab_store, child); */
		return FALSE;	/* return element not handled, then will be put in kolab store */
	} else {
		i_contact->addresses = g_list_append (i_contact->addresses, (Address *) address);
		return TRUE;
	}
}


/**
 * All fields of contact belong to name are handled in this function.
 *
 * Returns TRUE if element found and written in intern structure.
 * Returns FALSE in case of no fitting element.
 */
static gboolean
handle_contact_name (I_contact *i_contact, xmlNodePtr node)
{
	xmlNodePtr n_ptr; /* used in for loop */

	if (strcmp ((gchar*) node->name, KLB_CNT_NAME) != 0)
		return FALSE;

	/* iterate over all child elements of node an process them */
	for (n_ptr = node->children; n_ptr != NULL; n_ptr = n_ptr->next) {
		if (n_ptr->type == XML_ELEMENT_NODE) {
			if (handle_element_string(KLB_CNT_NAME_GIVEN_NAME, &i_contact->given_name, n_ptr) ||
			    handle_element_string(KLB_CNT_NAME_MIDDLE_NAMES, &i_contact->middle_names, n_ptr) ||
			    handle_element_string(KLB_CNT_NAME_LAST_NAME, &i_contact->last_name, n_ptr) ||
			    handle_element_string(KLB_CNT_NAME_FULL_NAME, &i_contact->full_name, n_ptr) ||
			    /* TODO: Contact: <initials> ?!!! */
			    handle_element_string(KLB_CNT_NAME_PREFIX, &i_contact->prefix, n_ptr) ||
			    handle_element_string(KLB_CNT_NAME_SUFFIX, &i_contact->suffix, n_ptr))
				continue;

			i_kolab_store_add_xml_element(i_contact->common, (gpointer) KOLAB_STORE_PTR_CONTACT_NAME, n_ptr);
		}
	}


	return TRUE;
}


/**
 * Converts a kolab XML address type to an enumeration value which is used in an
 * internal contact structure.
 *
 * @param  k_addr_type
 *         a kolab XML address type
 * @return enumeration value which is used in an internal contact structure
 */
static Icontact_address_type
address_type_k_to_icontact(gchar *k_addr_type)
{
	if (strcmp (k_addr_type, KLB_CNT_ADDRESS_TYPE_HOME) == 0)
		return ICONTACT_ADDR_TYPE_HOME;
	else if (strcmp (k_addr_type, KLB_CNT_ADDRESS_TYPE_BUSINESS) == 0)
		return ICONTACT_ADDR_TYPE_BUSINESS;
	else
		return ICONTACT_ADDR_TYPE_OTHER;
}


/**
 * map kolab phone type to icontact phone type
 */
static Icontact_phone_type
phone_type_k_to_icontact(gchar *k_phone_type)
{
	if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_ASSISTANT) == 0)
		return ICONTACT_PHONE_ASSISTANT;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_BUSINESS_1) == 0)
		return ICONTACT_PHONE_BUSINESS_1;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_BUSINESS_2) == 0)
		return ICONTACT_PHONE_BUSINESS_2;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_BUSINESS_FAX) == 0)
		return ICONTACT_PHONE_BUSINESS_FAX;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_CALLBACK) == 0)
		return ICONTACT_PHONE_CALLBACK;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_CAR) == 0)
		return ICONTACT_PHONE_CAR;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_COMPANY) == 0)
		return ICONTACT_PHONE_COMPANY;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_HOME_1) == 0)
		return ICONTACT_PHONE_HOME_1;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_HOME_2) == 0)
		return ICONTACT_PHONE_HOME_2;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_HOME_FAX) == 0)
		return ICONTACT_PHONE_HOME_FAX;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_ISDN) == 0)
		return ICONTACT_PHONE_ISDN;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_MOBILE) == 0)
		return ICONTACT_PHONE_MOBILE;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_OTHER) == 0)
		return ICONTACT_PHONE_OTHER;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_PAGER) == 0)
		return ICONTACT_PHONE_PAGER;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_PRIMARY) == 0)
		return ICONTACT_PHONE_PRIMARY;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_RADIO) == 0)
		return ICONTACT_PHONE_RADIO;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_TELEX) == 0)
		return ICONTACT_PHONE_TELEX;
	else if (strcmp (k_phone_type, KLB_CNT_PHONE_TYPE_TTYTDD) == 0)
		return ICONTACT_PHONE_TTYTDD;
	else
		return ICONTACT_PHONE_PRIMARY;
}
