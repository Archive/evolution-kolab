/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * priv-common-i-to-k.c
 *
 *  Created on: 30.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "priv-kolab.h" /* public part of this file */


/**
 * A conversion function, which converts an Enum Sensitivity object to
 * a string representation of Enum Sensitivity .
 *
 * @param sensitivity
 * 	  A Enum Sensitivity object generated from input string.
 *
 * @return The Enum Sensitivity representation in string format.
 */
static char*
sensitivity_i_to_k (Sensitivity sensitivity)
{
	switch (sensitivity) {
	case ICOMMON_SENSITIVITY_PUBLIC:
		return KLB_CMN_SENSITIVITY_PUBLIC;
	case ICOMMON_SENSITIVITY_PRIVATE:
		return KLB_CMN_SENSITIVITY_PRIVATE;
	case ICOMMON_SENSITIVITY_CONFIDENTIAL:
		return KLB_CMN_SENSITIVITY_CONFIDENTIAL;
	case ICOMMON_SENSITIVITY_NULL:
		return KLB_CMN_SENSITIVITY_PUBLIC; /* value doesn't matter, because if case is ICOMMON_SENSITIVITY_NULL the tag is not written */
	default:
		return KLB_CMN_SENSITIVITY_PUBLIC; /* default value */
	}
}


/**
 * Convert recurrence month from libekolabconv to kolab
 */
char*
recurrence_month_i_to_k (Month month)
{
	switch (month) {
	case I_COMMON_JAN:
		return KLB_INC_RCR_MONTH_JAN;
	case I_COMMON_FEB:
		return KLB_INC_RCR_MONTH_FEB;
	case I_COMMON_MAR:
		return KLB_INC_RCR_MONTH_MAR;
	case I_COMMON_APR:
		return KLB_INC_RCR_MONTH_APR;
	case I_COMMON_MAY:
		return KLB_INC_RCR_MONTH_MAY;
	case I_COMMON_JUN:
		return KLB_INC_RCR_MONTH_JUN;
	case I_COMMON_JUL:
		return KLB_INC_RCR_MONTH_JUL;
	case I_COMMON_AUG:
		return KLB_INC_RCR_MONTH_AUG;
	case I_COMMON_SEP:
		return KLB_INC_RCR_MONTH_SEP;
	case I_COMMON_OCT:
		return KLB_INC_RCR_MONTH_OCT;
	case I_COMMON_NOV:
		return KLB_INC_RCR_MONTH_NOV;
	case I_COMMON_DEC:
		return KLB_INC_RCR_MONTH_DEC;
	default:
		g_assert_not_reached();
	}
}

/**
 * Convert recurrence weekday from libekolabconv to kolab
 */
char*
recurrence_weekday_i_to_k(Week_day weekday)
{
	switch (weekday) {
	case I_COMMON_MONDAY:
		return KLB_INC_RCR_DAY_MONDAY;
	case I_COMMON_TUESDAY:
		return KLB_INC_RCR_DAY_TUESDAY;
	case I_COMMON_WEDNESDAY:
		return KLB_INC_RCR_DAY_WEDNESDAY;
	case I_COMMON_THURSDAY:
		return KLB_INC_RCR_DAY_THURSDAY;
	case I_COMMON_FRIDAY:
		return KLB_INC_RCR_DAY_FRIDAY;
	case I_COMMON_SATURDAY:
		return KLB_INC_RCR_DAY_SATURDAY;
	case I_COMMON_SUNDAY:
		return KLB_INC_RCR_DAY_SUNDAY;
	default:
		g_assert_not_reached();
	}
}

/**
 * Convert I_common to kolab XML xmlNodePtr
 */
void
common_i_to_k(xmlNodePtr root_node, I_common *i_common)
{
	GList *lsl = NULL;

	if (i_common == NULL)
		return;
	/* Construct children of root node. */
	add_child_node(root_node, KLB_CMN_PRODUCT_ID, KOLABCONV_PRODUCT_ID);
	g_assert(i_common->uid);
	add_child_node(root_node, KLB_CMN_UID, i_common->uid->str );
	if(i_common->body)
		add_child_node(root_node, KLB_CMN_BODY, i_common->body->str);
	if(i_common->categories)
		add_child_node(root_node, KLB_CMN_CATEGORIES, i_common->categories->str);
	if(i_common->creation_datetime) {
		gchar *creation_string = date_or_datetime_to_string(i_common->creation_datetime);
		add_child_node(root_node, KLB_CMN_CREATION_DATE, creation_string);
		g_free(creation_string);
	}
	if(i_common->last_modified_datetime) {
		gchar *modified_string = date_or_datetime_to_string(i_common->last_modified_datetime);
		add_child_node(root_node, KLB_CMN_LAST_MODIFICATION_DATE, modified_string);
		g_free(modified_string);
	}
	if(i_common->sensitivity)
		if (i_common->sensitivity != ICOMMON_SENSITIVITY_NULL)	/* if its ICOMMON_SENSITIVITY_NULL it is either in kolab store or has never been there */
			add_child_node(root_node, KLB_CMN_SENSITIVITY, sensitivity_i_to_k(i_common->sensitivity));
	if(i_common->vtimezone)
		add_child_node(root_node, KLB_CMN_VTIMEZONE, i_common->vtimezone);
	if(i_common->is_html_richtext)
		add_child_node(root_node, KLBX_CMN_RICHTEXT, i_common->is_html_richtext == TRUE ? "true" : "false" );
	/* add link attachments to the kolab xml */
	lsl = i_common->link_attachments;
	for (; lsl; lsl=lsl->next)
		add_child_node(root_node, KLB_CMN_LINK_ATTACHMENT, (gchar*)lsl->data);
	/* add inline attachment names to the kolab xml */
	lsl = i_common->inline_attachment_names;
	for (; lsl; lsl=lsl->next)
		add_child_node(root_node, KLB_CMN_INLINE_ATTACHMENT, (gchar*)lsl->data);

	k_evo_store_add_all(i_common, root_node);
}
