/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * kolab_constants.h
 *
 *  Created on: 28.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *              Umer Kayani <u.kayani@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

/*
 * This file holds all constants which are needed to read and write the kolab
 * format.
 */

#ifndef KOLAB_CONSTANTS_H_
#define KOLAB_CONSTANTS_H_

/**
 * All constants defined in the kolab format specification [1] does have the
 * prefix "KLB_". All constants which are used by kolab clients like kontact or
 * toltec connector and which are not defined in the kolab format specification
 * does have the prefix "KLBX_". Fields which are not used by other kolab
 * clients and are not part of the specification does have the prefix "KLBC_".
 *
 * [1] http://kolab.org/doc/kolabformat-2.0rc7-html/
 */

/* ------------------------------------------------------------- common fields { */

#define KLB_CMN_UID                                     "uid"
#define KLB_CMN_BODY                                    "body"
#define KLB_CMN_CATEGORIES                              "categories"
#define KLB_CMN_CREATION_DATE                           "creation-date"
#define KLB_CMN_LAST_MODIFICATION_DATE                  "last-modification-date"

/* tag from Kontact, showing if body text is with or without html */
#define KLBX_CMN_RICHTEXT								"knotes-richtext"

#define KLB_CMN_SENSITIVITY                             "sensitivity"
#define KLB_CMN_SENSITIVITY_PRIVATE                     "private"
#define KLB_CMN_SENSITIVITY_CONFIDENTIAL                "confidential"
#define KLB_CMN_SENSITIVITY_PUBLIC                      "public"

#define KLB_CMN_INLINE_ATTACHMENT                       "inline-attachment"
#define KLB_CMN_LINK_ATTACHMENT                         "link-attachment"
#define KLB_CMN_PRODUCT_ID                              "product-id"

#define KLB_CMN_VTIMEZONE                               "x-kolabconv-vtimezone"
#define KLB_CMN_EVO_STORE                               "x-kolabconv-store"


/* ------------------------------------------------------- own timezone fields } */

/* ------------------------------------------------------------- common fields } */

/* ---------------------------------------------------------- incidence fields { */

#define KLB_INC_SUMMARY                                 "summary"
#define KLB_INC_LOCATION                                "location"
#define KLB_INC_CREATOR                                 "creator"
#define KLB_INC_ORGANIZER                               "organizer"

/** display-name and smtp-address are used as subelements of creator, organizer
 * and attendee.
 */
#define KLB_INC_DISPLAY_NAME                            "display-name"
#define KLB_INC_SMTP_ADDRESS                            "smtp-address"

#define KLB_INC_START_DATE                              "start-date"
#define KLB_INC_ALARM                                   "alarm"

/* ----------------------------------------------------- advanced alarm fields { */

/* advanced alarm fields are not defined in the kolab format specification
 * see: http://www.kolab.org/pipermail/kolab-format/2008-September/000873.html
 */

#define KLBX_INC_ADV_ALARM                              "advanced-alarms"
#define KLBX_INC_ADV_ALARM_TYPE                         "type"
#define KLBX_INC_ADV_ALARM_TYPE_DISPLAY                 "display"
#define KLBX_INC_ADV_ALARM_TYPE_DISPLAY_TEXT            "text"
#define KLBX_INC_ADV_ALARM_TYPE_AUDIO                   "audio"
#define KLBX_INC_ADV_ALARM_TYPE_AUDIO_FILE              "file"
#define KLBX_INC_ADV_ALARM_TYPE_PROCEDURE               "procedure"
#define KLBX_INC_ADV_ALARM_TYPE_PROCEDURE_PROGRAM       "program"
#define KLBX_INC_ADV_ALARM_TYPE_PROCEDURE_ARGUMENTS     "arguments"
#define KLBX_INC_ADV_ALARM_TYPE_EMAIL                   "email"
#define KLBX_INC_ADV_ALARM_TYPE_EMAIL_ADDRESSES			"addresses"
#define KLBX_INC_ADV_ALARM_TYPE_EMAIL_ADDRESSES_ADDRESS	"address"
#define KLBX_INC_ADV_ALARM_TYPE_EMAIL_SUBJECT			"subject"
#define KLBX_INC_ADV_ALARM_TYPE_EMAIL_MAIL_TEXT			"mail-text"
#define KLBX_INC_ADV_ALARM_TYPE_EMAIL_ATTACHMENTS		"attachments"
#define KLBX_INC_ADV_ALARM_TYPE_EMAIL_ATTACHMENTS_ATT	"attachment"
#define KLBX_INC_ADV_ALARM_START_OFFSET                 "start-offset"
#define KLBX_INC_ADV_ALARM_END_OFFSET                   "end-offset"
#define KLBX_INC_ADV_ALARM_REPEAT_COUNT                 "repeat-count"
#define KLBX_INC_ADV_ALARM_REPEAT_INTERVAL              "repeat-interval"

/* ----------------------------------------------------- advanced alarm fields } */

#define KLB_INC_RECURRENCE                              "recurrence"

#define KLB_INC_RCR_CYCLE                               "cycle"
#define KLB_INC_RCR_CYCLE_DAILY                         "daily"
#define KLB_INC_RCR_CYCLE_WEEKLY                        "weekly"
#define KLB_INC_RCR_CYCLE_MONTHLY                       "monthly"
#define KLB_INC_RCR_CYCLE_YEARLY                        "yearly"

#define KLB_INC_RCR_CYCLETYPE                           "type"
#define KLB_INC_RCR_CYCLETYPE_YEARDAY                   "yearday"
#define KLB_INC_RCR_CYCLETYPE_WEEKDAY                   "weekday"
#define KLB_INC_RCR_CYCLETYPE_DAYNUMBER                 "daynumber"
#define KLB_INC_RCR_CYCLETYPE_MONTHDAY                  "monthday"

#define KLB_INC_RCR_INTERVAL                            "interval"

#define KLB_INC_RCR_DAY                                 "day"
#define KLB_INC_RCR_DAY_MONDAY                          "monday"
#define KLB_INC_RCR_DAY_TUESDAY                         "tuesday"
#define KLB_INC_RCR_DAY_WEDNESDAY                       "wednesday"
#define KLB_INC_RCR_DAY_THURSDAY                        "thursday"
#define KLB_INC_RCR_DAY_FRIDAY                          "friday"
#define KLB_INC_RCR_DAY_SATURDAY                        "saturday"
#define KLB_INC_RCR_DAY_SUNDAY                          "sunday"

#define KLB_INC_RCR_DAYNUMBER                           "daynumber"

#define KLB_INC_RCR_MONTH                               "month"
#define KLB_INC_RCR_MONTH_JAN                           "january"
#define KLB_INC_RCR_MONTH_FEB                           "february"
#define KLB_INC_RCR_MONTH_MAR                           "march"
#define KLB_INC_RCR_MONTH_APR                           "april"
#define KLB_INC_RCR_MONTH_MAY                           "may"
#define KLB_INC_RCR_MONTH_JUN                           "june"
#define KLB_INC_RCR_MONTH_JUL                           "july"
#define KLB_INC_RCR_MONTH_AUG                           "august"
#define KLB_INC_RCR_MONTH_AUG_TOLTEC                    "agust"
#define KLB_INC_RCR_MONTH_SEP                           "september"
#define KLB_INC_RCR_MONTH_OCT                           "october"
#define KLB_INC_RCR_MONTH_NOV                           "november"
#define KLB_INC_RCR_MONTH_DEC                           "december"

#define KLB_INC_RCR_RANGE                               "range"
#define KLB_INC_RCR_RANGETYPE                           "type"
#define KLB_INC_RCR_RANGETYPE_NONE                      "none"
#define KLB_INC_RCR_RANGETYPE_NUMBER                    "number"
#define KLB_INC_RCR_RANGETYPE_DATE                      "date"

#define KLB_INC_RCR_EXCLUSION                           "exclusion"

#define KLB_INC_ATTENDEE                                "attendee"

/** the subelements display-name and smtp-address in attendee are the same like
 * in orgaizer and creator => the above defined constants are also used here
 */

#define KLB_INC_ATTENDEE_STATUS                         "status"
#define KLB_INC_ATTENDEE_STATUS_NONE                    "none"
#define KLB_INC_ATTENDEE_STATUS_TENTATIVE               "tentative"
#define KLB_INC_ATTENDEE_STATUS_ACCEPTED                "accepted"
#define KLB_INC_ATTENDEE_STATUS_DECLINED                "declined"
#define KLBX_INC_ATTENDEE_STATUS_DELEGATED              "delegated"

#define KLB_INC_ATTENDEE_REQUEST_RESPONSE               "request-response"

#define KLB_INC_ATTENDEE_ROLE                           "role"
#define KLB_INC_ATTENDEE_ROLE_REQUIRED                  "required"
#define KLB_INC_ATTENDEE_ROLE_OPTIONAL                  "optional"
#define KLB_INC_ATTENDEE_ROLE_RESOURCE                  "resource"

/** not part of the kolab format specification but written by kontact */
#define KLBX_INC_ATTENDEE_INVITATION_SENT               "invitation-sent"

/* evolution alarm */
#define KLBC_INC_X_EVOLUTION_ALARM_UID					"x-evolution-alarm-uid"
#define	KLBC_INC_DESCRIPTION							"description"
#define	KLBC_INC_ACTION									"action"
#define	KLBC_INC_TRIGGER								"trigger"
#define	KLBC_INC_RELATED								"related"
#define	KLBC_INC_VALUE									"value"
#define	KLBC_INC_REPEAT									"repeat"
#define	KLBC_INC_DURATION								"duration"
#define	KLBC_INC_CUTYPE									"cutype"
#define	KLBC_INC_LANGUAGE								"language"

/* Undecided fields */
#if 0
#define	INCIDENCE_PILOT_SYNC_STATUS					"pilot-sync-status"
#define	INCIDENCE_DESCRIPTION						""
#endif

/* ---------------------------------------------------------- incidence fields } */

/* ------------------------------------------------------------ contact fields { */

#define KLB_CNT											"contact"

#define KLB_CNT_NAME									"name"
#define KLB_CNT_NAME_GIVEN_NAME							"given-name"
#define KLB_CNT_NAME_MIDDLE_NAMES						"middle-names"
#define KLB_CNT_NAME_LAST_NAME							"last-name"
#define KLB_CNT_NAME_FULL_NAME							"full-name"
#define KLB_CNT_NAME_INITIALS							"initials"
#define KLB_CNT_NAME_PREFIX								"prefix"
#define KLB_CNT_NAME_SUFFIX								"suffix"

#define KLB_CNT_FREE_BUSY_URL							"free-busy-url"
#define KLB_CNT_ORGANIZATION							"organization"
#define KLB_CNT_WEB_PAGE								"web-page"
#define KLB_CNT_IM_ADDRESS								"im-address"
#define KLB_CNT_DEPARTMENT								"department"
#define KLB_CNT_OFFICE_LOCATION							"office-location"
#define KLB_CNT_PROFESSION								"profession"
#define KLBX_CNT_ROLE									"role"
#define KLB_CNT_JOB_TITLE								"job-title"
#define KLB_CNT_MANAGER_NAME							"manager-name"
#define KLB_CNT_ASSISTANT								"assistant"
#define KLB_CNT_NICK_NAME								"nick-name"
#define KLB_CNT_SPOUSE_NAME								"spouse-name"
#define KLB_CNT_BIRTHDAY								"birthday"
#define KLB_CNT_ANNIVERSARY								"anniversary"
#define KLB_CNT_PICTURE									"picture"
#define KLBX_CNT_LOGO									"x-logo"
#define KLBX_CNT_SOUND									"sound"
#define KLB_CNT_CHILDREN								"children"
#define KLB_CNT_GENDER									"gender"
#define KLB_CNT_LANGUAGE								"language"

#define KLB_CNT_PHONE_LIST								"phone"			/* phone list */
#define KLB_CNT_PHONE_TYPE								"type"
#define KLB_CNT_PHONE_TYPE_ASSISTANT					"assistant"
#define KLB_CNT_PHONE_TYPE_BUSINESS_1					"business1"
#define KLB_CNT_PHONE_TYPE_BUSINESS_2					"business2"
#define KLB_CNT_PHONE_TYPE_BUSINESS_FAX					"businessfax"
#define KLB_CNT_PHONE_TYPE_CALLBACK						"callback"
#define KLB_CNT_PHONE_TYPE_CAR							"car"
#define KLB_CNT_PHONE_TYPE_COMPANY						"company"
#define KLB_CNT_PHONE_TYPE_HOME_1						"home1"
#define KLB_CNT_PHONE_TYPE_HOME_2						"home2"
#define KLB_CNT_PHONE_TYPE_HOME_FAX						"homefax"
#define KLB_CNT_PHONE_TYPE_ISDN							"isdn"
#define KLB_CNT_PHONE_TYPE_MOBILE						"mobile"
#define KLB_CNT_PHONE_TYPE_OTHER						"other"
#define KLB_CNT_PHONE_TYPE_PAGER						"pager"
#define KLB_CNT_PHONE_TYPE_PRIMARY						"primary"
#define KLB_CNT_PHONE_TYPE_RADIO						"radio"
#define KLB_CNT_PHONE_TYPE_TELEX						"telex"
#define KLB_CNT_PHONE_TYPE_TTYTDD						"ttytdd"
#define KLB_CNT_PHONE_NUMBER							"number"

#define KLB_CNT_EMAIL_LIST								"email"			/* email list */
#define KLB_CNT_EMAIL_DISPLAY_NAME						"display-name"
#define KLB_CNT_EMAIL_SMTP_ADDRESS						"smtp-address"
#define KLBC_CNT_EMAIL_TYPE								"x-evolution-email-type"
#define KLBC_CNT_EMAIL_X_EVOLUTION_UI_SLOT				"x-evolution-ui-slot"
#define KLBC_CNT_EMAIL_X_MOZILLA_HTML					"x-mozilla-html"

#define KLB_CNT_ADDRESS_LIST							"address"		/* address list */
#define KLB_CNT_ADDRESS_TYPE							"type"
#define KLB_CNT_ADDRESS_TYPE_HOME						"home"
#define KLB_CNT_ADDRESS_TYPE_BUSINESS					"business"
#define KLB_CNT_ADDRESS_TYPE_OTHER						"other"
#define KLB_CNT_ADDRESS_STREET							"street"
#define KLB_CNT_ADDRESS_LOCALITY						"locality"
#define KLB_CNT_ADDRESS_REGION							"region"
#define KLB_CNT_ADDRESS_POSTAL_CODE						"postal-code"
#define KLB_CNT_ADDRESS_COUNTRY							"country"
#define KLBX_CNT_ADDRESS_POBOX							"pobox"
#define KLB_CNT_PREFERRED_ADDRESS						"preferred-address"

/* GEO */
#define KLB_CNT_LATITUDE								"latitude"
#define KLB_CNT_LONGITUDE								"longitude"

#define KLBX_CNT_X_CUSTOM_LIST							"x-custom"
#define KLBX_CNT_X_CUSTOM_APP							"app"
#define KLBX_CNT_X_CUSTOM_APP_AIM						"messaging/aim"
#define KLBX_CNT_X_CUSTOM_APP_GROUPWISE					"messaging/groupwise"
#define KLBX_CNT_X_CUSTOM_APP_ICQ						"messaging/icq"
#define KLBX_CNT_X_CUSTOM_APP_YAHOO						"messaging/yahoo"
#define KLBX_CNT_X_CUSTOM_APP_MSN						"messaging/msn"
#define KLBX_CNT_X_CUSTOM_APP_XMPP						"messaging/xmpp"
#define KLBX_CNT_X_CUSTOM_APP_GADUGADU					"messaging/gadu"
#define KLBX_CNT_X_CUSTOM_APP_SKYPE						"messaging/skype"
#define KLBX_CNT_X_CUSTOM_NAME							"name"
#define KLBX_CNT_X_CUSTOM_NAME_BLOGFEED					"BlogFeed"
#define KLBX_CNT_X_CUSTOM_NAME_ALL						"All"
#define KLBX_CNT_X_CUSTOM_VALUE							"value"

#define KLBC_CNT_X_EVOLUTION_ADDRESS_EXT				"x-evolution-address-ext"
#define KLBC_CNT_CAL_URI								"cal-uri"
#define KLBC_CNT_X_EVOLUTION_VIDEO_URL					"x-evolution-video-url"
#define KLBC_CNT_X_EVOLUTION_FILE_AS					"x-evolution-file-as"
#define KLBC_CNT_KEY									"key"
#define KLBC_CNT_ICSCALENDAR							"ics-calendar"
#define KLBC_CNT_GEO									"geo"
#define KLBC_CNT_X_EVOLUTION_LIST						"x-evolution-list"
#define KLBC_CNT_X_SIP									"x-sip"
#define KLBC_CNT_X_EVOLUTION_BOOK_URI					"x-evolution-book-uri"
#define KLBC_CNT_X_EVOLUTION_LIST_SHOW_ADDRESSES		"x-evolution-list-show-addresses"


/* ------------------------------------------------------------ contact fields } */

/* -------------------------------------------------------------- event fields { */

#define KLB_EVENT										"event"

#define KLB_EVENT_SHOW_TIME_AS							"show-time-as"
#define KLB_EVENT_SHOW_TIME_AS_FREE						"free"
#define KLB_EVENT_SHOW_TIME_AS_TENTATIVE				"tentative"
#define KLB_EVENT_SHOW_TIME_AS_BUSY						"busy"
#define KLB_EVENT_SHOW_TIME_AS_OUT_OF_OFFICE			"outofoffice"
#define KLB_EVENT_COLOR_LABEL							"color-label"
#define KLB_EVENT_COLOR_LABEL_NONE 						"none"
#define KLB_EVENT_COLOR_LABEL_IMPORTANT 				"important"
#define KLB_EVENT_COLOR_LABEL_BUSINESS 					"business"
#define KLB_EVENT_COLOR_LABEL_PERSONAL 					"personal"
#define KLB_EVENT_COLOR_LABEL_VACATION 					"vacation"
#define KLB_EVENT_COLOR_LABEL_MUST_ATTEND 				"must-attend"
#define KLB_EVENT_COLOR_LABEL_TRAVEL_REQUIRED			"travel-required"
#define KLB_EVENT_COLOR_LABEL_NEEDS_PREPARATION 		"needs-preparation"
#define KLB_EVENT_COLOR_LABEL_BIRTHDAY 					"birthday"
#define KLB_EVENT_COLOR_LABEL_ANNIVERSARY 				"anniversary"
#define KLB_EVENT_COLOR_LABEL_PHONE_CALL 				"phone-call"
#define KLB_EVENT_END_DATE								"end-date"

#define KLBC_INCIDENCE_ATTENDEE_TYPE                     "x-kolabconv-type"
#define KLBC_INCIDENCE_ATTENDEE_TYPE_INDIVIDUAL                     "individual"
#define KLBC_INCIDENCE_ATTENDEE_TYPE_GROUP                     		"group"
#define KLBC_INCIDENCE_ATTENDEE_TYPE_RESOURCE                     "resource"
#define KLBC_INCIDENCE_ATTENDEE_TYPE_ROOM                     		"room"

/* -------------------------------------------------------------- event fields } */

/* --------------------------------------------------------------- task fields { */

#define KLB_TASK										"task"

#define KLB_TASK_PRIORITY								"priority"
#define KLBX_TASK_KCAL_PRIORITY							"x-kcal-priority"
#define KLB_TASK_COMPLETED								"completed"
#define KLB_TASK_STATUS									"status"
#define KLB_TASK_STATUS_NOT_STARTED						"not-started"
#define KLB_TASK_STATUS_IN_PROGRESS						"in-progress"
#define KLB_TASK_STATUS_COMPLETED 						"completed"
#define KLB_TASK_STATUS_WAITING_ON_SOMEONE_ELSE 		"waiting-on-someone-else"
#define KLB_TASK_STATUS_DEFERRED	 					"deferred"
#define KLB_TASK_DUE_DATE								"due-date"
#define KLB_TASK_PARENT									"parent"

#define KLBX_TASK_COMPLETED_DATE								"x-completed-date"

/* --------------------------------------------------------------- task fields } */

/* --------------------------------------------------------------- note fields { */

#define KLB_NOTE										"note"

#define KLB_NOTE_SUMMARY								"summary"
#define KLB_NOTE_BACKGROUND_COLOR						"background-color"
#define KLB_NOTE_FOREGROUND_COLOR						"foreground-color"

/* --------------------------------------------------------------- note fields } */

#endif /* KOLAB_CONSTANTS_H_ */
