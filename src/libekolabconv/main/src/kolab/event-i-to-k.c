/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * event-i-to-k.c
 *
 *  Created on: 30.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *              Umer Kayani <u.kayani@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

/* FIXME remove the use of strdup(), replace by g_strdup() */
#define _BSD_SOURCE
#define _SVID_SOURCE
#define _XOPEN_SOURCE 500

#include <string.h>

#include "priv-kolab.h"
#include "kolab.h" /* public part of this file */
#include "../util.h"

/*
 * prototypes for static functions
 */
static gchar *i_event_to_xml(const I_event *i_event);
static gchar *show_time_as_to_kolab_string (Show_time_as show_time_as);


Kolab_conv_mail*
conv_I_event_to_kolab_conv_mail (I_event **i_event_ptr, GError **error)
{
	GList* mail_part_list = NULL;

	g_assert(error != NULL && *error == NULL);
	log_debug ("\nconv_I_event_to_kolab_conv_mail(): convert I_event to Kolab_conv_mail.");

	if (*i_event_ptr) {
		I_event *i_event = *i_event_ptr;
		Kolab_conv_mail_part *mail_part;

		gchar * xml_text = i_event_to_xml(i_event);
		/* log_debug("\n\nXml Text:\n%s",xmlText); */

		/* ADD XML mailPart */
		mail_part = g_new0(Kolab_conv_mail_part, 1);
		mail_part->data = xml_text;
		mail_part->length = strlen(xml_text);
		/* static strings must be copied because they will get freed later */
		mail_part->mime_type = strdup("application/x-vnd.kolab.event");
		mail_part->name = strdup("kolab-common1.xml"); /* Change it after adding a field in contact.h */
		/* mailPart->order = 1; Change it after adding a field in contact.h */

		mail_part_list = g_list_append(mail_part_list, mail_part);

		mail_part_list = g_list_concat(mail_part_list, g_list_copy(i_event->incidence->common->kolab_attachment_store));

		free_i_event(i_event_ptr);
		*i_event_ptr = NULL;
	}

	return g_list_to_kolab_conv_mail(mail_part_list);
}

static gchar *
i_event_to_xml(const I_event *i_event)
{
	GString *xml_string = g_string_new("");

	xmlNodePtr root_node = NULL;
	xmlDocPtr doc = NULL;
	xmlChar *xml_buf = NULL;
	gchar *return_string = NULL;
	gint buf_size;

	doc = xmlNewDoc(BAD_CAST "1.0");
	root_node = xmlNewNode(NULL, BAD_CAST KLB_EVENT);
	add_property (root_node, "version", "1.0");
	/* xmlNodeSetContent(root_node, BAD_CAST "content"); */
	xmlDocSetRootElement(doc, root_node);

	conv_incidence_i_to_k(root_node, i_event->incidence);

	if (i_event->show_time_as)
		add_child_node(root_node, KLB_EVENT_SHOW_TIME_AS, show_time_as_to_kolab_string(i_event->show_time_as));
	if (i_event->end_date) {
		gchar *end_date_string = date_or_datetime_to_string(i_event->end_date);
		add_child_node(root_node, KLB_EVENT_END_DATE, end_date_string);
		g_free(end_date_string);
	}
	i_kolab_store_get_xml_nodes(i_event->incidence->common, i_event->incidence->common, root_node);

	/* Convert xml to a string */
	xmlDocDumpFormatMemoryEnc(doc, &xml_buf, &buf_size, "UTF-8", 1);
	g_string_append(xml_string, (gchar*) xml_buf);
	/* log_debug("%s", (char *) xmlbuff); */

	/* Free associated memory. */

	xmlFree(xml_buf);
	xmlFreeDoc(doc);

	return_string = g_string_free (xml_string, FALSE);
	return return_string;
}


/*
 * convert Show_time_as to Kolab string constant
 */
static gchar*
show_time_as_to_kolab_string (Show_time_as show_time_as)
{
	switch (show_time_as) {
	case SHOW_TIME_AS_FREE:
		return KLB_EVENT_SHOW_TIME_AS_FREE;
		break;
	case SHOW_TIME_AS_TENTATIVE:
		return KLB_EVENT_SHOW_TIME_AS_BUSY; /* KLB_EVENT_SHOW_TIME_AS_TENTATIVE; */
		break;
	case SHOW_TIME_AS_BUSY:
		return KLB_EVENT_SHOW_TIME_AS_BUSY;
		break;
	case SHOW_TIME_AS_OUT_OF_OFFICE:
		return KLB_EVENT_SHOW_TIME_AS_BUSY; /* KLB_EVENT_SHOW_TIME_AS_OUT_OF_OFFICE; */
		break;
	default:
		return KLB_EVENT_SHOW_TIME_AS_BUSY;
	}
}
