/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * note-k-to-i.c
 *
 *  Created on: 12.10.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 * 				Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


/*
 * This file holds operations to read the kolab note fields from an XML tree to
 * an internal structure.
 *
 * The related header file is: kolab.h
 */

#include <config.h>

#include <glib/gi18n-lib.h>

#include "priv-kolab.h"
#include "kolab.h" /* public part of this file */

/**
 * All note specific elements from kolab are handled in this function.
 *
 * Will be written to intern structure if found and returns TRUE, otherwise FALSE.
 */
static gboolean
read_note_element(I_note *i_note, xmlNodePtr node, GError **error)
{
	if (common_k_to_i(i_note->common, node, error))
		return TRUE;
	else if (*error != NULL)
		return FALSE;
	return
		handle_element_string(KLB_NOTE_SUMMARY, &i_note->summary, node);
}

/**
 * Evaluate the given Kolab XML node and fill the given internal note structure
 * with the relevant values.
 *
 * @param  i_note
 *         an internal note structure
 * @param  node
 *         a  Kolab XML node
 * @param  error
 *         a pointer to a NULL error object pointer which will be filled if an
 *         error occurs. It must be checked after calling this operation.
 */
static void
read_note(I_note *i_note, xmlNodePtr node, GError **error)
{
	/* iterate over all child elements of note_node an process them */
	xmlNodePtr n_ptr; /* used in for loop */
	for (n_ptr = node->children; n_ptr != NULL; n_ptr = n_ptr->next) {
		if (n_ptr->type == XML_ELEMENT_NODE) {
			gboolean node_processed = read_note_element(i_note, n_ptr, error);
			if (*error != NULL)
				return;
			if (!node_processed)
				i_kolab_store_add_xml_element(i_note->common, i_note->common, n_ptr);
		} else
			log_debug("ignored XML node with name %s", n_ptr->name);
	}
	/* TODO: Validate inote. (log warnings / create error if necessary) */
}

/**
 * Convert Kolab_conv_mail to I_note
 */
I_note*
conv_kolab_conv_mail_to_I_note (const Kolab_conv_mail *knote, GError **error)
{
	I_note *i_note = NULL;
	xmlDocPtr doc = NULL;
	xmlNodePtr node = NULL, n;

	log_debug ("\nconv_kolab_conv_mail_to_I_note(): convert Kolab_conv_mail to I_note.");

	if (*error != NULL)
		return NULL;

	/* get parsed kolab XML */
	i_note = new_i_note();
	doc = util_get_kolab_xml_part(knote, &i_note->common->kolab_attachment_store, error);
	/* find root node */
	for (n = doc->children; n != NULL; n = n->next)
		if (n->type == XML_ELEMENT_NODE && strcmp ((gchar*)n->name, KLB_NOTE) == 0)
			node = n;

	if (node == NULL) {
		g_set_error (error, KOLABCONV_ERROR_READ_KOLAB,
		             KOLABCONV_ERROR_READ_KOLAB_INVALID_KOLAB_XML,
		             _("Root tag is missing"));
		free_i_note(&i_note);
		xmlFreeDoc(doc);
		return NULL;
	}

	read_note(i_note, node, error);

	xmlFreeDoc(doc);
	if (*error != NULL) {
		free_i_note(&i_note);
		return NULL;
	}

	return i_note;
}
