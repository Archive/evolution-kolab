/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * priv-common-k-to-i.c
 *
 *  Created on: 12.10.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

/*
 * This file holds operations to read the kolab common fields from an XML tree
 * to an internal structure.
 *
 * The related header file is: k-abstract.h
 */

#include "priv-kolab.h"


/**
 * Parse the text node of the given xml element node if it is a sensitivity
 * element and set the sensitivity property in the icommon parameter.
 *
 * @param  icommon
 * @param  node
 * @return true if the given xml element is a sensitivity element
 */
static gboolean
handle_element_sensitivity (I_common *icommon, const xmlNodePtr node)
{
	gchar *value = NULL;

	if (strcmp ((gchar*) node->name, KLB_CMN_SENSITIVITY) != 0)
		return FALSE;

	value = xml_get_node_text(node);
	if (strcmp ((gchar*) value, KLB_CMN_SENSITIVITY_PUBLIC) == 0)
		icommon->sensitivity = ICOMMON_SENSITIVITY_PUBLIC;
	else if (strcmp ((gchar*) value, KLB_CMN_SENSITIVITY_PRIVATE) == 0)
		icommon->sensitivity = ICOMMON_SENSITIVITY_PRIVATE;
	else if (strcmp ((gchar*) value, KLB_CMN_SENSITIVITY_CONFIDENTIAL) == 0)
		icommon->sensitivity = ICOMMON_SENSITIVITY_CONFIDENTIAL;
	else {
		icommon->sensitivity = ICOMMON_SENSITIVITY_PUBLIC;
		log_warn("illegal sensitivity value '%s', set to public", value);
	}
	return TRUE;
}

/**
 * if the xml node is an inline-attachment node => add the value to the
 * inline_attachment_names list in the internal struct
 */
static gboolean
handle_element_inline_attachment(I_common *icommon, const xmlNodePtr node)
{
	gchar *value = NULL;

	if (strcmp ((gchar*) node->name, KLB_CMN_INLINE_ATTACHMENT) != 0)
		return FALSE;

	value = xml_get_node_text(node);
	value = g_strdup(value);
	icommon->inline_attachment_names = g_list_append(icommon->inline_attachment_names, value);
	return TRUE;
}

/**
 * handle link attachment XML node or return FALSE
 */
static gboolean
handle_element_link_attachment(I_common *icommon, const xmlNodePtr node)
{
	gchar *value = NULL;

	if (strcmp ((gchar*) node->name, KLB_CMN_LINK_ATTACHMENT) != 0)
		return FALSE;

	value = xml_get_node_text(node);
	value = g_strdup(value);
	icommon->link_attachments = g_list_append(icommon->link_attachments, value);
	return TRUE;
}

/**
 * Convert kolab xmlNodePtr to I_common
 *
 * @param node: child element of root node
 * @param error => *error != NULL and return FALSE
 */
gboolean
common_k_to_i(I_common *i_common, xmlNodePtr node, GError **error)
{
	if (k_evo_store_get_all(i_common, node, error))
		return TRUE;
	else if (*error != NULL)
		return FALSE;

	return
		handle_element_string(KLB_CMN_PRODUCT_ID, &i_common->product_id, node) ||
		handle_element_string(KLB_CMN_UID, &i_common->uid, node) ||
		handle_element_string(KLB_CMN_BODY, &i_common->body, node) ||
		handle_element_string(KLB_CMN_CATEGORIES, &i_common->categories, node) ||
		handle_element_date_or_datetime(KLB_CMN_CREATION_DATE, &i_common->creation_datetime, node) ||
		handle_element_date_or_datetime(KLB_CMN_LAST_MODIFICATION_DATE, &i_common->last_modified_datetime, node) ||
		handle_element_sensitivity(i_common, node) ||
		handle_element_chars(KLB_CMN_VTIMEZONE, &i_common->vtimezone, node) ||
		handle_element_boolean(KLBX_CMN_RICHTEXT, &i_common->is_html_richtext, node) ||
		handle_element_link_attachment(i_common, node) ||
		handle_element_inline_attachment(i_common, node);
}
