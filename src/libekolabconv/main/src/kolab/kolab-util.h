/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * kolab-util.h
 *
 *  Created on: 29.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


#ifndef KOLAB_UTIL_H_
#define KOLAB_UTIL_H_

#include <glib.h>		/* GError */
#include <libxml/tree.h>	/* XML parsing */
#include "../kolab-conv.h"	/* Kolab mail struct */
#include "../structs/common.h"	/* Date_or_datetime struct */

#if 0
typedef enum {
	KOLAB_EVOLUTION_COMMON_XML_TAG = 1,
	KOLAB_XML_TAG,
	EVOLUTION_XML_TAG,
	EVOLUTION_EXTENDED_XML_TAG,
	UNDEFINED_XML_TAG
} xml_tag_type;
#endif

GDate* string_to_g_date (gchar *dateString);

gchar* g_date_to_string (GDate *date);

gboolean handle_element_GList_String (gchar* list_name, gchar* element_name, GList **store_string_list, xmlNodePtr node);
gboolean handle_element_boolean (gchar* name, gboolean *store, xmlNodePtr node);
gboolean handle_element_chars (gchar* name, gchar **store, xmlNodePtr node);
gboolean handle_element_string (gchar* name, GString **store, xmlNodePtr node);
gboolean handle_element_integer (gchar* name, gint *store, xmlNodePtr node);
gboolean handle_element_double (gchar* name, gdouble *store, xmlNodePtr node);
gboolean handle_element_date (gchar* name, GDate** store, xmlNodePtr node);
gchar* xml_get_node_text(xmlNodePtr node);
gboolean xml_get_boolean(xmlNodePtr node);
xmlDocPtr util_get_kolab_xml_part(const Kolab_conv_mail *kmail, GList **other_parts, GError **error);
gboolean handle_element_date_or_datetime (gchar* name, Date_or_datetime** store, xmlNodePtr node);
gboolean handle_element_datetime (gchar* name, time_t** store, xmlNodePtr node);

/* xml functions */
void convert_GList_String_to_xmlNodes (GList *string_list, gchar* node_name, xmlNodePtr node);
gchar* convert_xmlNode_to_String (const xmlNodePtr node);
void log_xml_part (Kolab_conv_mail *kolab_mail);
void printKolabXmlMailPartArray(const GList* kolabconvMailArray);
void printXMLTree(xmlNodePtr node, gint depth);


gchar* date_or_datetime_to_string (const Date_or_datetime *dodt);
void datetime_to_string (const time_t *time, gchar *date_string);

xmlNodePtr add_child_node (xmlNodePtr root_node, gchar *childName, gchar *childContents);

void add_property (xmlNodePtr node, gchar *propertyName, gchar *propertyContents);

void format_utc_offset (gint utc_offset, gchar *buffer);

void k_write_datetime(struct tm *in, gchar *out, gboolean is_utc);

gint priority_xkcal_to_k(gint xkcal_prio);
gint priority_k_to_xkcal(gint k_prio);

#endif /* KOLAB_UTIL_H_ */
