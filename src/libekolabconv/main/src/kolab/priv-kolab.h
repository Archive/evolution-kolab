/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * priv-kolab.h
 *
 *  Created on: 30.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

/*
 * This is the header file of:
 *
 * - kolab_preserve.c
 * - k-read-common.c
 * - k-read-incidence.c
 * - k-write-common.c
 * - k-write-incidence.c
 */


#ifndef KOLAB_ABSTRACT_H_
#define KOLAB_ABSTRACT_H_


#include <glib.h>		  /* GError */
#include <libxml/tree.h>
#include "kolab-util.h"
#include "kolab-constants.h"
/* internal abstract structures */
#include "../structs/common.h"
#include "../structs/incidence.h"
/* internal structures */
#include "../structs/event.h"
#include "../structs/task.h"
#include "../structs/contact.h"
#include "../structs/note.h"

#include "../logging.h"

void conv_incidence_i_to_k(xmlNodePtr root_node, I_incidence *incidence);
gboolean conv_incidence_k_to_i(I_incidence *incidence, xmlNodePtr node, GError **error);

gboolean common_k_to_i(I_common *common, xmlNodePtr node, GError **error);
void common_i_to_k(xmlNodePtr root_node, I_common *common);

gchar* recurrence_month_i_to_k (Month month);
gchar* recurrence_weekday_i_to_k(Week_day weekday);

void i_kolab_store_add_xml_element(I_common *common, gpointer parent_ptr, const xmlNodePtr node);
void i_kolab_store_get_xml_nodes(I_common *common, gpointer parent_ptr, xmlNodePtr parent_node);

gboolean k_evo_store_get_all(I_common *common, xmlNodePtr node, GError **error);
void k_evo_store_add_all(I_common *common, xmlNodePtr root_node);

#endif /* KOLAB_ABSTRACT_H_ */
