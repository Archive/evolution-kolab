/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * note-i-to-k.c
 *
 *  Created on: 12.10.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "priv-kolab.h"
#include "kolab.h" /* public part of this file */
#include "../util.h"

/*
 * Prototypes for static functions
 */
static gchar *i_note_to_xml(const I_note *i_note);
static GList *i_note_to_xml_mail_part (I_note **i_note_ptr);


/**
 * Convert I_note to Kolab_conv_mail.
 */
Kolab_conv_mail*
conv_I_note_to_kolab_conv_mail (I_note **i_note, GError **error)
{
	GList* kolab_conv_mail_list = NULL;

	g_assert(error != NULL && *error == NULL);
	log_debug ("\nconv_I_note_to_kolab_conv_mail(): convert I_note to Kolab_conv_mail.");

	if (*i_note)
		kolab_conv_mail_list = i_note_to_xml_mail_part(i_note);

	return g_list_to_kolab_conv_mail(kolab_conv_mail_list);
}


/**
 * convert I_note to XML string
 */
static gchar *
i_note_to_xml(const I_note *i_note)
{
	GString *xml_string = g_string_new("");
	gchar *xml_ret_str = NULL;

	xmlNodePtr root_node;
	xmlDocPtr doc;
	xmlChar *xml_buf;
	gint buf_size;

	doc = xmlNewDoc(BAD_CAST "1.0");
	root_node = xmlNewNode(NULL, BAD_CAST KLB_NOTE);
	add_property(root_node, "version", "1.0");
	/* xmlNodeSetContent(root_node, BAD_CAST "content"); */
	xmlDocSetRootElement(doc, root_node);

	common_i_to_k(root_node, i_note->common);

	if (i_note->summary)
		add_child_node (root_node, KLB_NOTE_SUMMARY, i_note->summary->str);

	i_kolab_store_get_xml_nodes(i_note->common, i_note->common, root_node);

	/* Convert xml to a string */
	xmlDocDumpFormatMemoryEnc(doc, &xml_buf, &buf_size, "UTF-8", 1);
	g_string_append(xml_string, (gchar*) xml_buf);

	xmlFree(xml_buf);
	xmlFreeDoc(doc);

	xml_ret_str = g_string_free(xml_string, FALSE);
	return xml_ret_str;
}

/**
 * convert I_note to list of Kolab_conv_mail_part
 */
static GList*
i_note_to_xml_mail_part (I_note **i_note_ptr)
{
	I_note *i_note = *i_note_ptr;
	GList* mail_part_list = NULL;
	Kolab_conv_mail_part *mail_part;

	gchar *xml_text = i_note_to_xml(i_note);
	/* log_debug("\n\nXml Text:\n%s",xmlText); */

	/* ADD XML mailPart */
	mail_part = g_new0(Kolab_conv_mail_part, 1);
	mail_part->data = g_strdup(xml_text);
	mail_part->length = strlen(xml_text);
	mail_part->mime_type = g_strdup("application/x-vnd.kolab.note");
	mail_part->name = g_strdup("kolab-common1.xml"); /* Change it after adding a field in contact.h */
	/* mailPart->order = 1; Change it after adding a field in contact.h */

	mail_part_list = g_list_append(mail_part_list, (Kolab_conv_mail_part *)mail_part);

	mail_part_list = g_list_concat(mail_part_list, g_list_copy(i_note->common->kolab_attachment_store));

	free_i_note(i_note_ptr);
	*i_note_ptr = NULL;

	g_free(xml_text);
	return mail_part_list;
}

