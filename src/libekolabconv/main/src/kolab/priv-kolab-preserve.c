/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * priv-kolab-preserve.c
 *
 *  Created on: 12.10.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

/*
 * This file holds operations which are needed to preserve information which is
 * not mappable between the evolution data types and the kolab format.
 */

#include "priv-kolab.h"

/**
 * Checks if the given Kolab XML node is an evolution store node and adds its
 * content to the internal evolution store if this is true.
 *
 * @param  i_common
 *         Evolution hidden data will be added to the internal evolution store
 *         of this parameter if the given Kolab XML node is an evolution store
 *         node
 * @param  node
 *         A Kolab XML node
 * @param  error
 *         A pointer to a NULL error object pointer which will be filled if an
 *         error occurs. It must be checked after calling this operation.
 * @return TRUE if the XML node is an evolution store node
 */
gboolean
k_evo_store_get_all(I_common *i_common, xmlNodePtr node, GError **error)
{
	gchar *value = NULL;

	g_assert(error != NULL && *error == NULL);
	if (strcmp ((gchar*) node->name, KLB_CMN_EVO_STORE) != 0)
		return FALSE;

	value = xml_get_node_text(node);
	i_common->evolution_store = g_strdup(value);
	return TRUE;
}

/**
 * Adds all evolution store elements below the kolab xml root element.
 */
void
k_evo_store_add_all(I_common *i_common, xmlNodePtr root_node)
{
	if (i_common->evolution_store)
		xmlNewTextChild (root_node, NULL, BAD_CAST KLB_CMN_EVO_STORE, BAD_CAST i_common->evolution_store);
}

/**
 * Store a serialization of the given xml node for the given pointer key in the
 * kolab store.
 *
 * @param  common
 *         Holds the kolab store which is used to store the serialized xml node
 * @param  parent_ptr
 *         The key which is used to store the xml node in the kolab store
 * @param  node
 *         The xml node which should be stored
 */
void
i_kolab_store_add_xml_element(I_common *i_common, gpointer parent_ptr, const xmlNodePtr node)
{
	gchar *xml_str = convert_xmlNode_to_String (node);
	kolab_store_add_element(i_common, parent_ptr, xml_str, FALSE);
}

/**
 * Add all xml nodes which are stored in the kolab store for the given pointer
 * key as children to the given xml node.
 *
 * @param  common
 *         Holds the kolab store which is used to get the serialized xml nodes
 * @param  parent_ptr
 *         The key which is used to get the xml nodes from the kolab store
 * @param  parent_node
 *         The xml node which should contain the deserialized xml nodes
 */
void
i_kolab_store_get_xml_nodes(I_common *i_common, gpointer parent_ptr, xmlNodePtr parent_node)
{
	GList* list = kolab_store_get_element_list(i_common, parent_ptr);

	while (list != NULL) {
		xmlNodePtr node = NULL; /* used in for loop */
		gchar * xml_str = ((gchar *) list->data);
		xmlDocPtr xml_doc = xmlReadMemory (xml_str, (int) strlen (xml_str), "kolab.xml", NULL, 0);

		if (xml_doc == NULL) {
			/* Failed to parse */
			log_debug ("\nError parsing xmlSubTree: %s", xml_str);
			continue;
		}

		for (node = xml_doc->children; node != NULL; node = node->next)
			if (node->type == XML_ELEMENT_NODE)
				break;


		/* Add subXmlTree node to the main xml tree node. */
		xmlAddChild (parent_node, xmlCopyNode (node, 1));

		xmlFreeDoc (xml_doc);

		list = list->next;
	}
}
