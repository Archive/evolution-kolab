/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * task-i-to-k.c
 *
 *  Created on: 12.10.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "priv-kolab.h"
#include "kolab.h" /* public part of this file */
#include "../util.h"

/**
 * convert libekolabconv status to kolab status
 */
static gchar*
task_status_i_to_k (Task_status i_task_status)
{
	switch (i_task_status) {
	case I_TASK_NOT_STARTED:
		return KLB_TASK_STATUS_NOT_STARTED;
	case I_TASK_IN_PROGRESS:
		return KLB_TASK_STATUS_IN_PROGRESS;
	case I_TASK_COMPLETED:
		return KLB_TASK_STATUS_COMPLETED;
	case I_TASK_WAITING_ON_SOMEONE_ELSE:
		return KLB_TASK_STATUS_WAITING_ON_SOMEONE_ELSE;
	case I_TASK_DEFERRED:
		return KLB_TASK_STATUS_DEFERRED;
	default:
		return KLB_TASK_STATUS_NOT_STARTED;
	}
}

/**
 * convert I_task to XML string
 */
static gchar *
i_task_to_xml (const I_task *i_task) {
	GString *xml = g_string_new("");
	gchar *xml_ret_str = NULL;

	xmlNodePtr root_node;
	xmlDocPtr doc;
	xmlChar *xml_buf;
	gint buf_size;


	doc = xmlNewDoc(BAD_CAST "1.0");
	root_node = xmlNewNode(NULL, BAD_CAST KLB_TASK);
	add_property(root_node, "version", "1.0");
	/* xmlNodeSetContent(root_node, BAD_CAST "content"); */
	xmlDocSetRootElement(doc, root_node);

	conv_incidence_i_to_k(root_node, i_task->incidence);

	if (i_task->priority >= 0 && i_task->priority <= 9) {
		gchar *str = NULL;

		/* if a detailed priority value exists and is valid */

		/* set detailed priority */
		gchar *kcal_str = g_strdup_printf("%i", i_task->priority);
		add_child_node (root_node, KLBX_TASK_KCAL_PRIORITY, kcal_str);
		g_free(kcal_str);

		/* set normal priority from detailed priority
		 * (necessary for valid kolab standard and kontact also writes both)
		 */
		str = g_strdup_printf("%i", priority_xkcal_to_k(i_task->priority));
		add_child_node (root_node, KLB_TASK_PRIORITY, str);
		g_free(str);
	}

	if (i_task->completed <= 100) { /* i_task->completed is unsigned therfore >= 0 is implicit */
		gchar *str = g_strdup_printf("%i", i_task->completed);
		add_child_node (root_node, KLB_TASK_COMPLETED, str);
		g_free(str);
	}

	if (i_task->status)
		add_child_node (root_node, KLB_TASK_STATUS, task_status_i_to_k(i_task->status));

	if (i_task->due_date) {
		gchar *due_str = date_or_datetime_to_string(i_task->due_date);
		add_child_node (root_node, KLB_TASK_DUE_DATE, due_str);
		g_free(due_str);
	}
	if (i_task->completed_datetime) {
		gchar *date_string = g_new0(gchar, 50);
		datetime_to_string(i_task->completed_datetime, date_string);
		add_child_node (root_node, KLBX_TASK_COMPLETED_DATE, date_string);
		g_free(date_string);
	}

	i_kolab_store_get_xml_nodes(i_task->incidence->common, i_task->incidence->common, root_node);

	/* Convert xml to a string */
	xmlDocDumpFormatMemoryEnc(doc, &xml_buf, &buf_size, "UTF-8", 1);
	g_string_append(xml, (gchar*) xml_buf);
	/* log_debug("%s", (char *) xmlbuff); */

	/* Free associated memory. */

	xmlFree(xml_buf);
	xmlFreeDoc(doc);

	xml_ret_str = g_string_free(xml, FALSE);
	return xml_ret_str;
}

/**
 * Convert libekolabconv I_task to Kolab_conv_mail struct
 */
Kolab_conv_mail*
conv_I_task_to_kolab_conv_mail (I_task **i_task_ptr, GError **error)
{
	GList* mail_part_list = NULL;
	g_assert(error != NULL && *error == NULL);

	if (*i_task_ptr) {
		I_task *i_task = *i_task_ptr;
		Kolab_conv_mail_part *mail_part;

		gchar *xml = i_task_to_xml(i_task);
		/* log_debug("\n\nXml Text:\n%s",xmlText); */

		/* ADD XML mailPart */
		mail_part = g_new0(Kolab_conv_mail_part, 1);
		mail_part->data = g_strdup(xml);
		mail_part->length = strlen(xml);
		mail_part->mime_type = g_strdup("application/x-vnd.kolab.task");
		mail_part->name = g_strdup("kolab-common1.xml"); /* Change it after adding a field in contact.h */
		/* mailPart->order = 1; Change it after adding a field in contact.h */

		mail_part_list = g_list_append(mail_part_list, (Kolab_conv_mail_part *)mail_part);

		mail_part_list = g_list_concat(mail_part_list, g_list_copy(i_task->incidence->common->kolab_attachment_store));

		free_i_task(i_task_ptr);
		*i_task_ptr = NULL;
		g_free(xml);
	}

	return g_list_to_kolab_conv_mail(mail_part_list);
}
