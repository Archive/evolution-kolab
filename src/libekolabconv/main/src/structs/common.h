/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * common.h
 *
 *  Created on: 16.06.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *              Umer Kayani <u.kayani@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <glib.h>
#include <time.h>


#define COMMON_UNDEFINED					"undefined"



/*
 * These pointers are used as key for xml elements below the contact "name" element
 * as this element has no representation in the I_contact struct.
 */
#define KOLAB_STORE_PTR_CONTACT_NAME 1000
#define KOLAB_STORE_PTR_INC_ORGANIZER 1001
#define KOLAB_STORE_PTR_INC_RECURRENCE 1002


/* convenience macros for calling specialized free functions */
#define     G_STRING_FREE(x)    if ((x) != NULL) g_string_free ((x), 1)
#define     G_DATE_FREE(x)    	if ((x) != NULL) g_date_free (x)
/* G_LIST_FREE(x) has been replaced by generic_g_list_free() */
/* #define     G_LIST_FREE(x)    	if ((x) != NULL) g_list_free (x) */



/*
 * A structure for holding evolution name value pair.
 */
typedef struct {
	GString *name;
	GString *value;
} Evolution_field;



typedef enum {
	ICOMMON_SENSITIVITY_NULL = 0,
	ICOMMON_SENSITIVITY_PUBLIC,
	ICOMMON_SENSITIVITY_PRIVATE,
	ICOMMON_SENSITIVITY_CONFIDENTIAL
} Sensitivity;

/*
 * A structure for holding either date or datetime but no both.
 */
typedef struct {
	GDate *date;
	time_t *date_time;
} Date_or_datetime;


typedef enum {
	I_COMMON_NO_DAY = 0,
	I_COMMON_MONDAY = 1,
	I_COMMON_TUESDAY = 2,
	I_COMMON_WEDNESDAY = 4,
	I_COMMON_THURSDAY = 8,
	I_COMMON_FRIDAY = 16,
	I_COMMON_SATURDAY = 32,
	I_COMMON_SUNDAY = 64
} Week_day;

typedef enum {
	I_COMMON_MONTH_NULL = 0,
	I_COMMON_JAN,
	I_COMMON_FEB,
	I_COMMON_MAR,
	I_COMMON_APR,
	I_COMMON_MAY,
	I_COMMON_JUN,
	I_COMMON_JUL,
	I_COMMON_AUG,
	I_COMMON_SEP,
	I_COMMON_OCT,
	I_COMMON_NOV,
	I_COMMON_DEC
} Month;

/*
 * A structure for holding common fields.
 */
typedef struct{
	GString *product_id;
	GString *uid;
	GString *body;
	GString *categories;
	Date_or_datetime *creation_datetime;
	Date_or_datetime *last_modified_datetime;
	Sensitivity sensitivity;

	/* help variable for html parsing, written and true by kontact when html parsing needed */
	gboolean is_html_richtext;

	/* KPilot synchronization stuff */
	gboolean has_pilot_sync_id;
	gboolean has_pilot_sync_status;
	gulong pilot_sync_id;
	gint pilot_sync_status;

	/* stores kolab xml trees which could not be mapped to evolution but
	 * which should be preserved. Also the relation to the parent elements
	 * of the xml tree can be stored.
	 * Use the operations kolab_store_add_element() and
	 * kolab_store_get_element_list() to access this property.
	 */
	GHashTable *kolab_store;

	GList *kolab_attachment_store;	/* hidden & inline attachments of type Kolab_conv_mail_part */

	GList *inline_attachment_names;	/* elements of type gchar*; names of the kolab_attachment_store elements which are inline and not hidden attachments */

	GList *link_attachments;		/* list of gchar* URLs */

	gchar *evolution_store; 		/* elements of type char* */
	gchar *vtimezone; 			/* complete ICalendar VTIMEZONE block */
} I_common;


/**
 * Initializer functions for data structures
 */
I_common* new_i_common (void);
Date_or_datetime *new_date_or_datetime(void);

/*
 * destructor functions for data structures
 */
void generic_g_list_free(GList **l, void (*free_func) (void **data));
void generic_g_slist_free(GSList **l, void (*free_func) (void **data));
void free_primitive_data(void **data);
void free_bin_attachement(void **bin);
void free_g_string (void **data);
void free_i_common (I_common **icommon);
void free_date_or_datetime (Date_or_datetime **dt);
void free_evolution_field(Evolution_field **e_field);

/* void free_timezone (I_timezone **); */
/* void free_timezone_date (I_timezone_date **); */

/*
 *
 */
void kolab_store_add_element(I_common *common, gpointer parent_ptr, gchar *xml_str, gboolean reverse_order);
GList* kolab_store_get_element_list(I_common *common, gpointer parent_ptr);



/*
 * Some print functions to print structures, useful for debugging.
 */
void print_i_common (const I_common *common);
void print_kolab_store (const I_common *);
void print_kolab_attachment_store (const I_common *);
void print_evolution_store (const I_common *);
void print_extended_evolution_store (const I_common *);
void print_date_or_datetime (const Date_or_datetime *dodt_struct, const gchar *caption);



#endif /* COMMON_H_ */
