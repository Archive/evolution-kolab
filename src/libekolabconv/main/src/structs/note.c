/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * note.c
 *
 *  Created on: 15.12.2010
 *      Author: Andreas Grau <a.grau@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "note.h"

/**
 * Initialize I_note.
 *
 * Use free_i_note() to deallocate memory.
 */
I_note*
new_i_note (void)
{
	I_note* i_note = g_new0 (I_note, 1);
	i_note->common = new_i_common();

	return i_note;
}

/**
 * free memory used by I_note
 */
void
free_i_note (I_note **i_note)
{
	if (i_note && *i_note) {
		I_note *in = *i_note;

		free_i_common (&in->common);
		G_STRING_FREE (in->summary);
		g_free(in);

		*i_note = NULL;
	}
}
