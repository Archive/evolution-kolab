/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * task.h
 *
 *  Created on: 17.08.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef TASK_H_
#define TASK_H_

#include "incidence.h"

typedef enum {
	I_TASK_NOT_STARTED,
	I_TASK_IN_PROGRESS,
	I_TASK_COMPLETED,
	I_TASK_WAITING_ON_SOMEONE_ELSE,
	I_TASK_DEFERRED
} Task_status;

typedef struct {
	I_incidence *incidence;

	/* x-kcal-priority (detailed 0-9) saved in priority and normal priority (1-5)
	 * will be calculated from it when writing xml
	 */
	gint priority;

	guint completed;		/* default 0 */
	Task_status status;		/* default not-started */
	Date_or_datetime *due_date;	/* default not present */
	time_t *completed_datetime;	/* default: NULL */
} I_task;

I_task *new_i_task (void);

/*
 * destructor functions
 */
void free_i_task (I_task **i_task);

#endif /* TASK_H_ */
