/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * event.c
 *
 *  Created on: 18.08.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "event.h"
#include "../logging.h"

/**
 * Initialize I_event.
 */
I_event*
new_i_event (void)
{
	I_event* i_event = g_new0 (I_event, 1);

	i_event->incidence = new_i_incidence();

	return i_event;
}


/**
 * free function for I_event objects
 */
void
free_i_event (I_event **i_event)
{
	if (i_event && *i_event) {
		I_event *ie = *i_event;

		free_i_incidence (&ie->incidence);
		free_date_or_datetime(&ie->end_date);

		g_free(ie);

		*i_event = NULL;
	}
}


/**
 * print function for debugging
 */
void
print_i_event (const I_event *i_event)
{
	log_debug ("\n**************************** Printing I_event ********************\n");
	if (i_event) {
		if (i_event->incidence)
			print_incidence (i_event->incidence);

		log_debug ("\nshow_time_as: %d", i_event->show_time_as);
		print_date_or_datetime (i_event->end_date, "End Date");
	} else
		log_debug ("\nI_event is NULL.");
}
