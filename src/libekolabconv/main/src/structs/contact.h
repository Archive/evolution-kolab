/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * contact.h
 *
 *  Created on: 16.06.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *              Umer Kayani <u.kayani@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef CONTACT_H_
#define CONTACT_H_

#include <glib.h>
#include <string.h>

#include "common.h"
#include "../kolab-conv.h"

#define COORD_NOT_SET -999

/*
 * icontact phone types
 */
typedef enum {
	ICONTACT_PHONE_TYPE_NULL = 0,
	ICONTACT_PHONE_ASSISTANT,	/* 01 */
	ICONTACT_PHONE_BUSINESS_1,	/* 02 */
	ICONTACT_PHONE_BUSINESS_2,	/* 03 */
	ICONTACT_PHONE_BUSINESS_FAX,	/* 04 */
	ICONTACT_PHONE_CALLBACK,	/* 05 */
	ICONTACT_PHONE_CAR,		/* 06 */
	ICONTACT_PHONE_COMPANY,		/* 07 */
	ICONTACT_PHONE_HOME_1,		/* 08 */
	ICONTACT_PHONE_HOME_2,		/* 09 */
	ICONTACT_PHONE_HOME_FAX,	/* 10 */
	ICONTACT_PHONE_ISDN,		/* 11 */
	ICONTACT_PHONE_MOBILE,		/* 12 */
	ICONTACT_PHONE_OTHER,		/* 13 */
	ICONTACT_PHONE_PAGER,		/* 14 */
	ICONTACT_PHONE_PRIMARY,		/* 15 */
	ICONTACT_PHONE_RADIO,		/* 16 */
	ICONTACT_PHONE_TELEX,		/* 17 */
	ICONTACT_PHONE_TTYTDD		/* 18 */
} Icontact_phone_type;

/*
 * icontact address types
 */
typedef enum {
	ICONTACT_ADDR_TYPE_NULL = 0,
	ICONTACT_ADDR_TYPE_HOME,
	ICONTACT_ADDR_TYPE_BUSINESS,
	ICONTACT_ADDR_TYPE_OTHER
} Icontact_address_type;

/*
 * data structure for icontact phone numbers
 */
typedef struct {
	Icontact_phone_type type;
	gchar *number;
} Phone_number;

/*
 * data structure for icontact emails
 */
typedef struct {
	GString *display_name;
	GString *smtp_address;
} Email;

/*
 * data structure for icontact adresses
 */
typedef struct {
	Icontact_address_type type;
	GString *street;
	GString *pobox;
	GString *locality;
	GString *region;
	GString *postal_code;
	GString *country;
} Address;

/*
 * data structure for icontact emails
 */
typedef struct {
	GString *app;
	GString *name;
	GString *value;
} Custom;


/**
 * structure for internal contact representation
 *
 * IMPORTANT: use corresponding new_i_contact() and free_i_contact()
 *            functions to initialize and deallocate structure
 */
typedef struct {
	I_common *common;

	GString *given_name;
	GString *middle_names;
	GString *last_name;
	GString *full_name;
	GString *prefix;
	GString *suffix;

	GString *free_busy_url;
	GString *organization;
	GString *web_page;
	GString *department;
	GString *office_location;
	GString *profession;
	GString *job_title;
	GString *manager_name;
	GString *assistant;
	GString *nick_name;
	GString *spouse_name;
	GDate   *birthday;
	GDate   *anniversary;

	/* GString *picture_name; Name of the kolab attachment which contains the picture */
	Kolab_conv_mail_part *photo;

	GList *phone_numbers; 			/* elements of type "phone_number" */
	GList *emails;				/* elements of type "Email" */
	GList *addresses; 			/* elements of type "Address" */

	GList *custom_list;			/* elements of type "Custom" */

#define DEGREE_NOT_SET -999
	gdouble latitude;
	gdouble longitude;

} I_contact;

/*
 * Initializer functions.
 */
I_contact* new_i_contact (void);
Address* new_address (void);

/*
 * destructor functions
 */
void free_i_contact (I_contact **);
void free_address (void **);
void free_custom (void **);
void free_email (void **email);
void free_phone_number (void **);

/*
 * Some print functions to print structures, useful for debugging.
 */
void print_contact (I_contact*);

#endif /* CONTACT_H_ */
