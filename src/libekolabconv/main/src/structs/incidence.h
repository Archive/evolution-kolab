/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * incidence.h
 *
 *  Created on: 17.08.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef INCIDENCE_H_
#define INCIDENCE_H_

#include <glib.h>
#include "common.h"

typedef enum {
	I_REC_CYCLE_NULL = -1,
	I_REC_CYCLE_DAILY,
	I_REC_CYCLE_WEEKLY,
	I_REC_CYCLE_MONTHLY_DAYNUMBER,
	I_REC_CYCLE_MONTHLY_WEEKDAY,
	I_REC_CYCLE_YEARLY_MONTHDAY,
	I_REC_CYCLE_YEARLY_YEARDAY ,
	I_REC_CYCLE_YEARLY_WEEKDAY
} Recurrence_cycle;

typedef enum {
	I_INC_STATUS_NONE,
	I_INC_STATUS_TENTATIVE,
	I_INC_STATUS_ACCEPTED,
	I_INC_STATUS_DECLINED,
	I_INC_STATUS_DELEGATED
} Incidence_status;

typedef enum {
	I_INC_ROLE_REQUIRED,
	I_INC_ROLE_OPTIONAL,
	I_INC_ROLE_RESOURCE
} Incidence_role;

typedef enum {
	I_INC_CUTYPE_UNDEFINED = 0,
	I_INC_CUTYPE_INDIVIDUAL,
	I_INC_CUTYPE_GROUP,
	I_INC_CUTYPE_RESOURCE,
	I_INC_CUTYPE_ROOM
} Incidence_cutype;

typedef enum {
	I_ALARM_TYPE_DISPLAY = 1,
	I_ALARM_TYPE_AUDIO,
	I_ALARM_TYPE_PROCEDURE,
	I_ALARM_TYPE_EMAIL
} Alarm_type;

typedef struct{
	GString *display_name;			/* default "" */
	GString *smtp_address;			/* default "" */
	Incidence_status status;		/* no default */
	gboolean request_response;		/* default true */
	gboolean invitation_sent;		/* default true */
	Incidence_role role;			/* default "required" */
	Incidence_cutype cutype;
} Attendee;

typedef struct{
	Recurrence_cycle recurrence_cycle;	/* enum Recurrence_cycle */
	gint interval;				/* default 1 */

	/** Bitfield which combines values of RecurrenceDay enumeration
	 * can hold values from 0 (no day set) to 127 (all seven days set) */
	gint weekdays;
	gint day_number;			/* no default */
	Month month;				/* no default */

	/* at least one of range_date and range_number must be null. */
	GDate *range_date;
	gint *range_number;

	GList *exclusion;			/* no default, List of GDate */
} Recurrence;

typedef struct{
	GString *program;
	GString *arguments;
} Proc_param;

typedef struct{
	GList *addresses;	/* Elements of type GString containing email addresses */
	GString *subject;
	GString *mail_text;
	GList *attachments;	/* Elements of type GString containing attachments */
} Email_param;


typedef struct{
	Alarm_type type;
	/* TODO: Incidence: enabled? */
	gint start_offset; 	/* to set how many minutes from the event beginning the alarm */
	/* should trigger, it's relative so negative values are allowed; */
	gint end_offset;	/* to set how many minutes from the event end the alarm should */
	/* trigger, it's relative so negative values are allowed; */
	gint repeat_count;	/* to set how many times an alarm should be repeated after the first time it got triggered; */
	gint repeat_interval;	/* to set the amount of time between to repetition of the alarm. */

	/* I_INC_ROLE_OPTIONAL Fields */
	GString *display_text;	/* Applicable when type is "display" */
	GString *audio_file;	/* Applicable when type is "audio" */
	Proc_param *proc_param;	/* Applicable when type is "procedure" */
	Email_param *email_param;	/* Applicable when type is "email" */
} Alarm;

typedef struct{
	I_common *common;

	GString *summary; 			/* default "" */
	GString *location; 			/* default "" */
	GString *organizer_display_name;	/* default "" */
	GString *organizer_smtp_address;	/* default "" */
	Date_or_datetime *start_date;		/* default not present */
	GList *advanced_alarm;			/* elements of type Alarm, simple alarm is also mapped to adv. alarm and converted back */

	Recurrence *recurrence;
	GList * attendee;			/* List of type Attendee */
} I_incidence;

/*
 * initializer functions.
 */
I_incidence *new_i_incidence (void);
Recurrence* new_recurrence (void);
Alarm* new_alarm( Alarm_type alarmType );

/*
 * destructor functions
 */
void free_i_incidence (I_incidence **);
void free_alarm (void **);
void free_recurrence (Recurrence **);
void free_attendee (void **);
void free_proc_param (Proc_param **);
void free_email_param (Email_param **);

/*
 * output functions.
 */
void print_incidence(const I_incidence *i_incidence);

#endif /* INCIDENCE_H_ */
