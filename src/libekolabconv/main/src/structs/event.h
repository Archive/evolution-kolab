/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * event.h
 *
 *  Created on: 17.08.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef EVENT_H_
#define EVENT_H_

#include <string.h>

#include "incidence.h"
#include "common.h"

typedef enum {
	SHOW_TIME_AS_FREE = 1,
	SHOW_TIME_AS_TENTATIVE,
	SHOW_TIME_AS_BUSY,
	SHOW_TIME_AS_OUT_OF_OFFICE
} Show_time_as;

typedef struct {
	I_incidence *incidence;

	Show_time_as show_time_as;		/* default busy */
	Date_or_datetime *end_date;		/* default not present */

	/*
	 * <show-time-as>(string, default busy)</show-time-as>
	 * <color-label>(string, default none)</color-label>
	 * <end-date>(date or datetime, default not present)</end-date>
	 */
} I_event;

I_event* new_i_event (void);

void free_i_event (I_event **);

void print_i_event (const I_event *ievent);

#endif /* EVENT_H_ */
