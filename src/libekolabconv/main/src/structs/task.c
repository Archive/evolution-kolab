/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * task.c
 *
 *  Created on: 15.11.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "task.h"

/**
 * Initialize I_task
 */
I_task*
new_i_task (void)
{
	I_task* i_task = g_new0 (I_task, 1);
	i_task->incidence = new_i_incidence();
	i_task->priority = -1;

	return i_task;
}


/**
 * specific free function for I_task objects
 */
void
free_i_task (I_task **i_task_ptr)
{
	if (i_task_ptr && *i_task_ptr) {
		I_task *i_task = *i_task_ptr;

		free_i_incidence (&i_task->incidence);

		free_date_or_datetime(&i_task->due_date);

		if (i_task->completed_datetime != NULL) {
			g_free(i_task->completed_datetime);
			i_task->completed_datetime = NULL;
		}

		g_free(i_task);

		*i_task_ptr = NULL;
	}
}
