/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * logging.c
 *
 *  Created on: 20.01.2011
 *      Author: Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "logging.h"

#ifdef KOLABCONV_DEBUG

/**
 * Output a log message via the glib logger.
 */
void
log_debug (gchar *fmt, ...)
{
	va_list ap;
	va_start (ap, fmt);
	g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, fmt, ap);
	va_end(ap);
}

void
log_warn (gchar *fmt, ...)
{
	va_list ap;
	va_start (ap, fmt);
	g_logv (G_LOG_DOMAIN, G_LOG_LEVEL_INFO, fmt, ap);
	va_end(ap);
}


void
log_evolution_ical (const gchar *message, const ECalComponentWithTZ* epim)
{
	icalcomponent *ical_vevent = e_cal_component_get_icalcomponent ((ECalComponent*) epim->maincomp);
	icalcomponent *ical_vcal = icalcomponent_new_vcalendar ();
	icalcomponent *ical_vtz = NULL;
	const gchar *ical_str = NULL;

	if (epim->timezone != NULL) {
		ical_vtz = e_cal_component_get_icalcomponent ((ECalComponent*) epim->timezone);
		icalcomponent_add_component (ical_vcal, ical_vtz);
	}
	icalcomponent_add_component (ical_vcal, ical_vevent);

	/* memory returned by *_as_ical_string() is owned by libical */
	ical_str = icalcomponent_as_ical_string (ical_vcal);
	log_debug("%s\n%s", message, ical_str);

	if (ical_vtz != NULL)
		icalcomponent_remove_component (ical_vcal, ical_vtz);

	icalcomponent_remove_component (ical_vcal, ical_vevent);
	icalcomponent_free (ical_vcal);
}


void
log_evolution_vcard (const gchar *message, const EContact* epim)
{
	EVCardFormat version = EVC_FORMAT_VCARD_30;
	gchar *vcard_str = e_vcard_to_string ((EVCard *) epim, version);
	log_debug("%s\n%s", message, vcard_str);
	g_free(vcard_str);
}

void
log_kolab_mail (const gchar *message, const Kolab_conv_mail *kmail)
{
	GString *msg = g_string_new(message);
	g_string_append_printf(msg, "\n  attachment number : %d", kmail->length);
	guint i = 0;
	for (; i < kmail->length; i++) {
		Kolab_conv_mail_part *mpart = kmail->mail_parts+i;
		g_string_append_printf(msg, "\n  attachment : %d", i+1);
		g_string_append_printf(msg, "\n    name     : %s", mpart->name);
		g_string_append_printf(msg, "\n    mtype    : %s", mpart->mime_type);
		g_string_append_printf(msg, "\n    length   : %d", mpart->length);
		if (mpart->length < 10000)
			g_string_append_printf(msg, "\n    content  :\n%s", mpart->data);
	}
	log_debug("%s\n", msg->str);
	g_string_free(msg, TRUE);
}


#endif /* KOLABCONV_DEBUG */
