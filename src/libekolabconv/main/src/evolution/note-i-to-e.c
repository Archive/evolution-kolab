/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * note-i-to-e.c
 *
 *  Created on: 21.12.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


#include "priv-evolution.h"
#include "evolution.h"
#include "evolution-util.h"


/**
 * Convert libekolabconv I_note to ECalComponentWithTZ
 */
ECalComponentWithTZ*
conv_I_note_to_ECalComponentWithTZ (I_note **i_note_ptr, GError **error)
{
	ECalComponent *e_cal_comp = NULL;
	ECalComponentWithTZ *ectz = NULL;
	I_note *i_note = NULL;

	g_assert(error != NULL && *error == NULL);
	log_debug ("\nconv_I_note_to_ECalComponentWithTZ(): convert I_note struct to ECalcomponentWithTZ.");

	e_cal_comp = e_cal_component_new ();
	ectz = g_new0(ECalComponentWithTZ, 1);
	ectz->maincomp = e_cal_comp;

	i_note = *i_note_ptr;

	if (i_note) {
		e_cal_component_set_new_vtype (e_cal_comp, E_CAL_COMPONENT_JOURNAL);
		conv_common_i_to_e(ectz, i_note->common);
		i_evo_store_get_all(i_note->common, ectz);
		e_kolab_store_add_common_fields(ectz, i_note->common);
		kolab_attachment_store_i_to_e(e_cal_comp, i_note->common);

		if (i_note->summary) {
			char *sum_str = NULL;
			ECalComponentText *text = g_new0(ECalComponentText, 1);
			text->altrep = NULL;
			sum_str = g_strdup(i_note->summary->str);
			text->value = sum_str;
			e_cal_component_set_summary (e_cal_comp, text);
			e_cal_component_commit_sequence (e_cal_comp);
			g_free(sum_str);
			g_free(text);
		}
	}

	free_i_note(i_note_ptr);
	*i_note_ptr = NULL;

	return ectz;
}
