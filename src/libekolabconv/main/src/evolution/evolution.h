/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * evolution.h
 *
 *  Created on: 04.08.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef EVOLUTION_H_
#define EVOLUTION_H_

#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>

#include "../kolab-conv.h"
#include "../structs/contact.h"
#include "../structs/event.h"
#include "../structs/task.h"
#include "../structs/note.h"


I_contact* conv_EContact_to_I_contact (const EContact* econtact, GError** error);
EContact* conv_I_contact_to_EContact (I_contact **contact, GError** error);

I_event* conv_ECalComponentWithTZ_to_I_event (const ECalComponentWithTZ *eevent, GError **error);
ECalComponentWithTZ* conv_I_event_to_ECalComponentWithTZ (I_event **ievent, GError **error);

I_task* conv_ECalComponentWithTZ_to_I_task (const ECalComponentWithTZ *etask, GError **error);
ECalComponentWithTZ* conv_I_task_to_ECalComponentWithTZ (I_task **itask, GError **error);

I_note* conv_ECalComponentWithTZ_to_I_note (const ECalComponentWithTZ *enote, GError **error);
ECalComponentWithTZ* conv_I_note_to_ECalComponentWithTZ (I_note **inote, GError **error);


#endif /* EVOLUTION_H_ */
