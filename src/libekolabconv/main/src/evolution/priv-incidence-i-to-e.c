/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * priv-incidence-i-to-e.c
 *
 *  Created on: 21.12.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


#include "priv-evolution.h"
#include "evolution-util.h"

/*
 * Prototypes for static functions
 */
static ECalComponentAlarmAction alarm_type_i_to_e(Alarm_type alarmtype);
static icalrecurrencetype_weekday weekday_i_to_e(gint *weekdays);
static icalrecurrencetype_frequency frequency_i_to_e(Recurrence *recurrence);
static void recurrence_fields_i_to_e(ECalComponentWithTZ *ectz, I_incidence *i_incidence);
static icalparameter_partstat incidence_status_i_to_e (Incidence_status status);
static icalparameter_cutype incidence_cutype_i_to_e (Incidence_cutype status);
static icalparameter_role incidence_role_i_to_e (Incidence_role role);
static void recurrence_set_by_month_day(struct icalrecurrencetype *rec_type, const Recurrence *recurrence);
static void recurrence_set_range(struct icalrecurrencetype *rec_type, const Recurrence *recurrence);
static void recurrence_set_by_year_day(struct icalrecurrencetype *rec_type, const Recurrence *recurrence);
/* static void recurrence_set_by_month(struct icalrecurrencetype *rec_type, const Recurrence *recurrence); */
static void recurrence_set_by_set_pos(struct icalrecurrencetype *rec_type, const Recurrence *recurrence);
static void recurrence_set_by_day(struct icalrecurrencetype *rec_type, gint weekdays);

/**
 * Convert incidence from Evolution to libekolabconv.
 */
void
conv_incidence_i_to_e(ECalComponentWithTZ *ectz, I_incidence *i_incidence)
{
	conv_common_i_to_e(ectz, i_incidence->common);

	alarm_i_to_e(ectz, i_incidence);

	if (i_incidence->location)
		e_cal_component_set_location(ectz->maincomp, i_incidence->location->str);

	if (i_incidence->summary) {
		ECalComponentText *text = g_new0(ECalComponentText, 1);
		gchar *val_str = g_strdup(i_incidence->summary->str);
		text->altrep = NULL;
		text->value = val_str;
		e_cal_component_set_summary(ectz->maincomp, text);
		g_free(val_str);
		g_free(text);
	}

	if (i_incidence->organizer_display_name) {
		ECalComponentOrganizer *ecal_comp_org;

		ecal_comp_org = g_new0(ECalComponentOrganizer, 1);
		ecal_comp_org->language = NULL; /* g_strdup("Language"); */
		ecal_comp_org->sentby = NULL;
		ecal_comp_org->cn = g_strdup(i_incidence->organizer_display_name->str);
		ecal_comp_org->value = g_strdup(i_incidence->organizer_smtp_address->str);

		e_cal_component_set_organizer(ectz->maincomp, ecal_comp_org);
		g_free((gchar*) ecal_comp_org->cn);
		g_free((gchar*) ecal_comp_org->value);
		g_free(ecal_comp_org);
	}



	/* ********************************ectz->maincomp**SET DATES (CREATION, LASTMODIFICATION, START, END etc.)************************************ */

	if (i_incidence->start_date) {
		ECalComponentDateTime st;
		st.value = g_new0(struct icaltimetype, 1);
		st.tzid = NULL;

		date_or_datetime_i_to_e(i_incidence->start_date, st.value, FALSE);

		if (i_incidence->common)
			utc_to_localtime(&st, i_incidence->common->vtimezone);

		e_cal_component_set_dtstart(ectz->maincomp, &st);
		e_cal_component_commit_sequence (ectz->maincomp);

		e_cal_component_free_datetime(&st);
	}

	/* ******************************************SET ATTENDEE LIST******************************************************************************* */
	if (i_incidence->attendee) {
		GSList *attendee_list = NULL;
		GSList *attendee_list_ptr = NULL;
		ECalComponentAttendee *ca;

		GList *temp = i_incidence->attendee;
		while (temp) {
			Attendee *attendee = (Attendee *) temp->data;

			ca = g_new0 (ECalComponentAttendee, 1);
			if (attendee->display_name)
				ca->cn = g_strdup( attendee->display_name->str );
			ca->value = g_strdup( attendee->smtp_address->str );
			ca->rsvp = attendee->request_response;
			/* TODO: Incidence: attendee->invitation_sent; */
			ca->role = incidence_role_i_to_e( attendee->role );
			ca->status = incidence_status_i_to_e ( attendee->status);
			ca->cutype = incidence_cutype_i_to_e ( attendee->cutype);

			/* TODO: Fields Still remaining: 1. ca->cutype;2. ca->delfrom; 3. ca->delto; 4. ca->language; 5. ca->member; 6. ca->sentby; */

			attendee_list = g_slist_append (attendee_list, ca);

			temp = temp->next;
		}
		g_list_free(temp);
		e_cal_component_set_attendee_list( ectz->maincomp, attendee_list);
		/* ca->cn is a 'const gchar', so e_cal_component_free_attendee_list()
		 * won't free it - we need to manually free it here
		 */
		attendee_list_ptr = attendee_list;
		while (attendee_list_ptr != NULL) {
			ECalComponentAttendee *ca = (ECalComponentAttendee *) attendee_list_ptr->data;
			if (ca->cn != NULL)
				g_free ((gpointer)(ca->cn));
			attendee_list_ptr = g_slist_next (attendee_list_ptr);
		}
		e_cal_component_free_attendee_list(attendee_list);
	}
	recurrence_fields_i_to_e(ectz, i_incidence);
	e_kolab_store_add_incidence_fields(ectz, i_incidence);
	i_evo_store_get_all(i_incidence->common, ectz);
}



static
struct icaldurationtyp* create_duration(gint minutes)
{
	struct icaldurationtype *duration = g_new0(struct icaldurationtype, 1);
	if (minutes < 0) {
		duration->is_neg = TRUE;
		duration->minutes = -minutes;
	} else
		duration->minutes = minutes;
	return duration;
}

/**
 * Convert the internal advanced alarm list to the evolution alarm list
 */
void
alarm_i_to_e(ECalComponentWithTZ *ectz, I_incidence *i_incidence)
{
	/* ******************************************SET ALARM / ADVANCED ALARM ***************************** */

	if (i_incidence->advanced_alarm) {
		GList *alarm_list = i_incidence->advanced_alarm;

		while (alarm_list) {
			ECalComponentAlarm *e_alarm = e_cal_component_alarm_new();
			Alarm *i_alarm = (Alarm *) alarm_list->data;
			ECalComponentAlarmTrigger *trigger = NULL;
			ECalComponentAlarmRepeat *repeat = NULL;

			/* type / action */
			e_cal_component_alarm_set_action(e_alarm, alarm_type_i_to_e(i_alarm->type));

			/* action dependent fields */
			if (i_alarm->type == I_ALARM_TYPE_DISPLAY) {
				ECalComponentText *text = g_new0(ECalComponentText, 1);
				if(i_alarm->display_text)
					text->value = g_strdup(i_alarm->display_text->str);
				if(text->value != NULL)
					e_cal_component_alarm_set_description(e_alarm, text);

				if (text->altrep)
					g_free((gpointer)text->altrep);
				if (text->value)
					g_free((gpointer)text->value);
				g_free(text);
			}
			if (i_alarm->type == I_ALARM_TYPE_AUDIO && i_alarm->audio_file) {
				gchar *audio_url = g_strdup(i_alarm->audio_file->str);
				icalattach *attach = icalattach_new_from_url(audio_url);
				e_cal_component_alarm_set_attach (e_alarm, attach);
				g_free(audio_url);
				icalattach_unref(attach);
			}
			if (i_alarm->type == I_ALARM_TYPE_PROCEDURE && i_alarm->proc_param->program) {
				/* set program */
				gchar *procedure_url = g_strdup(i_alarm->proc_param->program->str);
				icalattach *attach = icalattach_new_from_url(procedure_url);
				e_cal_component_alarm_set_attach (e_alarm, attach);
				/* set arguments */
				if (i_alarm->proc_param->arguments) {
					ECalComponentText *text = g_new0(ECalComponentText, 1);
					text->value = g_strdup(i_alarm->proc_param->arguments->str);
					e_cal_component_alarm_set_description(e_alarm, text);
					g_free(text);
				}
				g_free(procedure_url);
				icalattach_unref(attach);
			}
			if (i_alarm->type == I_ALARM_TYPE_EMAIL) {
				GSList *attendee_list = NULL;
				ECalComponentAttendee *ca;
				GList *temp = NULL;

				/* set email text */
				if (i_alarm->email_param->mail_text) {
					ECalComponentText *text = g_new0(ECalComponentText, 1);
					text->value = g_strdup(i_alarm->email_param->mail_text->str);
					e_cal_component_alarm_set_description(e_alarm, text);
					g_free(text);
				}

				/* set mail recipients */
				temp = i_alarm->email_param->addresses;
				for (; temp; temp = temp->next) {
					GString *attendee = (GString *) temp->data;
					ca = g_new0 (ECalComponentAttendee, 1);
					ca->value = g_strdup( attendee->str );
					ca->rsvp = FALSE;
					ca->role = ICAL_ROLE_REQPARTICIPANT;
					ca->status = ICAL_PARTSTAT_NEEDSACTION;
					ca->cutype = ICAL_CUTYPE_INDIVIDUAL;
					attendee_list = g_slist_append (attendee_list, ca);
				}
				e_cal_component_alarm_set_attendee_list(e_alarm, attendee_list);
				e_cal_component_free_attendee_list(attendee_list);
			}

			/* trigger */
			trigger = g_new0(ECalComponentAlarmTrigger, 1);
			if (i_alarm->start_offset) {
				struct icaldurationtype *duration = create_duration(i_alarm->start_offset);
				trigger->type = E_CAL_COMPONENT_ALARM_TRIGGER_RELATIVE_START;
				trigger->u.rel_duration = *duration;
				g_free(duration);
				e_cal_component_alarm_set_trigger(e_alarm, *trigger);
			}
			else if (i_alarm->end_offset) {
				struct icaldurationtype *duration = create_duration(i_alarm->end_offset);
				trigger->type = E_CAL_COMPONENT_ALARM_TRIGGER_RELATIVE_END;
				trigger->u.rel_duration = *duration;
				g_free(duration);
				e_cal_component_alarm_set_trigger(e_alarm, *trigger);
			}
			g_free(trigger);

			/* repeat (count & interval) */
			repeat = g_new0(ECalComponentAlarmRepeat, 1);
			if (i_alarm->repeat_count)
				repeat->repetitions = i_alarm->repeat_count;
			if (i_alarm->repeat_interval) {
				struct icaldurationtype *duration = create_duration(i_alarm->repeat_interval);
				repeat->duration = *duration;
				g_free(duration);
			}
			e_cal_component_alarm_set_repeat(e_alarm, *repeat);
			g_free(repeat);

			e_cal_component_add_alarm(ectz->maincomp, e_alarm);
			e_cal_component_alarm_free(e_alarm);
			alarm_list = alarm_list->next;
		}
		g_list_free(alarm_list);
	}
}


/**
 * This function converts intern alarm type to evolution specific alarm type.
 */
static
ECalComponentAlarmAction alarm_type_i_to_e(Alarm_type alarmtype)
{
	switch (alarmtype) {
	case I_ALARM_TYPE_AUDIO:
		return E_CAL_COMPONENT_ALARM_AUDIO;
	case I_ALARM_TYPE_DISPLAY:
		return E_CAL_COMPONENT_ALARM_DISPLAY;
	case I_ALARM_TYPE_EMAIL:
		return E_CAL_COMPONENT_ALARM_EMAIL;
	case I_ALARM_TYPE_PROCEDURE:
		return E_CAL_COMPONENT_ALARM_PROCEDURE;
	default:
		return E_CAL_COMPONENT_ALARM_DISPLAY;
	}
}



/**
 * Returns one of the weekdays which is defined in the given weekdays set and
 * removes this weekday from the given bitset. The returned weekday is converted
 * to a value of the libical weekday enumeration.
 *
 * @param  weekdays
 *         A pointer to a weekday bitfield which combines values of the
 *         RecurrenceDay enumeration. The bitfield will be changed if it is
 *         greater than zero.
 * @return A value of the libical weekday enumeration which corresponds to one
 *         of the weekdays whioch is set in the given weekdays bitset parameter
 */
static icalrecurrencetype_weekday
weekday_i_to_e(gint *weekdays)
{
	if (*weekdays & I_COMMON_MONDAY) {
		*weekdays &= ~I_COMMON_MONDAY;
		return ICAL_MONDAY_WEEKDAY;
	}
	if (*weekdays & I_COMMON_TUESDAY) {
		*weekdays &= ~I_COMMON_TUESDAY;
		return ICAL_TUESDAY_WEEKDAY;
	}
	if (*weekdays & I_COMMON_WEDNESDAY) {
		*weekdays &= ~I_COMMON_WEDNESDAY;
		return ICAL_WEDNESDAY_WEEKDAY;
	}
	if (*weekdays & I_COMMON_THURSDAY) {
		*weekdays &= ~I_COMMON_THURSDAY;
		return ICAL_THURSDAY_WEEKDAY;
	}
	if (*weekdays & I_COMMON_FRIDAY) {
		*weekdays &= ~I_COMMON_FRIDAY;
		return ICAL_FRIDAY_WEEKDAY;
	}
	if (*weekdays & I_COMMON_SATURDAY) {
		*weekdays &= ~I_COMMON_SATURDAY;
		return ICAL_SATURDAY_WEEKDAY;
	}
	if (*weekdays & I_COMMON_SUNDAY) {
		*weekdays &= ~I_COMMON_SUNDAY;
		return ICAL_SUNDAY_WEEKDAY;
	}
	log_warn("weekday bitset is empty");
	return ICAL_NO_WEEKDAY;
}


/**
 * Convert the internal recurrence cycle to the iCal recurrence frequency
 */
static icalrecurrencetype_frequency
frequency_i_to_e(Recurrence *recurrence)
{
	/* I_REC_CYCLE_YEARLY_WEEKDAY is mapped to ICAL_MONTHLY_RECURRENCE (repeated every 12th month) */

	if (recurrence) {
		switch (recurrence->recurrence_cycle) {
		case I_REC_CYCLE_DAILY:
			return ICAL_DAILY_RECURRENCE;
		case I_REC_CYCLE_WEEKLY:
			return ICAL_WEEKLY_RECURRENCE;
		case I_REC_CYCLE_MONTHLY_DAYNUMBER:
		case I_REC_CYCLE_MONTHLY_WEEKDAY:
		case I_REC_CYCLE_YEARLY_WEEKDAY:
			return ICAL_MONTHLY_RECURRENCE;
		case I_REC_CYCLE_YEARLY_MONTHDAY:
		case I_REC_CYCLE_YEARLY_YEARDAY:
			/* I_REC_CYCLE_YEARLY_WEEKDAY is mapped to monthly every 12th month!! */
			return ICAL_YEARLY_RECURRENCE;
		default:
			return ICAL_NO_RECURRENCE;
		}
	} else
		return ICAL_NO_RECURRENCE;
}

/**
 * Convert the internal recurrence to the evolution recurrence
 */
static void
recurrence_fields_i_to_e(ECalComponentWithTZ *ectz, I_incidence *i_incidence)
{
	GSList *exdate_list = NULL;
	GList *excl_list = NULL;
	GSList *recur_list = NULL;
	struct icalrecurrencetype *rec_type = NULL;

	if(i_incidence->recurrence == NULL)
		return;

	/* ******************************************SET RECURRENCE EXCLUSIONS******************************************************************************* */

	excl_list = i_incidence->recurrence->exclusion;

	while (excl_list) {
		ECalComponentDateTime *ecal_comp_datetime;
		GDate *excl_date = (GDate *)excl_list->data;

		ecal_comp_datetime = g_new0(ECalComponentDateTime, 1);

		ecal_comp_datetime->tzid = g_strdup("UTC");
		ecal_comp_datetime->value = g_new0(icaltimetype, 1);
		ecal_comp_datetime->value->year = excl_date->year;
		ecal_comp_datetime->value->month = excl_date->month;
		ecal_comp_datetime->value->day = excl_date->day;
		g_date_free(excl_date);
		excl_list = excl_list->next;
		exdate_list = g_slist_append (exdate_list, ecal_comp_datetime);
	}

	e_cal_component_set_exdate_list (ectz->maincomp, exdate_list);
	g_slist_free(exdate_list);
	g_list_free(excl_list);
	e_cal_component_commit_sequence (ectz->maincomp);

	/* ******************************************SET RECURRENCE RULES******************************************************************************* */

	rec_type = g_new0(struct icalrecurrencetype, 1);
	icalrecurrencetype_clear(rec_type);

	/* I_REC_CYCLE_YEARLY_WEEKDAY gets special treatment: yearly recurrence on the n-th day
	 * of a month is mapped to a monthly recurrence every twelvth month...
	 */

	rec_type->freq = frequency_i_to_e(i_incidence->recurrence);
	recurrence_set_range(rec_type, i_incidence->recurrence);
	if (i_incidence->recurrence->recurrence_cycle == I_REC_CYCLE_YEARLY_WEEKDAY)
		rec_type->interval = (gshort)12;
	else
		rec_type->interval = (gshort)i_incidence->recurrence->interval;

	switch (i_incidence->recurrence->recurrence_cycle) {
	case I_REC_CYCLE_DAILY:
		recurrence_set_by_day(rec_type, i_incidence->recurrence->weekdays);
		break;
	case I_REC_CYCLE_WEEKLY:
		recurrence_set_by_day(rec_type, i_incidence->recurrence->weekdays);
		break;
	case I_REC_CYCLE_MONTHLY_DAYNUMBER:
		recurrence_set_by_month_day(rec_type, i_incidence->recurrence);
		break;
	case I_REC_CYCLE_MONTHLY_WEEKDAY:
		rec_type->by_set_pos[0] = (short int) i_incidence->recurrence->day_number;
		rec_type->by_set_pos[1] = ICAL_RECURRENCE_ARRAY_MAX;
		recurrence_set_by_day(rec_type, i_incidence->recurrence->weekdays);
		break;
	case I_REC_CYCLE_YEARLY_MONTHDAY:
		/* deactivated. month and monthday cannot be handled by evolution
		 * mapping is done by startdate
		 */
#if 0
		recurrence_set_by_month_day(rec_type, i_incidence->recurrence);
		recurrence_set_by_month(rec_type, i_incidence->recurrence);
#endif
		break;
	case I_REC_CYCLE_YEARLY_YEARDAY:
		recurrence_set_by_year_day(rec_type, i_incidence->recurrence);
		break;
	case I_REC_CYCLE_YEARLY_WEEKDAY:
		recurrence_set_by_day(rec_type, i_incidence->recurrence->weekdays);
		recurrence_set_by_set_pos(rec_type, i_incidence->recurrence);
		break;
	default:
		log_debug("\nError: Unknown recurrence Cycle.");
		break;
	}

	log_debug("\nFreq: %d", rec_type->freq);

	recur_list = g_slist_append(recur_list, rec_type);

	e_cal_component_set_rrule_list (ectz->maincomp, recur_list);
	e_cal_component_commit_sequence (ectz->maincomp);

	g_slist_free(recur_list);
	g_free(rec_type);
}

/**
 * Convert the internal incidence status to the corresponding evolution status
 */
static icalparameter_partstat
incidence_status_i_to_e (Incidence_status status)
{
	switch (status) {
	case I_INC_STATUS_NONE:
		return ICAL_PARTSTAT_NEEDSACTION;
	case I_INC_STATUS_TENTATIVE:
		return ICAL_PARTSTAT_TENTATIVE;
	case I_INC_STATUS_ACCEPTED:
		return ICAL_PARTSTAT_ACCEPTED;
	case I_INC_STATUS_DECLINED:
		return ICAL_PARTSTAT_DECLINED;
	case I_INC_STATUS_DELEGATED:
		return ICAL_PARTSTAT_DELEGATED;
	default:
		return ICAL_PARTSTAT_NEEDSACTION;
	}
}

/**
 * Convert the internal incidence cutype to the corresponding evolution cutype
 */
static icalparameter_cutype
incidence_cutype_i_to_e (Incidence_cutype status)
{
	switch (status) {
	case I_INC_CUTYPE_INDIVIDUAL:
		return ICAL_CUTYPE_INDIVIDUAL;
	case I_INC_CUTYPE_GROUP:
		return ICAL_CUTYPE_GROUP;
	case I_INC_CUTYPE_RESOURCE:
		return ICAL_CUTYPE_RESOURCE;
	case I_INC_CUTYPE_ROOM:
		return ICAL_CUTYPE_ROOM;
	case I_INC_CUTYPE_UNDEFINED:
		return ICAL_CUTYPE_UNKNOWN;
	default:
		return ICAL_CUTYPE_UNKNOWN;
	}
}

/**
 * Convert the internal incidence role to the corresponding evolution role
 */
static icalparameter_role
incidence_role_i_to_e (Incidence_role role)
{
	switch (role) {
	case I_INC_ROLE_REQUIRED:
		return ICAL_ROLE_REQPARTICIPANT;
	case I_INC_ROLE_OPTIONAL:
		return ICAL_ROLE_OPTPARTICIPANT;
	case I_INC_ROLE_RESOURCE:
		return ICAL_ROLE_NONPARTICIPANT;
	default:
		return ICAL_ROLE_REQPARTICIPANT;
	}
}

/**
 * Convert the internal recurrence day to evolution recurrence month day
 */
static void
recurrence_set_by_month_day(struct icalrecurrencetype *rec_type, const Recurrence *recurrence)
{
	if (recurrence->day_number != 0) {
		rec_type->by_month_day[0] = (short int) recurrence->day_number;
		rec_type->by_month_day[1] = ICAL_RECURRENCE_ARRAY_MAX;
	}
}

/**
 * Convert the internal recurrence range to the corresponding evolution range
 */
static void
recurrence_set_range(struct icalrecurrencetype *rec_type, const Recurrence *recurrence)
{
	if (recurrence->range_date) {
		rec_type->until.year = recurrence->range_date->year;
		rec_type->until.month = recurrence->range_date->month;
		rec_type->until.day = recurrence->range_date->day;
		rec_type->until.is_date = TRUE;
	} else if (recurrence->range_number) {
		rec_type->count = *(recurrence->range_number);
	} else {
		/* infinite recurrence */
	}
}

/**
 * Convert the internal recurrence day to evolution recurrence year day
 */
static void
recurrence_set_by_year_day(struct icalrecurrencetype *rec_type, const Recurrence *recurrence)
{
	if (recurrence->day_number >= 1 && recurrence->day_number <= 366) {
		rec_type->by_year_day[0] = (short int) recurrence->day_number;
		rec_type->by_year_day[1] = ICAL_RECURRENCE_ARRAY_MAX;
	}
}

#if 0
/**
 * TODO: write comment
 */
static void
recurrence_set_by_month(struct icalrecurrencetype *rec_type, const Recurrence *recurrence)
{
	if (recurrence->recurrence_cycle == I_REC_CYCLE_YEARLY_WEEKDAY) {
		if (recurrence->day_number >= -5 && recurrence->day_number <= 5 && recurrence->day_number != 0) {
			rec_type->by_month[0] = recurrence->month;
			rec_type->by_month[1] = ICAL_RECURRENCE_ARRAY_MAX;
		}
	} else {
		if (recurrence->day_number >= 1 && recurrence->day_number <= 31) {
			rec_type->by_month[0] = recurrence->month;
			rec_type->by_month[1] = ICAL_RECURRENCE_ARRAY_MAX;
		}
	}
}
#endif

/**
 * Convert the internal recurrence day to evolution recurrence week day
 */
static void
recurrence_set_by_set_pos(struct icalrecurrencetype *rec_type, const Recurrence *recurrence)
{
	if (recurrence->day_number >= -5 && recurrence->day_number <= 5 && recurrence->day_number != 0) {
		rec_type->by_set_pos[0] = (short int) recurrence->day_number;
		rec_type->by_set_pos[1] = ICAL_RECURRENCE_ARRAY_MAX;
	}
}



/**
 * Convert the internal recurrence weekday to evolution recurrence weekday
 */
static void
recurrence_set_by_day(struct icalrecurrencetype *rec_type, gint weekdays)
{
	gint i;
	for (i = 0; weekdays; i++)
		rec_type->by_day[i] = weekday_i_to_e(&weekdays) ;
	rec_type->by_day[i] = ICAL_RECURRENCE_ARRAY_MAX;
}
