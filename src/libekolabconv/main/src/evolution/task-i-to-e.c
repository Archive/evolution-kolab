/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * task-i-to-e.c
 *
 *  Created on: 21.12.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


#include "priv-evolution.h"
#include "evolution.h"
#include "evolution-util.h"
#include "../util.h"

/**
 * convert libekolabconv status to Evolution status
 */
static icalproperty_status
task_status_i_to_e (Task_status ical_status)
{
	switch (ical_status) {
	case I_TASK_NOT_STARTED:
		return ICAL_STATUS_NONE;
	case I_TASK_IN_PROGRESS:
		return ICAL_STATUS_INPROCESS;
	case I_TASK_COMPLETED:
		return ICAL_STATUS_COMPLETED;
	case I_TASK_WAITING_ON_SOMEONE_ELSE:
		return ICAL_STATUS_NONE;
	case I_TASK_DEFERRED:
		return ICAL_STATUS_NONE;
	default:
		return ICAL_STATUS_NONE;
	}
}


/**
 * Convert libekolabconv I_task to ECalComponentWithTZ
 */
ECalComponentWithTZ*
conv_I_task_to_ECalComponentWithTZ (I_task **i_task_ptr, GError **error)
{
	ECalComponent *e_cal_comp = NULL;
	ECalComponentWithTZ *ectz = NULL;
	I_task *i_task = NULL;

	g_assert(error != NULL && *error == NULL);
	log_debug ("\nconv_ECalComponentWithTZ_to_I_task(): convert ECalComponentWithTZ to I_task.");

	e_cal_comp = e_cal_component_new ();
	ectz = g_new0(ECalComponentWithTZ, 1);
	ectz->maincomp = e_cal_comp;

	i_task = *i_task_ptr;

	if (i_task) {
		e_cal_component_set_new_vtype (e_cal_comp, E_CAL_COMPONENT_TODO);
		conv_incidence_i_to_e(ectz, i_task->incidence);

		if (i_task->priority >= 0 && i_task->priority <= 9)
			/* normal priority will be calculated from xkcal */
			e_cal_component_set_priority(e_cal_comp, &i_task->priority);
		if (i_task->completed <= 100) /* guint is always >= 0 */
			e_cal_component_set_percent(e_cal_comp, (gint*) &i_task->completed);
		if (i_task->status)
			e_cal_component_set_status(e_cal_comp, task_status_i_to_e(i_task->status));
		if (i_task->due_date) {
			ECalComponentDateTime *date = g_new0(ECalComponentDateTime, 1);
			date->tzid = NULL;
			date->value = g_new0(struct icaltimetype, 1);
			date_or_datetime_i_to_e( i_task->due_date, date->value, FALSE);

			if (i_task->incidence && i_task->incidence->common)
				utc_to_localtime(date, i_task->incidence->common->vtimezone);

			e_cal_component_set_due(e_cal_comp, date);
			g_free(date->value);
			g_free(date);
		}
		if (i_task->completed_datetime) {
			icaltimetype *value = g_new0(struct icaltimetype, 1);
			datetime_i_to_e( i_task->completed_datetime, value, TRUE);

			e_cal_component_set_completed(e_cal_comp, value);
			g_free(value);
		}

		kolab_attachment_store_i_to_e(e_cal_comp, i_task->incidence->common);

		e_cal_component_commit_sequence (e_cal_comp);
	}

	free_i_task(i_task_ptr);
	*i_task_ptr = NULL;

	return ectz;
}
