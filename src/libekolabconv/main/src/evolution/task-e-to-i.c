/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * task-e-to-i.c
 *
 *  Created on: 21.12.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


#include "priv-evolution.h"
#include "evolution.h"
#include "evolution-util.h"
#include "../util.h"
#include "../logging.h"

/**
 * convert ICalender task status to libekolabconv status
 */
static Task_status
task_status_e_to_i (icalproperty_status i_cal_prop_stat)
{
	switch (i_cal_prop_stat) {
	case ICAL_STATUS_INPROCESS:
		return I_TASK_IN_PROGRESS;
	case ICAL_STATUS_COMPLETED:
		return I_TASK_COMPLETED;
	case ICAL_STATUS_CANCELLED:
		return I_TASK_NOT_STARTED;
	default:
		return I_TASK_NOT_STARTED;
	}
}

/**
 * Convert ECalComponentWithTZ to libekolabconv I_task
 */
I_task*
conv_ECalComponentWithTZ_to_I_task (const ECalComponentWithTZ *ectz, GError **error)
{
	I_task *i_task = NULL;

	g_assert(error != NULL && *error == NULL);
	log_debug ("\nconv_ECalComponentWithTZ_to_I_task(): convert ECalComponentWithTZ to I_task.");

	i_task = new_i_task();

	if (ectz) {
		ECalComponent *e_cal_comp = ectz->maincomp;
		ECalComponentDateTime *due_date = NULL;
		ECalComponentDateTime *completed_date = NULL;
		gint *priority = NULL;
		gint *percent = NULL;
		icalproperty_status *prop = NULL;

		/* log_debug("\nconvert_ECalComponent_to_itask is called..."); */
		if (e_cal_comp == NULL || e_cal_component_get_vtype (e_cal_comp) != E_CAL_COMPONENT_TODO)
			return i_task;

		/* Incidence */
		conv_incidence_e_to_i (ectz, i_task->incidence);

		/* Priority */
		e_cal_component_get_priority (e_cal_comp, &priority);
		if (priority)
			i_task->priority = *priority;
		e_cal_component_free_priority(priority);

		/* I_TASK_COMPLETED */
		e_cal_component_get_percent (e_cal_comp, &percent);
		if (percent == 0)
			i_task->completed = 0;
		else
			i_task->completed = (guint) *percent;

		/* Incidence_status */
		prop = g_new0(icalproperty_status, 1);
		e_cal_component_get_status (e_cal_comp, prop);
		i_task->status = task_status_e_to_i(*prop);
		g_free(prop);

		/* due_date */
		due_date = g_new0(ECalComponentDateTime, 1);
		e_cal_component_get_due (e_cal_comp, due_date);
		if (due_date && due_date->value) {
			localtime_to_utc (due_date, ectz->timezone);
			i_task->due_date = new_date_or_datetime();
			date_or_datetime_e_to_i (due_date->value, i_task->due_date);
		}
		e_cal_component_free_datetime(due_date);
		g_free (due_date);

		/* completed date */
		completed_date = g_new0(ECalComponentDateTime, 1);
		e_cal_component_get_completed(e_cal_comp, &completed_date->value);
		if (completed_date && completed_date->value)
			i_task->completed_datetime = datetime_e_to_i (completed_date->value);
		e_cal_component_free_datetime(completed_date);
		g_free (completed_date);

		return i_task;
	} else {
		log_debug("\nError: Can't generate I_task from ECalComponent, etask is NULL\n");
		return NULL;
	}
}
