/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * note-e-to-i.c
 *
 *  Created on: 21.12.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


#include "priv-evolution.h"
#include "evolution.h"
#include "evolution-util.h"
#include "../logging.h"


/**
 * Convert Evolution note to libekolabconv I_note.
 */
I_note*
conv_ECalComponentWithTZ_to_I_note (const ECalComponentWithTZ *e_note, GError **error)
{
	I_note *i_note = NULL;

	g_assert(error != NULL && *error == NULL);
	log_debug ("\nconv_ECalComponentWithTZ_to_I_note(): convert ECalcomponentWithTZ to I_note struct.");

	i_note = new_i_note();

	if (e_note) {
		ECalComponentText *text = NULL;
		ECalComponent *e_cal_comp = e_note->maincomp;

		if (e_cal_comp == NULL || e_cal_component_get_vtype(e_cal_comp) != E_CAL_COMPONENT_JOURNAL)
			return i_note;

		conv_common_e_to_i(e_note, i_note->common);
		e_kolab_store_get_fields(e_note, i_note->common);
		i_evo_store_add_cal_note(i_note->common, e_note);

		text = g_new0(ECalComponentText, 1);
		e_cal_component_get_summary(e_cal_comp, text);
		if (text)
			i_note->summary = g_string_new(text->value);
		g_free(text);

		return i_note;
	} else {
		log_debug("\nError: Can't generate I_note from ECalComponent, enote is NULL\n");
		return NULL;
	}
}
