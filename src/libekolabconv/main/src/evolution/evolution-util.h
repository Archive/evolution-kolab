/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * evolution-util.h
 *
 *  Created on: 26.10.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef EVOLUTION_UTIL_H_
#define EVOLUTION_UTIL_H_

#include <libebook/libebook.h>
#include <libical/ical.h>
#include <libecal/libecal.h>
#include <time.h>
#include "../structs/common.h"

void date_or_datetime_e_to_i(icaltimetype *t, Date_or_datetime *date);
time_t *datetime_e_to_i(icaltimetype *t);
void date_or_datetime_i_to_e(Date_or_datetime *date, icaltimetype *t, gboolean is_utc);
void datetime_i_to_e(time_t *date_time, icaltimetype *t, gboolean is_utc);

void utc_to_localtime(ECalComponentDateTime *ecdt, gchar *vtimezone);
void localtime_to_utc(ECalComponentDateTime *date_time, ECalComponent *local_tz);
icaltimezone* ecalcomponent_tz_get_icaltimezone(ECalComponent *ectz);

#endif /* EVOLUTION_UTIL_H_ */
