/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * event-e-to-i.c
 *
 *  Created on: 29.11.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "evolution.h" /* public interface of this file */
#include "priv-evolution.h"
#include "../logging.h"

/*
 * prototypes for static functions
 */
static Show_time_as transparency_to_show_time_as(ECalComponentTransparency e_transp);
static void process_event_e_to_i(const ECalComponentWithTZ *ectz, I_event *i_event, GError **error);

/**
 * Convert Evolution ECalComponentWithTZ to libekolabconv I_event
 */
I_event*
conv_ECalComponentWithTZ_to_I_event (const ECalComponentWithTZ *e_event, GError **error)
{
	I_event *ievent = NULL;

	log_debug ("\nconv_ECalComponentWithTZ_to_I_event(): convert ECalcomponentWithTZ to I_event.");

	ievent = new_i_event ();

	if (e_event) {
		process_event_e_to_i(e_event, ievent, error);
		return ievent;
	} else {
		log_debug("\nError: Can't generate I_contact from ECalComponent, eevent is NULL\n");
		return NULL;
	}
}

/**
 * convert ECalComponentTransparency to Show_time_as
 */
static Show_time_as
transparency_to_show_time_as(ECalComponentTransparency e_transp)
{
	switch (e_transp) {
	case E_CAL_COMPONENT_TRANSP_OPAQUE:
		return SHOW_TIME_AS_BUSY;
	case E_CAL_COMPONENT_TRANSP_TRANSPARENT:
		return SHOW_TIME_AS_FREE;
	default:
		return SHOW_TIME_AS_BUSY;
	}
}

/**
 * Process the neccessary steps for conversion.
 */
static void
process_event_e_to_i(const ECalComponentWithTZ *ectz, I_event *i_event, GError **error)
{
	ECalComponent *e_cal_comp = NULL;
	ECalComponentTransparency *e_transp = NULL;
	ECalComponentDateTime *e_end_date = NULL;

	g_assert(error != NULL && *error == NULL);
	e_cal_comp = ectz->maincomp;
	/* log_debug("\nconvert_ECalComponent_to_ievent is called..."); */
	if (e_cal_comp == NULL || e_cal_component_get_vtype(e_cal_comp) != E_CAL_COMPONENT_EVENT)
		return;

	conv_incidence_e_to_i(ectz, i_event->incidence);

	/* show_time_as */
	e_transp = g_new0(ECalComponentTransparency, 1);
	e_cal_component_get_transparency(e_cal_comp, e_transp);
	if (e_transp)
		i_event->show_time_as = transparency_to_show_time_as(*e_transp);

	g_free(e_transp);

	/* end_date */
	e_end_date = g_new0(ECalComponentDateTime, 1);
	e_cal_component_get_dtend (e_cal_comp, e_end_date);
	if (e_end_date && e_end_date->value) {
		localtime_to_utc(e_end_date, ectz->timezone);
		i_event->end_date = new_date_or_datetime();
		if (e_end_date->value->is_date) {
			/* need to subtract one day from end date because of the
			 * different interpretation by evolution (shows exclusive)
			 * and kolab clients (shows inclusive)
			 */
			struct icaldurationtype duration = icaldurationtype_null_duration();
			duration.days = 1;
			duration.is_neg = TRUE;
			*e_end_date->value = icaltime_add(*e_end_date->value, duration);
		}
		date_or_datetime_e_to_i( e_end_date->value, i_event->end_date);
	}
	e_cal_component_free_datetime(e_end_date);
	g_free(e_end_date);
}
