/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * event-i-to-e.c
 *
 *  Created on: 21.12.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *              Hendrik Helwich <h.helwich@tarent.de>,
 *              Peter Neuhaus <p.neuhaus@tarent.de>,
 *              Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#include "priv-evolution.h"
#include "evolution.h"
#include "evolution-util.h"

/**
 * Convert Show_time_as to ECalComponentTransparency
 */
static ECalComponentTransparency
show_time_as_to_transparency(Show_time_as sta)
{
	switch (sta) {
	case SHOW_TIME_AS_BUSY:
		return E_CAL_COMPONENT_TRANSP_OPAQUE;
	case SHOW_TIME_AS_FREE:
		return E_CAL_COMPONENT_TRANSP_TRANSPARENT;
	case SHOW_TIME_AS_TENTATIVE:
		return E_CAL_COMPONENT_TRANSP_OPAQUE;
	case SHOW_TIME_AS_OUT_OF_OFFICE:
		return E_CAL_COMPONENT_TRANSP_OPAQUE;
	default:
		return E_CAL_COMPONENT_TRANSP_OPAQUE;
	}
}


/**
 * Convert libekolabconv I_event to Evolution ECalComponentWithTZ.
 */
ECalComponentWithTZ*
conv_I_event_to_ECalComponentWithTZ (I_event **i_event_ptr, GError **error)
{
	ECalComponent* e_cal_comp = NULL;
	I_event *i_event = NULL;
	ECalComponentWithTZ *ectz = NULL;

	g_assert(error != NULL && *error == NULL);
	log_debug ("\nconv_I_event_to_ECalComponentWithTZ(): convert I_event to ECalcomponentWithTZ.");

	e_cal_comp = e_cal_component_new ();
	ectz = g_new0(ECalComponentWithTZ, 1);
	ectz->maincomp = e_cal_comp;

	i_event = *i_event_ptr;

	if (i_event) {
		/* type */
		e_cal_component_set_new_vtype (e_cal_comp, E_CAL_COMPONENT_EVENT);

		/* incidence & common */
		conv_incidence_i_to_e(ectz, i_event->incidence);

		/* show_time_as */
		if (i_event->show_time_as)
			e_cal_component_set_transparency(e_cal_comp, show_time_as_to_transparency(i_event->show_time_as));

		/* end_date */
		if (i_event->end_date && (i_event->end_date->date || i_event->end_date->date_time)) {
			ECalComponentDateTime *ed = g_new0(ECalComponentDateTime, 1);
			ed->value = g_new0(struct icaltimetype, 1);
			ed->tzid = NULL;
			date_or_datetime_i_to_e(i_event->end_date, ed->value, FALSE);

			if (i_event->incidence && i_event->incidence->common)
				utc_to_localtime(ed, i_event->incidence->common->vtimezone);

			if (ed->value->is_date) {
				/* need to add one day to the end date because of the
				 * different interpretation by evolution (shows exclusive)
				 * and kolab clients (shows inclusive)
				 */
				struct icaldurationtype duration = icaldurationtype_null_duration();
				duration.days = 1;
				*ed->value = icaltime_add(*ed->value, duration);
			}
			e_cal_component_set_dtend( e_cal_comp, ed);

			e_cal_component_free_datetime(ed);
		}

		kolab_attachment_store_i_to_e(e_cal_comp, i_event->incidence->common);

		e_cal_component_commit_sequence (e_cal_comp);
	}

	free_i_event(i_event_ptr);
	*i_event_ptr = NULL;

	return ectz;
}
