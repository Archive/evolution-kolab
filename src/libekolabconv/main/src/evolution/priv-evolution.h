/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * priv-evolution.h
 *
 *  Created on: 29.11.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>,
 *      		Peter Neuhaus <p.neuhaus@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */


#ifndef E_ABSTRACT_H_
#define E_ABSTRACT_H_

#include "../structs/common.h"
#include "../structs/incidence.h"
#include "../kolab-conv.h"
#include "../logging.h"
#include "evolution-util.h"

#define ICONTACT_KOLAB_STORE                                "X-KOLAB-STORE"
#define ICONTACT_KOLAB_STORE_ID                             "X-KOLAB-STORE-ID"
#define ICONTACT_KOLAB_CREATED                              "X-KOLAB-CREATED"
#define ICONTACT_KOLAB_STORE_ATTACHMENT                     "X-KOLAB-ATTACHMENT"
#define ICONTACT_KOLAB_STORE_ATTACHMENT_MTYPE               "X-MIME-TYPE"
#define ICONTACT_KOLAB_STORE_ATTACHMENT_NAME                "X-LABEL"

void conv_common_e_to_i(const ECalComponentWithTZ *ectz, I_common *common);
void conv_incidence_e_to_i(const ECalComponentWithTZ *ectz, I_incidence *incidence);

void conv_incidence_i_to_e(ECalComponentWithTZ *ectz, I_incidence *incidence);
void conv_common_i_to_e(ECalComponentWithTZ *ectz, I_common *common);

void e_kolab_store_get_fields(const ECalComponentWithTZ *ectz, I_common *icommon);
void e_kolab_store_add_common_fields(ECalComponentWithTZ *ectz, I_common *icommon);
void e_kolab_store_add_incidence_fields(ECalComponentWithTZ *ectz, I_incidence *incidence);

void i_evo_store_add_cal(I_common *icommon, const ECalComponentWithTZ *ectz);
void i_evo_store_add_cal_note(I_common *i_common, const ECalComponentWithTZ *ectz);
void i_evo_store_add_contact(EContact *eContact, I_common *icommon);
void i_evo_store_get_all(I_common *icommon, ECalComponentWithTZ *ectz);
void i_evo_store_get_all_contact(I_common *i_common, EContact *e_contact);

void alarm_i_to_e(ECalComponentWithTZ *ectz, I_incidence *i_incidence);
void kolab_attachment_store_i_to_e (ECalComponent* e_cal_comp, I_common *icommon);

#endif /* E_ABSTRACT_H_ */
