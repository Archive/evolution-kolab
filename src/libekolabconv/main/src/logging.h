/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * logging.h
 *
 *  Created on: 13.09.2010
 *      Author: Hendrik Helwich <h.helwich@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

#include "kolab-conv.h"

#ifndef LOGGING_H_
#define LOGGING_H_

/**
 * Include this file to output debug messages via the glib logger in the main
 * library. Normally this messages will be discarded but can be enabled for
 * developing by defining the precompiler flag KOLABCONV_DEBUG
 */

#ifdef KOLABCONV_DEBUG

/**
 * Output a log message via the glib logger.
 */
void log_debug (gchar *fmt, ...);
void log_warn (gchar *fmt, ...);
void log_evolution_ical (const gchar *message, const ECalComponentWithTZ* epim);
void log_evolution_vcard (const gchar *message, const EContact* epim);
void log_kolab_mail (const gchar *message, const Kolab_conv_mail *kmail);


#else

/* discard all logging messages in the main library */

#define log_debug(...) do {} while(0)
#define log_warn(...) do {} while(0)
#define log_evolution_ical(...) do {} while(0)
#define log_evolution_vcard(...) do {} while(0)
#define log_kolab_mail(...) do {} while(0)

#endif /* KOLABCONV_DEBUG */


#endif /* LOGGING_H_ */
