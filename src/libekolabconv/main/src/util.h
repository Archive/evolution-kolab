/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/*
 * util.h
 *
 *  Created on: 04.08.2010
 *      Author: Umer Kayani <u.kayani@tarent.de>,
 *      		Andreas Grau <a.grau@tarent.de>
 */

/*
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301, USA
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include <glib.h>
#include <libxml/tree.h>

#include "kolab-conv.h"


/*
 * String functions
 */
gchar **str_split(const gchar *str, const gchar *delim);
void free_str_tokens(char **tok);
gchar* trim_all_spaces(gchar* str);
gchar* trim (gchar *s);

/*
 * Helper functions for creating xml tree
 */
void print_klb_mail(Kolab_conv_mail* kmail);
Kolab_conv_mail *g_list_to_kolab_conv_mail (GList *glist);
Kolab_conv_mail_part* clone_kolab_conv_mail_part(Kolab_conv_mail_part *mpart);
GList *klb_conv_mail_to_g_list (const Kolab_conv_mail* klb_mail);
time_t time_gm(struct tm *tm);

/*
 * XFB helpers
 */
void klb_conv_xfb_base64_make_utf8_valid (icalcomponent *icalcomp_vfb);

#endif /* UTIL_H_ */
