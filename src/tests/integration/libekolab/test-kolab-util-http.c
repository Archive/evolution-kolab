/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            test-kolab-util-http.c
 *
 *  Thu Aug 12 14:27:45 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#define _GNU_SOURCE
#include <string.h>

#include <fcntl.h>
#include <sys/types.h>

#include <glib.h>
#include <stdlib.h>

#include <libekolab/camel-kolab-session.h>
#include <libekolabutil/camel-kolab-stream.h>
#include <libekolabutil/camel-system-headers.h>
#include <libekolabutil/kolab-util-camel.h>
#include <libekolabutil/kolab-util-http.h>
#include <libekolabutil/kolab-util-glib.h>

/*----------------------------------------------------------------------------*/

#define TESTCASE "test-kolab-util-http"
#define AUTH "auth"
#define SECURITY "security"

/*----------------------------------------------------------------------------*/

static gboolean
test_kolab_util_http (void)
{
	KolabUtilHttpJob *job = NULL;
	gssize nbytes = 0;
	gchar *config = NULL;
	gchar *url_string = NULL;
	gchar *username = NULL;
	gchar *password = NULL;
	gchar *pkcs11pin = NULL;
	gchar *outfilename = NULL;
	gchar *cmpfilename = NULL;
	CamelStream* file_stream = NULL;
	GKeyFile* key_file = NULL;
	gboolean ok = FALSE;
	GError *tmp_err = NULL;

	/* init subsystems */
	kolab_util_glib_init ();
	kolab_util_http_init (g_get_home_dir ());

	/* libcamel
	 *
	 * Curl init may configure the underlying SSL lib,
	 * but as far as SSL goes, we want Camel to rule here
	 *
	 * TODO check whether Camel session needs to be
	 *      initialized before or after libcurl
	 *
	 */
	ok = kolab_util_camel_init (&tmp_err);
	if (! ok) {
		g_warning ("%s: %s", __func__, tmp_err->message);
		g_error_free (tmp_err);
		return FALSE;
	}

	/* get config */
	key_file = g_key_file_new ();

	config = g_strdup (TESTCASE ".conf");
	g_return_val_if_fail (g_key_file_load_from_file (key_file, config, G_KEY_FILE_NONE, NULL), FALSE);

	outfilename = g_key_file_get_string (key_file, TESTCASE, "outfile", NULL);
	cmpfilename = g_key_file_get_string (key_file, TESTCASE, "cmpfile", NULL);
	url_string = g_key_file_get_string (key_file, TESTCASE, "url", NULL);
	username = g_key_file_get_string (key_file, AUTH, "username", NULL);
	password = g_key_file_get_string (key_file, AUTH, "password", NULL);
	pkcs11pin = g_key_file_get_string (key_file, SECURITY, "pkcs11pin", NULL);

	g_return_val_if_fail (outfilename != NULL, FALSE);
	g_return_val_if_fail (cmpfilename != NULL, FALSE);
	g_return_val_if_fail (url_string  != NULL, FALSE);
	g_return_val_if_fail (username != NULL, FALSE);
	g_return_val_if_fail (password != NULL, FALSE);
	g_return_val_if_fail (pkcs11pin != NULL, FALSE);

	/* create KolabHttpJob */
	job = kolab_util_http_job_new ();
	job->passwd = password;
	job->pkcs11pin = pkcs11pin;

	/* create CamelURL object */
	job->url = camel_url_new (url_string, 0);
	g_assert (job->url != NULL);

	/* set username for basic auth */
	camel_url_set_user (job->url, username);

	g_free (url_string);
	g_free (username);

	/* open filestream */
	file_stream = camel_kolab_stream_new_filestream (outfilename,
	                                                 O_RDWR|O_CREAT|O_TRUNC,
	                                                 0600,
	                                                 &tmp_err);
	if (tmp_err != NULL) {
		g_warning ("%s: %s",
		           __func__, tmp_err->message);
		g_error_free (tmp_err);
	}
	g_return_val_if_fail (file_stream != NULL, FALSE);

	/* allocate buffer for data from HTTP GET */
	job->buffer = g_byte_array_new ();

	/* issue HTTP GET request */
	nbytes = kolab_util_http_get (job, &tmp_err);
	if (tmp_err != NULL) {
		g_warning ("%s: %s",
		           __func__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}
	g_debug ("%s: %d bytes read", __func__, nbytes);

	/* write data to file */
	nbytes = camel_stream_write (file_stream,
	                             (gchar*)job->buffer->data,
	                             (gsize)job->buffer->len,
	                             NULL,
	                             &tmp_err);
	if (tmp_err != NULL) {
		g_warning ("%s: %s",
		           __func__, tmp_err->message);
		g_error_free (tmp_err);
		tmp_err = NULL;
	}
	g_debug ("%s: %d bytes written", __func__, nbytes);

	/* TODO check result file
	 *      (should return EXIT_FAILURE if unsuccessful)
	 */

	/* cleanup */
	kolab_util_http_job_free (job);
	camel_kolab_stream_free (file_stream);
	g_key_file_free (key_file);
	g_free (cmpfilename);
	g_free (outfilename);
	g_free (config);

	/* shutdown subsystems */
	kolab_util_glib_shutdown ();
	kolab_util_http_shutdown ();

	/* libcamel */
	/* ATTN camel_shutdown () currently segfaults when NSS
	 *      is using openCryptoki infrastructure. Be sure
	 *      to call camel_kolab_session_shutdown() only
	 *      after all other libs have been shut down
	 */
	ok = kolab_util_camel_shutdown (&tmp_err);
	if (! ok) {
		g_warning ("%s: %s", __func__, tmp_err->message);
		g_error_free (tmp_err);
	}

	return TRUE;
}

/*----------------------------------------------------------------------------*/

gint
main (gint argc, gchar** argv)
{
	gboolean ok = FALSE;

	(void)argc;
	(void)argv;

	ok = test_kolab_util_http ();
	if (! ok)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

/*----------------------------------------------------------------------------*/
