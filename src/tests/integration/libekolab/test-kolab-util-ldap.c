/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            test-kolab-util-ldap.c
 *
 *  Thu Aug 12 14:27:45 2010
 *  Copyright  2010  Silvan Marco Fin
 *  <silvan@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with main.c; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#define TESTCASE "test-kolab-util-ldap"
#define AUTH "auth"
#define SECURITY "security"

/*----------------------------------------------------------------------------*/

#include <libekolabutil/kolab-util-ldap.h>
#include <libekolabutil/kolab-util-glib.h>

#include <glib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

gint main (gint argc, gchar *argv[])
{
	(void)argc;
	(void)argv;
#if 0
	gchar *config;
	gchar *url_string;
	gchar *encryption_mode;
	gchar *username;
	gchar *password;
	gchar *pkcs11pin;
	KolabUtilLdap *ldap_test_obj = NULL;
	gssize nbytes;
	GKeyFile *key_file;

	kolab_util_glib_init ();

	key_file = g_key_file_new ();

	if (g_getenv ("SSL_DIR") == NULL) {
		g_error ("$SSL_DIR not set, this is most likely an error, exiting.");
	}

	config = g_strdup (TESTCASE ".conf");
	g_assert (TRUE == g_key_file_load_from_file (key_file, config,
	                                             G_KEY_FILE_NONE, NULL));

	ldap_test_obj = g_object_new(KOLAB_TYPE_UTIL_LDAP, NULL);
	g_assert (ldap_test_obj != NULL);

	url_string = g_key_file_get_string (key_file, TESTCASE, "url", NULL);
	g_assert (url_string != NULL);
	encryption_mode = g_key_file_get_string (key_file, SECURITY, "use_tls", NULL);
	g_assert (encryption_mode != NULL);
	pkcs11pin = g_key_file_get_string (key_file, SECURITY, "pkcs11pin", NULL);
	if (pkcs11pin != NULL) {
		ldap_test_obj->use_pkcs11=TRUE;
		kolab_util_ldap_set_pkcs11pin (ldap_test_obj, pkcs11pin);
		g_free (pkcs11pin);
	}

	kolab_util_ldap_set_encryption_mode (ldap_test_obj,
	                                     parse_ldap_encryption_mode (encryption_mode));

	username = g_key_file_get_string (key_file, AUTH, "username", NULL);
	g_assert (username != NULL);
	password = g_key_file_get_string (key_file, AUTH, "password", NULL);
	g_assert (password != NULL);

	kolab_util_ldap_set_credentials(ldap_test_obj, username, password);

	g_free (username);
	g_free (password);


	kolab_util_ldap_set_searchurl (ldap_test_obj, url_string);

	nbytes = kolab_util_ldap_get (ldap_test_obj);

	g_debug ("LDAP access returned bytes: %d", nbytes);

	g_object_unref (ldap_test_obj);
	g_key_file_free (key_file);
	g_free(config);
	g_free(url_string);

	return 0;
#endif
	return -1;
}
