/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            tdriver.c
 *
 *  Thu Jun 10 15:22:37 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>

#include <addressbook/e-book-backend-kolab.h>
#include <calendar/e-cal-backend-kolab.h>

#include <camel/camel-kolab-imapx-provider.h>

#include "tdriver.h"

/*----------------------------------------------------------------------------*/

int main (gint argc, gchar **argv) {
	(void)argc; (void)argv;
	printf ("this is the evolution-kolab test driver\n");
	return EXIT_SUCCESS;
}

/*----------------------------------------------------------------------------*/
