/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
/***************************************************************************
 *            camel-kolab-imapx-provider.h
 *
 *  Thu Jun 10 19:27:45 2010
 *  Copyright  2010  Christian Hilberg
 *  <hilberg@kernelconcepts.de>
 ****************************************************************************/

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor Boston, MA 02110-1301,  USA
 */

/*----------------------------------------------------------------------------*/

#ifndef _CAMEL_KOLAB_IMAPX_PROVIDER_H_
#define _CAMEL_KOLAB_IMAPX_PROVIDER_H_

/*----------------------------------------------------------------------------*/

/* Kolab prototypes (to be used in the backends, mainly) */
void camel_kolab_imapx_provider_module_init (void);

/*----------------------------------------------------------------------------*/

#endif /* _CAMEL_KOLAB_IMAPX_PROVIDER_H_ */

/*----------------------------------------------------------------------------*/
